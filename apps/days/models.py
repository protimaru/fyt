from django.db import models


class Day(models.Model):
    name = models.CharField(max_length=50)

    class Meta:
        db_table = 'days'
        ordering = ['id']

    @staticmethod
    def get_days(days_id):
        days = list(map(int, days_id))
        return Day.objects.filter(id__in=days)

    def __str__(self):
        return self.name
