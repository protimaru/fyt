import os

from django.urls import reverse
from django.contrib.auth.models import User
from django.db import models
from django.db.models.signals import post_save, pre_delete, pre_save
from django.dispatch import receiver

from apps.notifications.models import Notification
from apps.tasks import convert_video
from django.conf import settings
from django.utils.text import slugify

from apps.utils import utils


def uploadPostPhoto(instance, filename):
    return 'post/image/%s/%s' % (instance.post.owner.username, filename)


def uploadPostVideo(instance, filename):
    return 'post/video/%s/%s' % (instance.post.owner.username, filename)


def uploadPostVideoThumbnail(instance, filename):
    return 'post/thumbnail/%s/%s' % (instance.post.owner.username, filename)


class Post(models.Model):
    owner = models.ForeignKey(User, related_name='posts', on_delete=models.CASCADE)
    context = models.TextField(blank=True, null=True)
    slug = models.SlugField(blank=True, null=True)
    likes = models.ManyToManyField(User, related_name='users_like', blank=True)
    created = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    updated = models.DateTimeField(auto_now=True, blank=True, null=True)

    class Meta:
        ordering = ['-created']

    @property
    def get_liked_users(self):
        return [u.id for u in self.likes.all()]

    # @property
    def friends_liked(self):
        user = User.objects.first()
        if isinstance(user, User):
            post_ids = []
            friends_ids = [u.id for u in user.profile.friends.all()]
            for u in user.profile.friends.all():
                for p in u.users_like.all():
                    post_ids.append(p.id)
            print(post_ids)
            posts = Post.objects.filter(
                id__in=post_ids
            )
            res = {
                'users': friends_ids,
                'posts': [p.id for p in posts]
            }
            return res
        else:
            raise TypeError('')

    def get_absolute_url(self):
        return reverse('post:post_detail', kwargs={'slug': self.slug})

    def __str__(self):
        return self.context[:45] if self.context else 'some text'


class Comment(models.Model):
    post = models.ForeignKey(Post, on_delete=models.CASCADE)
    text = models.TextField()
    author = models.ForeignKey(User, related_name='commented_user', on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.post.context[:45]


class Reply(models.Model):
    comment = models.ForeignKey(Comment, on_delete=models.CASCADE)
    text = models.TextField()
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return '{}: {}'.format(self.comment.text, self.text[:45])


class Photo(models.Model):
    post = models.ForeignKey(Post, related_name='images', on_delete=models.CASCADE)
    photo = models.ImageField(
        upload_to=uploadPostPhoto, blank=True, null=True)
    user = models.ForeignKey(User, related_name='photos')
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def image_url(self):
        return self.photo.url

    def __str__(self):
        return '{}\'s photo'.format(self.post.context)


class Video(models.Model):
    post = models.ForeignKey(Post, related_name='videos', on_delete=models.CASCADE)
    video = models.FileField(upload_to=uploadPostVideo,
                             blank=True, null=True, default=None)
    thumbnail = models.ImageField(upload_to=uploadPostVideoThumbnail,
                                  blank=True, null=True, default=None)
    user = models.ForeignKey(User, related_name='videos')
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return '{}\'s video'.format(self.post.context)


class Album(models.Model):
    user = models.ForeignKey(User, default=2, related_name='album', on_delete=models.CASCADE)
    name = models.CharField(max_length=255, blank=True, null=True)
    album_art = models.ImageField(upload_to='albums/')
    images = models.ManyToManyField(Photo, blank=True)
    videos = models.ManyToManyField(Video, blank=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['-id', ]

    def __str__(self):
        return self.name

    @property
    def all_photos(self):
        return self.images.all()

    @property
    def all_videos(self):
        return self.videos.all()

    def add_photo(self, photo):
        if isinstance(photo, Photo):
            self.images.add(photo)
        else:
            raise TypeError('An argument must be a Photo instance')

    def add_video(self, video):
        if isinstance(video, Video):
            self.images.add(video)
        else:
            raise TypeError('An argument must be a Video instance')


@receiver(post_save, sender=Video)
def convert_video_to_480(instance, created, **kwargs):
    if created:
        location = str(settings.BASE_DIR) + '/media/' + str(instance.video)
        video = convert_video(location)
        t = video['thumbnail'].split('media')[-1]
        instance.thumbnail = t[1:]
        instance.save()


@receiver(post_save, sender=Post)
def post_save_notification(instance, created, **kwargs):
    if created:
        for friend in instance.owner.profile.friends.all():
            Notification.objects.create(
                user=friend,
                sender=instance.owner,
                text=settings.POST_CREATED,
                type_not="post",
                url=instance.get_absolute_url(),
            )


@receiver(pre_save, sender=Post)
def create_slug(instance, **kwargs):
    if not instance.slug:
        if not instance.slug:
            exist = True
            while exist:
                instance.slug = slugify(utils.random_str())
                sl = Post.objects.filter(slug__iexact=instance.slug)
                if not sl:
                    break


def delete_file(path):
    try:
        if os.path.isfile(path):
            os.remove(path)
    except FileNotFoundError:
        print("FILE NOT FOUND")


@receiver(pre_delete, sender=Post)
def delete_files_after_deleting_post(instance, **kwargs):
    videos = Video.objects.filter(post=instance)
    images = Photo.objects.filter(post=instance)
    if images:
        for image in images:
            delete_file(image.photo.path)
    if videos:
        for video in videos:
            if video.thumbnail:
                delete_file(video.thumbnail.path)
            delete_file(video.video.path)
