from django import forms
from .models import Post


class CreatePostForm(forms.ModelForm):
    class Meta:
        model = Post
        fields = ['owner', 'context', 'file']

    file = forms.FileField(widget=forms.FileInput(attrs={'id': 'inputfile'}))