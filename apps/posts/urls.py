from django.conf.urls import url
from .views import (
    AddLikeToPostView,
    CreateCommentView, CreatePostView, PostDeleteView, CreateReplyView,
    Get5CommentsView, Get5RepliesView, PostDetailView
)

urlpatterns = [
    url(r'^create/$', CreatePostView.as_view(), name='create'),
    url(r'^like/$', AddLikeToPostView.as_view(), name='like_post'),
    url(r'^get_last_five_comment/$', Get5CommentsView.as_view(), name='get_last_five_comment'),
    url(r'^get_last_five_reply/$', Get5RepliesView.as_view(), name='get_last_five_reply'),
    url(r'^add_comment/$', CreateCommentView.as_view(), name='add_comment'),
    url(r'^add_reply/$', CreateReplyView.as_view(), name='add_reply'),
    # url(r'^post_delete/(?P<pk>\d+)$', PostDeleteView.as_view(), name='post_delete'),
    url(r'^post_delete/$', PostDeleteView.as_view(), name='post_delete'),
    url(r'^post-view/(?P<slug>[\w-]+)/$', PostDetailView.as_view(), name='post_detail'),
]
