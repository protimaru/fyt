from django.contrib import admin
from .models import Post, Comment, Photo, Video, Reply, Album


class PostImageInline(admin.TabularInline):
    model = Photo


class PostAdmin(admin.ModelAdmin):
    inlines = [PostImageInline,]


admin.site.register(Post, PostAdmin)
admin.site.register(Album)
admin.site.register(Comment)
admin.site.register(Reply)
admin.site.register(Photo)
admin.site.register(Video)
