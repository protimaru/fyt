from django.http import HttpResponse, JsonResponse
from django.shortcuts import render, redirect
from django.templatetags.static import static
from django.views.generic import CreateView, View, DeleteView, DetailView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.conf import settings
from django.contrib.auth.models import User

from apps.activity.models import Action
from .forms import CreatePostForm
from django.core.urlresolvers import reverse
from django.core.serializers import serialize
from apps.activity.utils import create_action
from apps.posts.models import Post, Comment, Photo, Video, Reply, Album
from apps.video.models import Video
# from apps.api.serializers import FeedListSerializer
from apps.posts.models import Photo
from django.core.files.images import get_image_dimensions
from apps.api.posts.serializers import CommentListSerializer, ReplyListSerializer, PostListSerializer


class CreatePostView(CreateView):
    model = Post
    fields = ['context', ]

    def form_valid(self, form):
        form.instance.owner = self.request.user
        form.instance.save()
        form.save()
        files = self.request.FILES.getlist("file")
        if files:
            for file in files:
                ext = str(file).split('.')[-1].upper()
                if ext in settings.IMAGE_TYPES:
                    photo = Photo.objects.create(
                        post=form.instance,
                        photo=file,
                        user=self.request.user
                    )
                    photo.save()
                elif ext in settings.VIDEO_TYPES:
                    video = Video.objects.create(
                        post=form.instance,
                        video=file,
                        user=self.request.user
                    )
                    video.save()
        response = PostListSerializer(form.instance)
        return JsonResponse(response.data, safe=False)



class PostDeleteView(View):
    http_method_names = ["post"]

    def post(self, request):
        try:
            Post.objects.get(id=request.POST.get('pk')).delete()
            return JsonResponse({'status': 'ok'})
        except:
            return JsonResponse({'status': 'fail'})


class AddLikeToPostView(View):
    http_method_names = ["post"]

    def post(self, request):
        post_id = request.POST.get('id')
        action = request.POST.get('action')
        if post_id and action:
            try:
                post = Post.objects.get(id=post_id)
                if action == 'like':
                    post.likes.add(request.user)
                elif action == 'unlike':
                    # Action.objects.get(user=request.user, verb='likes', target=post).delete()
                    post.likes.remove(request.user)
                return JsonResponse({'status': 'ok'})
            except:
                pass
        return JsonResponse({'status': 'ko'})


class CreateCommentView(View):
    http_method_names = ['post']

    def post(self, request):
        user_id = request.POST.get('user_id')
        post_id = request.POST.get('post_id')
        text = request.POST.get('text')
        if user_id and post_id and text is not None:
            comment = Comment.objects.create(
                post_id=post_id, author=request.user, text=text)
            comment.save()
            # data = {
            #     'id': comment.id,
            #     'avatar': comment.author.profile.avatar.url if comment.author.profile.avatar else static(
            #         'images/profile/no_photo_male.png'),
            #     'author': str(comment.author),
            #     'created': str(comment.created.isoformat()),
            #     'text': str(comment.text),
            #     'comments': post.comment_set.count(),
            # }
            res = CommentListSerializer(comment)
            return JsonResponse(res.data, safe=False)
        else:
            return HttpResponse('invalid')


class CreateReplyView(View):
    http_method_names = ['post']

    def post(self, request):
        text = request.POST.get('text')
        user = User.objects.get(id=request.POST.get('user'))
        comment = Comment.objects.get(id=request.POST.get('comment_id'))
        reply = Reply.objects.create(comment=comment, text=text, author=user)
        reply.save()
        # data = {
        #     'id': str(reply.id),
        #     'author': str(reply.author),
        #     'text': str(reply.text),
        #     'created': str(reply.created.isoformat()),
        #     'user': str(reply.author.username),
        #     'avatar': str(reply.author.profile.avatar.url),
        # }
        res = ReplyListSerializer(reply)
        return JsonResponse(res.data, safe=False)


class Get5PostsView(View):
    http_method_names = ['post']

    def post(self, request):
        post_id = request.POST.get('post_id')
        if post_id:
            posts = Post.objects.get(id=post_id)
        else:
            posts = Post.objects.order_by('-id').all()[:5]
        comment_json = []
        for c in posts:
            data = {
                'post_id': str(c.post.id),
                'id': str(c.id),
                'author': str(c.author),
                'text': str(c.text),
                'created': str(c.created.isoformat()),
                'user': str(c.author.username),
                'avatar': str(c.author.profile.avatar.url),
            }
            comment_json.append(data)
        return JsonResponse(comment_json, safe=False)


class Get5CommentsView(View):
    http_method_names = ['post']

    def post(self, request):
        comment_id = request.POST.get('comment_id')
        post_id = int(request.POST.get('post_id'))
        if type(post_id) == int:
            post = Post.objects.get(id=post_id)
        else:
            return JsonResponse({'status': 'fail'})
        if comment_id:
            comments = Comment.objects.order_by('-id').filter(post=post, id__lt=comment_id)[:5]
        else:
            comments = Comment.objects.order_by('-id').filter(post=post)[:5]
        comment_json = []
        for c in comments:
            data = {
                'post_id': str(c.post.id),
                'id': str(c.id),
                'author': str(c.author),
                'text': str(c.text),
                'created': str(c.created.isoformat()),
                'user': str(c.author.username),
                'avatar': str(c.author.profile.avatar.url),
            }
            comment_json.append(data)
        return JsonResponse(comment_json, safe=False)


class Get5RepliesView(View):
    http_method_names = ['post']

    def post(self, request):
        reply_id = request.POST.get('reply_id')
        comment_id = request.POST.get('comment_id')
        if comment_id:
            comment = Comment.objects.get(id=comment_id)
        else:
            return JsonResponse({'status': 'fail'})

        if reply_id:
            replies = Reply.objects.order_by('-id').filter(comment=comment, id__lt=reply_id)[:5]
        else:
            replies = Reply.objects.order_by('-id').filter(comment=comment)[:5]
        reply_json = []
        for c in replies:
            data = {
                'post_id': str(c.comment.id),
                'id': str(c.id),
                'author': str(c.author),
                'text': str(c.text),
                'created': str(c.created.isoformat()),
                'user': str(c.author.username),
                'avatar': str(c.author.profile.avatar.url),
            }
            reply_json.append(data)
        return JsonResponse(reply_json, safe=False)


class PostDetailView(DetailView):
    model = Post
    template_name = 'post_detail.html'


# Gallery


class AlbumNewView(CreateView):
    model = Album
    fields = [
        'name',
    ]

    def form_valid(self, form):
        return super(AlbumNewView, self).form_valid(form)


class AlbumDeleteView(DeleteView):
    model = Album


class AlbumAddItems(LoginRequiredMixin, View):
    def post(self, request):
        pass
