from django.conf.urls import url

from apps.offer.views import NewOfferView

urlpatterns = [
    url("^create/$", NewOfferView.as_view(), name="new-offer")
]
