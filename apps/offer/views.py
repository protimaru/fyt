from django.shortcuts import redirect
from django.views.generic import CreateView

from apps.offer.models import Offer


class NewOfferView(CreateView):
    model = Offer
    fields = ['name', 'email', 'message', ]
    template_name = 'about_project.html'

    def form_valid(self, form):
        form.save()
        return redirect("/")
