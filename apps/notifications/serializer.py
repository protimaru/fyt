from django.contrib.auth.models import User
from rest_framework.serializers import ModelSerializer
from rest_framework.relations import StringRelatedField, HyperlinkedIdentityField
from rest_framework.fields import CharField

# from apps.api.serializers import UserDetailSerializer
# from apps.followers.models import Notification


# class UserDetailSerializer(ModelSerializer):
#     class Meta:
#         model = User
#         fields = [
#             'id',
#             'username',
#             'first_name',
# #         ]


# class NotificationListSerializer(ModelSerializer):
#     sender = UserDetailSerializer(read_only=True)
#     # sender = UserDetailSerializer(read_only=True)

#     class Meta:
#         model = Notification
#         # fields = '__all__'
#         fields = [
#             # 'user',
#             'sender',
#             'text',
#             'url'
#         ]
