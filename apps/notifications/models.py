from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
from channels.channel import Group
import json


class Notification(models.Model):
    user = models.ForeignKey(
        User, related_name='notifications', on_delete=models.CASCADE
    )
    sender = models.ForeignKey(
        User, related_name='notification_sender', on_delete=models.CASCADE
    )
    text = models.TextField(blank=True, null=True)
    type_not = models.CharField(max_length=50, blank=True, null=True)
    url = models.CharField(max_length=255, blank=True, null=True)
    updated = models.DateTimeField(auto_now=True)
    is_read = models.BooleanField(default=False)

    class Meta:
        ordering = ['-id']

    def __str__(self):
        return '{} {}'.format(self.user.username, self.text)


@receiver(post_save, sender=Notification)
def send_notification(instance, created, **kwargs):
    if created:
        channel = instance.user.profile.channel
        # if instance.type_not == 'friend_request':
        #     Group('room-%s' % channel).send(
        #         {
        #             'text': json.dumps(
        #                 {
        #                     'type': instance.type_not,
        #                     'text': 'user wants to be your friend'
        #                 }
        #             )
        #         }
        #     )
        if not instance.type_not == 'chat':
            Group('room-%s' % channel).send(
                {
                    'text': json.dumps(
                        {
                            'type': instance.type_not
                        }
                    )
                }
            )
