from django.conf.urls import url

from .views import mark_notification_as_read

urlpatterns = [
    url(r'^mark-as-read/$', mark_notification_as_read, name='mark-as-read')
]