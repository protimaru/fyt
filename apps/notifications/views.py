from django.http import HttpResponse, JsonResponse
from django.shortcuts import render
from django.contrib.auth.models import User
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK, HTTP_400_BAD_REQUEST
from rest_framework.views import APIView

from .models import Notification
# from .serializer import NotificationListSerializer


def mark_notification_as_read(request):
    if request.POST:
        not_id = request.POST.get('not_id')
        mark_all = request.POST.get('mark_all')
        if not_id:
            notification = Notification.objects.get(id=not_id)
            notification.is_read = True
            notification.save()
            if notification.type_not == 'chat':
                try:
                    notification.delete()
                except Exception as e:
                    pass
            return HttpResponse('ok')
        if mark_all:
            try:
                request.user.notifications.update(is_read=True)
                return HttpResponse('ok')
            except:
                pass
        else:
            return HttpResponse('fail')
    else:
        pass


def delete_message_notifications(request):
    try:
        Notification.objects.filter(type="chat").delete()
        result = "ok"
    except Notification.DoesNotExist:
        result = 404
    except Exception:
        result = 505
    return JsonResponse({"status": result})
