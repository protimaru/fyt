from django.db import models


class Country(models.Model):
    sortname = models.CharField(max_length=255, blank=True, null=True)
    name = models.CharField(max_length=255)
    phonecode = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        db_table = 'countries'

    def __str__(self):
        return self.name


class State(models.Model):
    name = models.CharField(max_length=255, default='')
    country = models.ForeignKey(Country, on_delete=models.CASCADE)

    class Meta:
        db_table = 'states'

    def __str__(self):
        return self.name


class City(models.Model):
    name = models.CharField(max_length=255, default='')
    state = models.ForeignKey(State, on_delete=models.CASCADE)

    class Meta:
        db_table = 'cities'
        ordering = ['name']

    def __str__(self):
        return self.name
