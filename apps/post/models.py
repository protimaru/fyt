from django.db import models
from django.contrib.auth.models import User

from apps.photo.models import Photo
from apps.video.models import Video

from apps.like.models import Like
from apps.comment.models import Comment


class Post(models.Model):
    image = models.ManyToManyField(Photo, related_name='post_images', blank=True)
    video = models.ManyToManyField(Video, related_name='post_videos', blank=True)
    text = models.TextField(blank=True, null=True)
    author = models.ForeignKey(User, related_name='new_posts', on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True)

    class Meta:
        db_table = 'new_posts'
        ordering = ['-id', ]

    def __str__(self):
        return self.author.username

    @property
    def likes_count(self):
        likes = Like.objects.filter(
            content_type__model='post',
            object_id=self.id
        )
        return likes.count()

    @property
    def liked_users(self):
        likes = Like.objects.filter(
            content_type__model='post',
            object_id=self.id
        ).values_list('author_id', flat=True)
        return list(likes)

    @property
    def comments_count(self):
        comments = Comment.objects.filter(
            content_type__model='post',
            object_id=self.id
        )
        return comments.count()
