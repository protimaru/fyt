from django.http import JsonResponse
from django.shortcuts import render
from django.views.generic import ListView, View
from django.conf import settings
from django.contrib.auth.mixins import LoginRequiredMixin

from apps.post.models import Post
from apps.video.models import Video
from apps.photo.models import Photo

from apps.api.post.serializers import PostListSerializer


class PostCreateView(LoginRequiredMixin, View):
    def post(self, request):
        files = request.FILES.getlist("file")
        txt = request.POST.get("context")
        images_id = []
        videos_id = []
        for file in files:
            ext = str(file).split('.')[-1].upper()
            if ext in settings.VIDEO_TYPES:
                v = Video.objects.create(
                    video=file,
                    description='',
                    author=request.user
                )
                videos_id.append(v.id)
            elif ext in settings.IMAGE_TYPES:
                p = Photo.objects.create(
                    image=file,
                    description='',
                    author=request.user
                )
                images_id.append(p.id)
        post = Post.objects.create(
            text=txt,
            author=request.user
        )
        if len(images_id) > 0:
            images = Photo.objects.filter(id__in=images_id)
            post.image.add(*images)
        if len(videos_id) > 0:
            videos = Video.objects.filter(id__in=videos_id)
            post.video.add(*videos)
        post.save()
        response = PostListSerializer(post)
        return JsonResponse(response.data, safe=False)
