from django.conf.urls import url

from .views import (
    PostCreateView
)


urlpatterns = [
    url(r'create', PostCreateView.as_view(), name="create"),
]
