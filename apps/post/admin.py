from django.contrib import admin

from .models import Post


class PostAdmin(admin.ModelAdmin):
    class Meta:
        model = Post
        fields = '__all__'

    readonly_fields = ['likes_count', 'liked_users', 'comments_count']

    def likes_count(self, obj):
        return obj.likes_count

    def liked_users(self, obj):
        return obj.liked_users

    def comments_count(self, obj):
        return obj.comments_count


admin.site.register(Post, PostAdmin)
