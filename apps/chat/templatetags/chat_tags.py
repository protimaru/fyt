from django.contrib.auth.models import User
from django import template

from apps.chat.models import Message

register = template.Library()


@register.simple_tag
def get_last_message(my_id, his_id):
    sender_user = User.objects.get(id=my_id)
    receiver_user = User.objects.get(id=his_id)

    room_id = 0

    for room in sender_user.room.all():
        if room in receiver_user.room.all():
            room_id = room.id

    last_message = Message.objects.filter(room_id=room_id).last()
    if last_message:
        message_sender = 'You: ' if last_message.sender == sender_user else ''.format(receiver_user.username)
        return '{}{}'.format(message_sender, last_message.text)
    else:
        return 'No messages'


@register.simple_tag
def get_unread_messages_count(my_id, his_id):
    sender_user = User.objects.get(id=my_id)
    receiver_user = User.objects.get(id=his_id)

    room_id = 0

    for room in sender_user.room.all():
        if room in receiver_user.room.all():
            room_id = room.id
    unread_message = Message.objects.filter(room_id=room_id, receiver=sender_user, read=False).count()
    return unread_message if unread_message > 0 else ''
