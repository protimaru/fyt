# from channels.auth import http_session_user, channel_session_user
from channels.auth import channel_session_user_from_http, channel_session_user
from channels.handler import AsgiRequest
from django.contrib.auth.models import User
from .models import Message, Room
import json
from channels.handler import AsgiRequest

from django.contrib.auth.models import User
# from urllib import parse
# from channels import Channel
from django.contrib.auth.models import AnonymousUser
from channels.channel import Group
# from channels.sessions import channel_session
# from django.core import serializers
from apps.utils.channels import auth_user


@channel_session_user_from_http
@auth_user
def ws_connect(message, room):
    print("ws_connect: ", room)
    Group("room-{}".format(room)).add(message.reply_channel)
    message.reply_channel.send({"accept": True})
    message.channel_session["username"] = message.user.username
    message.channel_session['room'] = room

# @channel_session_user_from_http


@channel_session_user
def ws_message(message):
    room = message.channel_session.get('room')
    print("ws_message: ", room)
    r = Room.objects.get(name=room)
    if message.user.id:
        user = User.objects.get(id=message.user.id)
    elif message.channel_session['username']:
        user = User.objects.get(username=message.channel_session['username'])
    for i in r.users.all():
        if user != i:
            receiver = i
    Message.objects.create(sender=user, receiver=receiver, room=r, text=message.content['text'])
    Group("room-{}".format(room)).send({'text': json.dumps({'message': message.content['text'],
                                                            'user': {
        'id': user.id,
        'username': user.username,
        'full_name': user.profile.fullname,
        'avatar': user.profile.avatar.url
    }})})


@channel_session_user
def ws_disconnect(message):
    room = message.channel_session['room']
    Group("chat-{}".format(room)).discard(message.reply_channel)
