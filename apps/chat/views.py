from django.core.cache import cache
from django.http import JsonResponse
from django.shortcuts import reverse

from .models import Room


def get_room_id(request):
    # if request.is_ajax():
    user1 = request.POST.get("user1")
    user2 = request.POST.get("user2")
    room_id = Room.get_room_id(user1, user2)
    cache.set("{}_room_id".format(request.user), room_id)
    return JsonResponse({"url": reverse("main")})
