import json
from django.db import models
from django.contrib.auth.models import User
from django.dispatch import receiver
from django.db.models.signals import post_save
from channels import Group
from django.core.serializers import serialize

from apps.notifications.models import Notification


class Room(models.Model):
    name = models.CharField(max_length=250)
    users = models.ManyToManyField(User, related_name='room', blank=True)

    @staticmethod
    def get_room_id(user1_id, user2_id):
        ids = [user1_id, user2_id]
        users = User.objects.filter(id__in=ids)
        for room in Room.objects.iterator():
            if set(room.users.all()) == set(users):
                room = room.name
                return room
            else:
                pass

    @staticmethod
    def get_room_name(user1_username, user2_username):
        usernames = [user1_username, user2_username]
        users = User.objects.filter(username__in=usernames)
        for room in Room.objects.iterator():
            if set(room.users.all()) == set(users):
                room = room.id
                return room
            else:
                pass

    def __str__(self):
        return self.name


class Message(models.Model):
    sender = models.ForeignKey(User, related_name='sent_messages', default=0, on_delete=models.CASCADE)
    receiver = models.ForeignKey(User, related_name='received_messages', default=0, on_delete=models.CASCADE)
    room = models.ForeignKey(Room, on_delete=models.CASCADE)
    text = models.TextField()
    read = models.BooleanField(default=False)
    created = models.DateTimeField(auto_now_add=True)

    # class Meta:
    #     ordering = ['-created']

    def __str__(self):
        return self.room.name


@receiver(post_save, sender=Message)
def push_notification(sender, instance, created, **kwargs):
    if created:
        if instance.receiver.profile.messages_status:
            Notification.objects.create(
                user=instance.receiver,
                sender=instance.sender,
                text='You have an unread message',
                type_not='chat',
                url=str(instance.receiver.get_absolute_url()) + 'chat/',
            )
        Group('room-{}'.format(instance.receiver.profile.channel)
              ).send({'text': json.dumps(
                  {
                      'id': instance.sender.id,
                      'username': instance.sender.username,
                      'text': instance.text,
                      'type_not': 'chat',
                  }
              )})
