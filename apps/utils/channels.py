from channels.handler import AsgiRequest
from django.contrib.auth.models import User, AnonymousUser


def auth_user(func):
    def wrapper(message, **kwargs):
        if message.user == AnonymousUser():
            try:
                channel = message.content['headers'][0][1].decode('utf-8')
                user = User.objects.get(auth_token=channel)
                message.user = user
            except:
                message.user = AnonymousUser()
        return func(message, **kwargs)
    return wrapper
