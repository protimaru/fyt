# import uuid
import random
import string
import json
from urllib import request
from django.db.models import Q
from django.conf import settings

from apps.posts.models import Post
from apps.activity.models import Action
from apps.followers.models import Follow
# from apps.accounts.models import Profile


def random_str(size=16, chars=string.ascii_lowercase + string.ascii_uppercase):
    return ''.join(random.choice(chars) for _ in range(size))


# def random_str(size=16, chars=string.ascii_lowercase + string.ascii_uppercase):
#     return uuid.uuid4()


def get_friends(user):
    followers = Follow.objects.filter(user=user, is_friend=True)
    following = Follow.objects.filter(follower=user, is_friend=True)
    friends = [followers, following]
    return friends


def get_friends_list(user):
    followers = Follow.objects.filter(user=user, is_friend=True)
    followers = [u.follower for u in followers]
    following = Follow.objects.filter(follower=user, is_friend=True)
    following = [u.user for u in following]
    return followers | following


def friends_count(user):
    return Follow.objects.filter(Q(user=user) | Q(follower=user), is_friend=True).count()


def get_friends_id(user):
    friends_id = list()
    # friends = get_friends(user)
    followers = Follow.objects.filter(user=user, is_friend=True).values_list('follower_id', flat=False)
    following = Follow.objects.filter(follower=user, is_friend=True).values_list('user_id', flat=False)
    for i in followers:
        if i[0] not in friends_id:
            friends_id.append(i[0])
    for i in following:
        if i[0] not in friends_id:
            friends_id.append(i[0])
    return friends_id


def get_feed(user):
    follow = Follow.objects.filter(follower=user, is_friend=False)
    follow_ids = [user.user.id for user in follow]
    friends = Follow.objects.filter(Q(user=user) | Q(follower=user), is_friend=True)
    friends_ids = [friend.user.id for friend in friends] + [friend.follower.id for friend in friends] + [user.id]
    actions = Action.objects.filter(user_id__in=friends_ids + follow_ids)
    return actions


def get_posts(user):
    u_id = []
    for u in user.profile.friends.all():
        u_id.append(u.id)
    u_id.append(user.id)
    posts = Post.objects.filter(owner_id__in=u_id)
    return posts.order_by('-id')


def posts(user, post_id=None):
    from apps.post.models import Post
    friends_id = [f.id for f in user.profile.friends.all()]
    friends_id.append(user.id)
    posts = Post.objects.filter(author_id__in=friends_id)
    if post_id:
        posts = posts.filter(id__lt=post_id).order_by('-id')[:5]
    else:
        posts = posts.order_by('-id')[:5]
    print(posts)
    return posts


def can_follow(user, user_):
    # following = Follow.objects.filter(user=user_, follower=user).values_list('user_id', flat=True)
    is_followed = list()
    followed = Follow.objects.filter(user=user_, follower=user).values_list('user_id', flat=False)
    for i in followed:
        is_followed.append(i[0])
    friends_id = get_friends_id(user)
    if user.id != user_.id:
        if user_.id not in friends_id and user_.id not in is_followed:
            return True
        else:
            return False
    else:
        return False


def get_location_info(ip_address):
    with open('we.txt', 'w') as f:
        f.write(ip_address)
    key = settings.IPINFODB_KEY
    url = 'http://api.ipinfodb.com/v3/ip-city/?key={}&ip={}&format=json'.format(key, ip_address)
    data = request.urlopen(url)
    data = data.read().decode('ascii')
    data = json.loads(data)
    return data


"""
http://api.ipinfodb.com/v3/ip-city/?key={}&ip={}&format=json'.format(
    '8dd18ea0734e6ee8fa5668c82a9f0fdbda8caa1fb0e13a399235c79ee92fb8b9',
    '80.80.218.227'
)
"""
