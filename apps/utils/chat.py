from apps.chat.models import Room

from apps.utils import utils


def create_room(user1, user2):
    created = Room.get_room_name(user1.username, user2.username)
    if not created:
        room = Room.objects.create(
            name=utils.random_str(),
        )
        room.users.add(user1, user2)
        room.save()
