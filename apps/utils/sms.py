import urllib
from django.conf import settings


def send_message(**kwargs):
    params = {
        'api_key': settings.NEXMO_API_KEY,
        'api_secret': settings.NEXMO_API_SECRET,
        'to': kwargs.get('to'),
        'from': settings.NEXMO_DEFAULT_FROM,
        'text': kwargs.get('text')
    }

    url = 'https://rest.nexmo.com/sms/json?' + urllib.parse.urlencode(params)

    response = urllib.request.urlopen(url)
