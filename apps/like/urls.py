from django.conf.urls import url

from .views import LikeAddView

urlpatterns = [
    url(r'^add/$', LikeAddView.as_view(), name='add')
]