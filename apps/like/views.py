from django.contrib.contenttypes.models import ContentType
from django.shortcuts import render
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import View
from django.http import JsonResponse, HttpResponse

from apps.like.models import Like


class LikeAddView(LoginRequiredMixin, View):
    def post(self, request):
        oi = request.POST.get("object_id")
        ct = request.POST.get("content_type")
        if oi and ct:
            check = Like.objects.filter(
                content_type__model=ct,
                object_id=int(oi),
                author=request.user
            )
            if check.exists():
                check.delete()
                return JsonResponse({"status": "ok"})
            else:
                if ct == "post":
                    from apps.post.models import Post
                    ct = ContentType.objects.get_for_model(Post)
                elif ct == "photo":
                    from apps.photo.models import Photo
                    ct = ContentType.objects.get_for_model(Photo)
                elif ct == "video":
                    from apps.video.models import Video
                    ct = ContentType.objects.get_for_model(Video)
                like = Like.objects.create(
                    content_type=ct,
                    object_id=oi,
                    author=request.user
                )
                like.save()
            return JsonResponse({"status": "ok"})
        else:
            return HttpResponse(status=500)
