from django import forms
from django.contrib.auth.models import User
from django.contrib.auth import authenticate
from .models import Profile
# from django.utils import timezone
import datetime


class UserRegisterForm(forms.ModelForm):
    class Meta:
        model = User
        # exclude = ['data_joined', 'date_joined']
        fields = ['username', 'email', 'password', 'first_name', 'last_name']

    password_confirm = forms.CharField(widget=forms.PasswordInput)

    def clean(self):
        username = self.cleaned_data.get('username')
        password1 = self.cleaned_data.get('password')
        password2 = self.cleaned_data.get('password_confirm')
        if password1 != password2:
            raise forms.ValidationError('passwords didn\'t match')
        user = User.objects.filter(username=username)
        if user:
            raise forms.ValidationError('username does exist!')
        return super(UserRegisterForm, self).clean()


class ProfileRegisterForm(forms.ModelForm):
    class Meta:
        model = Profile
        exclude = ['user']

    # gender = forms.CharField()
    year = forms.CharField()
    month = forms.CharField()
    day = forms.CharField()

    # location = forms.CharField()
    # date_of_birth = forms.DateField(widget=forms.SelectDateWidget)

    def clean(self):
        year = int(self.cleaned_data.get('year'))
        month = int(self.cleaned_data.get('month'))
        day = int(self.cleaned_data.get('day'))
        date = datetime.date(year, month, day)
        self.cleaned_data['date_of_birth'] = date
        # self.cleaned_data['location'] = ''
        # self.cleaned_data['last_seen'] = ''
        # self.cleaned_data['avatar'] = ''
        # self.cleaned_data['address'] = ''
        # print(year, month, day)
        # self.cleaned_data['date_of_birth'] = date
        # self.cleaned_data.get('date_of_birth') = '2017-08-24'
        # print('date:   ',  self.cleaned_data['date_of_birth'])
        super(ProfileRegisterForm, self).clean()


class UserLoginForm(forms.Form):
    username = forms.CharField()
    password = forms.CharField(widget=forms.PasswordInput)

    def clean(self):
        username = self.cleaned_data.get('username')
        password = self.cleaned_data.get('password')

        user = authenticate(username=username, password=password)
        if not user:
            raise forms.ValidationError('Username or Password is incorrect!')


class UserUpdateForm(forms.Form):
    username = forms.CharField()
    email = forms.CharField()
    first_name = forms.CharField()
    last_name = forms.CharField()

    def __init__(self, user, *args, **kwargs):
        super(UserUpdateForm, self).__init__(user, *args, **kwargs)
        self.user_id = user["user_id"]

    def clean(self):
        email = self.cleaned_data.get("email")
        username = self.cleaned_data.get("username")
        check_for_email = User.objects.filter(email=email).exclude(id=self.user_id)

        if check_for_email.exists():
            raise forms.ValidationError("email has already registered")

        check_for_username = User.objects.filter(username=username).exclude(id=self.user_id)

        if check_for_username.exists():
            raise forms.ValidationError("username exists")
        super(UserUpdateForm, self).clean()


class ProfileUpdateForm(forms.Form):
    gender = forms.CharField()
    phone = forms.CharField()
    birth_date = forms.DateField()
    address = forms.CharField()
    country = forms.CharField()
    city = forms.CharField()


class UpdatePassword(forms.Form):
    current_pswd = forms.CharField()
    new_pswd = forms.CharField()
    new_pswd_cnfrm = forms.CharField()

    # def clean_new_pswd(self):
    #     print("DATA:::::::::::::::", self.data)
    #     user = User.objects.get(id=self.data.get("user_id"))
    #     current_password = self.data.get("current_pswd")
    #     new_pswd = self.cleaned_data.get("new_pswd")
    #     new_pswd_cnfrm = self.cleaned_data.get("new_pswd_cnfrm")
    #     if new_pswd != new_pswd_cnfrm:
    #         raise forms.ValidationError(601)
    #     return super(UpdatePassword, self).clean_new_pswd()

    def clean(self):
        user = User.objects.get(id=self.data.get("user_id"))
        current_password = self.data.get("current_pswd")
        new_pswd = self.cleaned_data.get("new_pswd")
        new_pswd_cnfrm = self.cleaned_data.get("new_pswd_cnfrm")
        if user.check_password(current_password) is False:
            raise forms.ValidationError(600)
        if new_pswd != new_pswd_cnfrm:
            raise forms.ValidationError(601)
        return super(UpdatePassword, self).clean()
