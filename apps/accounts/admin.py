from django.contrib import admin
from apps.accounts.models import Profile
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User
from django.utils.html import format_html
from django.contrib.auth.models import User


# class ProfileInline(admin.StackedInline):
#     model = Profile
#     can_delete = False
#     verbose_name_plural = 'Profile'


class ModelADmin1(admin.ModelAdmin):
    # def image(self, instance):
    #     return format_html('<img src={} width="30" height="30">'.format(instance.avatar.url))
    #
    # image.short_description = "Avatar"
    # image.allow_tags = True
    # exclude = []
    list_display = ['fullname', ]


# class CustomUserAdmin(UserAdmin):
#     inlines = (ProfileInline,)

# admin.site.unregister(User)
# admin.site.register(User, CustomUserAdmin)
admin.site.register(Profile, ModelADmin1)
