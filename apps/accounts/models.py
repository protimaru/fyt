from django.contrib.auth import authenticate
from django.db import models
from django.contrib.auth.models import User
from channels.channel import Group
from django.db.models.signals import pre_save, post_save, m2m_changed
from django.dispatch import receiver
from django.contrib.auth.models import User
from django.contrib.staticfiles.templatetags.staticfiles import static
from django.core.urlresolvers import reverse
# from django.conf import settings
from allauth.socialaccount.models import SocialLogin
from allauth.socialaccount.signals import pre_social_login
from django.utils.translation import gettext as _
from django.conf import settings

from apps.notifications.models import Notification
from apps.geodata.models import Country, State, City
from apps.utils import utils
from .utils import random_string, img_upload
from django.core.cache import cache
import datetime


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    birth_date = models.DateField(blank=True, null=True)
    gender = models.SmallIntegerField(default=1)
    phone = models.CharField(max_length=255, blank=True, null=True, default='')
    country = models.ForeignKey(Country, blank=True, null=True, on_delete=models.SET_NULL)
    city = models.ForeignKey(City, blank=True, null=True, on_delete=models.SET_NULL)
    address = models.CharField(max_length=255, blank=True, null=True, default='')
    channel = models.CharField(unique=True, max_length=255, blank=True, null=True)
    avatar = models.ImageField(upload_to=img_upload, blank=True, null=True)
    location = models.CharField(max_length=255, blank=True, null=True, default='')
    language = models.CharField(max_length=255, blank=True, null=True, default='')
    notifications_status = models.BooleanField(default=True)
    messages_status = models.BooleanField(default=True)
    friends = models.ManyToManyField(User, related_name='friends', blank=True)
    friend_requests = models.ManyToManyField(User, related_name='req_friends', blank=True)
    updated = models.DateTimeField(auto_now=True)

    @property
    def fullname(self):
        if self.user.first_name and self.user.last_name:
            return '%s %s' % (self.user.first_name, self.user.last_name)
        elif self.user.first_name and not self.user.last_name:
            return self.user.first_name
        elif not self.user.first_name and self.user.last_name:
            return self.user.last_name
        elif not self.user.first_name and not self.user.last_name:
            return self.user.username

    @property
    def friends_count(self):
        return self.friends.count()

    @property
    def chatted_users(self):
        sent = self.user.sent_messages.all().distinct('receiver')
        received = self.user.received_messages.all().distinct('sender')
        # return users

    def last_seen(self):
        return cache.get('seen_{}'.format(self.user.username))

    def set_notifications_status(self, status):
        if type(status) == bool:
            self.notifications_status = status
            self.save()

    def set_messages_status(self, status):
        if type(status) == bool:
            self.messages_status = status
            self.save()

    @property
    def last_notifications(self):
        profile_updated = self.user.profile.updated
        queryset = Notification.objects.filter(
            updated__gt=profile_updated
        ).count()
        return queryset

    def online(self):
        if self.last_seen():
            now = datetime.datetime.now()
            if now > self.last_seen() + datetime.timedelta(
                seconds=settings.USER_ONLINE_TIMEOUT
            ):
                return False
            else:
                return True
        else:
            return False

    def update_profile(self, **kwargs):
        self.user.username = kwargs["username"]
        self.user.first_name = kwargs["first_name"]
        self.user.last_name = kwargs["last_name"]
        self.user.email = kwargs["email"]
        self.user.save()

    def update_password(self, **kwargs):
        self.user.set_password(kwargs["password"])
        self.user.save()
        return self.user.username, kwargs["password"]

    def get_avatar_url(self, request):
        return request.build_absolute_uri(self.avatar.url)

    def get_feed_feed(self):
        feeds = utils.get_feed(self.user).exclude(user=self.user)
        return feeds

    def get_home_feed(self):
        feeds = utils.get_feed(self.user)
        return feeds

    def send_notification(self, text):
        if text:
            Group('room-%s' % self.channel).send(
                {
                    'text': text
                }
            )

    def get_info(self):
        if not self.user.profile.birth_date:
            birth_date = {}
        else:
            birth_date = {
                'day': self.user.profile.birth_date.day,
                'month': self.user.profile.birth_date.month,
                'year': self.user.profile.birth_date.year,
            }
        if not self.user.profile.country:
            country = ''
        else:
            country = self.user.profile.country.country
        if not self.user.profile.city:
            city = ''
        else:
            city = self.user.profile.city.city
        data = {
            'username': self.user.username,
            'id': self.user.id,
            'first_name': self.user.first_name,
            'last_name': self.user.last_name,
            'birth_date': birth_date,
            'gender': self.user.profile.gender,
            'country': {
                'selected': country,
            },
            'city': {
                'selected': city,
            },
            'language': self.user.profile.language,
            'phone': self.user.profile.phone,
            'address': self.user.profile.address,
            'email': self.user.email,
        }
        return data

    @property
    def friendsUrl(self):
        return '/u/%s/friends/' % self.user.username

    @property
    def trainingsUrl(self):
        return '/u/%s/trainings/' % self.user.username

    @property
    def teamsUrl(self):
        return '/u/%s/teams/' % self.user.username

    @property
    def chatUrl(self):
        return '/u/%s/chat/' % self.user.username

    @property
    def galleryUrl(self):
        return '/u/%s/gallery/' % self.user.username

    @property
    def locationsUrl(self):
        return '/u/%s/map/' % self.user.username

    @property
    def settingsUrl(self):
        return '/u/%s/settings/' % self.user.username

    def __str__(self):
        return self.user.username

    class Meta:
        db_table = "profile"
        verbose_name = _('profile')
        verbose_name_plural = _('profiles')


@receiver(post_save, sender=User)
def create_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.get_or_create(user=instance)


@receiver(pre_save, sender=Profile)
def set_default_avatar(instance, **kwargs):
    if not instance.avatar:
        instance.avatar = 'default/no_photo_male.png'
    if not instance.channel:
        instance.channel = utils.random_str()


@receiver(m2m_changed, sender=Profile.friend_requests.through)
def send_notification(instance, action, pk_set, **kwargs):
    if action == 'post_add':
        from_user = User.objects.get(id=next(iter(pk_set)))
        Notification.objects.create(
            user=instance.user,
            sender=from_user,
            type_not='friend_request',
            text=settings.FRIEND_REQUESTED,
            url=instance.user.profile.friendsUrl
        )
