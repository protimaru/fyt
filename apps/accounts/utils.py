from string import ascii_lowercase, ascii_uppercase, digits
import random
from django.forms.utils import ErrorList


class FormErrors(ErrorList):

    def return_error_text(self):
        return "%s" % (
            ",".join(e for e in self.errors)
        )

    def __str__(self):
        return self.return_error_text()


def random_string(len=16, chars=ascii_uppercase+ascii_lowercase+digits):
    return ''.join(random.choice(chars) for _ in range(len))


def img_upload(instance, filename):
    return 'profile_images/{}/{}'.format(instance.user.username, filename)

