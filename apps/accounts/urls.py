from django.conf.urls import url
from .views import (
    CheckUserForSignIn, UserRegisterView,
    UserLoginView, user_logout, CheckUserForExist, ProfileUpdate,
    upload_photo, tag_people_search, ProfileUpdateView,
    PasswordUpdateView, add_friend, confirm_friend_request, unfriend, remove_friend,
    ProfileAvatarUploadView)

urlpatterns = [
    url(r'^login/$', UserLoginView.as_view(), name='login'),
    url(r'^logout/$', user_logout, name='logout'),
    url(r'^register/$', UserRegisterView.as_view(), name='register'),
    url(r'^update-profile/(?P<pk>\d+)/$', ProfileUpdateView.as_view(), name='profile-update'),
    url(r'^update-password/$', PasswordUpdateView.as_view(), name='password-update'),
    # url(r'^profile/(?P<username>\w+)/$', home, name='user-home'),
    url(r'^check_user/$', CheckUserForSignIn.as_view(), name='check_user'),
    url(r'^tag_people_search/$', tag_people_search, name='tag_people_search'),
    url(r'^(?P<username>[\w-]+)/settings/$', ProfileUpdate.as_view(), name='profile.update'),
    url(r'^check/$', CheckUserForExist.as_view(), name='check'),
    url(r'^upload_photo/$', upload_photo, name='upload_photo'),
    url(r'^upload-avatar', ProfileAvatarUploadView.as_view(), name='upload-avatar'),
    url(r'^add_friend/$', add_friend, name='add_friend'),
    url(r'^remove_friend/$', remove_friend, name='remove_friend'),
    url(r'^confirm_friend_request/$', confirm_friend_request, name='confirm_friend_request'),
    url(r'^unfriend/$', unfriend, name='unfriend'),
]
