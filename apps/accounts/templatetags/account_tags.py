from django import template
from apps.accounts.models import Profile

register = template.Library()


@register.assignment_tag
def online_users():
    # users = Profile.objects.filter(is_online=True)
    users = Profile.objects.all()
    # html = ''
    # for user in users:
    #     html += '<p>'+user.user.username+'</p>'
    return users
