from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import JsonResponse, HttpResponse, HttpResponseBadRequest
from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.views.generic import View, CreateView, UpdateView
from django.contrib.auth.models import User
from django.views.generic import View, UpdateView
from django.views.generic.base import TemplateResponseMixin
from django.core.urlresolvers import reverse
from django.conf import settings
from _datetime import datetime, timezone
from django.utils import timezone

from apps.photo.models import Photo
from .forms import UserRegisterForm, ProfileRegisterForm, UserLoginForm
from .models import Profile
from apps.followers.models import Follow
from apps.utils import utils

from apps.api.accounts.serializers import UserDetailSerializer

from apps.geodata.models import Country, State, City
from apps.utils import chat
from django.core.serializers import serialize
from .forms import UserUpdateForm, ProfileUpdateForm, UpdatePassword
from django.db.models import Q
from django.contrib.auth import update_session_auth_hash


class UserPasswordUpdate(LoginRequiredMixin, UpdateView):
    model = User
    fields = ['password', ]


class UserProfileUpdate(LoginRequiredMixin, UpdateView):
    model = User
    fields = [
        'username', 'email', 'first_name', 'last_name',
        'profile.birth_date', 'profile.gender', 'profile.phone',
        'profile.address'
    ]


# TODO asdad
class ProfileUpdate(LoginRequiredMixin, TemplateResponseMixin, View):
    template_name = 'user/settings.html'
    # http_method_names = ['GET', 'POST']

    def get(self, request, username):
        user = request.user
        user_ = User.objects.get(username__iexact=username)
        can_follow = utils.can_follow(user, user_)
        if request.user.profile.city:
            cities = City.objects.filter(state_id__in=request.user.profile.country.state_set.all())
        else:
            cities = []
        can_follow = can_follow if user != user_ else 'myself'
        context = {
            'countries': Country.objects.all(),
            'cities': cities,
            'can_follow': can_follow,
            'friends_count': utils.friends_count(user_),
            'user': user,
            'user_': user_
        }
        return render(request, self.template_name, context)


class ProfileUpdateView(LoginRequiredMixin, View):

    def post(self, request, *args, **kwargs):
        request.user.username = self.request.POST.get("username")
        request.user.first_name = self.request.POST.get("first_name")
        request.user.last_name = self.request.POST.get("last_name")
        request.user.email = self.request.POST.get("email")
        request.user.profile.country_id = self.request.POST.get("country")
        request.user.profile.city_id = self.request.POST.get("city")
        request.user.profile.gender = self.request.POST.get("gender")
        request.user.profile.address = self.request.POST.get("address")
        request.user.profile.birth_date = self.request.POST.get("birth_date")
        request.user.profile.phone = self.request.POST.get("phone")
        request.user.save()
        request.user.profile.save()
        return JsonResponse({"status": "Successfully updated!", "state": "success"})


class PasswordUpdateView(LoginRequiredMixin, View):
    http_method_names = ["post", "get"]

    def get(self, request):
        request.session['_auth_user_hash'] = request.user.get_session_auth_hash()
        return render(request, "user/settings.html")

    def post(self, request):
        current_password = self.request.POST.get("current_pswd")
        new_password = self.request.POST.get("new_pswd")
        new_password_confirm = self.request.POST.get("new_pswd_cnfrm")
        user_id = self.request.POST.get("user-id")
        # user = User.objects.get(id=int(user_id))
        user = request.user
        if new_password == new_password_confirm:
            if user.check_password(current_password):
                user.set_password(new_password)
                user.save()
                request.session['_auth_user_hash'] = user.get_session_auth_hash()
                update_session_auth_hash(request, user)
                return JsonResponse({"status": "Password is successfully updated!", "state": "success"})
            return JsonResponse({"status": "Current password is incorrect!", "state": "error"})
        return JsonResponse({"status": "Passwords must match!", "state": "error"})


class CheckUserForSignIn(LoginRequiredMixin, View):

    http_method_names = ["post"]

    def post(self, request):
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(username=username, password=password)
        if user:
            login(request, user)
            return JsonResponse({'status': 'ok', 'username': user.username})
        else:
            return JsonResponse({'status': 'false', 'message': 'Username or Password is incorrect!'})


class CheckUserForExist(LoginRequiredMixin, View):

    http_method_names = ["post"]

    def post(self, request):
        action = request.POST.get('action')
        if action == 'username':
            username = request.POST.get('username')
            user = User.objects.filter(username=username)
            if not user:
                return JsonResponse({'status': 'ok', 'message': ''})
            else:
                return JsonResponse({'status': 'false', 'message': 'Username or Password is incorrect!'})
        elif action == 'email':
            email = request.POST.get('email')
            user = User.objects.filter(email=email)
            if not user:
                return JsonResponse({'status': 'ok', 'message': ''})
            else:
                return JsonResponse({'status': 'false', 'message': 'Username or Password is incorrect!'})
        return HttpResponse()


class UserRegisterView(LoginRequiredMixin, CreateView):
    template_name = 'accounts/signup.html'
    model = User
    fields = [
        'username', 'first_name', 'last_name',
        'email']

    def form_valid(self, form):
        form.save()
        form.instance.set_password(self.request.POST.get('password'))
        form.instance.save()
        country = self.request.POST.get('country')
        city = self.request.POST.get('city')
        if city is not None:
            form.instance.profile.country_id = int(country)
            form.instance.profile.city_id = int(city)
        form.instance.profile.birth_date = self.request.POST.get('birth_date')
        form.instance.profile.address = self.request.POST.get('address')
        form.instance.profile.phone = self.request.POST.get('phone')
        form.instance.profile.gender = self.request.POST.get('gender')
        form.instance.profile.save()
        return JsonResponse({"status": "ok", "url": str("")})

    def form_invalid(self, form):
        print(form)
        return HttpResponse("error")


class UserLoginView(View):

    form_class = UserLoginForm
    template_name = 'accounts/signin.html'

    def get(self, request):
        context = {
            'form': self.form_class
        }
        return render(request, self.template_name, context)

    def post(self, request):
        form = UserLoginForm(request.POST)
        if form.is_valid():
            user = authenticate(username=request.POST.get('username'),
                                password=request.POST.get('password'))
            login(request, user)
            return redirect(reverse('main:user.home', kwargs={'username': user.username}))
        return render(request, self.template_name, {'form': form})


def user_logout(request):
    logout(request)
    return redirect('/')


def home(request, username):
    return HttpResponse(request.user.username)


class ProfileAvatarUploadView(LoginRequiredMixin, View):
    def post(self, request):
        avatar = request.FILES.get("avatar")
        photo = Photo.objects.create(
            image=avatar,
            description='',
            author=request.user
        )
        photo.save()
        profile = request.user.profile
        profile.avatar = photo.image
        profile.save()
        return JsonResponse({'url': profile.avatar.url})


# avatar
def upload_photo(request):
    import base64
    from django.core.files.base import ContentFile

    if request.POST:
        data = request.POST['image']
        format_, imgstr = data.split(';base64,')
        ext = format_.split('/')[-1]
        data = ContentFile(base64.b64decode(imgstr), name='temp.' + ext)
        user = request.user.profile
        user.avatar = data
        user.save()
        return JsonResponse({'url': user.avatar.url})
    else:
        return JsonResponse({'url': 'false'})


def tag_people_search(request):
    keyword = request.POST.get("keyword")
    try:
        user = request.user
        friends = utils.get_friends_id(user=user)
        friends = friends.filter(
            Q(username__icontains=keyword) |
            Q(first_name__icontains=keyword) |
            Q(last_name__icontains=keyword)
        )
        response = []
        for u in friends:
            response.append(
                {
                    "username": u.username,
                    "full_name": u.get_full_name(),
                    "avatar": u.profile.avatar.url,
                    "url": u.get_absolute_url
                }
            )
        return JsonResponse(response, safe=False)
    except:
        pass


# ---------------------------------------------- #

def add_friend(request):
    user = request.POST.get('user')
    user = User.objects.get(id=int(user))
    if request.user not in user.profile.friends.all():
        if request.user in user.profile.friend_requests.all():
            return JsonResponse({'status': 'ok'})
        user.profile.friend_requests.add(request.user)
        user.profile.save()
        return JsonResponse({'status': 'ok'})
    return JsonResponse({'status': 'ok'})


def remove_friend(request):
    user = request.POST.get('user')
    user = User.objects.get(id=int(user))
    if user in request.user.profile.friends.all():
        request.user.profile.friends.remove(user)
        request.user.profile.save()
    if request.user in user.profile.friends.all():
        user.profile.friends.remove(request.user)
    return JsonResponse({'status': 'ok', 'message': 'removed'})

# --------------------------------------------- #


def confirm_friend_request(request):
    action = request.POST.get('action')
    req__user_id = request.POST.get('req_user_id')
    req_user = User.objects.get(id=int(req__user_id))
    if action == 'confirm':
        request.user.profile.friend_requests.remove(req_user)
        request.user.profile.friends.add(req_user)
        req_user.profile.friends.add(request.user)
        req_user.profile.save()
        if req_user in request.user.profile.friend_requests.all():
            request.user.profile.friend_requests.remove(req_user)
            request.user.profile.save()
        chat.create_room(request.user, req_user)
        response = UserDetailSerializer(req_user)
        return JsonResponse(response.data, safe=False)
    elif action == 'ignore':
        request.user.profile.friend_requests.remove(req_user)
        return JsonResponse({'status': 'ok'})
    else:
        return HttpResponseBadRequest()


def unfriend(request):
    user = request.POST.get('user_id')
    user = User.objects.get(id=user)
    request.user.profile.friends.remove(user)
    request.user.profile.save()
    user.profile.friends.remove(request.user)
    user.profile.save()
    return JsonResponse({'status': 'ok', 'count': request.user.profile.friends.count()})
# ---------------------------------------------- #
