from django.conf.urls import url
from .views import unfriend, user_follow, ignore_request, confirm_request


urlpatterns = [
    url(r'^user_follow/$', user_follow, name='user_follow'),
    url(r'^ignore_request/$', ignore_request, name='ignore_request'),
    url(r'^confirm_request/$', confirm_request, name='confirm_request'),
    url(r'^unfriend/$', unfriend, name='unfriend')
]