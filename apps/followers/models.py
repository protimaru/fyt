import uuid
from django.db import models
from django.contrib.auth.models import User
from django.dispatch import receiver
from django.db.models.signals import post_save, pre_delete
from django.db.models import Q

from apps.chat.models import Room
from apps.notifications.models import Notification
from apps.utils import utils


class Follow(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    follower = models.ForeignKey(User, related_name='follower', on_delete=models.CASCADE)
    is_friend = models.BooleanField(default=False)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return '{} follows {}'.format(self.follower.username, self.user.username)


@receiver(post_save, sender=Follow)
def create_room(sender, instance, created, **kwargs):
    if created:
        try:
            user = instance.user
            user.profile.set_notifications_status(True)
            notification = Notification.objects.filter(
                user=instance.user,
                sender=instance.follower,
                type_not='follow',
                text='requested to follow',
                url=str(instance.user.get_absolute_url()) + 'friends/',
            )
            if notification.count() > 0:
                notification.delete()
            Notification.objects.create(
                user=instance.user,
                sender=instance.follower,
                type_not='follow',
                text='requested to follow',
                url=str(instance.user.get_absolute_url()) + 'friends/',
            )
        except:
            pass
        while True:
            name = uuid.uuid4()
            r = Room.objects.filter(name=name)
            if r.count() == 0:
                break
        room = Room.objects.create(name=name)
        room.users.add(instance.user, instance.follower)
        room.save()


@receiver(pre_delete, sender=Follow)
def delete_channel(sender, instance, *args, **kwargs):
    ids = [instance.user.id, instance.follower.id]
    users = User.objects.filter(id__in=ids)
    for room in Room.objects.iterator():
        if set(room.users.all()) == set(users):
            room.delete()
    # room = Room.objects.filter(users=users)
    # print(room)
    # room.delete()
