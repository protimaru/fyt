from django.db.models import Q
from django.http import HttpResponse, JsonResponse
from django.shortcuts import render
from django.contrib.auth.models import User
from apps.followers.models import Follow
from apps.notifications.models import Notification


def user_follow(request):
    user_id = request.POST.get('id')
    action = request.POST.get('action')
    if user_id and action:
        user = User.objects.get(id=user_id)
        if action == 'follow':
            following = Follow.objects.filter(Q(user=user, follower=request.user) | Q(user=request.user, follower=user))
            if not following.exists():
                follow = Follow.objects.create(
                    user=user,
                    follower=request.user)
                follow.save()
                return JsonResponse({'status': 'ok'})
            elif following.exists() and following.count() == 1:
                following.update(is_friend=True)
                return JsonResponse({'status': 'ok'})
        elif action == 'unfollow':
            obj = Follow.objects.filter(Q(user=user, follower=request.user) |
                                        Q(user=request.user, follower=user))
            obj.delete()
            return JsonResponse({'status': 'ok'})


def unfriend(request):
    role = request.POST.get('role')
    user_id = request.POST.get('user_id')
    follower_id = request.POST.get('follower_id')
    if role == 'user':
        user = User.objects.get(id=user_id)
        follower = User.objects.get(id=follower_id)
        Follow.objects.filter(user=user, follower=follower).delete()
    elif role == 'follower':
        user = User.objects.get(id=follower_id)
        follower = User.objects.get(id=user_id)
        Follow.objects.filter(user=user, follower=follower).delete()
    return HttpResponse('deleted')


def ignore_request(request):
    user_ = request.POST.get('user_id')
    user_ = User.objects.get(id=user_)

    user = request.user

    Follow.objects.filter(user=user, follower=user_, is_friend=False).delete()

    return HttpResponse('denied')


def confirm_request(request):
    # user_ = request.POST.get('user_id')
    user_ = User.objects.filter(id=request.POST.get('user_id'))

    user = request.user

    flw = Follow.objects.filter(user=user, follower=user_, is_friend=False).update(is_friend=True)
    try:
        Notification.objects.filter(
            user=user,
            sender=user_,
            type='follow'
        ).delete()
    except:
        pass
    # flw.is_friend =True
    # flw.save()
    return HttpResponse('confirmed')