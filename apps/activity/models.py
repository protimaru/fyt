from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.contrib.auth.models import User


class Action(models.Model):
    user = models.ForeignKey(User, related_name='activity', db_index=True, on_delete=models.CASCADE)
    verb = models.CharField(max_length=255)
    target_ct = models.ForeignKey(ContentType, blank=True, null=True, related_name='target_obj', on_delete=models.CASCADE)
    target_id = models.PositiveIntegerField(null=True, blank=True, db_index=True)
    target = GenericForeignKey('target_ct', 'target_id')
    created = models.DateTimeField(auto_now_add=True, db_index=True)

    class Meta:
        db_table = 'activities'
        ordering = ('-created',)
        # unique_together = (('user', 'verb', 'target_id'), )

    def __str__(self):
        if self.verb == 'likes':
            return '{} {} {}\'s {}'.format(self.user.username, self.verb, self.target.owner.username, self.target_ct)
        elif self.verb == 'commented':
            return '{} {} on {}\'s {}'.format(self.user.username, self.verb, self.target.owner.username, self.target_ct)
        elif self.verb == 'replied':
            return '{} {} to {}\'s {}'.format(self.user.username, self.verb, self.target.author.username, self.target_ct)
        elif self.verb == 'posted':
            return '{} created a new {}: {}'.format(self.user.username, self.target_ct, self.target.context if self.target.context else '')
        else:
            return 'aaaaa'
