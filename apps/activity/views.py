from django.contrib.auth.models import User
from django.core.files.images import get_image_dimensions
from django.http import HttpResponse, JsonResponse
from django.shortcuts import render, get_object_or_404
# from apps.activity.models import Follow
from apps.posts.models import Post
from apps.utils import utils
# from apps.api.serializers import PostListSerializer

from rest_framework.response import Response
from rest_framework.views import APIView
from django.views.generic import View


# class GetFeeds(APIView):

#     def post(self, request):
#         user = request.user
#         user_id = request.POST.get('user_id')
#         if request.user.id != user_id:
#             posts = Post.objects.filter(owner_id=user_id)
#             serializer = PostListSerializer(data=posts, many=True)
#             if serializer.is_valid():
#                 print(serializer.validated_data)
#             return Response(data=serializer.data)
# return Response(data={})
#         else:
#             location = request.POST.get('location')
#             if location == 'home':
#                 data = []
#                 feeds = user.profile.get_home_feed()
#                 post_id = request.POST.get('object_id')
#                 if post_id:
#                     feeds = feeds.filter(target_id__lt=post_id).order_by('-id')[:5]
#                 else:
#                     feeds = feeds.order_by('-id')[:5]
#                 if feeds:
#                     print("FEED: ", feeds)
#                     for feed in feeds:
#                         files = []
#                         try:
#                             for image in feed.target.images.all():
#                                 try:
#                                     width, height = get_image_dimensions(image.photo)
#                                     files.append({
#                                         'type': 'image',
#                                         'id': image.id,
#                                         'url': image.photo.url,
#                                         'width': width,
#                                         'height': height,
#
#                                     })
#                                 except FileNotFoundError:
#                                     pass
#                         except:
#                             pass
#                         try:
#                             for video in feed.target.videos.all():
#                                 files.append({
#                                     'type': 'video',
#                                     'id': video.id,
#                                     'url': video.video.url,
#
#                                 })
#                         except:
#                             pass
#                         data.append(
#                             {
#                                 'id': feed.target.id,
#                                 'context': feed.target.context,
#                                 'created': feed.target.created.isoformat(),
#                                 'comments': feed.target.comment_set.count(),
#                                 'media': files,
#                                 'like': {
#                                     'liked': 'yes' if request.user in feed.target.likes.all() else 'no',
#                                     'count': feed.target.likes.count(),
#                                 },
#                                 'action': feed.verb,
#                                 'actor': {
#                                     'id': feed.user.id,
#                                     'username': feed.user.username,
#                                     'fullname': feed.user.get_full_name()
#                                 },
#                                 'author': {
#                                     'id': feed.target.owner.id,
#                                     'fullname': feed.target.owner.get_full_name(),
#                                     'username': feed.target.owner.username,
#                                     'avatar': feed.target.owner.profile.avatar.url,
#                                     'url': feed.target.owner.get_absolute_url(),
#                                 }
#                             }
#                         )
#                 else:
#                     data = []
#                 return JsonResponse(data, safe=False)
#             elif location == 'feed':
#                 data = []
#                 feeds = user.profile.get_home_feed()
#                 post_id = request.POST.get('object_id')
#                 print(post_id)
#                 if post_id:
#                     feeds = feeds.filter(target_id__lt=post_id).order_by('-id')[:5]
#                 else:
#                     feeds = feeds.order_by('-id')[:5]
#                 if feeds:
#                     for feed in feeds:
#                         files = []
#                         # print(feed.target.images.all())
#                         if feed.target.images.all():
#                             for image in feed.target.images.all():
#                                 try:
#                                     width, height = get_image_dimensions(image.photo)
#                                     files.append({
#                                         'type': 'image',
#                                         'id': image.id,
#                                         'url': image.photo.url,
#                                         'width': width,
#                                         'height': height,
#
#                                     })
#                                 except FileNotFoundError:
#                                     # files.append({})
#                                     pass
#                             # response.append(files)
#                         if feed.target.videos.all():
#                             for video in feed.target.videos.all():
#                                 files.append({
#                                     'type': 'video',
#                                     'id': video.id,
#                                     'url': video.video.url,
#
#                                 })
#                         data.append(
#                             {
#                                 'id': feed.target.id,
#                                 'context': feed.target.context,
#                                 'created': feed.target.created.isoformat(),
#                                 'comments': feed.target.comment_set.count(),
#                                 'media': files,
#                                 'like': {
#                                     'liked': 'yes' if request.user in feed.target.likes.all() else 'no',
#                                     'count': feed.target.likes.count(),
#                                 },
#                                 'action': feed.verb,
#                                 'actor': {
#                                     'id': feed.user.id,
#                                     'username': feed.user.username,
#                                     'fullname': feed.user.get_full_name()
#                                 },
#                                 'author': {
#                                     'id': feed.target.owner.id,
#                                     'fullname': feed.target.owner.get_full_name(),
#                                     'username': feed.target.owner.username,
#                                     'avatar': feed.target.owner.profile.avatar.url,
#                                     'url': feed.target.owner.get_absolute_url(),
#                                 }
#                             }
#                         )
#                 else:
#                     data = []
#                 return JsonResponse(data, safe=False)
#
#
# class GetFriendsPosts(View):
#     def post(self, request):
#         user_id = request.POST.get('user_id')
#         if request.user.id != user_id:
#             posts = Post.objects.filter(owner_id=user_id)
#             serializer = PostListSerializer(posts, many=True)
#             return Response(data=serializer.data)
