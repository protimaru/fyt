import os
from celery import Celery
import moviepy.editor as mp
from django.conf import settings

app = Celery('tasks', broker='redis://localhost:6379/')


@app.task
def convert_video(video_file):
    dd = str(video_file).split('/')
    filename = str(dd[-1]).split('.')[-2]
    # ext = str(dd[-1]).split('.')[-1]
    thumb_path = str(settings.BASE_DIR) + '/media/post/video/thumbnails/'
    clip = mp.VideoFileClip(video_file)
    thumbnail = thumb_path + filename + '_thumbnail.jpg'
    if not os.path.exists(settings.BASE_DIR + '/media/post/video/thumbnails'):
        os.makedirs(settings.BASE_DIR + '/media/post/video/thumbnails')
    clip.save_frame(thumbnail, t=9.5)

    return {
        'thumbnail': thumbnail
    }
