from django.db import models
from django.contrib.auth.models import User

from apps.arenas.models import Arena


class Rating(models.Model):
    arena = models.ForeignKey(Arena, related_name='rating', on_delete=models.CASCADE)
    user = models.ForeignKey(User, related_name='arena_rating', on_delete=models.CASCADE)
    rating = models.FloatField(max_length=3)

    def __str__(self):
        return '{} on {}: {}'.format(
            self.user.username,
            self.arena.name,
            self.rating
        )
