from django.conf.urls import url

from apps.arenas.views import (
    NewArenaView, GetArenasCoordinates, ArenaCreateView,
    GetCurrentLocation
)


urlpatterns = [
    url(r'^create/arena/$', ArenaCreateView.as_view(), name='create.arenas-new'),
    url(r'^get/coordinates/$', GetArenasCoordinates.as_view(), name='get.coordinates'),
    url(r'^get/location/$', GetCurrentLocation.as_view(), name='get.location'),
]
