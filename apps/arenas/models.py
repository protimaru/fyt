from django.db import models

from apps.geodata.models import Country, City
from apps.sporttypes.models import SportType


class Arena(models.Model):
    country = models.ForeignKey(Country, blank=True, null=True, on_delete=models.DO_NOTHING)
    city = models.ForeignKey(City, blank=True, null=True, related_name='arenas', on_delete=models.DO_NOTHING)
    name = models.CharField(max_length=255)
    address = models.CharField(max_length=255)
    description = models.CharField(max_length=255, blank=True, null=True)
    email = models.CharField(max_length=255, default='', blank=True, null=True)
    phone = models.CharField(max_length=255, default='', blank=True, null=True)
    longitude = models.CharField(max_length=55)
    latitude = models.CharField(max_length=55)
    types = models.ManyToManyField(SportType, blank=True)

    class Meta:
        db_table = 'arenas'

    @property
    def get_icon(self):
        if self.types.count() > 1:
            return '/static/images/map/sportcomplex_orange.png'
        return self.types.first().icon.url

    def get_average_rating(self):
        try:
            rating = self.rating.aggregate(rating=models.Avg('rating'))
            rating = float('%.1f' % rating['rating'])
        except:
            rating = 0
        return rating

    @property
    def trainings_count(self):
        return self.training.count()

    def __str__(self):
        return self.name


class ArenaImage(models.Model):
    arena = models.ForeignKey(Arena, related_name='images', on_delete=models.CASCADE)
    image = models.ImageField(upload_to='arena_images')

    class Meta:
        db_table = 'arena_images'

    def __str__(self):
        return self.arena.name
