from django.contrib import admin

from .models import Arena, ArenaImage

admin.site.register(Arena)
admin.site.register(ArenaImage)
