from django import forms

from .models import Arena
from apps.trainings.models import Training


class NewArenaForm(forms.ModelForm):

    class Meta:
        model = Arena
        fields = '__all__'
        # fields = [
        #     'city',
        #     'name',
        #     'address',
        #     'phone',
        #     'description',
        #     'email',
        #     'longitude',
        #     'latitude',
        # ]


class CreateTrainingForm(forms.ModelForm):

    class Meta:
        model = Training
        fields = '__all__'
        # fields = [
        #     'arenas',
        #     'datetime',
        #     'days',
        #     'price',
        # ]
