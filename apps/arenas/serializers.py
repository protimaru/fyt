from rest_framework.serializers import ModelSerializer

from .models import Arena


class CoordinatesListSerializer(ModelSerializer):
    class Meta:
        model = Arena
        fields = '__all__'
