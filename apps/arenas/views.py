from django.contrib.auth.models import User
from django.core.serializers import serialize
from django.http import JsonResponse, HttpResponseRedirect, HttpResponse
from django.shortcuts import render, redirect
from django.views.generic import CreateView, View, TemplateView
from django.shortcuts import HttpResponse

from apps.trainings.models import TrainingRequest
from apps.geodata.models import Country, State, City
from apps.trainings.models import Training, TrainingImage
from apps.utils import utils
from apps.sporttypes.models import SportType
from apps.days.models import Day
from apps.arenas.models import Arena, ArenaImage
from .forms import NewArenaForm, CreateTrainingForm
from .serializers import CoordinatesListSerializer
from apps.api.map.serializers import TrainingListSerializer, ArenaListSerializer


class NewArenaView(View):
    # form_class = NewArenaForm
    # model = Arena

    def post(self, request):
        form = NewArenaForm(request.POST or None)
        if form.is_valid():
            # city_id = form.cleaned_data.get("city")
            form.save()
            for f in request.FILES.getlist("image"):
                arena_images = ArenaImage.objects.create(arena=form.instance, image=f)
                arena_images.save()
        else:
            print(form)
        return HttpResponse("ok")

    def form_invalid(self, form):
        print(form)
        return HttpResponse('fail')


class GetArenasCoordinates(View):

    def post(self, request):
        print(request.POST)
        c_code = request.POST.get('country_code')
        try:
            country = Country.objects.get(code=c_code)
            arenas = Arena.objects.filter(city__in=country.city.all())
        except City.DoesNotExist:
            arenas = []
        # response = serialize('json', arenas)
        response = CoordinatesListSerializer(arenas, many=True)
        return JsonResponse(response.data, safe=False)


class GetCurrentLocation(View):
    def get(self, request):
        return JsonResponse({})

    def post(self, request):
        got_location = request.POST.get('gotLocation')
        if got_location is not None and got_location == 'fail':
            ip = request.META.get('HTTP_X_FORWARDED_FOR')
            data = utils.get_location_info(ip_address=str(ip))
            return JsonResponse(data, safe=False)
        return JsonResponse({})


class ArenaCreateView(CreateView):
    template_name = 'user/my_trainings.html'
    model = Arena
    fields = (
        'country', 'city', 'name', 'address', 'description',
        'email', 'phone', 'longitude', 'latitude',
    )

    def form_valid(self, form):
        form.save()
        images = self.request.FILES.getlist('images')
        types = self.request.POST.getlist('types')
        if len(types) == 1:
            types = list(map(int, types))
            types = SportType.objects.get(id__in=types)
            form.instance.types.add(types)
        elif len(types) > 1:
            types = SportType.objects.filter(id__in=types)
            form.instance.types.add(*types)
        for image in images:
            ArenaImage.objects.create(arena=form.instance, image=image)
        response = ArenaListSerializer(form.instance)
        return JsonResponse(data=response.data, status=201)

    def form_invalid(self, form):
        print(form)
        return HttpResponse('error!')
