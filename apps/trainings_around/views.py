from django.views.generic import TemplateView

from apps.trainings.models import Training


class TrainingsAroundView(TemplateView):
    template_name = 'trainings_around.html'
