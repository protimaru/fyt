from django.conf.urls import url

from .views import (
   AlbumCreateView, AlbumDetailViewHTML, AlbumListViewHTML,
   AlbumAddFiles, AlbumUpdateView
)

urlpatterns = [
   url(r"^add/$", AlbumCreateView.as_view(), name="add"),
   url(r"^detail/$", AlbumDetailViewHTML.as_view(), name="detail"),
   url(r"^list/$", AlbumListViewHTML.as_view(), name="list"),
   url(r"^add_files/$", AlbumAddFiles.as_view(), name="add_files"),
   url(r"^album_update/$", AlbumUpdateView.as_view(), name="album_update")
]
