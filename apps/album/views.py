from itertools import chain
from operator import attrgetter

from django.contrib.auth.models import User
from django.shortcuts import render
from django.views.generic import View
from django.http import JsonResponse, HttpResponse

from apps.photo.models import Photo
from apps.video.models import Video
from .models import Album
from apps.api.album.serializers import AlbumListSerializer


class AlbumCreateView(View):
    __doc__ = """
        New album create View
    """

    def post(self, request):
        album = Album.new_album(request)
        response = AlbumListSerializer(album)
        if album:
            return JsonResponse(response.data, safe=False)
        else:
            return JsonResponse({"status": "fail"})


class AlbumListViewHTML(View):
    def post(self, request):
        user_id = request.POST.get("user_id")
        user = User.objects.get(id=int(user_id))
        albums = Album.objects.filter(author=user)
        if albums.count() == 0:
            return HttpResponse(
                """
                <div class="ui visible message">
                  <p>You can always see me</p>
                </div>
                """,
                content_type="text/html"
            )
        html = ''
        for album in albums:
            html += """
                <div class="gal-item col">
                    <a href="#" class="gal-detail" data-id=\"""" + str(album.id) + """\">
                        <div class="media-block media-smash">
                            <span class="edit-btn" title="Edit album" data-ptype="album" data-pid="album-edit"></span>
                            <img src=\"""" + str(album.album_art.url) + """"\">
                            <span class="media-count">
                                <span class="item-count img"><span class="count">""" + str(album.photo.count()) + """</span></span>
                                <span class="item-count video"><span class="count">""" + str(album.video.count()) + """</span></span>
                            </span>
                        </div>
                        <div class="media-title">""" + str(album.name) + """</div>
                    </a>
                </div>
            """
        html = """
            <div class="btn-area">
                 <div class="btn-wrapper">
                    <button type="button" data-pid="album-create" data-ptype="album" class="modal-trigger ui orange button txt-uppercase">Create album</button>
                 </div>
            </div>
            <div class="tab-inner">
                <div class="row">
        """ + html + """
        </div></div>
        """
        return HttpResponse(html, content_type='text/html')


class AlbumDetailViewHTML(View):
    def post(self, request):
        album_id = request.POST.get("album_id")
        content_type = request.META.get("HTTP_ACCEPT").split(",")[0]
        album = Album.objects.get(id=int(album_id))
        if content_type == "text/html":
            files_html = ""
            files = sorted(
                chain(
                    album.photo.all(),
                    album.video.all()
                ),
                key=attrgetter('created'),
                reverse=True
            )
            if len(files) == 0:
                return HttpResponse(
                    """
                    <div class="tab-inner">
                        <div class="desc-block">
                            <header>
                                <h3 class="desc-block__hd">""" + album.name + """</h3>
                                <div class="title update-state">Updated about a month ago</div>
                            </header>
                            <div class="desc-block__content">
                                <p>""" + album.description + """</p>
                            </div>
                            <div class="btn-wrapper">
                                <button type="button" data-id=\"""" + str(album.id) + """\" data-pid="add-photovideo" data-ptype="photovid" class="modal-trigger mt-detail ui orange button txt-uppercase">Add photo/video</button>
                                <button type="button" data-id=\"""" + str(album.id) + """\" data-pid="album-edit" data-ptype="album" class="edit-btn mt-detail ui orange button txt-uppercase">Edit</button>
                            </div>
                        </div>
                        <div class="row">
                            <div class="ui visible message mlist-empty">
                              <p>This album is empty!</p>
                            </div>
                        </div>
                    </div>
                    """,
                    content_type="text/html"
                )
            for file in files:
                if isinstance(file, Video):
                    files_html += """
                            <div class="gal-item col">
                                <a href="/_video/video/""" + str(file.id) + """/" data-fbcustom="album-item" data-type="iframe" class="media-block media-video" data-id=\"""" + str(file.id) + """\">
                                    <span class="hover-icon"></span>
                                    <span class="edit-btn" data-pid="edit-photovideo" data-ptype="vid" title="Edit item"></span>
                                    <img src=\"""" + str(file.thumbnail.url) + """\">
                                </a>
                            </div>
                        """
                elif isinstance(file, Photo):
                    files_html += """
                            <div class="gal-item col">
                                <a href=\"""" + str(file.image.url) + """\" data-fbcustom="album-item" data-type="image" class="media-block media-img" data-id=\"""" + str(file.id) + """\">
                                    <span class="hover-icon"></span>
                                    <span class="edit-btn" data-pid="edit-photovideo" data-ptype="photo" title="Edit item"></span>
                                    <img src=\"""" + str(file.image.url) + """\">
                                </a>
                            </div>
                        """
            html = """  <div class="tab-inner">
                            <div class="desc-block">
                                <header>
                                    <h3 class="desc-block__hd">""" + album.name + """</h3>
                                    <div class="title update-state">Updated about a month ago</div>
                                </header>
                                <div class="desc-block__content">
                                    <p>""" + album.description + """</p>
                                </div>
                                <div class="btn-wrapper">
                                    <button type="button" data-id=\"""" + str(album.id) + """\" data-pid="add-photovideo" data-ptype="photovid" class="modal-trigger mt-detail ui orange button txt-uppercase">Add photo/video</button>
                                    <button type="button" data-id=\"""" + str(album.id) + """\" data-pid="album-edit" data-ptype="album" class="edit-btn mt-detail ui orange button txt-uppercase">Edit</button>
                                </div>
                            </div>
                            <div class="row">
                                """ + files_html + """
                            </div>
                        </div>
    
            """
            return HttpResponse(html, content_type="text/html")
        elif content_type == "application/json":
            album = AlbumListSerializer(album)
            return JsonResponse(album.data, safe=False)


class AlbumAddFiles(View):
    __doc__ = """
        Add files to "album"
        Params:
            POST: album_id int, album_art image
    """

    def post(self, request):
        album = Album.objects.get(
            id=int(request.POST.get("album_id"))
        )
        result = album.add_files(request)
        return JsonResponse(result, safe=False)


class AlbumUpdateView(View):
    __doc__ = """
        Album Update View
        Params:
            POST: album_id int, name, description string
            album_art image
    """

    def post(self, request):
        album = Album.objects.get(
            id=int(
                request.POST.get("album_id")
            )
        )
        result = album.update_album(
            request.POST.get("name"),
            request.FILES.get("album_art"),
            request.POST.get("description")
        )
        return JsonResponse(result)
