from django.contrib.auth.models import User
from django.db import models
from django.conf import settings

from apps.api.photo.serializers import PhotoListSerializer
from apps.api.video.serializers import VideoListSerializer
from apps.photo.models import Photo
from apps.video.models import Video


class Album(models.Model):
    name = models.CharField(max_length=255)
    album_art = models.ImageField(upload_to='albums', blank=True)
    photo = models.ManyToManyField(Photo, blank=True)
    video = models.ManyToManyField(Video, blank=True)
    description = models.TextField()
    author = models.ForeignKey(User, related_name='albums')

    class Meta:
        db_table = 'new_album'
        ordering = ['-id', ]

    def __str__(self):
        return self.name

    @staticmethod
    def new_album(request):
        name = request.POST.get("name")
        album_art = request.FILES.get("album_art")
        description = request.POST.get("description")
        album = Album.objects.create(
            name=name,
            album_art=album_art,
            description=description,
            author=request.user
        )
        album.save()
        return album

    @staticmethod
    def check_files_for_exist(file):
        if isinstance(file, Photo):
            try:
                album = Album.objects.get(
                    photo__id__in=[file.id, ]
                )
                album.photo.remove(file)
                album.save()
            except Exception:
                raise Exception("Object not found on %s!" % Album.__class__.name)
        elif isinstance(file, Video):
            try:
                album = Album.objects.get(
                    video__id__in=[file.id, ]
                )
                album.photo.remove(file)
                album.save()
            except Exception:
                raise Exception("Object not found on %s!" % Album.__class__.name)

    def add_files(self, request):
        from apps.api.album.serializers import AlbumDetailSerializer
        from itertools import chain
        from operator import attrgetter
        p_id = []
        v_id = []
        description = request.POST.get("description")
        for file in request.FILES.getlist("file"):
            ext = str(file).split('.')[-1].upper()
            print(ext)
            if ext in settings.IMAGE_TYPES:
                photo = Photo.objects.create(
                    image=file,
                    description=description,
                    author=request.user
                )
                photo.save()
                self.photo.add(photo)
                self.save()
                p_id.append(photo.id)
            elif ext in settings.VIDEO_TYPES:
                video = Video.objects.create(
                    video=file,
                    description=description,
                    author=request.user
                )
                video.save()
                self.video.add(video)
                self.save()
                v_id.append(video.id)
            photos = Photo.objects.filter(id__in=p_id)
            videos = Video.objects.filter(id__in=v_id)
            files_ = sorted(
                chain(
                    photos,
                    videos
                ),
                key=attrgetter('id'),
                reverse=True
            )
            ff = []
            for f in files_:
                if isinstance(f, Photo):
                    ff.append(PhotoListSerializer(f).data)
                elif isinstance(f, Video):
                    ff.append(VideoListSerializer(f).data)
        # serializer = AlbumDetailSerializer(self)
        return ff

    def update_album(self, name=None, album_art=None, description=None):
        if name:
            self.name = name
        if album_art and album_art != "undefined":
            self.album_art = album_art
        if description:
            self.description = description
        self.save()
        return {"status": "ok"}
