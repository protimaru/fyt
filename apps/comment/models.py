from django.db import models
from django.contrib.auth.models import User
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType

from apps.reply.models import Reply


class Comment(models.Model):
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')
    text = models.TextField(blank=True, null=True)
    author = models.ForeignKey(User, related_name='comments')
    created = models.DateTimeField(auto_now_add=True)

    class Meta:
        db_table = 'new_comments'
        ordering = ['-id', ]

    def __str__(self):
        return self.author.username

    @property
    def replies(self):
        replies = Reply.objects.filter(
            content_type__model='comment',
            object_id=self.id
        )
        return replies

    @staticmethod
    def add_comment(request):
        ct = request.POST.get("content_type")
        oi = request.POST.get("object_id")
        txt = request.POST.get("text")
        app = ContentType.objects.get(app_label=ct)
        if app:
            comment = Comment.objects.create(
                content_type=app,
                object_id=int(oi),
                text=txt,
                author=request.user
            )
            comment_count = Comment.comments_count(
                ct, oi
            )
            from apps.api.comment.serializers import CommentListSerializer
            comment = CommentListSerializer(comment)
            comment = dict(comment.data)
            comment["comments_count"] = comment_count
            return comment
        else:
            raise Exception("Application not found!")

    @property
    def post_comments_count(self):
        count = Comment.objects.filter(
            content_type__model="post"
        ).count()
        return count

    @staticmethod
    def comments_count(app_name, id):
        model = ContentType.objects.get(
            app_label=app_name
        )
        comments = Comment.objects.filter(
            content_type=model,
            object_id=int(id)
        )
        print(comments)
        return comments.count()
