from django.contrib import admin

from .models import Comment


class CommentAdmin(admin.ModelAdmin):
    class Meta:
        model = Comment
        fields = '__all__'

    readonly_fields = ['replies',]

    def replies(self, obj):
        return obj.replies


admin.site.register(Comment, CommentAdmin)
