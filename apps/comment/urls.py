from django.conf.urls import url

from .views import CommentAddView, CommentsListForGallery


urlpatterns = [
    url(r'^add/$', CommentAddView.as_view(), name='add'),
    url(r'^get_for_gallery/$', CommentsListForGallery.as_view(), name='get'),
]
