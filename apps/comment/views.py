from django.contrib.contenttypes.models import ContentType
from django.shortcuts import render
from django.views.generic import View
from django.http import JsonResponse, HttpResponse

from apps.like.models import Like
from .models import Comment

# from apps.api.comment.serializers import CommentListSerializer


class CommentAddView(View):
    def post(self, request):
        comment = Comment.add_comment(request)
        # cmt = CommentListSerializer(comment)
        print(comment)
        return JsonResponse(comment, safe=False)


class CommentsListForGallery(View):
    def post(self, request):
        content_type = request.POST.get("content_type")
        object_id = request.POST.get("object_id")
        content_type = ContentType.objects.get(app_label=content_type)
        obj = content_type.model_class().objects.get(
            id=int(object_id)
        )
        like = Like.objects.filter(
            content_type__model=content_type,
            object_id=object_id,
        )
        print(like)
        comments = Comment.objects.filter(
            content_type__model=content_type,
            object_id=object_id
        )
        html = """
            <div class="about-author">
            <div class="author-img"><img src="/static/images/profile/no_photo_male.png"></div>
            <div class="author-about">
                <div class="author-about__name">""" + str(obj.author.profile.fullname) + """</div>
                <div class="author-about__time cp__u-date" data-datetime=\"""" + str(obj.created) + """\">""" + str(obj.created) + """</div>
            </div>
        </div>
        <div class="about-media">
            <div class="media-desc">
                <p>"""
        desc = str(obj.description) if obj.description else "No description"
        html += desc + """</p>
            </div>
            <div class="media-serv">
                <div class="media-serv__btns">
                    <a href="#" class="serv-btn comment" data-target="#micom" data-eltype="com" data-ptype=\"""" + str(obj.get_model_name) + """\" data-pid=\"""" + str(obj.id) + """\" data-state="on">
                        <span class="icon"></span>
                        <span class="txt"><span class="count">""" + str(comments.count()) + """</span> Comments</span>
                    </a>
                    <a href="#" data-count=\"""" + str(like.count()) + """\" data-ptype=\"""" + str(obj.get_model_name) + """\" data-pid=\"""" + str(obj.id) + """\" class="serv-btn like"""
        # liked = " active" if like else ""
        if like.filter(author=request.user).count() > 0:
            html += " active"
        html += """">
                        <span class="icon"></span>
                        <span class="txt"><span class="count">""" + str(like.count()) + """</span> Like</span>
                    </a>
                </div>
            </div>
            <div class="media-serv__comment" id="micom">"""

        if comments.count() > 5:
            html += """
                <div class="lmore-pilist">
                    <a href="#" class="lmore-trigger" data-ptype=\"""" + str(obj.get_model_name) + """\" data-pid=\"""" + str(obj.id) + """\" data-eltype="com">Show 5 more comments</a>
                </div>
            """
        html += """<div class="published comment"></div>
                <div class="item-add comment">
                    <div class="add-author"><img src=\"""" + str(request.user.profile.avatar.url) + """\"></div>
                    <div class="add-tarea">
                        <textarea class="tarea" rows="1" data-min-rows="1" data-ptype=\"""" + str(obj.get_model_name) + """\" data-pid=\"""" + str(obj.id) + """\" data-publishcon=".published.comment" data-eltype="com" placeholder="Write a comment"></textarea>
                    </div>
                </div>
            </div>
        </div>
        """
        return HttpResponse(html, content_type="text/html")
