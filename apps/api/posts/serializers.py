from rest_framework.serializers import (
    ModelSerializer,
    CharField, StringRelatedField,
    RelatedField,
    ListField, IntegerField
)

from apps.posts.models import Post, Comment, Photo, Video, Reply, Album
from apps.activity.models import Action

from apps.api.accounts.serializers import UserDetailSerializer


# Photo


class PhotoCreateSerializer(ModelSerializer):
    post_id = CharField()

    class Meta:
        model = Photo
        fields = ('photo', 'post')


class PhotoListSerializer(ModelSerializer):
    width = StringRelatedField(source='photo.width')
    height = StringRelatedField(source='photo.height')

    class Meta:
        model = Photo
        fields = ('id', 'photo', 'created', 'width', 'height')


# Video


class VideoListSerializer(ModelSerializer):
    height = CharField(source='thumbnail.height')
    width = CharField(source='thumbnail.width')

    class Meta:
        model = Video
        fields = ('id', 'video', 'thumbnail', 'height', 'width', 'created')


# Post


class PostCreateSerializer(ModelSerializer):
    class Meta:
        model = Post
        fields = [
            'id',
            'context',
        ]

    def validate(self, data):
        print(data)


class PostDetailSerializer(ModelSerializer):
    class Meta:
        model = Post
        fields = [
            'id',
            'context',
            'created',
            'owner'
        ]


class CreatePostSerializer(ModelSerializer):
    class Meta:
        model = Post
        fields = [
            # 'owner',
            'context',
            'created',
            'likes',
        ]


class PostListSerializer(ModelSerializer):
    images = PhotoListSerializer(many=True, read_only=True)
    videos = VideoListSerializer(many=True, read_only=True)
    likes = ListField(source='get_liked_users')
    likes_count = StringRelatedField(source="likes.all.count")
    owner = UserDetailSerializer(read_only=True)
    comments = IntegerField(source="comment_set.all.count")
    # created = DateTimeField(source='created.isoformat')

    class Meta:
        model = Post
        fields = ['id', 'context', 'owner', 'images', 'slug',
                  'videos', 'likes', 'likes_count', 'comments', 'created', ]


class PostLiSerializer(ModelSerializer):

    class Meta:
        model = Post
        # fields = ['id', 'context', 'owner', 'created']
        fields = '__all__'


class ActivityRelatedField(RelatedField):

    def to_internal_value(self, data):
        pass

    def to_representation(self, value):
        """
        Serialize bookmark instances using a bookmark serializer,
        and note instances using a note serializer.
        """
        if isinstance(value, Post):
            serializer = PostListSerializer(value)
        if isinstance(value, Comment):
            serializer = CommentListSerializer(value)
        if isinstance(value, Reply):
            serializer = ReplyListSerializer(value)
        return serializer.data


class FeedListSerializer(ModelSerializer):
    target = ActivityRelatedField(read_only=True)
    user = UserDetailSerializer(read_only=True)

    class Meta:
        model = Action
        fields = [
            "id",
            "verb",
            "target_id",
            "created",
            "user",
            "target",
            "target_ct",
        ]


# Comment


class CommentListSerializer(ModelSerializer):
    author = UserDetailSerializer(read_only=True)
    post = PostListSerializer(read_only=True)

    class Meta:
        model = Comment
        fields = [
            'post_id',
            'id',
            'text',
            'post',
            'author',
            'created',
        ]


class CommentUpdateSerializer(ModelSerializer):
    class Meta:
        model = Comment
        fields = [
            'text',
        ]


class CommentCreateSerializer(ModelSerializer):
    class Meta:
        model = Comment
        fields = [
            'id',
            'post',
            'text',
        ]


class CommentDeleteSerializer(ModelSerializer):
    class Meta:
        model = Comment
        fields = '__all__'


# Reply


class ReplyListSerializer(ModelSerializer):
    author = UserDetailSerializer(read_only=True)

    class Meta:
        model = Reply
        fields = [
            'id',
            'text',
            'author',
            'created',
        ]


class ReplyUpdateSerializer(ModelSerializer):
    class Meta:
        model = Reply
        fields = [
            'text',
        ]


class ReplyCreateSerializer(ModelSerializer):
    class Meta:
        model = Reply
        fields = [
            'id',
            'comment',
            'text',
        ]


class ReplyDeleteSerializer(ModelSerializer):
    class Meta:
        model = Reply
        fields = '__all__'


# Album

class AlbumListSerializer(ModelSerializer):
    user = UserDetailSerializer(read_only=True)
    images = PhotoListSerializer(many=True, read_only=True)
    videos = VideoListSerializer(many=True, read_only=True)

    class Meta:
        model = Album
        fields = [
            'id',
            'user',
            'album_art',
            'images',
            'videos',
            'created',
            'updated',
        ]


class AlbumCreateSerializer(ModelSerializer):
    class Meta:
        model = Album
        fields = [
            'name',
            'album_art'
        ]
