from django.conf import settings
from django.contrib.auth.models import User
from rest_framework.generics import ListAPIView, CreateAPIView, DestroyAPIView, RetrieveUpdateAPIView
from rest_framework.parsers import MultiPartParser, JSONParser, FormParser
from rest_framework.status import HTTP_200_OK
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated

from apps.api.posts import serializers as sr

# from apps.geodata.models import Country, City
from apps.posts.models import Post, Photo, Video, Reply, Comment, Album
from apps.utils import utils


# Post

class PostListAPIView(APIView):
    serializer_class = sr.PostListSerializer

    def get(self, request):
        # friends_id, posts_id = Post.friends_liked(request.user)
        # posts_id = Post.objects.all()
        # friends = User.objects.filter(id__in=friends_id)
        # posts = Post.objects.filter(id__in=posts_id)  # | request.user.posts.all()
        posts = Post.objects.all()  # | request.user.posts.all()
        ser = self.serializer_class(posts, many=True)
        return Response(ser.data)


class GetFeedsAndPostsAPIView(APIView):
    def post(self, request):
        # user_id = int(request.data.get('user_id'))
        object_id = request.data.get('object_id')
        # location = request.data.get('location')
        if object_id:
            object_id = int(object_id)
            posts = utils.posts(request.user, object_id)
            response = sr.PostListSerializer(posts, many=True)
        else:
            posts = utils.posts(request.user)
            response = sr.PostListSerializer(posts, many=True)
        return Response(response.data)


class CreatePostAPIView(APIView):
    parser_classes = (MultiPartParser, JSONParser, )

    def post(self, request):
        post_serializer = sr.CreatePostSerializer(data=request.data)
        if post_serializer.is_valid():
            post_serializer.save(owner=request.user)

        if request.data.get('file'):
            for file in request.data.getlist('file'):
                ext = str(file).upper().split('.')[-1]
                if ext in settings.IMAGE_TYPES:
                    photo = Photo.objects.create(post=post_serializer.instance, photo=file)
                    photo.save()
                if ext in settings.VIDEO_TYPES:
                    video = Video.objects.create(post=post_serializer.instance, video=file)
                    video.save()
        return Response(data=sr.PostListSerializer(post_serializer.instance).data, status=HTTP_200_OK)


# Photo


class PhotoCreateAPIView(CreateAPIView):
    serializer_class = sr.PhotoCreateSerializer
    parser_classes = [FormParser, MultiPartParser]


class PhotoListAPIView(ListAPIView):
    permission_classes = [IsAuthenticated, ]
    serializer_class = sr.PhotoListSerializer

    def get_queryset(self):
        return Photo.objects.filter(post__owner=self.request.user)


# Video


class VideoListAPIView(ListAPIView):
    serializer_class = sr.VideoListSerializer
    permission_classes = [IsAuthenticated, ]

    def get_queryset(self):
        return Video.objects.filter(post__owner=self.request.user)

# Comment


class CommentListAPIView(ListAPIView):
    serializer_class = sr.CommentListSerializer

    def get_queryset(self):
        post_id = self.request.GET.get('post_id')
        comment_id = self.request.GET.get('comment_id')
        if post_id:
            response = Comment.objects.order_by('-id').filter(post_id=int(post_id))[:5]
        if comment_id:
            response = Comment.objects.order_by('-id').filter(post_id=int(post_id), id__lt=int(comment_id))[:5]
        return response


class CommentUpdateAPIView(RetrieveUpdateAPIView):
    queryset = Comment.objects.all()
    serializer_class = sr.CommentUpdateSerializer
    lookup_field = 'id'


class CommentCreateAPIView(CreateAPIView):
    serializer_class = sr.CommentCreateSerializer
    queryset = Comment.objects.all()

    def perform_create(self, serializer):
        serializer.save(author=self.request.user)


class CommentDeleteAPIView(DestroyAPIView):
    queryset = Comment.objects.all()
    serializer_class = sr.CommentDeleteSerializer
    lookup_field = 'id'


# Reply


class ReplyListAPIView(ListAPIView):
    serializer_class = sr.ReplyListSerializer

    def get_queryset(self):
        comment_id = self.request.GET.get('comment_id')
        reply_id = self.request.GET.get('reply_id')
        if comment_id:
            response = Reply.objects.order_by('-id').filter(comment_id=int(comment_id))[:5]
        if reply_id:
            response = Reply.objects.order_by('-id').filter(comment_id=int(comment_id), id__lt=int(reply_id))[:5]
        return response


class ReplyCreateAPIView(CreateAPIView):
    serializer_class = sr.ReplyCreateSerializer
    queryset = Reply.objects.all()

    def perform_create(self, serializer):
        serializer.save(author=self.request.user)


class ReplyUpdateAPIView(RetrieveUpdateAPIView):
    queryset = Reply.objects.all()
    serializer_class = sr.ReplyUpdateSerializer
    lookup_field = 'id'


class ReplyDeleteAPIView(DestroyAPIView):
    queryset = Reply.objects.all()
    serializer_class = sr.ReplyDeleteSerializer
    lookup_field = 'id'


# Album

class AlbumListAPIView(ListAPIView):
    serializer_class = sr.AlbumListSerializer

    def get_queryset(self):
        return Album.objects.filter(user=self.request.user)


class AlbumCreateAPIView(CreateAPIView):
    permission_classes = [IsAuthenticated, ]
    parser_classes = [MultiPartParser, FormParser]
    serializer_class = sr.AlbumCreateSerializer
    queryset = Album.objects.all()

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)
