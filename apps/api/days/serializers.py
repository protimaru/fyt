from rest_framework.relations import StringRelatedField
from rest_framework.serializers import ModelSerializer

from apps.days.models import Day
from apps.trainings.models import TrainingTime


class TrainingTimeSerializer(ModelSerializer):
    class Meta:
        model = TrainingTime
        fields = [
            'time'
        ]


class DayListSerializer(ModelSerializer):
    training_day = TrainingTimeSerializer(many=True)

    class Meta:
        model = Day
        fields = ('id', 'name', 'training_day', )
