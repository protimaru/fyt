from rest_framework.generics import ListAPIView

from apps.api.days.serializers import DayListSerializer
from apps.days.models import Day


class DayListAPIView(ListAPIView):
    queryset = Day.objects.all()
    serializer_class = DayListSerializer
