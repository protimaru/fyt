from apps.video.models import Video

from rest_framework.serializers import ModelSerializer
from rest_framework.fields import IntegerField

from apps.api.accounts.serializers import UserDetailSerializer


class VideoListSerializer(ModelSerializer):
    width = IntegerField(source='thumbnail.width')
    height = IntegerField(source='thumbnail.height')
    author = UserDetailSerializer(read_only=True)

    class Meta:
        model = Video
        fields = [
            'id',
            'video',
            'width',
            'height',
            'thumbnail',
            'description',
            'likes_count',
            'comments_count',
            'in_albums',
            'author',
            'created',
            'get_model_name'
        ]


class VideoDetailSerializer(ModelSerializer):
    width = IntegerField(source='thumbnail.width')
    height = IntegerField(source='thumbnail.height')
    author = UserDetailSerializer(read_only=True)

    class Meta:
        model = Video
        fields = [
            'id',
            'video',
            'width',
            'height',
            'thumbnail',
            'description',
            'likes_count',
            'comments_count',
            'in_albums',
            'author',
            'created',
            'get_model_name'
        ]


class VideoUpdateSerializer(ModelSerializer):
    class Meta:
        model = Video
        fields = [
            'description',
        ]
