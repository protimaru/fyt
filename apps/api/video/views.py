from apps.album.models import Album
from apps.video.models import Video

from rest_framework.generics import ListAPIView, RetrieveAPIView, UpdateAPIView, DestroyAPIView
from rest_framework.response import Response

from .serializers import VideoListSerializer, VideoDetailSerializer, VideoUpdateSerializer


class VideoListAPIView(ListAPIView):
    queryset = Video.objects.all()
    serializer_class = VideoListSerializer


class VideoDetailAPIView(RetrieveAPIView):
    queryset = Video.objects.all()
    serializer_class = VideoDetailSerializer
    lookup_field = 'id'


class VideoDeleteAPIView(DestroyAPIView):
    queryset = Video.objects.all()
    lookup_field = 'id'


class VideoUpdateAPIView(UpdateAPIView):
    queryset = Video.objects.all()
    serializer_class = VideoUpdateSerializer
    lookup_field = 'id'

    def perform_update(self, serializer):
        data = self.request.data
        album_id = data.get("album_id")
        if album_id:
            album = Album.objects.get(id=int(album_id))
            album.video.add(self.get_object())
            album.save()
        super(VideoUpdateAPIView, self).perform_update(serializer)
