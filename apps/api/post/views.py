from rest_framework.views import APIView

from apps.post.models import Post

from rest_framework.generics import ListAPIView, DestroyAPIView, RetrieveAPIView
from rest_framework.response import Response

from apps.utils import utils
from .serializers import PostListSerializer, PostDetailSerializer


class PostListAPIView(APIView):
    def post(self, request):
        object_id = request.data.get('object_id')
        if object_id:
            object_id = int(object_id)
            posts = utils.posts(request.user, object_id)
            response = PostListSerializer(posts, many=True)
        else:
            posts = utils.posts(request.user)
            response = PostListSerializer(posts, many=True)
        return Response(response.data)


class PostDeleteAPIView(DestroyAPIView):
    queryset = Post.objects.all()
    lookup_field = 'id'


class PostDetailAPIView(RetrieveAPIView):
    queryset = Post.objects.all()
    serializer_class = PostDetailSerializer
    lookup_field = 'id'
