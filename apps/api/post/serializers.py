from apps.comment.models import Comment
from apps.post.models import Post

from rest_framework.serializers import ModelSerializer
from rest_framework.fields import SerializerMethodField

from apps.api.accounts.serializers import UserDetailSerializer
from apps.api.photo.serializers import PhotoListSerializer
from apps.api.video.serializers import VideoListSerializer


class PostListSerializer(ModelSerializer):
    author = UserDetailSerializer(read_only=True)
    image = PhotoListSerializer(read_only=True, many=True)
    video = VideoListSerializer(read_only=True, many=True)
    # comments_count = SerializerMethodField(source="get_comments_count")

    class Meta:
        model = Post
        fields = [
            'id',
            'text',
            'author',
            'image',
            'video',
            'created',
            'likes_count',
            'liked_users',
            'comments_count'
        ]

    def get_comments_count(self, obj):
        print(obj.id)
        return Comment.comments_count(
            app_name="post",
            id=obj.id
        )


class PostDetailSerializer(ModelSerializer):
    author = UserDetailSerializer(read_only=True)
    image = PhotoListSerializer(read_only=True, many=True)
    video = VideoListSerializer(read_only=True, many=True)

    class Meta:
        model = Post
        fields = [
            'id',
            'text',
            'author',
            'image',
            'video',
            'created',
            'likes_count',
            'liked_users',
            'comments_count',
        ]
