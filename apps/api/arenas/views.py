from rest_framework.views import APIView
from rest_framework.generics import CreateAPIView, ListAPIView, RetrieveAPIView
from rest_framework.response import Response
from rest_framework.parsers import MultiPartParser

from django.db.models import Q

from apps.days.models import Day
from apps.arenas.models import Arena, ArenaImage
from apps.geodata.models import Country, State, City

from apps.api.arenas.serializers import ArenaListSerializer


class ArenaListAPIView(ListAPIView):
    serializer_class = ArenaListSerializer

    def get_queryset(self):
        q = self.request.GET.get('name')
        # type_ = self.request.GET.get('type')
        # print(q)
        # print(type_)
        arenas = Arena.objects.all()
        if q:
            arenas = arenas.filter(name__icontains=q)
        # if q and type_:
        #     arenas = arenas.filter(types__name__icontains=type_)
        #     arenas = arenas.filter(
        #         Q(name__icontains=q) |
        #         Q(types__name__icontains=q)
        #     )
        #     print(arenas)
        return arenas


class GetArenasCoordinatesAPIView(APIView):

    def post(self, request):
        c_code = request.data.get('country_code')
        try:
            country = Country.objects.get(sortname=c_code)
            cities = City.objects.filter(state_id__in=country.state_set.all())
            arenas = Arena.objects.filter(city__in=cities)
        except City.DoesNotExist:
            arenas = []
        arenas = Arena.objects.all()
        serializer = ArenaListSerializer(arenas, many=True)
        return Response(data=serializer.data)
