from rest_framework.serializers import ModelSerializer, SerializerMethodField
from rest_framework.relations import StringRelatedField
from rest_framework.fields import IntegerField, Field, ImageField

from apps.arenas.models import Arena, ArenaImage
from apps.trainings.models import Training, TrainingImage
from apps.sporttypes.models import SportType

from apps.api.sporttypes.serializers import SportTypeListSerializer


class ArenaImagesSerializer(ModelSerializer):
    class Meta:
        model = ArenaImage
        fields = [
            'id',
            'image',
        ]


class ArenaListSerializer(ModelSerializer):
    icon = StringRelatedField(source='get_icon')
    rating = StringRelatedField(source='get_average_rating')
    types = SportTypeListSerializer(many=True, read_only=True)
    images = ArenaImagesSerializer(many=True)
    trainings_count = IntegerField(source='trainings.count')

    class Meta:
        model = Arena
        fields = [
            'id',
            'name',
            'address',
            'description',
            'email',
            'phone',
            'trainings_count',
            'longitude',
            'latitude',
            'icon',
            'rating',
            'types',
            'images',
        ]
