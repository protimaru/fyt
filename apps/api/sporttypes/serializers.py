from rest_framework.serializers import ModelSerializer

from apps.sporttypes.models import SportType


class SportTypeListSerializer(ModelSerializer):
    class Meta:
        model = SportType
        fields = [
            'id',
            'icon',
            'name',
        ]


class SportTypeDetailSerializer(ModelSerializer):
    class Meta:
        model = SportType
        fields = [
            'id',
            'icon',
            'name',
        ]


class SportTypeUpdateSerializer(ModelSerializer):
    class Meta:
        model = SportType
        fields = [
            'icon',
            'name',
        ]


class SportTypeCreateSerializer(ModelSerializer):
    class Meta:
        model = SportType
        fields = [
            'icon',
            'name',
        ]
