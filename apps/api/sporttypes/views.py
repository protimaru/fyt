from rest_framework.generics import (
    ListAPIView, UpdateAPIView, RetrieveAPIView,
    DestroyAPIView, CreateAPIView
)
from apps.sporttypes.models import SportType

from .serializers import (
    SportTypeListSerializer, SportTypeDetailSerializer,
    SportTypeUpdateSerializer, SportTypeCreateSerializer
)


class SportTypeListAPIView(ListAPIView):
    queryset = SportType.objects.all()
    serializer_class = SportTypeListSerializer


class SportTypeDetailAPIView(RetrieveAPIView):
    queryset = SportType.objects.all()
    serializer_class = SportTypeDetailSerializer
    lookup_field = 'id'


class SportTypeUpdateAPIView(UpdateAPIView):
    queryset = SportType.objects.all()
    serializer_class = SportTypeUpdateSerializer
    lookup_field = 'id'


class SportTypeCreateAPIView(CreateAPIView):
    queryset = SportType.objects.all()
    serializer_class = SportTypeCreateSerializer
    lookup_field = 'id'


class SportTypeDeleteAPIView(DestroyAPIView):
    queryset = SportType.objects.all()
    lookup_field = 'id'
