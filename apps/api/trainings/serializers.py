from rest_framework.serializers import ModelSerializer, SerializerMethodField
from rest_framework.relations import StringRelatedField
from rest_framework.fields import ReadOnlyField, Field, ImageField

# from apps.posts.models import Country, City
from apps.api.days.serializers import DayListSerializer
from apps.arenas.models import Arena, ArenaImage
from apps.trainings.models import Training, TrainingImage, TrainingPrice, TrainingTime
from apps.sporttypes.models import SportType
from apps.days.models import Day

from apps.api.arenas.serializers import ArenaListSerializer
from apps.api.sporttypes.serializers import SportTypeListSerializer

from apps.api.accounts.serializers import UsersListSerializer, UserDetailSerializer


class TrainingPriceSerializer(ModelSerializer):
    class Meta:
        model = TrainingPrice
        fields = '__all__'


class TrainingImagesSerializer(ModelSerializer):
    class Meta:
        model = TrainingImage
        fields = ('image',)


class TrainingTimeSerializer(ModelSerializer):
    class Meta:
        model = TrainingTime
        fields = [
            'time'
        ]


class TrainingListSerializer(ModelSerializer):
    arena = ArenaListSerializer(read_only=True)
    price = TrainingPriceSerializer(many=True, read_only=True)
    time = TrainingTimeSerializer(many=True, read_only=True)
    images = TrainingImagesSerializer(many=True, read_only=True)
    types = SportTypeListSerializer(read_only=True)
    days = DayListSerializer(many=True, read_only=True)
    members = UsersListSerializer(many=True, read_only=True)
    user = SerializerMethodField('get_trainer')

    class Meta:
        model = Training
        fields = [
            'id',
            'arena',
            'name',
            'description',
            'types',
            'images',
            'price',
            'time',
            'days',
            'members',
            'user',
            'trainer_name',
            'trainer_image',
            'email',
            'trainer_description',
        ]

    def get_trainer(self, instance):
        if instance.get_user:
            serialize = UserDetailSerializer(instance.get_user)
            return serialize.data
        return None
