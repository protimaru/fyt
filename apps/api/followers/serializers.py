from rest_framework.serializers import ModelSerializer

from apps.followers.models import Follow


class FollowCreateSerializer(ModelSerializer):
    class Meta:
        model = Follow
        fields = [
            'user',
        ]


class FollowDeleteSerializer(ModelSerializer):
    class Meta:
        model = Follow
        fields = [
            'user',
            'follower',
        ]
