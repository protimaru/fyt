from rest_framework.generics import CreateAPIView, RetrieveDestroyAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.parsers import JSONParser

from apps.followers.models import Follow

from apps.api.followers.serializers import FollowCreateSerializer, FollowDeleteSerializer


class FollowUserAPIView(CreateAPIView):
    parser_classes = [JSONParser, ]
    queryset = Follow.objects.all()
    serializer_class = FollowCreateSerializer

    def perform_create(self, serializer):
        user = int(self.request.data.get('user'))
        serializer.save(user_id=user, follower=self.request.user)


class UnFollowUserAPIView(RetrieveDestroyAPIView):
    queryset = Follow.objects.all()
    serializer_class = FollowDeleteSerializer

    def get_queryset(self):
        try:
            user = (self.request.POST.get('user'))
            follower = int(self.request.POST.get('follower'))
            follow = Follow.objects.get(user_id=user, follower_id=follower)
        except (Follow.DoesNotExist, Exception):
            user = (self.request.POST.get('user'))
            follower = int(self.request.POST.get('follower'))
            follow = Follow.objects.get(user_id=follower, follower_id=user)
        return follow
