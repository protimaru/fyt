from rest_framework.generics import ListAPIView, DestroyAPIView, UpdateAPIView, RetrieveAPIView

from apps.comment.models import Comment

from .serializers import CommentListSerializer, CommentUpdateSerializer, CommentDetailSerializer, \
    CommentGenericListSerializer


class CommentListAPIView(ListAPIView):
    serializer_class = CommentListSerializer

    def get_queryset(self):
        app = self.request.GET.get("app")
        obj_id = self.request.GET.get("app_id")
        comment_id = self.request.GET.get("comment_id")
        comments = Comment.objects.all()
        if app:
            comments = comments.filter(
                content_type__model=app,
            )
        if obj_id:
            comments = Comment.objects.filter(
                content_type__model=app,
                object_id=int(obj_id)
            )[:5]
        if comment_id:
            comments = Comment.objects.filter(
                content_type__model=app,
                object_id=int(obj_id),
                id__lt=int(comment_id)
            )[:5]
        return comments


class CommentDeleteAPIView(DestroyAPIView):
    queryset = Comment.objects.all()
    lookup_field = 'id'


class CommentUpdateAPIView(UpdateAPIView):
    serializer_class = CommentUpdateSerializer
    queryset = Comment.objects.all()
    lookup_field = 'id'


class CommentDetailAPIView(RetrieveAPIView):
    serializer_class = CommentDetailSerializer
    queryset = Comment.objects.all()
    lookup_field = 'id'


class CommentList(ListAPIView):
    queryset = Comment.objects.all()
    serializer_class = CommentGenericListSerializer
