from django.contrib.auth.models import User
from rest_framework.fields import Field, IntegerField
from rest_framework.relations import RelatedField
from rest_framework.serializers import ModelSerializer, HyperlinkedModelSerializer

from apps.comment.models import Comment
from apps.photo.models import Photo
from apps.post.models import Post
from apps.posts.models import Album
from apps.video.models import Video

from apps.api.accounts.serializers import UserDetailSerializer
from apps.api.post.serializers import PostListSerializer
from apps.api.video.serializers import VideoListSerializer
from apps.api.photo.serializers import PhotoListSerializer
from apps.api.album.serializers import AlbumListSerializer


class CommentGenericRelatedSerializer(RelatedField):

    def to_representation(self, value):
        if isinstance(value, Post):
            return PostListSerializer(value).data
        elif isinstance(value, Photo):
            return PhotoListSerializer(value).data
        elif isinstance(value, Video):
            return VideoListSerializer(value).data
        elif isinstance(value, Album):
            return AlbumListSerializer(value).data
        raise Exception('Unexpected type of tagged object')


class CommentListSerializer(ModelSerializer):
    author = UserDetailSerializer(read_only=True)
    post = IntegerField(source='post_comments_count')

    class Meta:
        model = Comment
        fields = [
            'id',
            'text',
            'author',
            'created',
            'post'
        ]


class CommentDetailSerializer(ModelSerializer):
    author = UserDetailSerializer(read_only=True)
    post = IntegerField(source='post_comments_count')

    class Meta:
        model = Comment
        fields = [
            'id',
            'text',
            'author',
            'created',
            'post'
        ]


class CommentUpdateSerializer(ModelSerializer):

    class Meta:
        model = Comment
        fields = [
            'text',
        ]


# class CommentCreateSerializer(ModelSerializer):
#     class Meta:
#         model = Comment
#         fields = [
#
#         ]
#

class CommentGenericListSerializer(ModelSerializer):
    content_object = CommentGenericRelatedSerializer(read_only=True)

    class Meta:
        model = Comment
        fields = [
            # 'content_type',
            # 'object_id',
            'content_object',
            'text',
            'user',
            'created',
        ]


"""
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')
    text = models.TextField(blank=True, null=True)
    user = models.ForeignKey(User, related_name='comments')
    created = models.DateTimeField(auto_now_add=True)
"""