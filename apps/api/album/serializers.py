from rest_framework.serializers import ModelSerializer

from apps.album.models import Album

from apps.api.accounts.serializers import UserDetailSerializer
from apps.api.photo.serializers import PhotoListSerializer
from apps.api.video.serializers import VideoListSerializer


class AlbumListSerializer(ModelSerializer):
    author = UserDetailSerializer(read_only=True)
    photo = PhotoListSerializer(many=True)
    video = VideoListSerializer(many=True)

    class Meta:
        model = Album
        fields = [
            'id',
            'name',
            'album_art',
            'photo',
            'video',
            'description',
            'author',
        ]


class AlbumDetailSerializer(ModelSerializer):
    author = UserDetailSerializer(read_only=True)
    photo = PhotoListSerializer(many=True)
    video = VideoListSerializer(many=True)

    class Meta:
        model = Album
        fields = [
            'id',
            'name',
            'album_art',
            'photo',
            'video',
            'description',
            'author',
        ]


class AlbumUpdateSerializer(ModelSerializer):
    class Meta:
        model = Album
        fields = [
            'name',
            'album_art',
            'description',
        ]
