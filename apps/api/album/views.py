from rest_framework.generics import (
    ListAPIView, RetrieveAPIView,
    DestroyAPIView, UpdateAPIView
)
from rest_framework.permissions import IsAuthenticated
from rest_framework.parsers import MultiPartParser, FormParser

from apps.album.models import Album

from .serializers import (
    AlbumListSerializer, AlbumDetailSerializer,
    AlbumUpdateSerializer
)

class AlbumListAPIView(ListAPIView):
    permission_classes = [IsAuthenticated, ]
    serializer_class = AlbumListSerializer

    def get_queryset(self):
        return Album.objects.filter(
            author=self.request.user
        )


class AlbumDetailAPIView(RetrieveAPIView):
    queryset = Album.objects.all()
    serializer_class = AlbumDetailSerializer
    permission_classes = [IsAuthenticated, ]
    lookup_field = 'id'


class AlbumDeleteAPIView(DestroyAPIView):
    queryset = Album.objects.all()
    lookup_field = 'id'


class AlbumUpdateAPIView(UpdateAPIView):
    parser_classes = [MultiPartParser, FormParser]
    serializer_class = AlbumUpdateSerializer
    queryset = Album.objects.all()
    lookup_field = 'id'
    #
    # def update(self, request, *args, **kwargs):
    #     album_art = request.data.get("album_art")
    #     if album_art != "undefined":
    #         instance = self.get_object()
    #         instance.album_art = album_art
    #         instance.save()
    #     # self.get_object()
    #     super().update(request, *args, **kwargs)
    #
    # def perform_update(self, serializer):
    #     print("asdsadsa")
    #     print(super().perform_update(serializer))
    #     serializer.save()
