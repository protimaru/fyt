from rest_framework.generics import ListAPIView, DestroyAPIView, UpdateAPIView, RetrieveAPIView

from apps.reply.models import Reply

from .serializers import ReplyListSerializer, ReplyUpdateSerializer, ReplyDetailSerializer


class ReplyListAPIView(ListAPIView):
    serializer_class = ReplyListSerializer

    def get_queryset(self):
        reply_id = self.request.GET.get("reply_id")
        obj_id = self.request.GET.get("app_id")
        replies = Reply.objects.filter(
            content_type__model="comment"
        )
        if obj_id:
            replies = Reply.objects.filter(
                content_type__model="comment",
                object_id=int(obj_id)
            )[:5]
        if reply_id:
            replies = Reply.objects.filter(
                content_type__model="comment",
                object_id=int(obj_id),
                id__lt=int(reply_id)
            )[:5]
        return replies


class ReplyDeleteAPIView(DestroyAPIView):
    queryset = Reply.objects.all()
    lookup_field = 'id'


class ReplyUpdateAPIView(UpdateAPIView):
    serializer_class = ReplyUpdateSerializer
    queryset = Reply.objects.all()
    lookup_field = 'id'


class ReplyDetailAPIView(RetrieveAPIView):
    serializer_class = ReplyDetailSerializer
    queryset = Reply.objects.all()
    lookup_field = 'id'
