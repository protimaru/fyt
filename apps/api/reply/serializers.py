from rest_framework.serializers import ModelSerializer

from apps.reply.models import Reply

from apps.api.accounts.serializers import UserDetailSerializer


class ReplyListSerializer(ModelSerializer):
    author = UserDetailSerializer(read_only=True)

    class Meta:
        model = Reply
        fields = [
            'id',
            'text',
            'author',
            'created',
        ]


class ReplyUpdateSerializer(ModelSerializer):
    class Meta:
        model = Reply
        fields = [
            'text',
        ]


class ReplyDetailSerializer(ModelSerializer):
    author = UserDetailSerializer(read_only=True)

    class Meta:
        model = Reply
        fields = [
            'id',
            'text',
            'author',
            'created',
        ]
