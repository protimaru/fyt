from channels_api.bindings import ResourceBinding

from apps.chat.models import Message

from .serializers import MessageSerializer


class MessageBinding(ResourceBinding):
    serializer_class = MessageSerializer
    queryset = Message.objects.all()
    model = Message
