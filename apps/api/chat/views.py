from django.contrib.auth.models import User
from rest_framework.views import APIView
from rest_framework.response import Response

from apps.chat.models import Room


class GetRoom(APIView):
    def post(self, request):
        data = request.data
        user1 = data.get('user1')
        user2 = data.get('user2')
        room = Room.get_room_name(user1, user2)
        return Response(data={'room': room})
