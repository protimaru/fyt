from django.conf.urls import url

# from apps.api.accounts.views import (
#     UserPasswordUpdateAPIView,
#     UserFriendsListAPIView, UserSignUpAPIView, CheckUsernameAndEmailAPIView,
#     UserProfileUpdateAPIView, UserListAPIView, UsersSearch, UserAvatarUploadAPIView,
#     FriendsListAPIView
# )

# ------------------------------

from apps.api.accounts import views as account_views
from apps.api.arenas import views as arena_views
from apps.api.days import views as day_views
from apps.api.chat import views as chat_views
from apps.api.geodata import views as geodata_views
from apps.api.notifications import views as not_views
from apps.api.posts import views as post_views
from apps.api.sporttypes import views as sporttype_views
from apps.api.trainings import views as training_views

from apps.api.album import views as _album
from apps.api.post import views as _post
from apps.api.photo import views as _photo
from apps.api.video import views as _video
from apps.api.comment import views as _comment
from apps.api.reply import views as _reply

# ------------------------------

# listspatterns = [
#     url(r'^post/$', PostListAPIView.as_view(), name='post'),
#     # url(r'^countries/$', CountryListAPIView.as_view(), name='countries'),
#     url(r'^feed/$', GetFeedListAPIView.as_view(), name='feed'),
#     url(r'^feed-web/$', GetFeedsAndPostsAPIView.as_view(), name='feed-web'),
#     url(r'^comment/$', CommentListAPIView.as_view(), name='comments'),
#     url(r'^reply/$', ReplyListAPIView.as_view(), name='reply'),
#     # url(r'^trainings/$', GetTrainingsAPIView.as_view(), name='trainings'),
#     url(r'^notifications/$', GetNotificationsListAPIView.as_view(), name='nt'),
# ]

urlpatterns = [
    # url(r'^test-url/$', TestForS.as_view(), name='test-url'),
    url(r'^auth/login/$', account_views.UserLoginAPIView.as_view(), name='login'),
    url(r'^get-room/$', chat_views.GetRoom.as_view(), name='get-room'),
    url(r'^auth/register/$', account_views.UserSignUpAPIView.as_view(), name='reg'),
    # url(r'^info/$', CountriesAPIView.as_view(), name='info'),
    # url(r'^list/', include(listspatterns, namespace='list')),
    # url(r'^create/photo$', PhotoCreateAPIView.as_view(), name='create.photo'),
    # url(r'^create/comment/$', CommentCreateAPIView.as_view(), name='create.comment'),
    # url(r'^create/reply/$', ReplyCreateAPIView.as_view(), name='create.reply'),
    # url(r'^create/post/$', CreatePostAPIView.as_view(), name='create.post'),
    # # url(r'^user/(?P<username>[\w-]+)/$', UserDetailSerializer, name='user.detail'),
    # url(r'^user/friends/$', account_views.UserFriendsListAPIView.as_view(), name='user.detail'),
    # url(r'^add-like/$', AddLike.as_view(), name='add-like'),
    # url(r'^get-city/$', geodata_views.CityGetByCountryAPIView.as_view(), name='get-city'),

    # Notifications
    url(r'^notification/list/$', not_views.NotificationListAPIView.as_view(), name='notification.list'),

    # Accounts
    url(r'^profile/(?P<username>[\w-]+)/new-password/$', account_views.UserPasswordUpdateAPIView.as_view(), name='set-new-password'),
    url(r'^profile/(?P<username>[\w-]+)/update-profile/$', account_views.UserProfileUpdateAPIView.as_view(), name='update-profile'),
    url(r'^profile/(?P<username>[\w-]+)/update-avatar/$', account_views.UserAvatarUploadAPIView.as_view(), name='avatar-upload'),
    url(r'^profile/(?P<username>[\w-]+)/detail/$', account_views.UserDetailAPIView.as_view(), name='user.detail'),
    url(r'^profile/check/email-username/$', account_views.CheckUsernameAndEmailAPIView.as_view()),
    url(r'^profile/friendslist/$', account_views.FriendsListAPIView.as_view()),
    url(r'^profile/list/search/$', account_views.UsersSearch.as_view(), name='users-search'),
    url(r'^profile/list/$', account_views.UserListAPIView.as_view(), name='users-list'),

    # Days
    url(r'^days/list/days/$', day_views.DayListAPIView.as_view(), name='days'),

    # Arenas
    url(r'^arena/get/coordinates/$', arena_views.GetArenasCoordinatesAPIView.as_view(), name='get-coordinates'),
    url(r'^arena/list/arenas/$', arena_views.ArenaListAPIView.as_view(), name='arenas'),

    # Trainings
    url(r'^training/list/trainings/$', training_views.GetTrainingsAPIView.as_view(), name='get-coordinates'),
    url(r'^training/detail/trainings/(?P<pk>\d+)$', training_views.TrainingsDetailAPIView.as_view(), name='detail.trainings'),
    url(r'^training/trainings-all/$', training_views.TrainingsListAPIView.as_view(), name='trainings-all'),

    # Sport Types
    url(r'^sporttype/list/types/$', sporttype_views.SportTypeListAPIView.as_view(), name='types'),

    # Follow
    # url(r'^follower/unfollow/$', UnFollowUserAPIView.as_view(), name='user-unfollow'),
    # url(r'^follower/follow/$', FollowUserAPIView.as_view(), name='user-follow'),

    # GeoData
    url(r'^geodata/countries/$', geodata_views.CountriesListAPIView.as_view(), name='countries'),
    url(r'^geodata/cities/$', geodata_views.CityGetByCountryAPIView.as_view(), name='cities'),

    # Post
    url(r'^posts/albums/$', post_views.AlbumListAPIView.as_view(), name='post.album.list'),
    url(r'^posts/album/create$', post_views.AlbumCreateAPIView.as_view(), name='post.album.create'),
    url(r'^posts/comment/update/(?P<id>\d+)/$', post_views.CommentUpdateAPIView.as_view(), name='post.comment.update'),
    url(r'^posts/comment/delete/(?P<id>\d+)/$', post_views.CommentDeleteAPIView.as_view(), name='post.comment.delete'),
    url(r'^posts/comments/$', post_views.CommentListAPIView.as_view(), name='post.comments'),
    url(r'^posts/reply/update/(?P<id>\d+)/$', post_views.ReplyUpdateAPIView.as_view(), name='post.reply.update'),
    url(r'^posts/reply/delete/(?P<id>\d+)/$', post_views.ReplyDeleteAPIView.as_view(), name='post.reply.delete'),
    url(r'^posts/replies/$', post_views.ReplyListAPIView.as_view(), name='post.replies'),
    url(r'^posts/photos/$', post_views.PhotoListAPIView.as_view(), name='post.photo.list'),
    url(r'^posts/videos/$', post_views.VideoListAPIView.as_view(), name='post.video.list'),
    url(r'^posts/list/$', post_views.GetFeedsAndPostsAPIView.as_view(), name='post.list'),
    url(r'^posts/create/photo$', post_views.PhotoCreateAPIView.as_view(), name='post.create.photo'),
    url(r'^posts/create/comment/$', post_views.CommentCreateAPIView.as_view(), name='post.create.comment'),
    url(r'^posts/create/reply/$', post_views.ReplyCreateAPIView.as_view(), name='post.create.reply'),
    url(r'^posts/create/post/$', post_views.CreatePostAPIView.as_view(), name='post.create.post'),
    url(r'^posts/xaxa/$', post_views.PostListAPIView.as_view(), name='post.xaxa'),

    # **************************************************************************************************************** #

    url(r'^sporttype/list/$', sporttype_views.SportTypeListAPIView.as_view(), name='sporttype.list'),
    url(r'^sporttype/create/$', sporttype_views.SportTypeCreateAPIView.as_view(), name='sporttype.create'),
    url(r'^sporttype/detail/(?P<id>\d+)/$', sporttype_views.SportTypeDetailAPIView.as_view(), name='sporttype.detail'),
    url(r'^sporttype/update/(?P<id>\d+)/$', sporttype_views.SportTypeUpdateAPIView.as_view(), name='sporttype.update'),
    url(r'^sporttype/delete/(?P<id>\d+)/$', sporttype_views.SportTypeDeleteAPIView.as_view(), name='sporttype.delete'),

    url(r'^geo-data/country/list/$', geodata_views.CountriesListAPIView.as_view(), name='country.list'),
    url(r'^geo-data/country/detail/(?P<id>\d+)/$', geodata_views.CountryDetailAPIView.as_view(), name='country.detail'),
    url(r'^geo-data/country/update/(?P<id>\d+)/$', geodata_views.CountryUpdateAPIView.as_view(), name='country.update'),
    url(r'^geo-data/country/delete/(?P<id>\d+)/$', geodata_views.CountryDeleteAPIView.as_view(), name='country.delete'),
    url(r'^geo-data/city/list/$', geodata_views.CityGetByCountryAPIView.as_view(), name='city.list'),
    url(r'^geo-data/city/detail/(?P<id>\d+)/$', geodata_views.CityDetailAPIView.as_view(), name='city.detail'),
    url(r'^geo-data/city/update/(?P<id>\d+)/$', geodata_views.CityUpdateAPIView.as_view(), name='city.update'),
    url(r'^geo-data/city/delete/(?P<id>\d+)/$', geodata_views.CityDeleteAPIView.as_view(), name='city.delete'),


    # **************************************************************************************************************** #

    url(r'^user/list/$', account_views.UserProfileListAPIView.as_view(), name='user.list'),
    url(r'^user/detail/(?P<id>\d+)/$', account_views.UserProfileDetailAPIView.as_view(), name='user.detail'),


    url(r'album/list/$', _album.AlbumListAPIView.as_view(), name="_album.list"),
    url(r'album/detail/(?P<id>\d+)/$', _album.AlbumDetailAPIView.as_view(), name="_album.detail"),
    url(r'album/delete/(?P<id>\d+)/$', _album.AlbumDeleteAPIView.as_view(), name="_album.delete"),
    url(r'album/update/(?P<id>\d+)/$', _album.AlbumUpdateAPIView.as_view(), name="_album.update"),
    # TODO UPDATE APIView
    # TODO CREATE APIView

    url(r'post/list/', _post.PostListAPIView.as_view(), name="_post.list"),
    url(r'post/detail/(?P<id>\d+)/$', _post.PostDetailAPIView.as_view(), name="_post.detail"),
    url(r'post/delete/(?P<id>\d+)/$', _post.PostDeleteAPIView.as_view(), name="_post.delete"),
    # TODO UPDATE APIView
    # TODO CREATE APIView

    url(r'photo/list/', _photo.PhotoListAPIView.as_view(), name="photo.list"),
    url(r'photo/detail/(?P<id>\d+)/$', _photo.PhotoDetailAPIView.as_view(), name="photo.detail"),
    url(r'photo/update/(?P<id>\d+)/$', _photo.PhotoUpdateAPIView.as_view(), name="photo.update"),
    url(r'photo/delete/(?P<id>\d+)/$', _photo.PhotoDeleteAPIView.as_view(), name="photo.delete"),
    # TODO CREATE APIView

    url(r'video/list/', _video.VideoListAPIView.as_view(), name="video.list"),
    url(r'video/detail/(?P<id>\d+)/$', _video.VideoDetailAPIView.as_view(), name="video.detail"),
    url(r'video/update/(?P<id>\d+)/$', _video.VideoUpdateAPIView.as_view(), name="video.update"),
    url(r'video/delete/(?P<id>\d+)/$', _video.VideoDeleteAPIView.as_view(), name="video.delete"),
    # TODO CREATE APIView

    url(r'comment/list/', _comment.CommentListAPIView.as_view(), name="comment.list"),
    url(r'comment/list-new/', _comment.CommentList.as_view(), name="comment.list-new"),
    url(r'comment/detail/(?P<id>\d+)/$', _comment.CommentDetailAPIView.as_view(), name="comment.detail"),
    url(r'comment/update/(?P<id>\d+)/$', _comment.CommentUpdateAPIView.as_view(), name="comment.update"),
    url(r'comment/delete/(?P<id>\d+)/$', _comment.CommentDeleteAPIView.as_view(), name="comment.delete"),
    # TODO CREATE APIView

    url(r'reply/list/', _reply.ReplyListAPIView.as_view(), name="reply.list"),
    url(r'reply/detail/(?P<id>\d+)/$', _reply.ReplyDetailAPIView.as_view(), name="reply.detail"),
    url(r'reply/update/(?P<id>\d+)/$', _reply.ReplyUpdateAPIView.as_view(), name="reply.update"),
    url(r'reply/delete/(?P<id>\d+)/$', _reply.ReplyDeleteAPIView.as_view(), name="reply.delete"),
    # TODO CREATE APIView
]
