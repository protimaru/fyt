from rest_framework.generics import ListAPIView
from rest_framework.permissions import IsAuthenticated
# from rest_framework.response import Response

from apps.notifications.models import Notification

from . import serializers as sr


class NotificationListAPIView(ListAPIView):
    permission_classes = [IsAuthenticated, ]
    serializer_class = sr.NotificationListSerailizer

    def get_queryset(self):
        queryset = Notification.objects.filter(
            user=self.request.user
        )
        return queryset
