from rest_framework.serializers import ModelSerializer

from apps.notifications.models import Notification
from apps.api.accounts.serializers import UserDetailSerializer


class NotificationListSerailizer(ModelSerializer):
    sender = UserDetailSerializer(read_only=True)

    class Meta:
        model = Notification
        fields = [
            'sender',
            'text',
            'id',
            'url',
            'is_read'
        ]
