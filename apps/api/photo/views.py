from apps.album.models import Album
from apps.photo.models import Photo

from rest_framework.generics import ListAPIView, RetrieveAPIView, UpdateAPIView, DestroyAPIView
from rest_framework.response import Response

from .serializers import PhotoListSerializer, PhotoDetailSerializer, PhotoUpdateSerializer


class PhotoListAPIView(ListAPIView):
    queryset = Photo.objects.all()
    serializer_class = PhotoListSerializer


class PhotoDetailAPIView(RetrieveAPIView):
    queryset = Photo.objects.all()
    serializer_class = PhotoDetailSerializer
    lookup_field = 'id'


class PhotoDeleteAPIView(DestroyAPIView):
    queryset = Photo.objects.all()
    lookup_field = 'id'


class PhotoUpdateAPIView(UpdateAPIView):
    queryset = Photo.objects.all()
    serializer_class = PhotoUpdateSerializer
    lookup_field = 'id'

    def perform_update(self, serializer):
        set_avatar = self.request.data.get("set_avatar")
        album_id = self.request.data.get("album_id")
        if album_id:
            Album.check_files_for_exist(self.get_object())
            album = Album.objects.get(id=int(album_id))
            album.photo.add(self.get_object())
            album.save()
        if set_avatar:
            self.request.user.profile.avatar = self.get_object().image
            self.request.user.profile.save()
        super(UpdateAPIView, self).perform_update(serializer)
