from rest_framework.serializers import ModelSerializer
from rest_framework.fields import IntegerField

from apps.photo.models import Photo
from apps.api.accounts.serializers import UserDetailSerializer


class PhotoListSerializer(ModelSerializer):
    width = IntegerField(source='image.width')
    height = IntegerField(source='image.height')
    author = UserDetailSerializer(read_only=True)

    class Meta:
        model = Photo
        fields = [
            'id',
            'image',
            'width',
            'height',
            'author',
            'description',
            'likes_count',
            'comments_count',
            'created',
            'get_model_name',
            'is_avatar',
            'in_albums'
        ]


class PhotoDetailSerializer(ModelSerializer):
    width = IntegerField(source='image.width')
    height = IntegerField(source='image.height')
    author = UserDetailSerializer(read_only=True)

    class Meta:
        model = Photo
        fields = [
            'id',
            'image',
            'width',
            'height',
            'author',
            'description',
            'created',
            'get_model_name',
            'likes_count',
            'comments_count',
            'is_avatar',
            'in_albums',
        ]


class PhotoUpdateSerializer(ModelSerializer):

    class Meta:
        model = Photo
        fields = [
            'description',
        ]
