import random
import string


def generate():
    return ''.join(random.choice(string.ascii_lowercase) for i in range(15))


a = generate()
print(type(a))
