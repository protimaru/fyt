from django.contrib.auth.models import User
from rest_framework.views import APIView
from rest_framework.generics import ListAPIView, RetrieveAPIView, UpdateAPIView, RetrieveUpdateAPIView
from rest_framework.status import (
    HTTP_200_OK, HTTP_400_BAD_REQUEST, HTTP_401_UNAUTHORIZED, HTTP_202_ACCEPTED, HTTP_403_FORBIDDEN
)
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from rest_framework.authtoken.models import Token
from rest_framework.authentication import TokenAuthentication

from django.db.models import Q
from django.contrib.auth import authenticate

# from apps.trainings.models import Training, TrainingImage, TrainingRequest
from apps.accounts.models import Profile
from apps.followers.models import Follow
from apps.api.accounts.serializers import (
    UserLoginSerializer, UserDetailSerializer, UserFriendsListSerializer,
    UserUpdateSerializer, UsersListSerializer,
    UserAvatarUploadSerializer
)


class CheckUsernameAndEmailAPIView(APIView):
    def post(self, request):
        if 'username' in request.data:
            username = request.data.get('username')
            user = User.objects.filter(username=username).exclude(id=self.request.user.id)
            if user.exists():
                return Response({'status': 1})
            else:
                return Response({'status': 0})
        elif 'email' in request.data:
            email = request.data.get('email')
            if '@' not in email:
                return Response(status=HTTP_400_BAD_REQUEST)
            user = User.objects.filter(email=email).exclude(id=self.request.user.id)
            if user.exists():
                return Response({'status': 1})
            else:
                return Response({'status': 0})


class UserSignUpAPIView(APIView):
    def post(self, request):
        try:
            user_data = {
                'username': request.data.get("username"),
                'first_name': request.data.get("first_name"),
                'last_name': request.data.get("last_name"),
                'email': request.data.get("email"),
                'password': request.data.get("password")
            }
            profile_data = {
                'gender': request.data.get("gender"),
                'birth_date': request.data.get("birth_date"),
            }
            user = User.objects.create(**user_data)
            user.set_password(user_data['password'])
            user.save()
            Profile.objects.filter(user=user).update(**profile_data)
            # profile.save()
            return Response(data={'status': 'ok'}, status=HTTP_200_OK)
        except:
            return Response(
                data={'status': 'fail'},
                status=HTTP_400_BAD_REQUEST
            )


class UserLoginAPIView(APIView):
    serializer_class = UserLoginSerializer
    # permission_classes = [TokenAuthentication,]
    authentication_classes = [TokenAuthentication, ]

    def get(self, request):
        content = {
            'user': str(request.user),  # `django.contrib.auth.User` instance.
            'auth': str(request.auth),  # None
        }
        return Response(content)

    def post(self, request, *args, **kwargs):
        username = request.data["username"]
        password = request.data["password"]

        user = authenticate(username=username, password=password)

        if not user:
            return Response(
                {
                    "error": "Login failed"
                },
                status=HTTP_401_UNAUTHORIZED)
        token, _ = Token.objects.get_or_create(user=user)
        return Response({"token": token.key, "username": username})


class UserDetailAPIView(RetrieveAPIView):
    serializer_class = UserDetailSerializer
    lookup_field = 'username'
    lookup_url_kwarg = "username"
    queryset = User.objects.all()


class ProfileDetailAPIView(APIView):
    permission_classes = [IsAuthenticated, ]

    def get(self, request):
        user = request.user
        userdata = {
            'id': user.id,
            'username': user.username,
            'first_name': user.first_name,
            'last_name': user.last_name,
            'full_name': user.get_full_name(),
            'email': user.email,
            'birth_date': user.profile.birth_date,
            'address': user.profile.address,
            'phone': user.profile.phone,
            'country': user.profile.country.country,
            'city': user.profile.city.city,
            # 'avatar': user.profile.avatar.url,
            'avatar': request.build_absolute_uri(user.profile.avatar.url),
        }
        return Response(data=userdata, status=HTTP_200_OK)

    def post(self, request):
        return Response({"status": "ok"}, status=HTTP_200_OK)


class UserFriendsListAPIView(ListAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = UserFriendsListSerializer

    def get_queryset(self):
        followers = Follow.objects.filter(
            user=self.request.user, is_friend=True)
        following = Follow.objects.filter(
            follower=self.request.user, is_friend=True)
        followers = followers.values_list('follower_id', flat=True)
        following = following.values_list('user_id', flat=True)
        queryset = list(following) + list(followers)
        queryset = User.objects.filter(id__in=queryset)
        return queryset


class UserPasswordUpdateAPIView(APIView):
    def post(self, request):
        user = request.user
        data = request.data
        current_password = data.get('current_password')
        new_password = data.get('new_password')
        if not user.check_password(current_password):
            return Response(data={'status': 'fail'})
        user.set_password(new_password)
        user.save()
        return Response(data={'status': 'ok'})


# class UserProfileUpdateAPIView(APIView):
#     def post(self, request):
#         data = request.data
#         print(data)
#         return Response(data={'status': 'ok'})


class UserProfileUpdateAPIView(UpdateAPIView):
    queryset = User.objects.all()
    serializer_class = UserUpdateSerializer


class UserListAPIView(ListAPIView):
    serializer_class = UsersListSerializer

    def get_queryset(self):
        q = self.request.GET.get('name')
        if q:
            users = User.objects.filter(
                Q(username__icontains=q) |
                Q(first_name__icontains=q) |
                Q(last_name__icontains=q)
            ).exclude(id=self.request.user.id)
        else:
            users = User.objects.all()
        return users


class UserAvatarUploadAPIView(RetrieveUpdateAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = User.objects.all()
    serializer_class = UserAvatarUploadSerializer
    lookup_field = 'username'

    def update(self, request, *args, **kwargs):
        if not kwargs.get('username') == request.user.username:
            return Response(status=HTTP_403_FORBIDDEN)
        user = request.user
        user.profile.avatar = request.data.get('avatar')
        user.profile.save()
        return Response(data={'avatar': user.profile.avatar.url}, status=HTTP_202_ACCEPTED)


class UsersSearch(ListAPIView):
    serializer_class = UsersListSerializer

    def get_queryset(self):
        users = User.objects.all()
        q = self.request.GET.get('q')
        if q:
            users = users.filter(
                Q(username__icontains=q) |
                Q(first_name__icontains=q) |
                Q(last_name__icontains=q)
            )
        return users


class FriendsListAPIView(ListAPIView):
    serializer_class = UsersListSerializer

    def get_queryset(self):
        return self.request.user.profile.friends.all()


# New APIViews
from django.contrib.auth.models import User
from django.db.models import Q

from rest_framework.generics import (
    ListAPIView, RetrieveAPIView
)
from rest_framework.permissions import IsAuthenticated

from apps.accounts.models import Profile

from .serializers import (
    UserProfileListSerializer, UserProfileDetailSerializer
)


class UserProfileListAPIView(ListAPIView):
    serializer_class = UserProfileListSerializer
    permission_classes = [IsAuthenticated, ]

    def get_queryset(self):
        q = self.request.GET.get("q")
        # q = data.get("q")
        print(q)
        if q:
            users = User.objects.filter(
                Q(first_name__icontains=q) |
                Q(last_name__icontains=q) |
                Q(email__icontains=q)
            )
            return users
        return User.objects.all()


class UserProfileDetailAPIView(RetrieveAPIView):
    permission_classes = [IsAuthenticated, ]
    queryset = User.objects.all()
    serializer_class = UserProfileDetailSerializer
    lookup_field = 'id'
