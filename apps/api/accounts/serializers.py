from rest_framework.fields import ReadOnlyField, BooleanField, FileField
from rest_framework.relations import StringRelatedField
from rest_framework.serializers import (
    ModelSerializer, Serializer,
    CharField, ValidationError,
    HyperlinkedModelSerializer
)
# from rest_framework.fields import CharField
from django.contrib.auth import get_user_model
from django.contrib.auth import authenticate

from apps.trainings.models import Training
from apps.accounts.models import Profile

User = get_user_model()


class UserAvatarUploadSerializer(ModelSerializer):
    avatar = FileField(source='profile.avatar')

    class Meta:
        model = User
        fields = ('avatar', )


class UserLoginSerializer(Serializer):
    username = CharField(max_length=255)
    password = CharField(max_length=255)

    def validate(self, data):
        username = data.get("username")
        password = data.get("password")
        user = authenticate(username=username, password=password)
        if not user:
            raise ValidationError("error")
        return user


class ProfileSerializer(ModelSerializer):
    friends_count = ReadOnlyField()
    is_online = BooleanField(source='online')
    # country = CountryListSerializer(read_only=True)
    # city = CityListSerializer(read_only=True)

    class Meta:
        model = Profile
        fields = [
            'id',
            'birth_date',
            'gender',
            'phone',
            'country',
            'city',
            'address',
            'friends_count',
            'channel',
            'avatar',
            'location',
            'language',
            'is_online',
            'notifications_status',
            'messages_status',
            'updated',
        ]


class UsersListSerializer(ModelSerializer):

    url = CharField(source='get_absolute_url')
    profile = ProfileSerializer(read_only=True)

    class Meta:
        model = User
        fields = [
            'id',
            'username',
            'first_name',
            'last_name',
            'email',
            'url',
            'profile',
        ]


class UserDetailSerializer(ModelSerializer):
    url = CharField(source='get_absolute_url')
    profile = ProfileSerializer(read_only=True)

    class Meta:
        model = User
        fields = [
            'id',
            'username',
            'first_name',
            'last_name',
            'email',
            'url',
            'profile'
        ]


class UserFriendsListSerializer(ModelSerializer):
    profile = ProfileSerializer(read_only=True)

    class Meta:
        model = User
        fields = [
            'id',
            'username',
            'first_name',
            'last_name',
            'email',
            'profile',
        ]


class UserRegisterSerializer(ModelSerializer):

    class Meta:
        model = User
        fields = [
            'username',
            'first_name',
            'last_name',
            'email',
            # 'birth_date',
            # 'address',
            # 'phone',
        ]


class ProfileUpdateSerializer(ModelSerializer):

    class Meta:
        model = Profile
        fields = [
            'birth_date',
            'phone',
            'address',
            'language',
        ]


class UserUpdateSerializer(ModelSerializer):
    profile = ProfileUpdateSerializer()

    class Meta:
        model = User
        fields = [
            'username',
            'first_name',
            'last_name',
            'email',
            'profile'
        ]

    def update(self, instance, data):
        profile_data = data.get('profile')
        profile = Profile.objects.filter(user=instance)
        profile.update(**profile_data)
        return instance


class UserPasswordUpdateSerializer(Serializer):
    current_password = CharField()
    first_password = CharField()
    second_password = CharField()


# New ModelSerializers


class ProfileListSerializer(ModelSerializer):
    class Meta:
        model = Profile
        fields = [
            'id',
            'birth_date',
            'gender',
            'phone',
            'country',
            'city',
            'address',
            'channel',
            'avatar',
            'location',
            'language',
            'notifications_status',
            'messages_status',
            'friends',
            'friend_requests',
            'updated',
            'fullname',
        ]


class UserProfileListSerializer(ModelSerializer):
    profile = ProfileListSerializer(read_only=True)

    class Meta:
        model = User
        fields = [
            'id',
            'first_name',
            'last_name',
            'email',
            'profile'
        ]


class UserProfileDetailSerializer(ModelSerializer):
    profile = ProfileListSerializer(read_only=True)

    class Meta:
        model = User
        fields = [
            'id',
            'first_name',
            'last_name',
            'email',
            'profile'
        ]
