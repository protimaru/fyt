from django.contrib.auth.models import User
from rest_framework.generics import RetrieveUpdateAPIView
from rest_framework.response import Response
from rest_framework.status import HTTP_202_ACCEPTED, HTTP_403_FORBIDDEN
from rest_framework.permissions import IsAuthenticated
from apps.api.accounts.serializers import UserAvatarUploadSerializer


class UserAvatarUploadAPIView(RetrieveUpdateAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = User.objects.all()
    serializer_class = UserAvatarUploadSerializer
    lookup_field = 'username'

    def update(self, request, *args, **kwargs):
        if not kwargs.get('username') == request.user.username:
            return Response(status=HTTP_403_FORBIDDEN)
        user = request.user
        user.profile.avatar = request.data.get('avatar')
        user.profile.save()
        return Response(data={'avatar': user.profile.avatar.url}, status=HTTP_202_ACCEPTED)
