from django.contrib.auth.models import User
from django.db.models import Q

from rest_framework.generics import ListAPIView

from apps.api.accounts.serializers import UsersListSerializer


class UsersSearch(ListAPIView):
    serializer_class = UsersListSerializer

    def get_queryset(self):
        users = User.objects.all()
        q = self.request.GET.get('q')
        if q:
            users = users.filter(
                Q(username__icontains=q) |
                Q(first_name__icontains=q) |
                Q(last_name__icontains=q)
            )
        return users


class FriendsListAPIView(ListAPIView):
    serializer_class = UsersListSerializer

    def get_queryset(self):
        return self.request.user.profile.friends.all()
