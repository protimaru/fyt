from rest_framework.serializers import ModelSerializer, SerializerMethodField
from rest_framework.relations import StringRelatedField
from rest_framework.fields import ReadOnlyField, Field, ImageField

# from apps.posts.models import Country, City
from apps.arenas.models import Arena, ArenaImage
from apps.trainings.models import Training, TrainingImage
from apps.sporttypes.models import SportType
from apps.days.models import Day

from apps.api.accounts.serializers import UsersListSerializer, UserDetailSerializer

import base64, uuid
from django.core.files.base import ContentFile
import six
from django.contrib.auth.models import User


class SportTypeSerializer(ModelSerializer):
    class Meta:
        model = SportType
        fields = [
            'id',
            'icon',
            'name',
        ]


class ArenaImagesSerializer(ModelSerializer):
    class Meta:
        model = ArenaImage
        fields = [
            'id',
            'image',
        ]


class ArenaListSerializer(ModelSerializer):
    icon = StringRelatedField(source='get_icon')
    rating = StringRelatedField(source='get_average_rating')
    types = SportTypeSerializer(many=True, read_only=True)
    images = ArenaImagesSerializer(many=True)
    trainings_count = ReadOnlyField()

    class Meta:
        model = Arena
        fields = [
            'id',
            'name',
            'address',
            'description',
            'email',
            'phone',
            'trainings_count',
            'longitude',
            'latitude',
            'icon',
            'rating',
            'types',
            'images',
        ]


class TrainingImagesSerializer(ModelSerializer):
    class Meta:
        model = TrainingImage
        fields = ('image',)


class DayListSerializer(ModelSerializer):
    class Meta:
        model = Day
        fields = ('id', 'name',)


class TrainingListSerializer(ModelSerializer):
    arena = ArenaListSerializer(read_only=True)
    images = TrainingImagesSerializer(many=True, read_only=True)
    types = SportTypeSerializer(read_only=True)
    days = DayListSerializer(many=True, read_only=True)
    members = UsersListSerializer(many=True, read_only=True)
    user = SerializerMethodField('get_trainer')

    class Meta:
        model = Training
        fields = [
            'arenas',
            'id',
            'name',
            'description',
            'types',
            'images',
            'days',
            'members',
            'user',
            'trainer_name',
            'trainer_image',
            'email',
            'trainer_description',
        ]

    def get_trainer(self, instance):
        if instance.get_user:
            serialize = UserDetailSerializer(instance.get_user)
            return serialize.data
        return None


# Custom image field - handles base 64 encoded images

class Base64ImageField(ImageField):

    def to_internal_value(self, data):
        # print(data)
        for d in data:
            for i in d:
            # print(type(d))
                if i['image'].startswith('data:image'):
                    # base64 encoded image - decode
                    format, imgstr = data.split(';base64,') # format ~= data:image/X,
                    ext = format.split('/')[-1] # guess file extension
                    id = uuid.uuid4()
                    data = ContentFile(base64.b64decode(imgstr), name = id.urn[9:] + '.' + ext)
                    # print(data)
            return super(Base64ImageField, self).to_internal_value(data)

# class Base64ImageField(ImageField):
#
#     def to_internal_value(self, data):
#
#         # Check if this is a base64 string
#         if isinstance(data, six.string_types):
#             # Check if the base64 string is in the "data:" format
#             if 'data:' in data and ';base64,' in data:
#                 # Break out the header from the base64 content
#                 header, data = data.split(';base64,')
#
#             # Try to decode the file. Return validation error if it fails.
#             try:
#                 decoded_file = base64.b64decode(data)
#             except TypeError:
#                 self.fail('invalid_image')
#
#             # Generate file name:
#             file_name = str(uuid.uuid4())[:12]  # 12 characters are more than enough.
#             # Get the file name extension:
#             file_extension = self.get_file_extension(file_name, decoded_file)
#
#             complete_file_name = "%s.%s" % (file_name, file_extension,)
#
#             data = ContentFile(decoded_file, name=complete_file_name)
#
#         return super(Base64ImageField, self).to_internal_value(data)
#
#     def get_file_extension(self, file_name, decoded_file):
#
#         extension = imghdr.what(file_name, decoded_file)
#         extension = "jpg" if extension == "jpeg" else extension
#
#         return extension


class TrainingCreateSerializer(ModelSerializer):
    # days = DayListSerializer(many=True)
    images = Base64ImageField()

    # images = Field()

    class Meta:
        model = Training
        fields = ('arenas', 'name', 'days', 'images',)

    def create(self, validated_data):
        # print(validated_data)
        days = validated_data.pop('days')
        # trainings = Training.objects.create(**validated_data)
        # trainings.days.add(*days)
        # trainings.save()
        # return trainings
        return validated_data
