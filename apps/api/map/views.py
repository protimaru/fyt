from rest_framework.views import APIView
from rest_framework.generics import CreateAPIView, ListAPIView, RetrieveAPIView
from rest_framework.response import Response
from rest_framework.parsers import MultiPartParser

from django.db.models import Q

from apps.geodata.models import Country, State, City
from apps.days.models import Day
from apps.arenas.models import Arena, ArenaImage
from apps.sporttypes.models import SportType
from apps.trainings.models import Training, TrainingImage, TrainingRequest

from apps.api.map.serializers import (
    ArenaListSerializer, TrainingListSerializer, TrainingCreateSerializer,
    DayListSerializer, SportTypeSerializer
)


class DayListAPIView(ListAPIView):
    queryset = Day.objects.all()
    serializer_class = DayListSerializer


class SportTypeListAPIView(ListAPIView):
    queryset = SportType.objects.all()
    serializer_class = SportTypeSerializer


class ArenaListAPIView(ListAPIView):
    serializer_class = ArenaListSerializer

    def get_queryset(self):
        q = self.request.GET.get('name')
        # type_ = self.request.GET.get('type')
        # print(q)
        # print(type_)
        arenas = Arena.objects.all()
        if q:
            arenas = arenas.filter(name__icontains=q)
        # if q and type_:
        #     arenas = arenas.filter(types__name__icontains=type_)
        #     arenas = arenas.filter(
        #         Q(name__icontains=q) |
        #         Q(types__name__icontains=q)
        #     )
        #     print(arenas)
        return arenas


class GetArenasCoordinatesAPIView(APIView):

    def post(self, request):
        c_code = request.data.get('country_code')
        try:
            country = Country.objects.get(code=c_code)
            arenas = Arena.objects.filter(city__in=country.city.all())
        except City.DoesNotExist:
            arenas = []
        arenas = Arena.objects.all()
        serializer = ArenaListSerializer(arenas, many=True)
        return Response(data=serializer.data)


class GetTrainingsAPIView(APIView):
    def post(self, request):
        arena_id = request.data.get('arena_id')
        if arena_id:
            arena_id = int(arena_id)
            trainings = Training.objects.filter(arena_id=arena_id)
            serializer = TrainingListSerializer(trainings, many=True)
            return Response(data=serializer.data)


class TrainingCreateAPIView(CreateAPIView):
    queryset = Training.objects.all()
    serializer_class = TrainingCreateSerializer
    parser_classes = (MultiPartParser, )


# class TraningCreateAPIView(APIView):
#     def post(self, request):
#         print(request.data)
#         Training.objects
#         return Response(data={'status': 'ok'})

class TrainingsListAPIView(ListAPIView):
    serializer_class = TrainingListSerializer

    def get_queryset(self):
        type_ = self.request.GET.get('q')
        date = self.request.GET.get('date')
        trainings = Training.objects.filter(created_by=self.request.user)
        if type_:
            trainings = trainings.filter(types_id=int(type_))
        if date:
            if date == 'asc':
                trainings = trainings.order_by('date')
            elif date == 'desc':
                trainings = trainings.order_by('-date')
        return trainings


class TestForS(APIView):
    def post(self, request):
        print(request.data)
        return Response()


class TrainingsDetailAPIView(RetrieveAPIView):
    queryset = Training.objects.all()
    serializer_class = TrainingListSerializer


class FilterArenasAPiView(ListAPIView):
    serializer_class = ArenaListSerializer

    def get_queryset(self):
        type_ = self.request.GET.get("type")
        arenas = Arena.objects.all()
        if type:
            arenas = arenas.filter(types__in=type_)
        return arenas
