from rest_framework.serializers import ModelSerializer

from apps.geodata.models import Country, State, City


class CountryListSerializer(ModelSerializer):
    class Meta:
        model = Country
        fields = [
            'id',
            'sortname',
            'name',
            'phonecode',
        ]


class CountryDetailSerializer(ModelSerializer):
    class Meta:
        model = Country
        fields = [
            'id',
            'sortname',
            'name',
            'phonecode',
        ]


class CountryUpdateSerializer(ModelSerializer):
    class Meta:
        model = Country
        fields = [
            'sortname',
            'name',
            'phonecode',
        ]


class StateSerializer(ModelSerializer):
    class Meta:
        model = State
        fields = '__all__'


class CityListSerializer(ModelSerializer):
    class Meta:
        model = City
        fields = [
            'name',
        ]


class CityDetailSerializer(ModelSerializer):
    class Meta:
        model = City
        fields = [
            'name',
        ]


class CityUpdateSerializer(ModelSerializer):
    class Meta:
        model = City
        fields = [
            'name',
        ]
