from rest_framework.generics import ListAPIView, RetrieveAPIView, DestroyAPIView, UpdateAPIView

from apps.geodata.models import Country, State, City

from .serializers import (
    CountryListSerializer, CountryDetailSerializer, CountryUpdateSerializer,
    CityListSerializer, CityDetailSerializer, CityUpdateSerializer
)


class CountriesListAPIView(ListAPIView):
    queryset = Country.objects.all()
    serializer_class = CountryListSerializer


class CountryDetailAPIView(RetrieveAPIView):
    serializer_class = CountryDetailSerializer
    queryset = Country.objects.all()
    lookup_field = 'id'


class CountryDeleteAPIView(RetrieveAPIView):
    queryset = Country.objects.all()
    lookup_field = 'id'


class CountryUpdateAPIView(RetrieveAPIView):
    serializer_class = CountryUpdateSerializer
    queryset = Country.objects.all()
    lookup_field = 'id'


class CityGetByCountryAPIView(ListAPIView):
    serializer_class = CityListSerializer

    def get_queryset(self):
        q = self.request.GET.get('name')
        cities = City.objects.all()
        if q:
            country = Country.objects.get(sortname=q)
            cities = City.objects.filter(state_id__in=country.state_set.all())
        return cities


class CityDetailAPIView(RetrieveAPIView):
    serializer_class = CityDetailSerializer
    queryset = City.objects.all()
    lookup_field = 'id'


class CityDeleteAPIView(RetrieveAPIView):
    queryset = City.objects.all()
    lookup_field = 'id'


class CityUpdateAPIView(RetrieveAPIView):
    serializer_class = CityUpdateSerializer
    queryset = City.objects.all()
    lookup_field = 'id'
