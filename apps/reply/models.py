from django.db import models
from django.contrib.auth.models import User
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType


class Reply(models.Model):
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')
    text = models.TextField(blank=True, null=True)
    author = models.ForeignKey(User, related_name='replies')
    created = models.DateTimeField(auto_now_add=True)

    class Meta:
        db_table = 'new_replies'
        ordering = ['-id', ]

    def __str__(self):
        return self.author.username

    @staticmethod
    def add_reply(request):
        ct = ContentType.objects.get(app_label="comment")
        txt = request.POST.get("text")
        comment_id = request.POST.get("comment_id")
        reply = Reply.objects.create(
            content_type=ct,
            object_id=comment_id,
            text=txt,
            author=request.user
        )
        reply.save()
        return reply

