from django.http import JsonResponse
from django.shortcuts import render
from django.views.generic import View

from .models import Reply

from apps.api.reply.serializers import ReplyListSerializer


class ReplyAddView(View):
    def post(self, request):
        reply = Reply.add_reply(request)
        rpl = ReplyListSerializer(reply)
        return JsonResponse(rpl.data, safe=False)
