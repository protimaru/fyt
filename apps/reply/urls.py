from django.conf.urls import url

from .views import ReplyAddView

urlpatterns = [
    url(r'^add/$', ReplyAddView.as_view(), name='add')
]
