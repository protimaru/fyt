from django.contrib.auth.models import User
from django.contrib.contenttypes.models import ContentType
from django.db.models.signals import post_save
from django.db import models
from django.dispatch import receiver
from django.conf import settings

import moviepy.editor as mp
import os

from apps.comment.models import Comment
from apps.like.models import Like


def upload_video(instance, filename):
    return 'videos/{}/{}'.format(instance.author.username, filename)


class Video(models.Model):
    video = models.FileField(upload_to=upload_video)
    thumbnail = models.ImageField(blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    author = models.ForeignKey(User, related_name='new_videos')
    created = models.DateTimeField(auto_now_add=True)

    class Meta:
        db_table = 'new_videos'
        ordering = ['-id', ]

    def __str__(self):
        return '{} {}'.format(
            self.author.profile.fullname,
            self.video.name
        )

    @property
    def get_model_name(self):
        return str(self.__class__.__name__).lower()

    @property
    def in_albums(self):
        from apps.album.models import Album
        albums = Album.objects.filter(
            video__id__in=[self.id, ]
        )
        return [a.id for a in albums]

    @property
    def likes_count(self):
        app = ContentType.objects.get_for_model(model=self.__class__)
        like = Like.objects.filter(
            content_type=app,
            object_id=self.id
        )
        return like.count()

    @property
    def comments_count(self):
        app = ContentType.objects.get_for_model(model=self.__class__)
        comment = Comment.objects.filter(
            content_type=app,
            object_id=self.id
        )
        return comment.count()


@receiver(post_save, sender=Video)
def make_thumbnail(instance, created, **kwargs):
    if created:
        filename = instance.video.name.split('/')
        filename = filename[-1].split('.')[0]
        thumb_name = '{}_{}'.format(filename, 'thumb.jpg')
        thumb_path = '{}/{}'.format(
            settings.THUMBNAIL_ROOT,
            instance.author.username
        )
        video = mp.VideoFileClip(instance.video.path)
        if not os.path.exists(thumb_path):
            os.makedirs(thumb_path)
        thumbnail = '{}/{}/{}'.format(
            settings.THUMBNAIL_ROOT,
            instance.author.username,
            thumb_name
        )
        video.save_frame(thumbnail, t=3.0)
        instance.thumbnail = thumbnail.split('media')[-1][1:]
        instance.save()
