from django.conf.urls import url

from .views import VideoDetailView, VideoUpdateView, VideoDeleteView, VideoPlayView


urlpatterns = [
    url(r"^detail/$", VideoDetailView.as_view(), name="detail"),
    url(r"^update", VideoUpdateView.as_view(), name="update"),
    url(r"^delete/(?P<video_id>\d+)/", VideoDeleteView.as_view(), name="delete"),
    url(r'^video/(?P<id>\d+)/$', VideoPlayView.as_view(), name='video_detail'),
]