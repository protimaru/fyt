from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import JsonResponse, HttpResponse
from django.shortcuts import render
from django.views.generic import View

from apps.album.models import Album
from apps.api.video.serializers import VideoListSerializer
from apps.video.models import Video


class VideoDetailView(View):
    def post(self, request):
        video_id = request.POST.get("video_id")
        if video_id:
            video = Video.objects.get(id=int(video_id))
            video = VideoListSerializer(video)
            return JsonResponse(video.data)
        else:
            return JsonResponse(
                {
                    "status": "error",
                    "message": "something went wrong!"
                }
            )


class VideoUpdateView(LoginRequiredMixin, View):

    def get(self, request):
        profile = request.user.profile
        album = []
        video_id = request.GET.get("video_id")
        if video_id:
            video = Video.objects.get(
                id=int(video_id)
            )
            for a in Album.objects.all():
                if video in a.video.all():
                    album.append(a.id)
            response = {
                "video": {
                    "id": video.id,
                    "video": video.video.url,
                    "thumbnail": video.thumbnail.url,
                    "thumbnail_width": str(video.thumbnail.width),
                    "thumbnail_height": str(video.thumbnail.height),
                    "description": video.description,
                    "created": video.created,
                },
                "album": album
            }
            return JsonResponse(response)
        return JsonResponse(
            {
                "status": "error",
                "message": "something went wrong!"
            }
        )

    def post(self, request):
        user = request.user
        profile = request.user.profile
        video_id = request.GET.get("video_id")
        video = Video.objects.get(
            id=int(video_id)
        )
        description = request.POST.get("description")
        album_id = request.POST.get("album_id")
        if description:
            video.description = description
            video.save()
        if album_id:
            album = Album.objects.get(id=int(album_id))
            album.video.add(video)
            album.save()
        return JsonResponse({"status": "ok"})


class VideoDeleteView(View):
    def get(self, request, video_id):
        if video_id:
            video = Video.objects.get(id=int(video_id))
            video.delete()
            return JsonResponse({"status": "ok"})
        else:
            return JsonResponse({"status": "err"})


class VideoPlayView(View):
    def get(self, request, id):
        try:
            video = Video.objects.get(id=int(id))
            html = """
                <!DOCTYPE html>
                <html lang="en">
                <head>
                <meta charset="UTF-8">
                <title>Document</title>
                <link rel="stylesheet" href="/static/css/video-js.css">
                    <style>
                    *, *:before, *:after {
                        box-sizing: border-box;
                    }
                    html, body {
                        width: 100%;
                        height: 100%;
                        margin: 0;
                        padding: 0;
                        overflow: hidden;
                    }
                    body .video-js {
                        width: 100%;
                        height: 100%;
                    }
                    </style>
                </head>
                <body>
                    <video id="v""" + str(video.id) + """" class="video-js vjs-big-play-centered" controls preload="auto" poster="" autoplay>
                    <source src='""" + video.video.url + """' type="video/mp4">
                    <p class="vjs-no-js">To view this video please enable JavaScript, and consider upgrading to a web browser that.
                    <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a>
                    </p>
                    </video>
                    <script src="/static/js/jquery-3.2.1.min.js"></script>
                    <script src="/static/js/video.js"></script>
                    <script>videojs("v""" + str(video.id) + """", {}, function() {});</script>
                </body>
                </html>
            """
            return HttpResponse(html, content_type='text/html')
        except Video.DoesNotExist:
            not_found = """
                <!DOCTYPE html>
                <html lang="en">
                <head>
                <meta charset="UTF-8">
                <title>Document</title>
                </head>
                <body>
                <h1>VIDEO NOT FOUND!</h1>
                </html>
                </body>
            """
            return HttpResponse(not_found, content_type='text/html')
