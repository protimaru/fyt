from django.contrib import admin

from .models import SportType

admin.site.register(SportType)
