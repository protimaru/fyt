from django.apps import AppConfig


class SporttypeConfig(AppConfig):
    name = 'sporttypes'
