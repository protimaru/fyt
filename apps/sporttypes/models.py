from django.db import models


class SportType(models.Model):
    icon = models.ImageField(upload_to='sport_types')
    name = models.CharField(max_length=255)

    class Meta:
        db_table = 'sport_types'

    def __str__(self):
        return self.name
