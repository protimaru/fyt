from django.http import JsonResponse
# from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User


@login_required
def follow(request):
    follow_user_id = request.POST.get('id')
    if follow_user_id:
        user = User.objects.get(id=follow_user_id)
        request.user.following.follow_user(user)
        response = {'status': 'ok'}
        return JsonResponse(response)
    else:
        response = {'status': 'error'}
        return JsonResponse(response)


@login_required
def unfollow(request):
    unfollow_user_id = request.POST.get('id')
    if unfollow_user_id:
        user = User.objects.get(id=unfollow_user_id)
        request.user.following.follow_user(user)
        response = {'status': 'ok'}
        return JsonResponse(response)
    else:
        response = {'status': 'error'}
        return JsonResponse(response)
