from django.conf.urls import url

from . import views

urlpatterns = [
    url('follow/$', views.follow, name='follow'),
    url('unfollow/$', views.unfollow, name='unfollow'),
]
