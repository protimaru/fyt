from django.db import models

from django.contrib.auth.models import User


class Follow(models.Model):
    user = models.OneToOneField(User, related_name='following', on_delete=models.CASCADE)
    following = models.ManyToManyField(User, blank=True)

    class Meta:
        db_table = 'following'
        ordering = ['-id', ]

    def __str__(self):
        return self.user.profile.fullname

    @property
    def all_following_users(self):
        return self.following.all()

    def follow_user(self, user):
        if isinstance(user, User):
            self.following.add(user)
        else:
            raise TypeError('An argument must be a "User" instance')

    def unfollow_user(self, user):
        if isinstance(user, User):
            self.following.remove(user)
        else:
            raise TypeError('An argument must be a "User" instance')
