from django.apps import AppConfig


class TrainingConfig(AppConfig):
    name = 'trainings'
