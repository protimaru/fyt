from django.contrib.auth.models import User
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver

from apps.arenas.models import Arena
from apps.notifications.models import Notification
from apps.sporttypes.models import SportType
from apps.days.models import Day


class Training(models.Model):
    arena = models.ForeignKey(Arena, related_name='trainings', on_delete=models.CASCADE)
    name = models.CharField(max_length=255, default='')
    days = models.ManyToManyField(Day, blank=True)
    date = models.DateField(blank=True, null=True)
    description = models.TextField(default='', blank=True, null=True)
    members = models.ManyToManyField(User, blank=True)
    types = models.ForeignKey(SportType, blank=True, null=True, on_delete=models.CASCADE)
    trainer_id = models.IntegerField(blank=True, null=True)
    trainer_name = models.CharField(max_length=255, blank=True, null=True)
    trainer_image = models.ImageField(blank=True, null=True)
    email = models.CharField(max_length=255, blank=True, null=True)
    phone = models.CharField(max_length=255, blank=True, null=True)
    trainer_description = models.TextField(blank=True, null=True)
    created_by = models.ForeignKey(User, blank=True, null=True, related_name='my_trainings', on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True)

    class Meta:
        db_table = 'trainings'

    @property
    def get_user(self):
        if self.trainer_id:
            user = User.objects.get(id=int(self.trainer_id))
            return user
        return None

    def __str__(self):
        if self.name:
            return self.name
        else:
            return 'Unnamed Training'


class TrainingTime(models.Model):
    training = models.ForeignKey(Training, related_name='time', on_delete=models.CASCADE)
    day = models.ForeignKey(Day, related_name='training_day', blank=True, null=True, on_delete=models.CASCADE)
    time = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        db_table = 'training_times'
        ordering = ['day_id']

    def __str__(self):
        return str(self.training.name) + ". " + str(self.day.name) + ": " + str(self.time)


class TrainingPrice(models.Model):
    training = models.ForeignKey(Training, related_name='price', on_delete=models.CASCADE)
    price = models.CharField(max_length=255, blank=True, null=True)
    duration = models.CharField(max_length=255, blank=True, null=True)
    currency = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        db_table = 'training_prices'
        ordering = ['price']

    def __str__(self):
        return self.training.name


class TrainingUsersTime(models.Model):
    user = models.ForeignKey(User, blank=True, null=True, related_name='training_time', on_delete=models.CASCADE)
    training = models.ForeignKey(Training, related_name='users_time', blank=True, null=True, on_delete=models.CASCADE)
    time = models.ForeignKey(TrainingTime, related_name='training_time', blank=True, on_delete=models.CASCADE)

    class Meta:
        db_table = 'training_users_times'

    def __str__(self):
        return self.training.name


class TrainingImage(models.Model):
    training = models.ForeignKey(Training, related_name='images')
    image = models.ImageField(upload_to='training_images')

    class Meta:
        db_table = 'training_images'

    def __str__(self):
        return self.training.name


class TrainingRequest(models.Model):
    choice = (
        (1, 'Accept'),
        (0, 'Reject')
    )
    training = models.ForeignKey(Training, on_delete=models.CASCADE)
    sender = models.ForeignKey(User, related_name='training_request_sent', on_delete=models.CASCADE)
    receiver = models.ForeignKey(User, related_name='training_request_received', on_delete=models.CASCADE)
    status = models.IntegerField(choices=choice, blank=True, null=True)

    class Meta:
        db_table = 'training_requests'


@receiver(post_save, sender=TrainingRequest)
def create_training_request(instance, created, **kwargs):
    if created:
        to_user = instance.receiver
        sender = instance.sender
        Notification.objects.create(user=to_user, text='invited to the trainings',
                                    sender=sender,
                                    type_not='training_request',
                                    url=to_user.profile.trainingsUrl)
