from django.conf.urls import url

from apps.trainings.views import (
    GetListTraining, MyTrainings,
    CreateTraining, delete_training
)


urlpatterns = [
    url(r'^delete/trainings/(?P<pk>\d+)/$', delete_training, name='delete.trainings'),
    url(r'^create/training/$', CreateTraining.as_view(), name='create.trainings'),
    # url(r'^get/trainings/$', GetListTraining.as_view(), name='get.trainings'),
    url(r'^(?P<username>[\w-]+)/get/trainings/$', MyTrainings.as_view(), name='get.trainings'),

]
