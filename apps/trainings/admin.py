from django.contrib import admin

from .models import Training, TrainingImage, TrainingRequest, TrainingTime, TrainingUsersTime, TrainingPrice

admin.site.register(Training)
admin.site.register(TrainingImage)
admin.site.register(TrainingRequest)
admin.site.register(TrainingTime)
admin.site.register(TrainingUsersTime)
admin.site.register(TrainingPrice)
