import json
from django.contrib.auth.models import User
from django.core.serializers import serialize
from django.http import JsonResponse, HttpResponse
from django.views.generic import CreateView, View, TemplateView

from apps.trainings.models import Training, TrainingRequest, TrainingImage, TrainingTime, TrainingPrice, TrainingUsersTime
from apps.arenas.models import Arena
from apps.utils import utils
from apps.sporttypes.models import SportType
from apps.days.models import Day
from apps.api.trainings.serializers import TrainingListSerializer


class GetListTraining(View):

    def get(self, request):
        trainings = Training.objects.all()
        response = serialize('json', trainings)
        return JsonResponse(response, safe=False)


class MyTrainings(TemplateView):
    template_name = 'user/my_trainings.html'

    def get_context_data(self, **kwargs):
        response = super(MyTrainings, self).get_context_data(**kwargs)
        response['user'] = self.request.user
        response['user_'] = User.objects.get(username__iexact=self.kwargs['username'])
        can_follow = utils.can_follow(response['user'], response['user_'])
        can_follow = can_follow if response['user'] != response['user_'] else 'myself'
        response['can_follow'] = can_follow
        response['friends_count'] = utils.friends_count(response['user'])
        return response


# TODO create TrainingRequest
class CreateTraining(CreateView):
    template_name = 'user/my_trainings.html'
    model = Training
    fields = [
        'name', 'description', 'date', 'types',
        'trainer_id', 'trainer_name', 'trainer_image', 'email',
        'phone', 'trainer_description'
    ]

    def form_valid(self, form):
        arena = Arena.objects.get(id=int(self.request.POST.get('arenas')))
        form.instance.arena = arena
        form.instance.created_by = self.request.user
        form.instance.description = self.request.POST.get('description')
        form.save()
        trainingTimes = []
        day_times = json.loads(self.request.POST.get('day_times'))
        for day, time in day_times.items():
            d = Day.objects.get(name__icontains=day)
            for i in time:
                trainingTime = TrainingTime.objects.create(training=form.instance, day=d, time=i)
                trainingTime.save()
                trainingTimes.append(trainingTime.id)
        days = json.loads(self.request.POST.get('days'))
        for day in days['days']:
            d = Day.objects.get(id=day)
            form.instance.days.add(d)
        type_ = int(self.request.POST.get('type'))
        form.instance.types = SportType.objects.get(id=type_)
        form.instance.members.add(self.request.user)
        trainer_id = self.request.POST.get('trainer_id')
        if trainer_id is not None:
            trainer = User.objects.get(id=int(trainer_id))
            form.instance.trainer_id = trainer.id
            form.instance.trainer_name = trainer.profile.fullname
            form.instance.trainer_image = trainer.profile.avatar
            form.instance.email = trainer.email
            form.instance.phone = trainer.profile.phone
            form.instance.trainer_description = self.request.POST.get('trainer_description')
            form.instance.save()
        images = self.request.POST.getlist("image")
        for image in images:
            TrainingImage.objects.create(training=form.instance, image=image)
        images = self.request.FILES.getlist("image")
        if images:
            for image in images:
                TrainingImage.objects.create(
                    training=form.instance,
                    image=image
                )
        trainingTimes = TrainingTime.objects.filter(id__in=trainingTimes)
        for time in trainingTimes:
            TrainingUsersTime.objects.create(
                training=form.instance,
                user=self.request.user,
                time=time
            )
        plan = self.request.POST.get("plan")
        plan = json.loads(plan)
        for p in plan:
            TrainingPrice.objects.create(
                training=form.instance,
                duration=p["duration"],
                price=p["price"],
                currency=p["currency"],
            )
        invited = self.request.POST.get('invited')
        if invited:
            invited = [int(x) for x in invited]
            invited_users = User.objects.filter(id__in=invited)
            for u in invited_users:
                TrainingRequest.objects.create(
                    training=form.instance,
                    sender=self.request.user,
                    receiver=u,
                )
        form.instance.save()
        response = TrainingListSerializer(form.instance)
        return JsonResponse(response.data)


def delete_training(request, pk):
    if not request.user.is_authenticated():
        return HttpResponse(status=403)
    try:
        Training.objects.get(id=int(pk)).delete()
        return JsonResponse({'message': 'Object has been deleted'})
    except Training.DoesNotExist:
        return JsonResponse({'message': 'Object not found'})
