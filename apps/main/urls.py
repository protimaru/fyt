from django.conf.urls import url

from apps.accounts.views import ProfileUpdate

from .views import menu, gallery, friends, chat


urlpatterns = [
    url(r'^u/(?P<username>[\w-]+)/search/$', friends.search_users, name='user.search'),
    url(r'^u/(?P<username>[\w-]+)/gallery/album-html/$', gallery.GalleryDetailHTMLView.as_view(), name='user.gallery.detail-html'),
    url(r'^u/(?P<username>[\w-]+)/gallery/album/$', gallery.GalleryDetailView.as_view(), name='user.gallery.detail'),

    url(r'^u/(?P<username>[\w-]+)/results/$', friends.search_users_result, name='user.search.results'),

    # Chat
    url(r'^messages_h_old/$', chat.messages_h, name='user.messages_h'),
    url(r'^messages_h/$', chat.messages_h_new, name='user.messages_h'),
    # url(r'^map/$', main_map, name='map'),

    # Menu
    url(
        r'^$',
        menu.home,
        name='user.index'
    ),
    url(
        r'^u/(?P<username>[\w-]+)/gallery/$',
        menu.GalleryView.as_view(),
        name='user.gallery.list'
    ),
    url(
        r'^u/(?P<username>[\w-]+)/settings/$',
        ProfileUpdate.as_view(),
        name='user.settings'
    ),
    url(
        r'^u/(?P<username>[\w-]+)/feed/$',
        menu.MainUserFeedView.as_view(),
        name='user.feed'
    ),
    url(
        r'^u/(?P<username>[\w-]+)/friends/$',
        menu.MainFriendsListView.as_view(),
        name='user.friends'
    ),
    url(
        r'^u/(?P<username>[\w-]+)/chat/$',
        menu.MainUserChatView.as_view(),
        name='user.chat'
    ),
    url(
        r'^u/(?P<username>[\w-]+)/map/$',
        menu.MainUserMapView.as_view(),
        name='user.map'
    ),
    url(
        r'^u/(?P<username>[\w-]+)/trainings/$',
        menu.MyTrainings.as_view(),
        name='user.trainings'
    ),
    url(
        r'^u/(?P<username>[\w-]+)/about_project/$',
        menu.about_project,
        name='user.about_project'
    ),
    url(
        r'^contact/$',
        menu.contact_us,
        name='contact'
    ),
    url(
        r'^u/(?P<username>[\w-]+)/$',
        menu.MainUserHomeView.as_view(),
        name='user.home'
    ),
]
