from django import template
from django.contrib.auth.models import User

from apps.accounts.models import Profile
from apps.followers.models import Follow
from apps.chat.models import Room
from django.db.models.query import Q
register = template.Library()


@register.simple_tag
def get_room(f_user, s_user):
    room_id = 0
    for room in Room.objects.iterator():
        if f_user and s_user in room.users.all():
            print(room)
            room_id = room.name
    return room_id


@register.simple_tag
def get_room_id(user1_id, user2_id):
    room_ = ''
    ids = [user1_id, user2_id]
    users = User.objects.filter(id__in=ids)
    for room in Room.objects.iterator():
        if set(room.users.all()) == set(users):
            room_ = room.name
            return room_
        else:
            pass
