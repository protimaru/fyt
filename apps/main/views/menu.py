from django.shortcuts import render, redirect, reverse
from django.contrib.auth.models import User
from django.http import JsonResponse
from django.core.urlresolvers import reverse
from django.views.generic import View, TemplateView, ListView
from django.views.generic.base import TemplateResponseMixin
from django.db.models import Q
from django.contrib.auth.mixins import LoginRequiredMixin

from apps.trainings.models import Training
from apps.notifications.models import Notification
from apps.chat.models import Room, Message
from apps.followers.models import Follow
from apps.utils import utils
from django.core.cache import cache


def home(request):
    if request.user.is_authenticated():
        return redirect(reverse('main:user.home',
                                kwargs={'username': request.user.username}))
    else:
        return render(request, 'index.html', {})


class MainUserHomeView(LoginRequiredMixin, View):

    template_name = 'user/home.html'

    def get(self, request, username):
        request.COOKIES['notifyCount'] = "hello"
        request.user.profile.set_messages_status(status=True)
        user = request.user
        user_ = User.objects.get(username__iexact=username)
        from apps.posts.forms import CreatePostForm
        online_users = User.objects.filter(id__in=utils.get_friends_id(user))
        context = {
            'user': user,
            'user_': user_,
            'friends_count': utils.friends_count(user_),
            'posts': user_.posts.all(),
            'form': CreatePostForm,
            'online_users': online_users,
        }
        notifications = Notification.objects.filter(
            user=request.user,
            is_read=False
        ).count()
        response = render(request, self.template_name, context)
        response.set_cookie(key='notifyCount', value=notifications)
        return response


class MainUserFeedView(LoginRequiredMixin, View):

    http_method_names = ['get']
    template_name = 'user/feed.html'

    def get(self, request, username):
        request.user.profile.set_messages_status(status=True)
        user = request.user
        user_ = User.objects.get(username__iexact=username)
        friends = utils.get_friends(user)
        feeds = utils.get_feed(user)
        online_users = User.objects.filter(id__in=utils.get_friends_id(user))
        context = {
            'user': user,
            'user_': user_,
            'friends': friends,
            'feeds': feeds,
            'friends_count': utils.friends_count(user),
            'online_users': online_users,
        }
        return render(request, self.template_name, context)


class MainUserChatView(LoginRequiredMixin, View):

    template_name = 'user/messages.html'

    def get(self, request, username, *args, **kwargs):
        request.user.profile.set_messages_status(status=False)
        room_id = cache.get("{}_room_id".format(request.user))
        if room_id:
            cache.delete("{}_room_id".format(request.user))
            room = room_id
        else:
            room = ""
        user = request.user
        user_ = User.objects.get(username__iexact=username)
        messages = Message.objects.all()
        friends = utils.get_friends(user)
        rooms = Room.objects.filter(users=request.user)
        online_users = User.objects.filter(id__in=utils.get_friends_id(user))
        context = {
            'user': user,
            'user_': user_,
            'friends': friends,
            'messages': messages,
            'friends_count': utils.friends_count(user),
            'rooms': rooms,
            'online_users': online_users,
            'room': room,
        }
        return render(request, self.template_name, context)


class MainUserMapView(LoginRequiredMixin, TemplateResponseMixin, View):
    template_name = "user/map.html"

    def get(self, request, username):
        request.user.profile.set_messages_status(status=True)
        user = request.user
        user_ = User.objects.get(username__iexact=username)
        context = {
            'user': user,
            'user_': user_,
        }
        return render(request, self.template_name, context)


class MyTrainings(ListView):
    template_name = 'user/my_trainings.html'
    context_object_name = 'trainings'

    def get_queryset(self):
        return Training.objects.filter(created_by=self.request.user)

    def get_context_data(self, **kwargs):
        response = super(MyTrainings, self).get_context_data(**kwargs)
        response['user'] = self.request.user
        response['user_'] = User.objects.get(
            username__iexact=self.kwargs['username'])
        response['friends_count'] = utils.friends_count(response['user'])
        response['requested_trainings'] = self.request.user.training_request_received.all()
        return response


class MainFriendsListView(LoginRequiredMixin, View):

    template_name = 'user/friends.html'

    def get(self, request, username):
        Notification.objects.filter(
            user=request.user,
            type_not="friend_request"
        ).delete()
        request.user.profile.set_messages_status(status=True)
        user = request.user
        user_ = User.objects.get(username__iexact=username)
        online_users = User.objects.filter(id__in=utils.get_friends_id(user))
        friends = Follow.objects.filter(
            Q(follower=user_) | Q(user=user_), is_friend=True)
        context = {
            'user': user,
            'user_': user_,
            'friends': friends,
            'online_users': online_users,
            'friends_count': utils.friends_count(user_),
        }
        return render(request, self.template_name, context)

    def post(self, request, username):
        request.user.profile.set_messages_status(status=True)
        user1 = request.POST.get("user1")
        user1 = User.objects.get(id=int(user1))
        user2 = request.POST.get("user2")
        user2 = User.objects.get(id=int(user2))
        if user1 and user2:
            room_id = Room.get_room_id(user1.id, user2.id)
            cache.set("{}_room_id".format(request.user), room_id)
            return JsonResponse({"status": reverse("main:user.chat", kwargs={"username": request.user.username})})


class GalleryView(View):
    template_name = 'user/gallery.html'

    def get(self, request, username):
        user = request.user
        user_ = User.objects.get(username__iexact=username)
        online_users = User.objects.filter(id__in=utils.get_friends_id(user))
        from itertools import chain
        from operator import attrgetter
        # files = list(
        #     chain(
        #         request.user.new_photos.all(),
        #         request.user.new_videos.all()
        #     )
        # )
        files = sorted(
            chain(
                request.user.new_photos.all(),
                request.user.new_videos.all()
            ),
            key=attrgetter('created'),
            reverse=True
        )
        context = {
            'user': user,
            'user_': user_,
            'online_users': online_users,
            'friends_count': utils.friends_count(user_),
            'albums': request.user.albums.all(),
            'files': files
        }
        return render(request, self.template_name, context)


def about_project(request, username):
    return render(request, 'about_project.html')


def contact_us(request):
    return render(request, "about_project.html")
