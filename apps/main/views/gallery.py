from django.contrib.auth.models import User
from django.http import HttpResponse
from django.shortcuts import render
from django.views.generic import View

from apps.posts.models import Album

from apps.utils import utils


class GalleryDetailView(View):
    template_name = 'user/gallery.html'

    def get(self, request, username):
        context = {
            'albums': request.user.album.all()
        }
        user = request.user
        user_ = User.objects.get(username__iexact=username)
        can_follow = utils.can_follow(user, user_)
        can_follow = can_follow if user != user_ else 'myself'
        online_users = User.objects.filter(id__in=utils.get_friends_id(user))
        context = {
            'user': user,
            'user_': user_,
            'online_users': online_users,
            'friends_count': utils.friends_count(user_),
            'can_follow': can_follow,
        }
        return render(request, self.template_name, context)


class GalleryDetailHTMLView(View):
    def post(self, request):
        album_id = request.POST.get('album_id')
        if album_id:
            album = Album.objects.get(id=int(album_id))
        html = """
        <h1>""" + album.name + """</h1>
        """
        return HttpResponse(html, content_type='text/html')
