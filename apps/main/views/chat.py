from django.contrib.auth.models import User
from django.http import JsonResponse


from apps.chat.models import Room, Message


def messages_h(request):
    sender = User.objects.get(id=request.POST.get('user'))
    receiver = User.objects.get(id=request.POST.get('user_'))
    room_id = 0
    for room in Room.objects.iterator():
        if sender in room.users.all():
            if receiver in room.users.all():
                room_id = room.id
    data = []
    m = {}
    me = []
    data.append({'sender_avatar': sender.profile.avatar.url,
                 'receiver_avatar': receiver.profile.avatar.url})
    messages = Message.objects.filter(room__id=room_id)
    if request.user == sender:
        for m in messages:
            m.read = True
            m.save()
    for message in messages:
        m = {
            'message_type': 'sent' if sender == message.sender else 'received',
            'sender': message.sender.username,
            'receiver': message.receiver.username,
            'text': message.text,
            'created': message.created
        }
        me.append(m)
    data.append(me)
    return JsonResponse(data, safe=False)


def messages_h_new(request):
    user = request.user
    # user_ = User.objects.get(username__iexact=username)
    room_id = request.POST.get('room_id')
    message_id = request.POST.get('message_id')
    messages = Message.objects.filter(room__name=room_id)

    # Mark all messages as read
    for message in messages:
        if user == message.receiver:
            message.read = True
            message.save()

    if message_id:
        messages = messages.filter(id__lt=message_id).order_by('-id')[:15]
    else:
        messages = messages.order_by('-id')[:15]
    data = []
    for message in messages:
        message_dict = {
            'message_id': message.id,
            'message_type': 'sent' if user == message.sender else 'received',
            'sender': message.sender.username,
            'sender_avatar': message.sender.profile.avatar.url,
            'receiver': message.receiver.username,
            'receiver_avatar': message.receiver.profile.avatar.url,
            'text': message.text,
            'created': message.created
        }
        data.append(message_dict)
    return JsonResponse(data, safe=False)
