from django.conf.urls.static import static
from django.contrib.auth.models import User
from django.shortcuts import render
from django.http import JsonResponse
from django.db.models import Q
from django.urls import reverse

from apps.followers.models import Follow


users_list_search = []
users_list_search_count = []


def search_users_result(request, username):
    user = User.objects.get(username__iexact=username)
    friends = Follow.objects.filter(
        Q(follower=user) | Q(user=user), is_friend=True)
    global users_list_search
    global users_list_search_count
    if not users_list_search_count:
        users_list_search_count = [0]
    if not users_list_search:
        users_list_search = [User.objects.all()]
    users = users_list_search
    context = {
        'users': users,
        'count': users_list_search_count[0],
        'friends': friends,
    }
    return render(request, 'user/results.html', context)


def search_users(request, username):
    q = request.POST.get('search')
    user = User.objects.filter(Q(username__contains=q) |
                               Q(first_name__iexact=q) |
                               Q(last_name__iexact=q)).exclude(username=request.user.username)
    preview = []
    global users_list_search
    global users_list_search_count
    users_list_search = [user, ]
    users_list_search_count = [user.count()]
    count = user.count() if user else 0
    for u in user:
        if u.profile.avatar:
            avatar = u.profile.avatar.url
        else:
            avatar = static('images/profile/no_photo_male.png')
        data = {
            'username': u.username,
            'photo': str(avatar),
            'url': str(u.get_absolute_url()),
        }
        preview.append(data)
    if preview:
        users_list = {'data': preview, 'count': count, 'success': 'ok',
                      'url': reverse('main:user.search.results',
                                     kwargs={'username': request.user.username})}
    else:
        users_list = {'success': 'false'}
    return JsonResponse(users_list, safe=False)
