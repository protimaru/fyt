from django.db import models
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError, ObjectDoesNotExist


class Team(models.Model):
    name = models.CharField(max_length=255)
    admin = models.ForeignKey(User, related_name='team_admin', blank=True)
    members = models.ManyToManyField(User, related_name='team_member', blank=True)
    moderators = models.ManyToManyField(User, related_name='team_moderator', blank=True)
    invited = models.ManyToManyField(User, related_name='team_invited', blank=True)
    requested = models.ManyToManyField(User, related_name='team_requested', blank=True)

    class Meta:
        db_table = 'teams'
        ordering = ['-id', ]

    def inviteUser(self, user):
        if isinstance(user, User):
            if user not in self.invited.all() and user not in self.members.all():
                self.invited.add(user)
        else:
            raise ValueError('Argument must be a User instance')

    def removeInvitedUser(self, user):
        if isinstance(user, User):
            if user in self.invited.all() and user not in self.members.all():
                self.invited.remove(user)
            elif user in self.members.all():
                raise ValidationError('User is already member of %s team!' % self.name)
        else:
            raise TypeError('Argument must be a User instance')

    def removeMember(self, user):
        if isinstance(user, User):
            if user in self.members.all():
                if user in self.invited.all():
                    self.invited.remove(user)
                self.members.remove(user)
            else:
                raise ObjectDoesNotExist('User does not exist in %s team' % self.name)
        else:
            raise TypeError('Argument must be a User instance')

    def addMember(self, user):
        if isinstance(user, User):
            self.members.add(user)
        else:
            raise TypeError('Argument must be a User instance')

    def __str__(self):
        return self.name
