from django.conf.urls import url

from .views import PhotoVideoUploadView, PhotoDetailView, PhotoUpdateView, PhotoDeleteView


urlpatterns = [
   url(r"^photo-video-upload/$", PhotoVideoUploadView.as_view(), name="photo-video-upload"),
   url(r"^detail/$", PhotoDetailView.as_view(), name="detail"),
   url(r"^update", PhotoUpdateView.as_view(), name="update"),
   url(r"^delete/(?P<photo_id>\d+)/$", PhotoDeleteView.as_view(), name="delete"),
]
