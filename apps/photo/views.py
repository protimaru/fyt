from itertools import chain
from operator import attrgetter
from django.shortcuts import render
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import View
from django.http import JsonResponse
from django.conf import settings

from apps.album.models import Album
from apps.api.video.serializers import VideoListSerializer
from apps.photo.models import Photo
from apps.video.models import Video

from apps.api.photo.serializers import PhotoListSerializer


class PhotoVideoUploadView(LoginRequiredMixin, View):
    def post(self, request):
        files = request.FILES.getlist("file")
        description = request.POST.get("description")
        images_id = []
        videos_id = []
        if files:
            for file in files:
                ext = str(file).split('.')[-1].upper()
                if ext in settings.IMAGE_TYPES:
                    p = Photo.objects.create(
                        image=file,
                        description=description,
                        author=request.user
                    )
                    p.save()
                    images_id.append(p.id)
                elif ext in settings.VIDEO_TYPES:
                    v = Video.objects.create(
                        video=file,
                        description=description,
                        author=request.user
                    )
                    v.save()
                    videos_id.append(v.id)
            photos = Photo.objects.filter(id__in=images_id)
            videos = Video.objects.filter(id__in=videos_id)
            # photos_serialized = PhotoListSerializer(photos, many=True).data
            # videos_serialized = VideoListSerializer(videos, many=True).data
            files_ = sorted(
                chain(
                    photos,
                    videos
                ),
                key=attrgetter('id'),
                reverse=True
            )
            ff = []
            for f in files_:
                if isinstance(f, Photo):
                    ff.append(PhotoListSerializer(f).data)
                elif isinstance(f, Video):
                    ff.append(VideoListSerializer(f).data)
            return JsonResponse(ff, safe=False)
        return JsonResponse({"status": "fail"})


class PhotoDetailView(LoginRequiredMixin, View):
    def post(self, request):
        photo_id = request.POST.get("photo_id")
        if photo_id:
            photo = Photo.objects.get(id=int(photo_id))
            photo = PhotoListSerializer(photo)
            return JsonResponse(photo.data)
        else:
            return JsonResponse(
                {
                    "status": "error",
                    "message": "something went wrong!"
                }
            )


class PhotoUpdateView(LoginRequiredMixin, View):

    def get(self, request):
        profile = request.user.profile
        album = []
        photo_id = request.GET.get("photo_id")
        if photo_id:
            photo = Photo.objects.get(
                id=int(photo_id)
            )
            for a in Album.objects.all():
                if photo in a.photo.all():
                    album.append(a.id)
            response = {
                "photo": {
                    "id": photo.id,
                    "image": photo.image.url,
                    "description": photo.description,
                    "created": photo.created,
                },
                "avatar": photo.image.name == profile.avatar.name,
                "album": album
            }
            return JsonResponse(response)
        return JsonResponse(
            {
                "status": "error",
                "message": "something went wrong!"
            }
        )

    def post(self, request):
        user = request.user
        profile = request.user.profile
        photo_id = request.GET.get("photo_id")
        photo = Photo.objects.get(
            id=int(photo_id)
        )
        set_avatar = request.POST.get("set_avatar")
        description = request.POST.get("description")
        album_id = request.POST.get("album_id")
        if set_avatar:
            profile.avatar = photo.image
            profile.save()
        if description:
            photo.description = description
            photo.save()
        if album_id:
            album = Album.objects.get(id=int(album_id))
            album.photo.add(photo)
            album.save()
        return JsonResponse({"status": "ok"})


class PhotoDeleteView(View):
    def get(self, request, photo_id):
        if photo_id:
            photo = Photo.objects.get(id=int(photo_id))
            photo.delete()
            return JsonResponse({"status": "ok"})
        else:
            return JsonResponse({"status": "err"})
