from django.contrib.auth.models import User
from django.db import models
from django.contrib.contenttypes.models import ContentType

from apps.comment.models import Comment
from apps.like.models import Like


def upload_photo(instance, filename):
    return 'images/{}/{}'.format(instance.author.username, filename)


class Photo(models.Model):
    image = models.ImageField(upload_to=upload_photo)
    description = models.TextField(blank=True, null=True)
    author = models.ForeignKey(User, related_name='new_photos')
    created = models.DateTimeField(auto_now_add=True)

    class Meta:
        db_table = 'new_photos'
        ordering = ['-id', ]

    def __str__(self):
        return '{} {}'.format(
            self.author.profile.fullname,
            self.image.name
        )

    @property
    def get_model_name(self):
        return str(self.__class__.__name__).lower()

    @property
    def is_avatar(self):
        return self.author.profile.avatar.name == self.image.name

    @property
    def in_albums(self):
        from apps.album.models import Album
        albums = Album.objects.filter(
            photo__id__in=[self.id, ]
        )
        return [a.id for a in albums]

    @property
    def likes_count(self):
        app = ContentType.objects.get_for_model(model=self.__class__)
        like = Like.objects.filter(
            content_type=app,
            object_id=self.id
        )
        return like.count()

    @property
    def comments_count(self):
        app = ContentType.objects.get_for_model(model=self.__class__)
        comment = Comment.objects.filter(
            content_type=app,
            object_id=self.id
        )
        return comment.count()
