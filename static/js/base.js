$(function() {

    // header slide down menu(profile menu);
    $("#pm-trigger").click(function(e) {
        e.preventDefault();
        e.stopPropagation();
        $($(this).data('target')).transition('slide down');
    });


    // hide on click outside the target element block;
    $(document).click(function() {
        if($('#p-menu').css('display') != 'none') {
            $('#p-menu').transition('slide down');
        }

        // notify menu
        if( $('#notify-menu').css('display') != 'none' ) {
            $('#notify-menu').transition('scale');
        }
    });

    // serch user on header menu;
    var sVal = '';
    $('#search-user').keyup(function(e) {
        console.log(123);
        console.log( $(this).val() );
        if( e.keyCode == 13 ) {
            return false;
        }
        if($(this).val() !== '') {
            console.log('call ajax');
            sVal = $(this).val();
            var url = window.location.origin + '/u/' + $('#current_user_username').val() + '/search/';
            $.ajax({
                method: 'POST',
                url: url,
                data: { search:sVal },
                success: function(response) {
                    console.log(response);
                    $('.results').children().remove();
                    $('.view-all').children().remove();
                    if(response.success === 'ok') {
                        console.log(response.data);
                        for(var i = 0; i < response.data.length; i++) {
                            $('.results')
                                .append(
                                    '<a href="' + response.data[i].url +
                                    '" class="rs-item"><img src="' + response.data[i].photo +
                                    '" title="' + response.data[i].username +
                                    '"><span>' + response.data[i].username + '</span></a>'
                                );
                        }
                        /*
                        if(response.count >= 2) {
                            $('.view-all').append('<a href="' + response.url + '" class="rs-item">View all'
                                + '<span id="va_count">(' + response.count + ')</span></a>');
                        }*/
                        $('.results').parent().fadeIn(250);
                    }
                    else {
                        $('.view-all').children().remove();
                        $('.results').children().remove();
                        $('.results').append('<div class="rs-item not-f">No results!</div>').parent().fadeIn(250);
                    }
                }
            });
        }
        else {
            $('.view-all').children().remove();
            $('.results').children().remove();
            $('.results').parent().fadeOut(250);
        }
    });

    $('#search-user').on('keydown', function(e) {
        var keyCode = e.keyCode || e.which;
        if (keyCode === 13) {
            e.preventDefault();
            return false;
        }
    })
    .siblings('button[type=submit]').click(function(e) {
        e.preventDefault();
    });
});