function initMap() {
  var latlng = $('#map').data('location'),
    title = $('#map').data('title') || 'This location',
    icon = $('#map').data('icon');

    latlng = latlng.split(',');
    latlng = {
      lat: parseFloat(latlng[0]),
      lng: parseFloat(latlng[1]),
    };

    var map = new google.maps.Map(document.getElementById('map'), {
      center: { lat: -34.397, lng: 150.644 },
      zoom: 15,
      mapTypeControl: false,
      streetViewControl: false,
      rotateControl: false
    });
    map.setCenter(latlng);

    var marker = new google.maps.Marker({
      position: latlng,
      map: map,
      title: title,
      animation: google.maps.Animation.DROP
    });

    if( icon ) {
      marker.setIcon( icon );
    }
    marker.addListener('click', function() {
      infowindow.open(map, marker);
      map.setCenter(this.getPosition());
    });

    var infowindow = new google.maps.InfoWindow({
      content: title
    });
}

var map,
  iw,
  markers = [];

function initializeMap() {
  map = new google.maps.Map(document.getElementById('ms-map'), {
    zoom: 15,
    mapTypeControl: false,
    streetViewControl: false,
    rotateControl: false
  });

  iw = new google.maps.InfoWindow();

  var pos = getCookie('location');
  if( !pos ) {
    // Try HTML5 geolocation.
    if( navigator.geolocation ) {
      navigator.geolocation.getCurrentPosition(function( position ) {
        pos = {
          lat: position.coords.latitude,
          lng: position.coords.longitude
        };
        map.setCenter( pos );
        setCookie( 'location', JSON.stringify( pos ), { path: '/' } );
      }, function( error ) {
        // on location error
        console.log('Error: The Geolocation service failed. error.code:' + error.code);
      });
    } else {
      // on location error;
      console.log('Error: Your browser doesn\'t support geolocation.');
    }
  } else {
    pos = JSON.parse(pos);
    map.setCenter( pos );
  }
}


// request training section
$(function() {

  // temp section
  $('#qweSend').click(function(e) {
    e.preventDefault();
    e.stopPropagation();

    doAjaxRequest();
  });

  // call req training have workout modal
  $('.reqtraining-have').click(function(e) {
    e.stopPropagation();
    $('.training-have').modal('show');
  });

  // req training step form actions
  $('.training-have .actions button').click(function(e) {
    e.preventDefault();
    var stepContainer = $('.stepcontent'),
      btnNext = $('#rhw-next'),
      btnPrev = $('#rhw-prev'),
      currentLayer = getLayer( $(this), 'data-current' ),
      nextLayer,
      prevLayer,
      stepCurrent;

    if( currentLayer ) {
      nextLayer = getLayer( currentLayer, 'data-next' );
      prevLayer = getLayer( currentLayer, 'data-prev' );

      if( $(this).hasClass('next') || $(this)[0] == btnNext[0] ) {
        if( nextLayer ) {
          currentLayer.removeClass('active');
          currentLayer.scrollTop(0);
          nextLayer.removeClass('mv-out');
          nextLayer.addClass('mv-in active');
          btnNext.attr('data-current', ('#' + nextLayer.attr('id')));
          btnPrev.attr('data-current', ('#' + nextLayer.attr('id')));

          // reset step item;
          stepCurrent = stepContainer.find('.step[data-ref="' + ('#' + currentLayer.attr('id')) + '"]');
          if( stepCurrent && stepCurrent.length ) {
            stepCurrent.removeClass('active');
          }

          // reinit currentLayer;
          currentLayer = nextLayer;

          // init new step item;
          stepCurrent = stepContainer.find('.step[data-ref="' + ('#' + currentLayer.attr('id')) + '"]');
          if( stepCurrent && stepCurrent.length ) {
            stepCurrent.removeClass('disabled');
            stepCurrent.addClass('active');
          }
        }
      } else if( $(this).hasClass('prev') || $(this) == btnPrev ) {
        if( prevLayer ) {
          currentLayer.removeClass('mv-in active');
          currentLayer.addClass('mv-out');
          currentLayer.scrollTop(0);
          prevLayer.addClass('active');
          btnNext.attr('data-current', ('#' + prevLayer.attr('id')));
          btnPrev.attr('data-current', ('#' + prevLayer.attr('id')));

          // reset step item;
          stepCurrent = stepContainer.find('.step[data-ref="' + ('#' + currentLayer.attr('id')) + '"]');
          if( stepCurrent && stepCurrent.length ) {
            stepCurrent.removeClass('active');
            stepCurrent.addClass('disabled');
          }

          // reinit currentLayer;
          currentLayer = prevLayer;

          // init new step item;
          stepCurrent = stepContainer.find('.step[data-ref="' + ('#' + currentLayer.attr('id')) + '"]');
          if( stepCurrent && stepCurrent.length ) {
            stepCurrent.addClass('active');
          }
        }
      }

      ( getLayer( currentLayer, 'data-next' ) ) ? btnNext.text('Next') : btnNext.text('Submit');
      ( getLayer( currentLayer, 'data-prev' ) ) ? btnPrev.addClass('active') : btnPrev.removeClass('active');

      if( btnNext.text().toLowerCase() == 'submit' ) {
        $('#qweSend').show();
      } else {
        $('#qweSend').hide();
      }
    }
  });

  var serverResponseEmulateData = {
      mon: [ '10:00 - 12:00', '13:00 - 15:00', '17:00 - 19:00'  ],
      tues: [ '09:00 - 11:00', '14:00 - 16:00', '17:00 - 19:00'  ],
      wed: [ '10:30 - 12:30', '14:00 - 16:00', '18:00 - 20:00'  ],
      thurs: [ '10:00 - 12:00', '13:00 - 15:00', '17:00 - 19:00'  ],
      fri: [ '09:00 - 11:00', '12:00 - 14:00', '16:00 - 18:00'  ],
      sat: [ '10:00 - 12:00', '14:00 - 16:00', '18:00 - 20:00'  ],
      sun: [ '13:00 - 15:00', '16:00 - 18:00'  ],
    },
    arrDayTimes = [],
    arrDayTimesDeleted = [],
    arrTimes = [];
  // create time section
  $('#myselect').dropdown({
    onAdd: function( addedValue ) {
      var targetCon = $('#timechoose-sec');
      if( arrDayTimes.indexOf( addedValue ) === -1 ) {
        // do ajax request
        arrDayTimes[addedValue] = serverResponseEmulateData[ addedValue ];
        arrDayTimesDeleted[addedValue] = serverResponseEmulateData[ addedValue ];
        var tempArr = serverResponseEmulateData[ addedValue ];
        for( var i = 0; i < tempArr.length; i++ ) {
          arrTimes.push( tempArr[i] );
        }
      } else {
        arrDayTimesDeleted[addedValue] = arrDayTimes[addedValue];
        for( var i = 0; i < arrDayTimes[addedValue].length; i++ ) {
          arrTimes.push( arrDayTimes[addedValue][i] );
        }
      }
      arrTimes = arrTimes.unique();
      arrTimes.sort();
      console.log(arrDayTimes);
      console.log(arrTimes);

      targetCon.find('.time-selects').children().remove();
      targetCon.find('.time-selects').append( createTimeBlockTemplate( arrTimes ) );
      targetCon.find('#timeslct .ui.dropdown').dropdown();
      console.log(targetCon.find('#timeslct'));
    },
    onRemove: function( removedValue ) {
      if( arrDayTimes[ removedValue ] ) {
        var targetCon = $('#timechoose-sec'),
          rmDayTimes = arrDayTimes[removedValue],
          tempArr = [];

        delete arrDayTimesDeleted[removedValue];
        for( var key in arrDayTimesDeleted ) {
          console.log( key );
          if( key !== removedValue ) {
            for( var i = 0; i < arrDayTimesDeleted[key].length; i++ ) {
              tempArr.push( arrDayTimesDeleted[key][i] );
            }
          }
        }

        tempArr = tempArr.unique();
        arrTimes = intersect( tempArr, arrTimes );
        console.log( arrTimes );
        targetCon.find('.time-selects').children().remove();
        targetCon.find('.time-selects').append( createTimeBlockTemplate( arrTimes ) );
        targetCon.find('#timeslct .ui.dropdown').dropdown();
      } else {
        console.log( 'not exist' );
      }
    }
  });

    // delete request trainings
  $('.reqtraining-delete').click(function() {
    var parent = $('.req-trainings'),
      con = $( $('.reqtraining-delete').data('target') );
    if( !$(this).hasClass('active') ) {
      $(this).addClass('active');
      if( con && con.length ) {
        // to do ajax function and on success run below code
        $(this).append('<div class="ui active dimmer"><div class="ui loader"></div></div>');
        setTimeout(function() {
          con.fadeOut(250, function() {
            $(this).remove();

            // check reqItemCount; if count equals 0; close container;
            var itemList = parent.find('.reqlist-con').children();
            if( itemList && !itemList.length ) {
              var toggleIcon = parent.find('.toggler-icon');
              toggleIcon.removeClass('active');
              toggleIcon.find('i').removeClass('up');
              toggleIcon.find('i').addClass('down');
              parent.find('.trainings-togglecon').slideToggle(250);
            }
            parent.find('.allreqtraining-count').text( itemList.length );
          });
        }, 2000);
      }
    } else {
      console.log('already activated!');
      return;
    }
  });

  function createTimeBlockTemplate( data ) {
    var str = '';
    if( data && data.length ) {
      str = '<div class="time-block" id="timeslct">' +
            '<label>Choose time</label>' +
            '<div class="time">' +
            '<select class="ui dropdown" name="td-time">' +
            '<option value="">Choose time</option>';

      for( var i = 0; len = data.length, i < len; i++ ) {
        str += '<option value="' + data[i] + '">' + data[i] + '</option>';
      }
      str += '</select></div></div>';
    }
    return str;
  }

  function doAjaxRequest() {
    var name = $('.step-form input[name="flname"]').val(),
      email = $('.step-form input[name="email"]').val(),
      phone = $('.step-form input[name="phone"]').val(),
      plan = $('.step-form input[name="plan"]').val(),
      date = $('.step-form input[name="date"]').val(),
      days = $('.step-form select[name="days"]').val(),
      time = $('.step-form select[name="td-time"]').val(),
      data = '';

    data = 'name='+name+'&email='+email+'&phone='+phone+'&plan='+plan+'&date='+date+'&days='+days+'&time='+time;
    console.log(data);
    $.ajax({
      url: window.location.href,
      type: 'POST',
      data: data,
      success: function( response ) {
        console.log( response );
      },
      error: function( err ) {
        console.log( err );
      }
    });
  }

});



// my training list section
$(function() {

  // bind popup to trainer info container on my trainings list item
  bindPopupToEl('.titem__trainer-con', {
    on: 'click',
    position: 'bottom center'
  });

    // temp delete my training
  $('body').on('click', '.delete-mytraining', function() {
    var con = $( $(this).data('target') ),
      countCon = $('.mytlistcon__hd-title span'),
      listCon = $('.mytlist-con'),
      id = $(this).data('pid');

    if( isNumeric(id) ) {
      $.ajax({
        url: '/training/delete/trainings/' + id,
        type: 'GET',
        success: function( response ) {
          if( response != 'Object not found' ) {
            if( con && con.length ) {
              con.fadeOut(250, function() {
                $(this).remove();
                countCon.text( listCon.children().length );
              });
            }
          }
        },
        error: function( error ) {
          console.log( error.text );
        }
      });
    }
  });

  // sort my trainings list by date[asc/desc]
  $('#mytlist-sortbydate').click(function() {
    var typeArr = [ 'asc', 'desc' ],
      thisBtn = $(this),
      type = thisBtn.attr('data-sort');

    if( type && typeArr.indexOf( type ) !== -1 ) {
      $.get(
        '/api/training/trainings-all/',
        { date: type },
        function( response ) {
          var myTrainingListCon = $('.mytraining-list .mytlist-con ');
          myTrainingListCon.children().remove();

          if( response instanceof Array && response.length ) {
            if( myTrainingListCon && myTrainingListCon.length ) {
              for( var i = 0; len = response.length, i < len; i++ ) {
                var template = createMyTrainingItemTemplate( response[i] );
                if( template ) {
                  myTrainingListCon.append( template );
                }
              }
            }

            bindPopupToEl('.titem__trainer-con', {
              on: 'click',
              position: 'bottom center'
            });

            bindPopupToEl('.tgridcol-content.participants', {
              on: 'click',
              position: 'bottom left'
            });

            // training attached images
            $('.tgridcol-content.images').each(function() {
              var t = $(this).siblings($(this).data('target'));
              if( t && t.length && !t.children().length ) {
                $(this).popup({
                  on: 'click',
                  position: 'bottom center',
                  title: 'Info',
                  content: 'There is no attached images for this training'
                });
              }
            });

            if( type === 'asc' ) {
              type = 'desc';
              thisBtn
                .attr('data-sort', type)
                .find('i')
                .removeClass('up')
                .addClass('down');
            } else {
              type = 'asc';
              thisBtn.attr('data-sort', type)
                .find('i')
                .removeClass('down')
                .addClass('up');
            }
          }
        }
      );
    }
  });

  // sort my trainings list by sport type;
  $('.mytlist__sort-type input[type=hidden]').change(function() {
    var typeId = $(this).val();
    if( typeId != '' ) {
      if( typeId == 'all' ) {
        typeId = ''
      } else if( isNumeric( typeId ) ) {
        typeId = +typeId;
      }
      $.get(
        '/api/training/trainings-all/',
        { q: typeId },
        function( response ) {
          console.log( response );
          var myTrainingListCon = $('.mytraining-list .mytlist-con ');
          myTrainingListCon.children().remove();
          $('.mytraining-list .mytlistcon__hd-title span').text( response.length );

          if( response instanceof Array && response.length ) {
            if( myTrainingListCon && myTrainingListCon.length ) {
              for( var i = 0; len = response.length, i < len; i++ ) {
                var template = createMyTrainingItemTemplate( response[i] );
                if( template ) {
                  myTrainingListCon.append( template );
                }
              }

              bindPopupToEl('.titem__trainer-con', {
                on: 'click',
                position: 'bottom center'
              });

              bindPopupToEl('.tgridcol-content.participants', {
                on: 'click',
                position: 'bottom left'
              });

              // training attached images
              $('.tgridcol-content.images').each(function() {
                var t = $(this).siblings($(this).data('target'));
                if( t && t.length && !t.children().length ) {
                  $(this).popup({
                    on: 'click',
                    position: 'bottom center',
                    title: 'Info',
                    content: 'There is no attached images for this training'
                  });
                }
              });
            }
          }
        }
      )
    }
  });

});



// create training section
$(function() {

  // set date picker
  var today = new Date();
  $('#ct-tstartdp').calendar({
    type: 'date',
    minDate: new Date(today.getFullYear(), today.getMonth(), today.getDate()),
  });

  // create trainings days checkbox click handling
  var ctDayTimeCon = $('.cttraining-daytime');
  ctDayTimeCon.find('input[type=checkbox]').change(function(e) {
    var timeCon = ctDayTimeCon.find('.cttraining-times'),
      elId = $(this).data('chb'),
      title = $(this).data('title');

    title = ( title ) ? title : $(this).siblings('label').text().trim();
    if( $(this).prop('checked') ) {
      if( elId ) {
        timeCon.append(
          createTimeBlckTemplate( elId, title )
        );
      }
    } else {
      if( elId ) {
        timeCon.find('div[data-id="' + elId + '"]').remove();
      }
    }
  });

  // get trainings type list
  var trainingListTypeCon = $('#cttraining-type'),
    selectedTypeName = '';
  $.get('/api/sporttype/list/types/', function( data ) {
    if( data instanceof Array && data.length ) {
      fillDDMenu( data, trainingListTypeCon.find('.menu') );

      var sortMenu = $('.mytlist__sort-type .menu');
      fillDDMenu( data, sortMenu );
      sortMenu.append(
        '<div class="item" data-value="all" style="text-align: center">' +
        '<span>All</span>' +
        '</div>'
      )
    }
  });
  trainingListTypeCon.find('input[type=hidden]').change(function() {
    if( $(this).val() ) {
      var txtCon = $(this).siblings('.text').find('span');
      if( txtCon && txtCon.length ) {
        selectedTypeName = txtCon.text().trim().toLowerCase();
      }
      errMsg( $($(this).data('msg')), 'off' );
    }
  });
  trainingListTypeCon.focus(function() {
    errMsg( $(this).parent(), 'off' );
  });
  trainingListTypeCon.focusout(function() {
    checkField( $(this).find('input[type=hidden]'), true );
  });


  // get days list
  var trainingDays = $('#cttraining-days');
  $.get('/api/days/list/days', function( data ) {
    if( data instanceof Array && data.length ) {
      var menu = trainingDays.siblings('.menu');
      for( var i = 0; len = data.length, i < len; i++ ) {
        trainingDays.append(
          '<option value="' + data[i].id + '">' + data[i].name + '</option>'
        );
        menu.append(
          '<div class="item" data-value="' + data[i].id + '">' + data[i].name + '</div>'
        );
      }
    }
  });


  // get friend list
  var ctstepFriendsListCon = $('#sb-all');
  $.get('/api/profile/friendslist/', function( data ) {
    console.log(data);
    if( data instanceof Array && data.length ) {
      var menu = ctstepFriendsListCon.find('.menu');
      if( menu && menu.length ) {
        for( var i = 0; len = data.length, i < len; i++ ) {
          var t = (data[i].first_name + ' ' + data[i].last_name).trim(),
            user = {
              id: data[i].id,
              name: t ? t : data[i].username,
              avatar: data[i].profile.avatar
            };

          menu.append(
            '<div class="item" data-value="' + user.id + '" data-pid="sb-all">' +
            '<span class="usitem-imgcon" title="' + user.name + '"><img src="' + user.avatar + '"></span>' +
            '<div class="usitem__content-hd">' + user.name + '</div>' +
            '</div>'
          );
        }
      }
    }
  });


  var trainingName = $('.ctstep-form form input[name=training-name]');
  trainingName.focus(function() {
    errMsg( $(this).parent(), 'off' );
  });
  trainingName.focusout(function() {
    checkField( $(this), true );
  });


  var trainingDesc = $('.ctstep-form form textarea[name=training-desc]');
  trainingDesc.focus(function() {
    errMsg( $(this).parent(), 'off' );
  });
  trainingDesc.focusout(function() {
    checkField( $(this), true );
  });


  // create trainings upload photo preview img
  var ctFiles = [];
  $('#ct-inputphoto').change(function(e) {
    var point = ctFiles.length,
      errors = '';

    // collect all files to array;
    for( var i = 0; len = this.files.length, i < len; i++ ) {
      ctFiles.push({
        index: generateID(),
        file: this.files[i]
      });
    }

    if( this.files.length ) {
      errMsg( $('.ctstep-layer .photo-preview').parent(), 'off' );
    }

    if( !ctFiles ) {
      errors = "File upload not supported by your browser.";
      alert(errors);
      return;
    }

    if ( ctFiles && ctFiles[0] ) {
      for( var i = point; len = ctFiles.length, i < len; i++ ) {
        var fileOb = ctFiles[i],
          type = fileOb.file.type.split('/')[0];

        if( checkFileExt( fileOb.file, type ) ) {
          setPreviewPhoto( fileOb, ctFiles, i );
        } else {
          errors = 'File not supported!';
        }
      }
    }

    if ( errors ) {
      alert(errors);
    }
  });

  $('#cttrainer-inputphoto').change(function(e) {
    var imgCon = $(this).parent(),
      file = this.files[0],
      type = file.type.split('/')[0];

    if( checkFileExt( file, type ) ) {

      errMsg( $($(this).data('msg')), 'off' );
      errMsg( $( '.ctstep-layer.active .trainerinfo-msg' ), 'off' );

      var reader = new FileReader();
      reader.onload = function(e) {
        var img = new Image();
        img.src = e.target.result;
        img.addEventListener('load', function() {
          var imgCSS = '',
            imgWidth = this.naturalWidth,
            imgHeight = this.naturalHeight;

          if( imgHeight / imgWidth > 1 ) {
            imgCSS = 'min-height: 100%; width: 100%; ';
            var computedHeight = ( imgCon.width() * imgHeight / imgWidth ),
              mTop = ( imgCon.height() - computedHeight ) / 2;
            imgCSS += ( 'margin-top: ' + mTop + 'px' );
          } else {
            imgCSS = 'height: 100%; min-width: 100%; ';
            var computedWidth = ( imgCon.height() * imgWidth / imgHeight ),
              mLeft = ( imgCon.width() - computedWidth ) / 2;
            imgCSS += ( 'margin-left: ' + mLeft + 'px' );
          }
          imgCon.find('img').remove();
          imgCon.append( img ).addClass('img');
          imgCon.find('img').attr( 'style', imgCSS ).addClass('uploaded');
        });

        var label = imgCon.find('label');
        if( !label.hasClass( 'uploaded-lb' ) ) {
          label.addClass('uploaded-lb');
        }
      };
      reader.readAsDataURL( file );
    }
  });




  // create trainings choose location step
  // handling input element 'input' event;
  var ctstepSearchLocInput = $('#ctstep-searchloc'),
    ctstepRsMenu = ctstepSearchLocInput.siblings('.rs-menu');

    /*
     ctstepSearchLocInput.keyup(function(e) {
     if( e.keyCode == 8 || e.keyCode == 46 ) {
     $(this).val('');
     }
     });
     */

  ctstepSearchLocInput[0].addEventListener('input', function(e) {
    var v = this.value.trim();
    if( v != '' ) {
      $.get(
        '/api/arena/list/arenas/',
        { name: v, type: selectedTypeName },
        function( response ) {
          if( response instanceof Array && response.length ) {
            ctstepRsMenu.children().remove();
            for( var i = 0; len = response.length, i < len; i++ ) {
              ctstepRsMenu.append(
                $('<div>', {
                  class: 'rsmenu-item',
                  'data-id': response[i].id,
                  'data-title': response[i].name,
                  'data-location': response[i].latitude + ',' + response[i].longitude,
                  click: function() {
                    ctstepSearchLocInput.val( $(this).find('.title').text() );
                    $(this).parent().fadeOut(250);

                    var location = $(this).data('location');
                    if( location ) {
                      location = location.split(',');
                      if( isNumeric( location[0] ) && isNumeric( location[1] ) ) {
                        location = {
                          lat: +location[0],
                          lng: +location[1]
                        };
                        ctstepRsMenu.siblings('input[type=hidden]').val( $(this).data('id') );
                        errMsg( $('.cttraining-locid'), 'off' );
                        console.log(location);

                        var a = getMarkerByLocation({
                          title: $(this).data('title'),
                          icon: $(this).find('img').attr('src'),
                          location: location,
                        });
                        console.log(a);
                      }
                    }
                  }
                })
                  .append(
                    '<span class="item-img" title="' + response[i].name + '"><img src="' + response[i].types[0].icon + '"></span>'
                  )
                  .append(
                    '<span class="title"><b>' + response[i].name + '</b>, ' + response[i].address + '</span>'
                  )
              ).fadeIn(250);
            }
          } else {
            ctstepRsMenu.children().remove();
            ctstepRsMenu.append('<div class="rsmenu-item">No results found!</div>').fadeIn(250);
            // ctstepRsMenu
            //     .fadeOut(250)
            //     .children().remove();
          }
        });
    } else {
      ctstepRsMenu
        .fadeOut(250)
        .children().remove();
    }
  });

  ctstepSearchLocInput.focusout(function() {
    ctstepRsMenu.fadeOut(250);
  });


  // create trainings about trainer step - search trainer
  var ctstepSearchUserInput = $('#ctstep-searchuser'),
    ctstepUserListMenu = ctstepSearchUserInput.siblings('.rs-menu');

    /*
     ctstepSearchUserInput.keyup(function(e) {
     if( e.keyCode == 8 || e.keyCode == 46 ) {
     $(this).val('');

     }
     });
     */

  ctstepSearchUserInput[0].addEventListener('input', function(e) {
    var v = this.value.trim(),
      inputHidden = $(this).siblings('input[type=hidden]');
    if( v != '' ) {
      $.get(
        '/api/profile/list/',
        { name: v },
        function( response ) {
          if( response instanceof Array && response.length ) {
            ctstepUserListMenu.children().remove();
            for( var i = 0; len = response.length, i < len; i++ ) {
              var t = (response[i].first_name + ' ' + response[i].last_name).trim(),
                user = {
                  id: response[i].id,
                  name: ( t ? t : response[i].username ),
                  avatar: response[i].profile.avatar,
                  email: response[i].email,
                  phone: response[i].profile.phone
                };
              ctstepUserListMenu.append(
                $('<div>', {
                  class: 'rsmenu-item',
                  'data-id': user.id,
                  'data-title': user.name,
                  'data-email': user.email,
                  'data-phone': user.phone,
                  click: function() {
                    ctstepSearchUserInput.val( $(this).find('.title').text() );
                    $(this).parent()
                      .fadeOut(250)
                      .siblings('input[type=hidden]').val( $(this).data('id') );

                    console.log( $(this).parent().siblings('input[type=hidden]').val() );
                    trainerFormSet(
                      '#ct-trainerform',
                      true,
                      {
                        name: $(this).data('title'),
                        email: $(this).data('email'),
                        phone: $(this).data('phone'),
                        avatar: $(this).find('img').attr('src')
                      }
                    );
                  }
                })
                  .append(
                    '<span class="item-img circle" title="' + user.name + '"><img src="' + user.avatar + '"></span>'
                  )
                  .append(
                    '<span class="title">' + user.name + '</span>'
                  )
              ).fadeIn(250);
            }
          } else {
            // ctstepUserListMenu.fadeOut(250).children().remove();
            ctstepUserListMenu.children().remove();
            ctstepUserListMenu.append('<div class="rsmenu-item def">No results found!</div>').fadeIn(250);
            trainerFormSet(
              '#ct-trainerform',
              false
            );
            inputHidden.val('');
          }
        }
      );
    } else {
      ctstepUserListMenu.fadeOut(250).children().remove();
      trainerFormSet(
        '#ct-trainerform',
        false
      );
      inputHidden.val('');
    }
  });
  ctstepSearchUserInput.focusout(function() {
    ctstepUserListMenu.fadeOut(250);
  });
  ctstepSearchUserInput.change(function() {
    if( $(this).val() != '' ) {
      errMsg( $('.ctstep-layer.active'), 'off' );
    }
  });

  // trainer fields section for manually filling
  var trainerName = $('.contact-info input[name=trainer-name]'),
    trainerEmail = $('.contact-info input[name=trainer-email]'),
    trainerPhone = $('.contact-info input[name=trainer-phone]'),
    trainerPhoto = $('.contact-info input[name=trainer-photo]');

  trainerName.focus(function() {
    errMsg( $(this).parent(), 'off' );
  });
  trainerName.focusout(function() {
    checkField( $(this), true );
  });
  trainerName.change(function() {
    if( !$(this).val() ) {
      errMsg( $( '.ctstep-layer.active .trainerinfo-msg' ), 'off' );
    }
  });

  trainerEmail.focus(function() {
    errMsg( $(this).parent(), 'off' );
    errMsg( $( '.ctstep-layer.active .trainerinfo-msg' ), 'off' );
  });
  trainerEmail.focusout(function() {
    checkField( $(this), true );
  });
  trainerEmail.change(function() {
    if( !$(this).val() ) {
      errMsg( $( '.ctstep-layer.active .trainerinfo-msg' ), 'off' );
    }
  });

  trainerPhone.focus(function() {
    errMsg( $(this).parent(), 'off' );
    errMsg( $( '.ctstep-layer.active .trainerinfo-msg' ), 'off' );
  });
  trainerPhone.focusout(function() {
    checkField( $(this), true );
  });
  trainerPhone.change(function() {
    if( !$(this).val() ) {
      errMsg( $( '.ctstep-layer.active .trainerinfo-msg' ), 'off' );
    }
  });


  // create trainings add tariff plan trigger
  $('.addtrigger > a').click(function(e) {
    e.preventDefault();
    e.stopPropagation();

    var parentCon = $(this).parent().parent();
    if( parentCon && parentCon.length ) {
      var index = parentCon.find('.row').length + 1;
      if( index < 10 ) {
        console.log(index);
        var str = '<div class="row tplan" data-index="' + index + '">' +
          '<div class="col col04 pdr10 field-blck">' +
          '<label>Duration*</label>' +
          '<div class="col-wrapper">' +
          '<div class="input-gr">' +
          '<input type="text" class="ctfield" name="training-planduration' + index + '" data-title="training-planduration" placeholder="2" required>' +
          '</div>' +
          '<div class="input-gr">' +
          '<select class="ui dropdown ctfield" name="measure' + index + '" data-title="measure">' +
          '<option value="">time</option>' +
          '<option value="hour">hour</option>' +
          '<option value="day">day</option>' +
          '<option value="week">week</option>' +
          '<option value="month">month</option>' +
          '</select>' +
          '</div>' +
          '</div>' +
          '</div>' +
          '<div class="col col06 pdl10 field-blck">' +
          '<div class="col-wrapper">' +
          '<div class="input-gr">' +
          '<label>Price*</label>' +
          '<input type="text" class="ctfield" name="training-planprice' + index + '" data-title="training-planprice" placeholder="Tariff plan price" required>' +
          '</div>' +
          '<div class="input-gr">' +
          '<label>Currency</label>' +
          '<select class="ui dropdown ctfield" data-title="currency" name="currency' + index + '">' +
          '<option value="">Choose currency</option>' +
          '<option value="aed">AED</option>' +
          '<option value="usd">USD</option>' +
          '<option value="rub">RUB</option>' +
          '<option value="eur">EUR</option>' +
          '</select>' +
          '</div>' +
          '</div>' +
          '</div>' +
          '</div>';
        $(str).insertAfter( parentCon.find('.row').eq(index - 2) );
        parentCon.find('.row').eq(index - 1).find('select').dropdown();
      } else {
        $(this).parent().remove();
      }
    }
  });


  // create trainings invite friend selection
  $('body').on('click', '.selection-block .menu .item', function() {
    $(this).toggleClass('active');
    var parentCon = $( '#' + $(this).data('pid') ),
      input = '';

    if( parentCon && parentCon.length ) {
      input = parentCon.find('input[type=hidden]');
    }

    var val = $(this).data('value');
    if( $(this).hasClass('active') ) {
      if (isNumeric(val)) {
        val = +val;
        var symbol = input.val() ? ',' : '';
        input.val(input.val() + symbol + val);
      }
    } else {
      var arr = input.val().split(',');
      if( isNumeric( val ) ) {
        var index = arr.indexOf( val.toString() );
        if( index !== -1 ) {
          arr.splice( index, 1 );
        }
        input.val( arr.join(',') );
      }
    }
  });

  $('.slctblock-delimiter .btn-gr .ui.button').click(function(e) {
    e.preventDefault();
    var action = $(this).data('action'),
      fullList = $('.selection-block.all'),
      selectedList = $('.selection-block.selected');

    if( fullList && fullList.length
      && selectedList && selectedList.length ) {
      switch ( action ) {
        case 'all-left':
          fullList.find('input[type=hidden]').val('');
          selectedList.find('input[type=hidden]').val('');
          moveAllItemsTo( fullList.find('.item'), selectedList.find('.menu') )
          break;
        case 'sclt-left':
          var itemList = getSelectedItemsFromInput( fullList.find('input[type=hidden]') );
          moveItemsTo( fullList.find('.item'), selectedList.find('.menu'), itemList )
          fullList.find('input[type=hidden]').val('');
          break;
        case 'sclt-right':
          var itemList = getSelectedItemsFromInput( selectedList.find('input[type=hidden]') );
          moveItemsTo( selectedList.find('.item'), fullList.find('.menu'), itemList );
          selectedList.find('input[type=hidden]').val('');
          break;
        case 'all-right':
          selectedList.find('input[type=hidden]').val('');
          fullList.find('input[type=hidden]').val('');
          moveAllItemsTo( selectedList.find('.item'), fullList.find('.menu') );
          break;
      }
    }
  });


  // create trainings multi-step form
  $('#ct-togglecon .ctbtn-area .ui.button').click(function(e) {
    e.preventDefault();

    // send ajax request
    if( $(this).attr('type') == 'submit' ) {
      var formData = new FormData(),
        formCon = $('#ct-togglecon form'),
        t = [];

      // collect all files from input[type=file] (trainings 'add photo' section)
      for( var i = 0; i < ctFiles.length; i++ ) {
        if( ctFiles[i] && ctFiles[i].file ) {
          formData.append('image', ctFiles[i].file);
        }
      }

      var t = formCon.serializeArray(),
        needlElNames = {
          'training-name': 'name',
          'training-type': 'type',
          'training-desc': 'description',
          'cttraining-locid': 'arenas',
          'date': 'date',
          'cttraining-userid': 'trainer_id',
          'trainer-email': 'email',
          'trainer-phone': 'phone',
          'trainer-name': 'trainer_name',
          'trainer-about': 'trainer_description',
          'invslcted': 'invited'
        },
        k = Object.keys( needlElNames );

      if( t.length ) {
        for( var i = 0; len = t.length, i < len; i++ ) {
          if( !isEmpty(t[i]) && k.indexOf( t[i].name ) !== -1 ) {
            switch( t[i].name ) {
              case 'date':
                var d = new Date();
                if( t[i].value ) {
                  d.setTime( Date.parse( t[i].value ) );
                  t[i].value = d.getFullYear() + '-' + (d.getMonth() + 1) + '-' + d.getDate();
                  formData.append( needlElNames[ t[i].name ], t[i].value );
                }
                break;
              case 'invslcted':
                var items = formCon
                  .find('input[name=invslcted]')
                  .siblings('.menu').find('.item');
                if( items && items.length ) {
                  for( var q = 0; q < items.length; q++ ) {
                    formData.append( needlElNames[ t[i].name ], +items.eq(q).data('value') );
                  }
                }
                break;
              case 'cttraining-userid':
                if( t[i].value ) {
                  formData.append( needlElNames[ t[i].name ], t[i].value );
                } else {
                  var imgInput = $('#cttrainer-inputphoto');
                  if( imgInput[0].files.length ) {
                    var file = imgInput[0].files[0];
                    formData.append('trainer_image', file );
                  }
                }
                break;
              default:
                formData.append( needlElNames[ t[i].name ], t[i].value );
            }
          }
        }

        // append trainings days to formData;
        var chbList = formCon.find('.days-chb');
        if( chbList && chbList.length ) {
          var ob = {
            'days': []
          };
          for( var i = 0; len = chbList.length, i < len; i++ ) {
            if( chbList.eq(i).prop('checked') ) {
              var id = chbList.eq(i).data('chb');
              if( isNumeric( id ) ) {
                ob['days'].push( +id );
              }
            }
          }
          if( ob['days'].length ) {
            formData.append('days', JSON.stringify(ob));
          }
        }

        // append trainings times to formData;
        var timeList = formCon.find('.tm-list');
        if( timeList && timeList.length ) {
          var tOb = {};
          for( var i = 0; len = timeList.length, i < len; i++ ) {
            var dayTitle = timeList.eq(i).data('title');
            if( dayTitle ) {
              dayTitle = dayTitle;
              tOb[dayTitle] = [];

              var str = '',
                tItem = timeList.eq(i).find('.tm-item');

              if( tItem && tItem.length ) {
                for( var j = 0; j < tItem.length; j++ ) {
                  var start = tItem.eq(j).find('.start'),
                    end = tItem.eq(j).find('.end');
                  // prefix = ( j > 0 ) ? '-' : '';
                  if( start.val() && end.val() ) {
                    // str += prefix + start.val() + '_' + end.val();
                    tOb[dayTitle].push( start.val() + '-' + end.val() );
                  }
                }
              }
              // if( str ) {
              //     formData.append( dayTitle, str );
              // }
              // str = '';
            }
          }

          // delete empty property;
          for( k in tOb ) {
            console.log(tOb[k]);
            if( !tOb[k].length ) {
              delete tOb[k];
            }
          }

          if( !isEmpty(tOb) ) {
            formData.append( 'day_times', JSON.stringify(tOb) );
          }
        }


        var tplanRows = formCon.find('.row.tplan'),
          arrTplan = [];
        if( tplanRows && tplanRows.length ) {
          for( var i = 0; len = tplanRows.length, i < len; i++ ) {
            var f = tplanRows.eq(i).find(':input'),
              arr = [];
            if( f && f.length ) {
              arr = $.map(f, function(v) {
                var str = '';
                switch(v.getAttribute('data-title')) {
                  case 'training-planduration': str = 'duration';
                    break;
                  case 'measure': str = 'duration_measure';
                    break;
                  case 'training-planprice': str = 'price';
                    break;
                  case 'currency': str = 'currency';
                    break;
                }
                if( str && v.value ) {
                  return str + ' ' + v.value
                }
              });

              var ob = {};
              if( arr.length == f.length ) {
                for( var q = 0; len = arr.length, q < len; q++ ) {
                  var item = arr[q].split(' ');
                  if( q == 0 ) {
                    ob[item[0]] = item[1] + ' ' + arr[q + 1].split(' ')[1];
                  } else if( q == 1 ) {
                    continue;
                  } else {
                    ob[item[0]] = item[1];
                  }
                }
                // arr.forEach(function(item) {
                //     item = item.split(' ');
                //     ob[item[0]] = item[1];
                // });
              }

              if( !isEmpty(ob) ) {
                arrTplan.push(ob);
              }
            }
          }
        }
        console.log(arrTplan);
        if( !isEmpty(arrTplan) ) {
          formData.append('plan', JSON.stringify(arrTplan));
        }
      }

      // loader activate;
      var ctCon = $('#ct-togglecon');
      ctCon.prepend(
        '<div class="ui active dimmer" id="ct-loader">' +
        '<div class="ui big text loader">Loading</div>' +
        '</div>'
      );

      formData.append( 'csrfmiddlewaretoken', getCookie('csrftoken') );
      $.ajax({
        url: '/training/create/training/',
        type: 'POST',
        data: formData,
        cache: false,
        processData: false,
        contentType: false,
        success: function( response ) {
          console.log( response );
          // call function to create my training list item template
          if( response ) {

            var mytrList = $('.mytraining-list .mytlist-con');
            mytrList.append(
              createMyTrainingItemTemplate( response )
            );

            var counterTxt = $('.mytlistcon__hd-title span');
            if( isNumeric( counterTxt.text() ) ) {
              counterTxt.text( +counterTxt.text() + 1 );
            }

            $('html, body').animate({
              scrollTop: mytrList.children().eq( mytrList.children().length - 1).offset().top
            }, 1500);

            bindPopupToEl('.titem__trainer-con', {
              on: 'click',
              position: 'bottom center'
            });

            bindPopupToEl('.tgridcol-content.participants', {
              on: 'click',
              position: 'bottom left'
            });

            // remove loader
            ctCon.find('#ct-loader').remove();
            resetCtForm( ctCon.find('form') );
          }
          // call function to create my trainings list item template
        },
        error: function( err ) {
          console.log( err.text );
        }
      });
    }

    var parentCon = $('#ct-togglecon .form-wrapper'),
      indicator = parentCon.find('.progressbar'),
      btnPrev = parentCon.find('#ctbtn-prev'),
      btnNext = parentCon.find('#ctbtn-next'),
      currentLayer = getLayer( $(this), 'data-current' ),
      nextLayer,
      prevLayer;

    if( currentLayer ) {

      nextLayer = getLayer( currentLayer, 'data-next' );
      prevLayer = getLayer( currentLayer, 'data-prev' );

      if( $(this)[0] == btnNext[0] ) {
        if( nextLayer ) {

          var number = currentLayer.data('number');
          switch( number ) {
            case 1:
                var inputs = currentLayer.find(':input').not('button, input[type=button]'),
                  counter = 0;
                for( var i = 0; len = inputs.length, i < len; i++ ) {
                  if( !checkField( inputs.eq(i), true ) ) {
                    counter++;
                  }
                }
                console.log(counter);
                if( counter ) {
                  return false;
                }
              break;
            case 2:
                if( !ctFiles.length ) {
                  errMsg( currentLayer.find('.photo-preview').parent(), 'on', 'Please, select at least 1 photo!', 'prepend' );
                  return false;
                } else {
                  errMsg( currentLayer.find('.photo-preview').parent(), 'off' );
                }
              break;
            case 3:
                var input = currentLayer.find('input[name=cttraining-locid]');
                if( input.val() == '' || !isNumeric( input.val() ) ) {
                  errMsg( $(input.data('msg')), 'on', 'Please, select location by entering name in search field or add new location!' );
                  return false;
                }
              break;
            case 4:
                var datepicker = currentLayer.find('input[name=date]'),
                  dayList = currentLayer.find('input[type=checkbox]'),
                  timeItems = currentLayer.find('.tm-item'),
                  counter = 0;

                if( datepicker.val() == '' ) {
                  counter++;
                  errMsg( $('#ct-tstartdp'), 'on', 'This field is required!' );
                } else {
                  errMsg( $('#ct-tstartdp'), 'off' );
                }

                if( !checkFieldGroup( dayList, { state: true, con: $('.cttraining-daytime .cttraining-days'), addType: 'prepend' } ) ) {
                  counter++;
                } else {
                  errMsg( $('.cttraining-daytime .cttraining-days'), 'off' );
                }
                console.log( timeItems );
                for( var i = 0; len = timeItems.length, i < len; i++ ) {
                  var tstart = timeItems.eq(i).find('.start'),
                    tend = timeItems.eq(i).find('.end');

                    if( tstart.val() == '' || tend.val() == '' ) {
                      errMsg( $(tstart.data('msg')), 'on', 'This fields are required!' );
                      counter++;
                    }
                }

                if( counter ) {
                  return false;
                } else {
                  currentLayer.find('.err-msg').remove();
                }
              break;
            case 5:
                var item = currentLayer.find('.field-blck'),
                  counter = 0;
                console.log( item );
                for( var i = 0; len = item.length, i < len; i++ ) {
                  var inputs = item.eq(i).find(':input');
                  console.log(inputs);
                  if( inputs.eq(0).val() == '' || inputs.eq(1).val() == '' ) {
                    errMsg( item.eq(i), 'on', 'This fields are required!' );
                    counter++;
                  } else {
                    errMsg( item.eq(i), 'off' );
                  }
                }

                if( counter ) {
                  return false;
                }
                currentLayer.find('.err-msg').remove();

              break;
            case 6:
                var userId = $('#ctstep-searchuser'),
                  otherInputs = currentLayer.find('.manual-info'),
                  counter = 0;

                if( !userId.val() ) {
                  for( var i = 0; len = otherInputs.length, i < len; i++ ) {
                    if( !checkField( otherInputs.eq(i), true ) ) {
                      counter++;
                    } else {
                      errMsg( otherInputs.eq(i).parent(), 'off' );
                    }
                  }
                }

                if( counter ) {
                  errMsg( currentLayer.find('.trainerinfo-msg'), 'on', 'Please, select trainer by entering in search field or fill all fields below manually!', 'prepend' );
                  return false;
                }
                currentLayer.find('.err-msg').remove();
              break;
          }


          currentLayer.removeClass('active');
          nextLayer.addClass('active');

          btnNext.attr('data-current', ('#' + nextLayer.attr('id')));
          btnPrev.attr('data-current', ('#' + nextLayer.attr('id')));

          // reinit currentLayer;
          currentLayer = nextLayer;
          if( currentLayer.find('#ms-map') ) {
            google.maps.event.trigger(map, 'resize');
          }

          var index = parentCon.find('.ctstep-layer').index( currentLayer );
          indicator.find('li').eq(index).addClass('active');
        }
      } else {
        if( prevLayer ) {
          currentLayer.removeClass('active');
          prevLayer.addClass('active');

          btnNext.attr('data-current', ('#' + prevLayer.attr('id')));
          btnPrev.attr('data-current', ('#' + prevLayer.attr('id')));

          // reinit currentLayer;
          currentLayer = prevLayer;
          var index = parentCon.find('.ctstep-layer').index( currentLayer );
          indicator.find('li').eq(index+1).removeClass('active');
        }
      }

      getLayer( currentLayer, 'data-next' ) ? btnNext.text('Next').attr('type', 'button') : btnNext.text('Submit').attr('type', 'submit');
      getLayer( currentLayer, 'data-prev' ) ? btnPrev.addClass('active') : btnPrev.removeClass('active');
    }
  });

  function resetCtForm( selector ) {
    var formCon = $(selector);
    formCon.find('.ctstep-layer.active').removeClass('active');
    formCon.find('.ctstep-layer').eq(0).addClass('active');

    var fieldList = formCon.find(':input').not('button, input[type=submit], input[type=reset]');

    for( var i = 0; len = fieldList.length, i < len; i++ ) {
      var tagName = fieldList[i].tagName.toLowerCase(),
        type = ( tagName == 'input' ) ? fieldList[i].getAttribute('type') : '';

      if( tagName == 'select' ) {
        fieldList.eq(i).val('');
      }

      if( type == 'checkbox' || type == 'radio' ) {
        fieldList.eq(i).prop('checked', false);
      } else {
        fieldList.eq(i).val('');
      }
    }

    // reset dropdowns
    formCon.find('.ui.dropdown').dropdown('clear');

    // reset training preview img
    formCon.find('.photo-preview .photo__preview-item.img').remove();

    // reset user marker
    if( marker ) {
      marker.setMap( null );
      marker = null;
    }

    // reset datepicker
    formCon.find('#ct-tstartdp').calendar('clear');

    // reset day time blocks
    formCon.find('.day-sect').remove();

    // reset tariff plan blocks;
    var row = formCon.find('.row.tplan');
    if( row.length > 1 ) {
      var rm = row.filter(function(i) {
        return i > 0;
      });
      rm.remove();
    }


    // reset trainer photo
    trainerFormSet( '#ct-trainerform', false );


    // reset invited user list
    var list = formCon.find('#sb-selected .menu .item'),
      targetCon = formCon.find('#sb-all .menu');
    for( var i = 0; len = list.length, i < len; i++ ) {
      list.eq(i).attr('data-pid', 'sb-all');
      targetCon.append( list.eq(i) );
    }

    // reset step btns
    formCon.find('#ctbtn-next')
      .attr('type', 'button')
      .attr('data-current', '#ctstep-con1')
      .text('Next');
    formCon.find('#ctbtn-prev')
      .removeClass('active')
      .attr('data-current', '#ctstep-con1');

    // reset indicator
    var li = formCon.find('.progressbar li');
    li.removeClass('active');
    li.eq(0).addClass('active');

    // reset center map
    var loc = getCookie('location');
    if( !loc ) {
      initializeMap();
    } else {
      loc = JSON.parse(loc);
      map.setCenter(loc);
    }
  }

  var marker;

});


// all
$(function() {

  // init dropdowns on create trainings form
  $('.ui.dropdown').dropdown();

  // toggle containers
  $('.trainingcon .toggler').click(function(e) {
    e.preventDefault();
    e.stopPropagation();

    var targetCon = $( $(this).data('target') );
    targetCon.slideToggle(250);
    var toggleIcon = $(this).find('.toggler-icon');
    if( toggleIcon.hasClass('active') ) {
      toggleIcon.removeClass('active');
      toggleIcon.find('i').removeClass('up');
      toggleIcon.find('i').addClass('down');
    } else {
      toggleIcon.addClass('active');
      toggleIcon.find('i').addClass('up');
      toggleIcon.find('i').removeClass('down');

      // do ajax function to change state requested trainings and run code below;
      $(this).find('.newreqtraining-count').fadeOut(0).text('');
    }
  });

  // bind popup to trainings participants info container on my trainings list item and req. trainings item
  bindPopupToEl('.tgridcol-content.participants', {
    on: 'click',
    position: 'bottom left'
  });

    // training days modal
  $('body').on('click', '.tgridcol-content.daytime', function() {
    var modalContent = $(this).siblings('.training-daytime');
    console.log(modalContent);
    if( modalContent && modalContent.length ) {
      var trainingModal = $('.training').eq(0);
      if( trainingModal && trainingModal.length ) {
        trainingModal.find('.header').text( modalContent.data('title') );
        trainingModal.find('.content').html( modalContent.html() );
        trainingModal.modal('show');
      }
    }
  });

  // training price modall
  $('body').on('click', '.tgridcol-content.price', function() {
    var modalContent = $(this).siblings('.training-price');
    console.log(modalContent);
    if( modalContent && modalContent.length ) {
      var trainingModal = $('.training').eq(0);
      if( trainingModal && trainingModal.length ) {
        trainingModal.find('.header').text( modalContent.data('title') );
        trainingModal.find('.content').html( modalContent.html() );
        trainingModal.modal('show');
      }
    }
  });

  // training map modal
  $('body').on('click', '.tgridcol-content.address', function() {
    var trainingModal = $('.training-location').eq(0);
    if( trainingModal && trainingModal.length ) {
      var location = $(this).data('location'),
        locName = $(this).find('.loc-name').text(),
        locIcon = $(this).data('icon');
      if( location ) {
        trainingModal.find('#map').attr('data-location', location);
        trainingModal.find('#map').attr('data-title', locName);
        trainingModal.find('#map').attr('data-icon', locIcon);

        trainingModal.modal('show');
        initMap();
      }
    }
  });

  // trainings attached images slider modal
  $('.tgridcol-content.images').each(function() {
    var t = $(this).siblings($(this).data('target'));
    if( t && t.length && !t.children().length ) {
      $(this).popup({
        on: 'click',
        position: 'bottom center',
        title: 'Info',
        content: 'There is no attached images for this training'
      });
    }
  });

  $('body').on('click', '.tgridcol-content.images', function() {
    var sliderPro,
      target = $(this).siblings($(this).data('target'));

    if( target && target.length ) {
      var modalContainer = $('.ui.basic.img-reviewer'),
        p = target.children().eq(0);
      modalContainer.find('.content').children().remove();
      modalContainer.find('.content').append( p.parent().html() );

      if( p && p.length ) {
        var q = modalContainer.find('#' + p.attr('id'));
        q.attr('id', q.attr('id') + 1);

        if( p.data('action') == 'sl-prew' ) {
          q.css('visibility', 'hidden');
          modalContainer.find('.content').append(
            '<div class="ui active dimmer">' +
            '<div class="ui text loader">Loading</div>' +
            '</div>'
          );

          setTimeout(function() {
            modalContainer.find('.ui.active.dimmer').remove();
            q.css('visibility', 'visible');
            q.sliderPro({
              width: '100%',
              height: 600,
              arrows: true,
              buttons: false,
              fullScreen: true,
              thumbnailsPosition: 'right',
              thumbnailWidth: 160,
              thumbnailHeight: 120,
              thumbnailPointer: true
            });
            sliderPro = $('#' + q.attr('id')).data('sliderPro');
            modalContainer
              .find('.sp-full-screen-button.sp-fade-full-screen')
              .append('<img src="/static/images/icon/expand.png">');
          }, 1200);
        }

        // show modal
        modalContainer.modal({
          onHidden: function() {
            if( sliderPro ) {
              sliderPro.destroy();
            }
          }
        }).modal('show');
      }
    }
  });

});



// add new location modal event handling
$(function() {

  var map,
    marker;
  function addNewLocMap() {
    map = new google.maps.Map(( document.getElementById('addloc-map') ), {
      center: { lat: -34.397, lng: 150.644 },
      zoom: 15,
      mapTypeControl: false,
      streetViewControl: false,
      rotateControl: false
    });

    var pos = getCookie('location');
    if( !pos ) {
      // Try HTML5 geolocation.
      if( navigator.geolocation ) {
        navigator.geolocation.getCurrentPosition(function( position ) {
          pos = {
            lat: position.coords.latitude,
            lng: position.coords.longitude
          };
          setCookie( 'location', JSON.stringify( pos ), { path: '/' } );
          map.setCenter( pos );
        }, function( error ) {
          // on location error
          console.log('Error: The Geolocation service failed. error.code:' + error.code);
        });
      } else {
        // on location error;
        console.log('Error: Your browser doesn\'t support geolocation.');
      }
    } else {
      pos = JSON.parse( pos );
      map.setCenter( pos );
    }
  }

  function setMapByAddress( address, map ) {
    if( address && map ) {
      var geocoder = new google.maps.Geocoder;
      geocoder.geocode({'address': address}, function(results, status) {
        if (status === 'OK') {
          map.setCenter( results[0].geometry.location );
          map.setZoom(14);
          if( marker ) {
            marker.setMap( null );
            marker = null;
          }
        } else {
          console.log('Geocode was not successful for the following reason: ' + status);
        }
      });
    }
  }

  // add new location on creating trainings
  $('#addloc-trigger').click(function(e) {
    e.preventDefault();
    var addLocModal = $('.ui.modal.new-loc');
    addLocModal
      .modal({
        onShow: function() {
          var header = $('#main > header');
          if( header && header.length ) {
            var m = $(this).css('margin-top');
            m = parseInt(m);
            if( isNumeric(m) ) {
              m = +m + header.height() / 2;
              $(this).css('margin-top', m + 'px');
            }
          }
        },
        onVisible: function() {
          addNewLocMap();

          // get trainings sport type list
          $.get('/api/sporttype/list/types/', function( response ) {
            if( response instanceof Array && response.length ) {
              var typeList = $('.cttraining-addlocation form .typelist');
              if( typeList && typeList.length ) {
                typeList.children().remove();
                for( var i = 0; len = response.length, i < len; i++ ) {
                  typeList.append(
                    '<div class="typeitem">' +
                    '<input type="checkbox" id="ttype-chb' + i + '" class="newloc-ttype" name="' + response[i].name + '" data-id="' + response[i].id + '">' +
                    '<label for="ttype-chb' + i + '">' +
                    '<span class="chb">' +
                    '<span class="span-inner">' +
                    '<span class="square"></span>' +
                    '<i class="check square icon"></i>' +
                    '</span>' +
                    '</span>' +
                    '<span class="item-icon"><img src="' + response[i].icon + '" title="' + response[i].name + '"></span>' + response[i].name +
                    '</label>' +
                    '</div>'
                  );
                }
              }
            }
          });

        },
        onHide: function() {
          // reset form()
          addLocModal.find('form').clearForm();

          $('#addnewloc-country').parent().dropdown('clear');

          $('#addnewloc-city').children().remove();
          $('#addnewloc-city').parent().dropdown('clear');

          // remove preview uploaded photo
          resetUploadPreviewPhoto( '.ui.modal.new-loc .locp-pitem.img' );
          nLocFiles = [];
        },
        onApprove: function($element) {

          var formCon = $('.cttraining-addlocation form'),
            counter = 0,
            inputs = formCon.find(':input').filter('[required]');

          for( var i = 0; len = inputs.length, i < len; i++ ) {
            if( !checkField( inputs.eq(i), true ) ) {
              counter++;
            } else {
              errMsg( inputs.eq(i), 'off' );
            }
          }

          // validate checkbox list
          var c = $('.sport-type .ttouter input[type=checkbox]');
          if( !checkFieldGroup( c, { state: true, con: $('.sport-type .ttouter'), addType: 'prepend' } ) ) {
            counter++;
          } else {
            errMsg( $('.sport-type .ttouter'), 'off' );
          }

          if( !nLocFiles.length ) {
            errMsg( $('.loc-photo .pc-outer'), 'on', 'Please, select at least 1 photo!', 'prepend' );
            counter++;
          } else {
            errMsg( $('.loc-photo .pc-outer'), 'off' );
          }


          if( !counter ) {
            // set Loader;
            addLocModal.find('.content')
              .prepend(
                '<div class="ui active inverted dimmer">' +
                '<div class="ui text loader">Loading</div>' +
                '</div>'
              );

            // prepare form data to send;
            var fd = new FormData();
            formCon = $('.cttraining-addlocation form');
            if( formCon && formCon.length ) {
              var res = formCon.serializeArray(),
                names = {
                  'newloc-name': 'name',
                  'newloc-phone': 'phone',
                  'newloc-email': 'email',
                  'newloc-desc': 'description',
                  'country': 'country',
                  'city': 'city',
                  'newloc-address': 'address',
                  'ct-addnewloc': ''

                },
                k = Object.keys( names );

              if( res.length ) {
                for (var i = 0; len = res.length, i < len; i++) {
                  if (!isEmpty(res[i]) && k.indexOf(res[i].name) !== -1) {
                    switch( res[i].name ) {
                      case 'ct-addnewloc':
                        console.log(res[i].value);
                        if( res[i].value ) {
                          var val = res[i].value.split(',');
                          fd.append('latitude', val[0]);
                          fd.append('longitude', val[1]);
                        }
                        break;
                      case 'country':
                        if( res[i].value ) {
                          var val = res[i].value.split('_')[1];
                          if( val ) {
                            fd.append('country', val);
                          }
                        }
                        break;
                      case 'city':
                        console.log( 'City!' );
                        if( res[i].value ) {
                          var val = res[i].value;
                          if( isNumeric(val) ) {
                            fd.append('city', +val);
                          }
                        }
                        break;
                      case 'newloc-address':
                        if( res[i].value ) {
                          fd.append('address', res[i].value);
                        } else {
                          var val = res[i].value;
                          if( !val ) {
                            val = $('#addnewloc-country').find(':selected').text() + ', ' +
                              $('#addnewloc-city').find(':selected').text();
                          }
                          fd.append('address', val);
                        }
                        break;
                      default:
                        fd.append( names[ res[i].name ], res[i].value );
                    }
                  }
                }
              }

              // append files
              if( nLocFiles.length ) {
                for( var i = 0; len = nLocFiles.length, i < len; i++ ) {
                  fd.append( 'images', nLocFiles[i].file );
                }
              }

              // append checkbox values(trainings types)
              var chbList = formCon.find('.newloc-ttype:checked');
              if( chbList && chbList.length ) {
                for( var i = 0; i < chbList.length, i < len; i++ ) {
                  var id = chbList.eq(i).data('id');
                  if( isNumeric( id ) ) {
                    fd.append('types', +id);
                  }
                }
              }
            }

            fd.append('csrfmiddlewaretoken', getCookie('csrftoken'));
            // send form data;
            $.ajax({
              url: '/arena/create/arena/',
              type: 'POST',
              data: fd,
              cache: false,
              contentType: false,
              processData: false,
              success: function( response ) {
                console.log( response );
                addLocModal.find('.ui.dimmer').remove();
                addLocModal.modal('hide');
                if( !isEmpty( response ) ) {
                  getMarkerByLocation({
                    title: response.name,
                    icon: response.icon,
                    location: {
                      lat: +response.latitude,
                      lng: +response.longitude
                    }
                  });
                  $('.ctstep-form input[name="cttraining-locid"]').val( response.id );
                  errMsg( $('.cttraining-locid'), 'off' );

                  // reset form()
                  addLocModal.find('form').clearForm();

                  $('#addnewloc-country').parent().dropdown('clear');

                  $('#addnewloc-city').children().remove();
                  $('#addnewloc-city').parent().dropdown('clear');

                  // remove preview uploaded photo
                  resetUploadPreviewPhoto( '.ui.modal.new-loc .locp-pitem.img' );
                  nLocFiles = [];
                }
              },
              error: function( error ) {
                console.log( error );
              }
            });
          }

          return false;
        }
      })
      .modal('show');

  });

  $('#getmarker-btn').click(function(e) {
    e.preventDefault();
    var input = $(this).siblings('input[type=hidden]');

    if( marker != null ) {
      marker.setMap( null );
      marker = null;
    }

    if( marker == null ) {
      marker = new google.maps.Marker({
        title: 'Choose location',
        icon: '/static/images/map/userMarker.png',
        map: map,
        position: map.getCenter(),
        draggable: true,
        animation: google.maps.Animation.BOUNCE
      });
    }


    input.val( marker.getPosition().lat() + ',' + marker.getPosition().lng() );

    if( input.val() != '' ) {
      errMsg( $(input.data('msg')), 'off' );
    }

    marker.addListener('dragend', function() {
      input.val( marker.getPosition().lat() + ',' + marker.getPosition().lng() );

      if( input.val() != '' ) {
        errMsg( $(input.data('msg')), 'off' );
      }
    });
  });

  // get country list
  $.get( '/api/geodata/countries', function( data ) {
    if( data ) {
      for( var i = 0; len = data.length, i < len; i++ ) {
        $('#addnewloc-country').append(
          '<option value="' + data[i].sortname + '_' + data[i].id + '">' + data[i].name + '</option>'
        );
      }
    }
  });

  // change map center on select country or city;
  $('.addnewloc-select select').change(function() {
    var address = '';
    if( $(this).val() ) {
      if ($(this).data('cat') == 'country') {
        var val = $(this).val().split('_'),
          defaultCity = $(this).find('option:selected').text();
        if( val[0] ) {
          $.get(
            '/api/geodata/cities/',
            { name: val[0].toUpperCase() },
            function( response ) {
              console.log( response );
              var cities;
              if( response instanceof Array && response.length ) {
                cities = response;
              } else {
                cities = [{
                  id: defaultCity,
                  name: defaultCity
                }];
              }

              var cityList = $('#addnewloc-city');
              cityList.children().remove();
              cityList.dropdown('clear');
              if( cityList && cityList.length ) {
                cityList.append('<option value="">Choose city</option>');
                for( var i = 0; len = cities.length, i < len; i++ ) {
                  cityList.append('<option value="' + cities[i].id + '" data-title="' + cities[i].name + '">' + cities[i].name + '</option>');
                }
              }
            }
          )
        }
        address = val[1];
      } else if( $(this).data('cat') == 'city' ) {
        var countryVal = $('#addnewloc-country').val().split('_')[0],
          cityName = $(this).find('option:selected').data('title');
        if( cityName ) {
          address = ( countryVal ) ? ( countryVal + ', ' + cityName ) : cityName;
        }
      }
    }

    if( address ) {
      console.log( address );
      setMapByAddress( address, map );
    }
  });

  // add new location, location photos
  var nLocFiles = [];
  $('#ctnewloc-inputphoto').change(function() {
    var point = nLocFiles.length,
      errors = '';

    // collect all files to array;
    for( var i = 0; len = this.files.length, i < len; i++ ) {
      nLocFiles.push({
        index: generateID(),
        file: this.files[i]
      });
    }

    if( this.files.length ) {
      errMsg( $('.loc-photo .pc-outer'), 'off' );
    }

    if( !nLocFiles ) {
      errors = "File upload not supported by your browser.";
      alert(errors);
      return;
    }

    if ( nLocFiles && nLocFiles[0] ) {
      for( var i = point; len = nLocFiles.length, i < len; i++ ) {
        var fileOb = nLocFiles[i],
          type = fileOb.file.type.split('/')[0];

        if( checkFileExt( fileOb.file, type ) ) {
          addLocPreviewPhoto( fileOb, nLocFiles, $(this).siblings('.locp-pitem').not('.img').eq(0) );
        } else {
          errors = 'File not supported!';
        }
      }
    }

    if ( errors ) {
      alert(errors);
    }
  });


  var formCon = $('.cttraining-addlocation form'),
    newlocName = formCon.find('input[name=newloc-name]'),
    newlocPhone = formCon.find('input[name=newloc-phone]'),
    newlocEmail = formCon.find('input[name=newloc-email]'),
    newlocDesc = formCon.find('textarea[name=newloc-desc]'),
    newlocCountry = $('#addnewloc-country'),
    newlocCity = $('#addnewloc-city'),
    newlocAddr = $('input[name=newloc-address]');

    newlocName.focus(function() {
      errMsg( $(this).parent(), 'off' );
    });
    newlocName.focusout(function() {
      checkField( $(this), true );
    });

    newlocPhone.focus(function() {
      errMsg( $(this).parent(), 'off' );
    });
    newlocPhone.focusout(function() {
      checkField( $(this), true );
    });

    newlocEmail.focus(function() {
      errMsg( $(this).parent(), 'off' );
    });
    newlocEmail.focusout(function() {
      checkField( $(this), true );
    });

    newlocDesc.focus(function() {
      errMsg( $(this).parent(), 'off' );
    });
    newlocDesc.focusout(function() {
      checkField( $(this), true );
    });

    newlocCountry.parent().focus(function() {
      errMsg( $(this).parent(), 'off' );
    });
    newlocCountry.parent().focusout(function() {
      checkField( $(this).find('select'), true );
    });

    newlocCity.parent().focus(function() {
      errMsg( $(this).parent(), 'off' );
    });
    newlocCity.parent().focusout(function() {
      checkField( $(this).find('select'), true );
    });


    $('body').on('click', '.sport-type .ttouter input[type=checkbox]', function() {
      var c = $('.sport-type .ttouter input[type=checkbox]'),
      con = $('.sport-type .ttouter');
      if( c.filter(':checked').length ) {
        errMsg( con, 'off' );
      }
    });

});


function getMarkerByLocation( ob ) {
  var marker = {};
  if( markers && markers.length ) {
    for( var i = 0; i < markers.length; i++ ) {
      markers[i].setMap(null);
      markers[i] = null;
    }
    markers = [];
  }
  if( !isEmpty(ob) ) {
    marker = new google.maps.Marker({
      title: ob.title,
      icon: ob.icon,
      position: ob.location,
      map: map,
      animation: google.maps.Animation.DROP
    });
    map.setCenter( marker.getPosition() );

    marker.addListener('click', function() {
      iw.setContent( marker.getTitle() );
      iw.open(map, marker);
      map.setCenter( marker.getPosition() );
    });

    markers.push(marker);
  }
  return marker;
}

function fillDDMenu( data, selector ) {
  var menuCon = $(selector);
  if( ( data instanceof Array && data.length )
    && ( menuCon && menuCon.length ) ) {
    for( var i = 0; len = data.length, i < len; i++ ) {
      menuCon.append(
        '<div class="item" data-value="' + data[i].id + '">' +
        '<img class="ui mini avatar image" src="' + data[i].icon + '">' +
        '<span>' + data[i].name + '</span>' +
        '</div>'
      )
    }
  }
}

function createTimeBlckTemplate( elId, title ) {
  var template = '';

  if( elId && title ) {
    template = $('<div class="day-sect" data-id="' + elId + '">')
      .append(
        $('<div>', {
          class: 'remove',
          title: 'Remove trainings day',
          'data-pid': elId,
          click: function() {
            var pid = $(this).data('pid');
            if( pid ) {
              var parent = $(this).parent();
              if( parent.data('id') == pid ) {
                parent.fadeOut(250, function() {
                  $(this).remove();
                  $('input[data-chb=' + pid + ']').prop('checked', false);
                });
              }
            }
          }
        })
          .append('<div class="rm-inner"><i class="trash outline icon"></i></div>')
      )
      .append('<div class="day-title">' + title + '</div>')
      .append(
        $('<div class="time-blck">')
          .append(
            $('<div class="tm-list" data-pid="' + elId + '" data-title="' + title + '">')
              .append(
                timeBlckItemTemplate( elId, 1 )
              )
          )
          .append(
            $('<div class="tm-btns"></div>')
              .append(
                $('<button>', {
                  class: 'ui inverted orange button',
                  click: function(e) {
                    e.preventDefault();
                    var timeListCon = $(this).parent().siblings('.tm-list');
                    if( timeListCon && timeListCon.length ) {
                      var dayId = timeListCon.data('pid'),
                        countIndex = timeListCon.children().length;

                      countIndex = ( isNumeric( countIndex ) ) ? +countIndex + 1 : 0;
                      dayId = ( isNumeric( dayId ) ) ? +dayId : 0;

                      if( countIndex && dayId ) {
                        timeListCon.append( timeBlckItemTemplate( dayId, countIndex ) );
                      }
                    }
                  }
                })
                  .append('<i class="plus icon"></i> Add one more')
              )
          )
      )
  }

  return template;
}

function timeBlckItemTemplate( dayIndex, countIndex ) {
  return '<div class="tm-item">' +
    '<div class="ui calendar">' +
    '<div class="ui input left icon">' +
    '<i class="calendar icon"></i>' +
    '<input type="text" name="' + dayIndex + '_day-starttime' + countIndex + '" data-day="' + dayIndex + '" data-msg=".day-sect[data-id=' + dayIndex + ']" class="time-item start" placeholder="Start time">' +
    '</div>' +
    '</div>' +
    '<div class="ui calendar">' +
    '<div class="ui input left icon">' +
    '<i class="calendar icon"></i>' +
    '<input type="text" name="' + dayIndex + '_day-endtime' + countIndex + '" data-day="' + dayIndex + '" data-msg=".day-sect[data-id=' + dayIndex + ']" class="time-item end" placeholder="End time">' +
    '</div>' +
    '</div>' +
    '</div>'
}


function trainerFormSet( selector, state, data ) {
  var con = $(selector);
  if( con && con.length ) {
    state = ( typeof state == 'boolean' ) ? state : false;

    if( isEmpty( data ) ) {
      data = {
        name: '',
        email: '',
        phone: '',
        avatar: '',
      };

    }

    // form fields
    con.find('input[name="trainer-name"]')
      .val( data.name )
      .prop('disabled', state);

    con.find('input[name="trainer-email"]')
      .val( data.email )
      .prop('disabled', state);

    con.find('input[name="trainer-phone"]')
      .val( data.phone )
      .prop('disabled', state);

    con.find('#cttrainer-inputphoto').val('');

    // trainer image
    var imgCon = con.find('.trainer-photo');
    if( imgCon && imgCon.length ) {
      var label = imgCon.find('label');
      label.removeClass('uploaded-lb');
      if( data.avatar ) {
        label.css('display', 'none');
        var img = imgCon.find('img');
        if( img && img.length ) {
          img.removeClass('uploaded');
          img.attr('src', data.avatar).attr('title', data.name);
        } else {
          imgCon.append('<img src="' + data.avatar + '" title="' + data.name + '">');
          imgCon.addClass('img');
        }
      } else {
        imgCon.find('img').remove();
        label.css('display', 'block');
        imgCon.removeClass('img');
      }
    }
  }
}

function getSelectedItemsFromInput( selector ) {
  var input = $(selector),
    arr = [];
  if( input && input.length ) {
    var temp = input.val().split(',');
    for( var i = 0; len = temp.length, i < len; i++ ) {
      if( isNumeric(temp[i]) ) {
        arr.push( +temp[i] );
      }
    }
  }
  return arr;
}

function moveItemsTo( listFrom, listTo, arr ) {
  if( ( listFrom && listFrom.length )
    && ( listTo && listTo.length )
    && ( arr instanceof Array && arr.length )) {

    var pid = listTo.data('pid');
    for( var i = 0; len = listFrom.length, i < len; i++ ) {
      for( var j = 0; jlen = arr.length, j < jlen; j++ ) {
        if( arr[j] == +listFrom.eq(i).data('value') ) {
          listTo.append( listFrom.eq(i).removeClass('active').clone().attr('data-pid', pid) );
          listFrom.eq(i).remove();
        }
      }
    }
  }
}

function moveAllItemsTo( listFrom, listTo ) {
  if( ( listFrom && listFrom.length )
    && ( listTo && listTo.length )) {

    var pid = listTo.data('pid');
    for( var i = 0; len = listFrom.length, i < len; i++ ) {
      listFrom.eq(i).removeClass('active');
      listTo.append( listFrom.eq(i).clone().attr('data-pid', pid) );
      listFrom.eq(i).remove();
    }
  }
}

// read file and set to preview block;
function setPreviewPhoto( ob, fileArray, index ) {
  var reader = new FileReader(),
    previewContainer = $('.fieldset-inner .photo__preview-item').not('.img').eq(0);

  previewContainer.find('label').remove();
  previewContainer
    .append('<div class="item-overlay"></div>')
    .append(
      $('<span>', {
        class: 'rm-photo',
        title: 'remove photo',
        click: function() {
          $(this).parent().remove();
          if(fileArray.length != 0) {
            fileArray = deleteByIndexFromArray( fileArray, ob.index );
          }
        }
      }).append('<i class="remove icon"></i>')
    )
    .addClass('img');

  previewContainer.parent().append(
    '<div class="photo__preview-item">' +
    '<label for="ct-inputphoto"><i class="plus icon"></i></label>' +
    '</div>'
  );

  reader.onload = function( e ) {
    var image = new Image();
    image.src = e.target.result;
    fileArray[index]['src'] = image.src;

    image.addEventListener('load', function() {
      var imgCSS = '',
        imgWidth = this.naturalWidth,
        imgHeight = this.naturalHeight;

      if( imgHeight / imgWidth > 1 ) {
        imgCSS = 'min-height: 100%; width: 100%; ';
        var computedHeight = ( previewContainer.width() * imgHeight / imgWidth ),
          mTop = ( previewContainer.height() - computedHeight ) / 2;
        imgCSS += ( 'margin-top: ' + mTop + 'px' );
      } else {
        imgCSS = 'height: 100%; min-width: 100%; ';
        var computedWidth = ( previewContainer.height() * imgWidth / imgHeight ),
          mLeft = ( previewContainer.width() - computedWidth ) / 2;
        imgCSS += ( 'margin-left: ' + mLeft + 'px' );
      }
      previewContainer.append( image );
      previewContainer.find('img').attr( 'style', imgCSS );
    });
  };
  reader.readAsDataURL( ob.file );
}


// read file and set to preview block( add new location on create trainings )
function addLocPreviewPhoto( ob, fileArray, previewCon ) {
  var reader = new FileReader();
  if( previewCon && previewCon.length ) {

    previewCon.find('label').remove();
    previewCon
      .append('<div class="item-overlay"></div>')
      .append(
        $('<span>', {
          class: 'rm-photo',
          title: 'remove photo',
          click: function() {
            $(this).parent().remove();
            if(fileArray.length != 0) {
              fileArray = deleteByIndexFromArray( fileArray, ob.index );
            }
          }
        }).append('<i class="remove icon"></i>')
      )
      .addClass('img');

    previewCon.parent().append(
      '<div class="locp-pitem">' +
      '<label for="ctnewloc-inputphoto"><i class="plus icon"></i></label>' +
      '</div>'
    );

    reader.onload = function( e ) {
      var image = new Image();
      image.src = e.target.result;

      image.addEventListener('load', function() {
        var imgCSS = '',
          imgWidth = this.naturalWidth,
          imgHeight = this.naturalHeight;

        if( imgHeight / imgWidth > 1 ) {
          imgCSS = 'min-height: 100%; width: 100%; ';
          var computedHeight = ( previewCon.width() * imgHeight / imgWidth ),
            mTop = ( previewCon.height() - computedHeight ) / 2;
          imgCSS += ( 'margin-top: ' + mTop + 'px' );
        } else {
          imgCSS = 'height: 100%; min-width: 100%; ';
          var computedWidth = ( previewCon.height() * imgWidth / imgHeight ),
            mLeft = ( previewCon.width() - computedWidth ) / 2;
          imgCSS += ( 'margin-left: ' + mLeft + 'px' );
        }
        previewCon.append( image );
        previewCon.find('img').attr( 'style', imgCSS );
      });
    };
    reader.readAsDataURL(ob.file);
  }

}


// my trainings list item tempate
function createMyTrainingItemTemplate( data ) {
  var template = '';
  if( !isEmpty( data ) ) {
    var trainerBlock = '',
      trainerPageUrl = '',
      trainerAvatar = '',
      membersBlock = '',
      tplan = '',
      tdays = '',
      attachedImages = '';

    if( data.user ) {
      trainerPageUrl = '<div class="trainerinfo-item">' +
        '<a href="' + data.user.url + '">Visit to trainer page</a>' +
        '</div>';
      trainerAvatar = data.user.profile.avatar;
    }

    trainerBlock = '<div class="titem__trainer">' +
      '<span class="titem__trainer-title txt-orange">Trainer:</span>' +
      '<div class="titem__trainer-con">' +
      '<span class="titrainer-img"><img src="' + trainerAvatar + '" title="' + data.trainer_name + '"></span>' +
      '<span class="titem_-trainer-name">' + data.trainer_name + '</span>' +
      '</div>' +
      '<div class="trainer-info ui special popup">' +
      '<div class="trainerinfo-item">' +
      '<span>E-mail:</span>' +
      '<a href="mailto:' + data.email + '">' + data.email + '</a>' +
      '</div>' +
      '<div class="trainerinfo-item">' +
      '<span>Phone:</span>' +
      '<a href="tel:123 45 67">123 45 67</a>' +
      '</div>' +
      '<div class="trainerinfo-item">' +
      '<span>About:</span>' +
      '<div class="tabout-txt">' + customSubStr( data.trainer_description, 150 ) + '</div>' +
      '</div>' +
      trainerPageUrl +
      '</div>' +
      '</div>';

    if( data.members ) {
      var memberCount = data.members.length,
        memberList = '';

      memberList = '<div class="ui special popup user-selection">' +
        '<div class="us-wrapper">';
      for( var i = 0; i < memberCount; i++ ) {
        var name = ( data.members[i].first_name && data.members[i].last_name ) ? ( data.members[i].first_name + ' ' + data.members[i].last_name ) : data.members[i].username;
        memberList += '<a href="' + data.members[i].url + '" class="us-item">' +
          '<div class="us__item-img">' +
          '<span class="usitem-imgcon"><img src="' + data.members[i].profile.avatar + '" title="' + name + '"></span>' +
          '</div>' +
          '<div class="us__item__content">' +
          '<div class="usitem__content-hd">' + name + '</div>' +
          '</div>' +
          '</a>'
      }
      memberList += '</div></div>';

      membersBlock = '<div class="tgridcol-content participants">' +
        '<img src="/static/images/friend-marker.png" alt="Participants">' +
        '<span>Participants ' + memberCount + ' persons</span>' +
        '</div>' +
        memberList;
    } else {
      membersBlock = '<div class="tgridcol-content participants">' +
        '<img src="/static/images/friend-marker.png" alt="Participants">' +
        '<span>Participants 0 persons</span>' +
        '</div>'
    }

    if( data.price && data.price.length ) {
      tplan += '<div class="tgridcol-content price">' +
        '<img src="/static/images/notify-marker.png" alt="Price">' +
        '<span>Plan</span>' +
        '</div>' +
        '<div class="training-price" data-title="Price of trainings">' +
        '<div class="trainings-scltprice">' +
        '<div class="trainingprice-title txt-orange">My tariff:</div>' +
        '<div class="trainingprice-txt">' + data.price[0].price + ' ' + data.price[0].currency + ' / ' + data.price[0].duration + '</div>' +
        '</div>' +
        '<div class="trainings-scltprice">' +
        '<div class="trainingprice-title">Aviable:</div>' +
        '<table class="ui fixed celled table">' +
        '<thead>' +
        '<tr>' +
        '<th>Duration</th>' +
        '<th>Cost</th>' +
        '</tr>' +
        '</thead>' +
        '<tbody>';

      for( var i = 0; len = data.price.length, i < len; i++ ) {
        tplan += '<tr>' +
          '<td>' + data.price[i].duration + '</td>' +
          '<td>' + data.price[i].price + ' ' + data.price[i].currency + '</td>' +
          '</tr>';
      }
      tplan += '</tbody>' +
        '</table>' +
        '</div></div>';
    }


    if( data.days && data.days.length ) {
      tdays = '<div class="tgridcol-content daytime">' +
        '<img src="/static/images/hour-marker.png" alt="Time">' +
        '<span>Days</span>' +
        '</div>' +
        '<div class="training-daytime" data-title="Days of trainings">' +
        '<div class="trainings-slctdays">' +
        '<div class="trainingdays-title txt-orange">My days:</div>' +
        '<table class="ui fixed celled table">' +
        '<thead>' +
        '<tr>' +
        '<th>Day</th>' +
        '<th>Time</th>' +
        '</tr>' +
        '</thead>' +
        '<tbody>' +
        '<tr>' +
        '<td>' + data.days[0].name + '</td>' +
        '<td>' + data.days[0].training_day[0].time + '</td>' +
        '</tr>' +
        '</tbody>' +
        '</table>' +
        '</div>' +
        '<div class="trainings-slctdays">' +
        '<div class="trainingdays-title">Aviable:</div>' +
        '<table class="ui fixed celled table">' +
        '<thead>' +
        '<tr>' +
        '<th>Day</th>' +
        '<th>Time</th>' +
        '</tr>' +
        '</thead>' +
        '<tbody>';

      for( var i = 0; len = data.days.length, i < len; i++ ) {
        tdays += '<tr>' +
          '<td>' + data.days[i].name + '</td>';
        if( data.days[i].training_day.length ) {
          tdays += '<td>';
          for( var j = 0; jlen = data.days[i].training_day.length, j < jlen; j++ ) {
            tdays += data.days[i].training_day[j].time + '<br/>';
          }
          tdays += '</td>';
        }
        tdays += '</tr>';
      }

      tdays += '</tbody>' +
        '</table>' +
        '</div></div>';
    }

    attachedImages = '<div class="tgridcol-content images" data-target=".tattached-images">' +
      '<img src="/static/images/picture.png" alt="Attached images">' +
      '<span>See attached pictures</span>' +
      '</div>' +
      '<div class="tattached-images">';
    if( data.images && data.images.length ) {
      var img = data.images;
      if( img.length > 1 ) {
        var slides = '<div class="sp-slides">',
          thumbnails = '<div class="sp-thumbnails">';
        attachedImages += '<div class="slider-pro" data-action="sl-prew" id="mytlist-attachedImg">';

        for( var i = 0; len = img.length, i < len; i++ ) {
          slides += '<div class="sp-slide">' +
            '<img src="' + img[i].image + '" class="sp-image lid">' +
            '</div>';
          thumbnails += '<div class="sp-thumbnail">' +
            '<img src="' + img[i].image + '" class="sp-thumbnail-image lid">' +
            '</div>';
        }
        slides += '</div>';
        thumbnails += '</div>';

        attachedImages += slides + thumbnails;
        attachedImages += '</div>';
      } else {
        attachedImages += '<div class="attachedimg-present" data-action="prew" id="mytlist-attachedImg">' +
          '<img src="' + img[0].image + '">' +
          '</div>';
      }
    }

    attachedImages += '</div>';


    template += '<div class="mytlistcon-item" id="mylistid-' + data.id + '">' +
      '<div class="mytlistcon__item-hd">' +
      '<div class="titem__type">' +
      '<span class="titype-img"><img src="' + data.types.icon + '"></span>' +
      '<span class="titem__type-txt">' + data.types.name + '</span>' +
      '</div>' +
      trainerBlock +
      '</div>' +
      '<div class="mytlistcon__item-b">' +
      '<div class="titem-info">' +
      '<div class="titeminfo-left">' +
      '<div class="titem__name">' +
      '<span>Name of training</span>' +
      '<div class="titem__name-txt txt-uppercase">' + data.name + '</div>' +
      '</div>' +
      '<div class="titeminfo-grid">' +
      '<div class="titeminfo__grid-col">' +
      '<div class="tgridcol-content address" data-location="' + data.arena.latitude + ',' + data.arena.longitude + '" data-icon="' + data.types.icon + '">' +
      '<img src="/static/images/map-marker.png">' +
      '<span class="loc-name">' + data.arena.name + '</span>' +
      '</div>' +
      '</div>' +
      '<div class="titeminfo__grid-col">' +
      tplan +
      '</div>' +
      '<div class="titeminfo__grid-col">' +
      tdays +
      '</div>' +
      '<div class="titeminfo__grid-col">' +
      membersBlock +
      '</div>' +
      '<div class="titeminfo__grid-col">' +
      attachedImages +
      '</div>' +
      '</div>' +
      '</div>' +
      '<div class="titeminfo-right">' +
      '<button type="button" data-target="#mylistid-' + data.id + '" data-pid="' + data.id + '" class="ui grey button txt-uppercase delete-mytraining">Delete trainings</button>' +
      '</div>' +
      '</div>' +
      '<div class="titem-desc">' +
      '<div class="titem__desc-title">Description of training</div>' +
      '<div class="titem__desc-txt">' +
      customSubStr( data.description, 150 ) +
      '</div>' +
      '</div>' +
      '</div>' +
      '</div>';
  }

  return template;
}

function resetUploadPreviewPhoto( selector ) {
  var previewItem = $(selector);
  if( previewItem && previewItem.length ) {
    previewItem.remove();
  }
}

// returns jquery ob which selector was named on attribute 'attrName' for 'jOb';
function getLayer( jOb, attrName ) {
    var temp = jOb ? $(jOb.attr(attrName)) : null;
    return temp ? (( temp && temp.length ) ? temp : null) : null;
}