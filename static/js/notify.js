var wsNotify,
    notifyChannel = document.getElementById('profile_channel').value,
    protocol = ( window.location.protocol === 'http:' ) ? 'ws://' : 'wss://';

console.log('protocol: ' + protocol);

// set notifyCount cookie;
if( document.cookie.indexOf( 'notifyCount' ) === -1 ) {
	setCookie( 'notifyCount', 0, {path: '/'} );
	console.log(' cookie created! ');
}

// initialize websocket notify object;
wsNotify = new WebSocket(protocol + window.location.host + '/chat/' + notifyChannel);

wsNotify.onopen = function() {
	console.log('Connected to: ' + window.location.host + '/chat/' + notifyChannel);
	console.log('Watching to notification...');
};

wsNotify.onmessage = function( message ) {
	console.log( message );
	var count = getCookie( 'notifyCount' ),
		data = JSON.parse( message.data );


	if( checkWLPathName( /\/chat\/$/ig ) ) {
		console.log( 'Welcome to chat...' );
		var	userOb = $('.chat .friend__usercard.inactive[data-user-id=' + data.id + ']');

		if( userOb.length == 1 ) {
			var	uMsg = userOb.find('.unread-msgcount'),
				lastSendedMsg = userOb.find('.state-info'),
				currentUMCount = uMsg.text().trim();

			if( isNumeric( currentUMCount ) ) {
				currentUMCount = parseInt( currentUMCount );
				currentUMCount++;
				uMsg.text( currentUMCount ).show();
			} else {
				uMsg.text( 1 ).show();
			}
			if( lastSendedMsg && lastSendedMsg.length ) {
				var shortMsg = data.text.trim();
				shortMsg = ( shortMsg.length > 20 ) ? ( shortMsg.substr(0, 20) + '...' ) : shortMsg;
				lastSendedMsg.text( shortMsg );
			}
		}

		// if notify type not 'chat', [ type: follow, comment, reply, post ];
		if( data.type != undefined && data.type != 'chat' ) {
			console.log( 'notifyType: ' + 'notCHAT' );
			count = isNumeric( count ) ? parseInt( count ) : 0;
			count++;
			setCookie( 'notifyCount', count, { path: '/' } );

			$('a.notify')
				.transition('tada')
				.removeClass('notify-off')
				.addClass('notify-on')
				.find('.notify-count').text('+ ' + count);
		}
	} else {
		count = isNumeric( count ) ? parseInt( count ) : 0;
		count++;
		setCookie( 'notifyCount', count, { path: '/' } );

		$('a.notify')
			.transition('tada')
			.removeClass('notify-off')
			.addClass('notify-on')
			.find('.notify-count').text('+ ' + count);
	}
};

wsNotify.onclose = function(e) {
	console.log('Notify channel closed!');
	console.log('code: ' + e.code + ' reason: ' + e.reason);
};

wsNotify.onerror = function(e) {
	console.log('error: ' + e.message);
};


$(function() {

    // set notifyCount to Cookie variable;
    // setCookieFromDOMElementText('.notify .notify-count');

    // set notify count;
    var tempCount = getCookie( 'notifyCount' );
    if( !isNumeric( tempCount ) ) {
        tempCount = 0;
        setCookie( 'notifyCount', tempCount, {path: '/'} );
    } else {
        tempCount = parseInt( tempCount );
        if( tempCount > 0 ) {
            $('a.notify')
                .transition('tada')
                .removeClass('notify-off')
                .addClass('notify-on')
                .find('.notify-count').text('+ ' + tempCount);
        } else {
            $('a.notify')
                .find('.notify-count').text('');
        }
    }


    // notify;
    $('a.notify')
        .one('click', function() {
            $(this)
                .removeClass('notify-on')
                .addClass('notify-off')
                .find('.notify-count').text('');
            setCookie( 'notifyCount', 0, { path: '/'} );
            console.log($(this).find('.notify-count').text());
        })
        .click(function(e) {
            e.preventDefault();

            var notifyCon = $($(this).data('target')),
                newSec = notifyCon.find('.new.sec'),
                oldSec = notifyCon.find('.old.sec');

            if( notifyCon.attr('data-state') == 'off' ) {
                notifyCon.find('.nmul-inner').append(
                    '<div class="overlay"><div class="overlay-inner"><div class="ui active loader"></div></div></div>'
                );

                // get notifications
                $.ajax({
                    url: '/api/notification/list/',
                    type: 'GET',
                    // data: 'user_id='+$('#user').val(),
                    success: function( response ) {
                        notifyCon.find('.overlay');
                        if ( response && response.length ) {
                            var newSecCon = newSec.find('.content'),
                                oldSecCon = oldSec.find('.content');

                            var item;
                            newSecCon.children().remove();
                            oldSecCon.children().remove();
                            for( var i = 0; len = response.length, i < len; i++ ) {
                                console.log( response[i] );
                                if( !response[i].is_read ) {
                                    item = createNotifyMenuItem( response[i] );
                                    newSecCon.append( item );
                                } else {
                                    item = createNotifyMenuItem( response[i] );
                                    oldSecCon.append( item );
                                }
                            }

                            // set title to sections: 'New' and 'Old';
                            newSec.find('.title').remove();
                            if( newSecCon.children().length ) {
                                newSec.prepend('<div class="title">New</div>');
                            }
                            oldSec.find('.title').remove();
                            if( oldSecCon.children().length ) {
                                oldSec.prepend('<div class="title">Old</div>');
                            }
                        }

                        // remove loader;
                        notifyCon.find('.overlay').remove();
                    },
                    error: function( err ) {
                        console.log( err );
                    }
                });

                notifyCon.attr('data-state', 'on');
            } else {
                notifyCon.attr('data-state', 'off');
            }
        });

    $('a.notify').popup({
        popup: $('a.notify').data('target'),
        on: 'click',
        position: 'bottom center'
    });


    // prevent default to notification menu area on scrolling;
    var elem = $('.nm .nm-ul .nmul-inner')[0];
    if (elem.addEventListener) {
      if ('onwheel' in document) {
        // IE9+, FF17+, Ch31+
        elem.addEventListener("wheel", onWheel);
      } else if ('onmousewheel' in document) {
        // устаревший вариант события
        elem.addEventListener("mousewheel", onWheel);
      } else {
        // Firefox < 17
        elem.addEventListener("MozMousePixelScroll", onWheel);
      }
    } else { // IE8-
      elem.attachEvent("onmousewheel", onWheel);
    }

    function onWheel(e) {
      console.log(e.type);
      // wheelDelta не дает возможность узнать количество пикселей
      var delta = e.deltaY || e.detail || e.wheelDelta,
          elem = this;
      console.log(this);

      if (delta < 0 && elem.scrollTop == 0) {
        e.preventDefault();
        console.log('1) preventDefault - active');
      } else {
        console.log('1) preventDefault - NOT active');
      }

      if (delta > 0 && elem.scrollHeight - elem.clientHeight === elem.scrollTop) {
        e.preventDefault();
        console.log('(exp): preventDefault - active');
      } else {
        console.log('(exp): preventDefault - NOT active');
      }

      console.log('delta: ' + delta);
      console.log('elem.scrollTop: ' + elem.scrollTop);
      console.log('exp: ' + (elem.scrollHeight - elem.clientHeight - elem.scrollTop));
      console.log('--------------------------');
    }



    $('#ppnm .mark-asread').click(function(e) {
        e.preventDefault();
        $.ajax({
            url: '/notifications/mark-as-read/',
            type: 'POST',
            data: 'mark_all=' + true,
            success: function( response ) {
                console.log( response );
            },
            error: function( err ) {
                console.log(err);
            }
        });
    });

    // create notification menu item;
    function createNotifyMenuItem( data ) {
        var username = ( data.sender.fullname ? data.sender.fullname : data.sender.username ),
            csrfmiddlewaretoken = $('input[name="csrfmiddlewaretoken"]').val()
        return $('<a>', {
                    href: data.url,
                    class: 'nm-item',
                    'data-id': data.id,
                    click: function(e) {
                        e.preventDefault();
                        var href = $(this).attr('href');
                        $.ajax({
                            url: '/notifications/mark-as-read/',
                            type: 'POST',
                            data: 'not_id=' + $(this).data('id'),
                            success: function( response ) {
                                if( response.toString().toLowerCase() == 'ok' ) {
                                    if( data.type_not === 'chat' ) {
                                        $.ajax({
                                            url: data.url,
                                            type: 'POST',
                                            data: 'user1=' + $('#user').val() + '&user2=' + data.sender.id,
                                            success: function( response ) {
                                                if( response.status ) {
                                                    window.location.href = response.status;
                                                }
                                            },
                                            error: function( response ) {
                                                console.log( response );
                                            }
                                        })
                                    } else {
                                        window.location = href;
                                    }

                                }
                            },
                            error: function( err ) {

                            }
                        });
                    }
                })
                .append(
                    '<div class="nm-inner">' +
                        '<span class="nmitem-img"><img src="' + data.sender.profile.avatar + '" title="' + username + '"></span>' +
                        '<div class="nmitem-desc">' +
                            '<div class="flexb">' +
                                '<span class="nm-text"><span class="txt-sb">' + username + '</span>: ' + data.text + '</span>' +
                            '</div>' +
                        '</div>' +
                    '</div>'
                );
}
});

function setCookieFromDOMElementText( selector ) {
	var jOb = $(selector);
	if( jOb && jOb.length ) {
		var count = jOb.text().trim();
		if( isNumeric( count ) ) {
			count = parseInt( count );
			setCookie( 'notifyCount', count, { path: '/' } );
		}
	}
}