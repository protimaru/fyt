window.onbeforeunload = function () {
  window.scrollTo(0, 0);
}
$(function() {
    $(this).scrollTop(0);
    var wrapper = $('.sc-wrapper'),
        processing = false;

    // load post on page load;
    $.ajax({
        url: '/api/post/list/',
        data: 'location=home&object_id=',
        type: 'POST',
        success: function( response ) {
            console.log(response);
            if( !isEmpty( response ) ) {
                addPostArrayToDOM( $('.feed-list'), response, 'append' );
                processing = false;
            } else {
                console.log('response is empty');
                processing = true;
            }
        },
        error: function ( err ) {
            console.log( err )
            processing = true;
        }
    });


    // load last post item on scroll bottom;
    $(window).scroll(function() {
        var wpHeight = wrapper[0].offsetHeight;
        // console.log( window.pageYOffset + ' + ' + window.innerHeight + '(' + (window.pageYOffset + window.innerHeight) + ') >' + wpHeight );
        if( !processing && ( window.pageYOffset + window.innerHeight > wpHeight ) ) {
            console.log('call ajax!');
            var lastPostID = getLastMsgId( $('.feed'), 'id' );
            lastPostID = ( lastPostID ) ? lastPostID : '';
            processing = true;

            $.ajax({
                url: '/api/post/list/',
                data: 'location=home&object_id=' + lastPostID,
                type: 'POST',
                success: function( response ) {
                    console.log(response);
                    if( !isEmpty( response ) ) {
                        addPostArrayToDOM( $('.feed-list'), response, 'append' );
                        processing = false;
                    } else {
                        console.log('response is empty');
                        processing = true;
                    }
                },
                error: function ( err ) {
                    console.log( err )
                    processing = true;
                }
            });
        }
    });
});