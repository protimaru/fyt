$(function() {

  $('.ui.dropdown').dropdown();

  $('.psettings-tabs a').click(function(e) {
    e.preventDefault();
    e.stopPropagation();
    var tabTriggers = $('.psettings-tabs a');
    if( tabTriggers.length > 0 ) {
      for( var i = 0; len = tabTriggers.length, i < len; i++ ) {
        if( tabTriggers.eq(i) != $(this) ) {
          tabTriggers.eq(i).removeClass('active');
          $(tabTriggers.eq(i).data('target')).removeClass('state-active');
        }
      }
    }
    $(this).addClass('active');
    $($(this).data('target')).addClass('state-active');

    getInfo();
  });
  $('.psettings-tabs a.active').trigger('click');


  // change password;
  $('#btn-changepswd').click(function(e) {
    e.preventDefault();
    e.stopPropagation();
    var currP = $('input[name=pswd-current]'),
      newP = $('input[name=pswd-new]'),
      newPConfirm = $('input[name=pswd-confirm]'),
      uName = $('input[name=user-uname]').val() || '';

    if( checkPswd( newP.val(), newPConfirm.val() ) && currP.val() ) {
      $.ajax({
        url: '/account/update-password/',
        type: 'POST',
        data: '&current_pswd=' + currP.val() + '&new_pswd=' + newP.val() + '&new_pswd_cnfrm=' + newPConfirm.val() +
        '&user-id=' + $('#user_').val(),
        success: function( response ) {
          console.log( response );
          if( response ) {
            currP.val('');
            newP.val('');
            newPConfirm.val('');

            showStateMsg( response.state, response.status );
          }
        },
        error: function( error ) {
          console.log( error );
        }
      });
    } else {
      showStateMsg( 'error', 'Passwords do\'t match!' );
      console.log( 'pswd not match!' );
    }
  });


  // update settings
  $('#btn-save').click(function(e) {
    e.preventDefault();
    e.stopPropagation();

    var fName = $('input[name=user-fname]'),
      lName = $('input[name=user-lname]'),
      uName = $('input[name=user-uname]'),
      email = $('input[name=email]'),
      phone = $('input[name=phone]'),
      addr = $('input[name=address]'),
      year = $('#slct-date-year'),
      month = $('#slct-date-month'),
      day = $('#slct-date-day'),
      gender = $('input[name=gender]'),
      country = $('#slct-country'),
      city = $('#slct-city'),
      dayString = ( parseInt( day.val() ) / 10 >= 1 ) ? day.val() : ( '0' + day.val() ),
      monthString = ( ( parseInt( month.val() ) + 1 ) / 10 >= 1 ) ? ( parseInt( month.val() ) + 1 ) : ( '0' + ( parseInt( month.val() ) + 1 ) );

    var counter = 0,
      sendArr = {};
      sendArr['user-id'] = $('#user_').val();
      sendArr['first_name'] = fName.val();
      sendArr['last_name'] = lName.val();
      sendArr['username'] = uName.val();
      sendArr['email'] = email.val();
      sendArr['phone'] = phone.val();
      sendArr['address'] = addr.val();
      sendArr['birth_date'] = ( year.val() + '-' + monthString + '-' + dayString );
      sendArr['gender'] = gender.val();
      sendArr['country'] = country.val();
      sendArr['city'] = city.val();

    if( fName.prop('required') ) {
      if( fName.val() ) { counter++; }
    }
    if( lName.prop('required') ) {
      if( lName.val() ) { counter++; }
    }
    if( uName.prop('required') ) {
      if( uName.val() ) { counter++; }
    }
    if( email.prop('required') ) {
      if( email.val() ) { counter++; }
    }
    if( gender.prop('required') ) {
      if( gender.val() ) { counter++; }
    }
    if( country.prop('required') ) {
      if( country.val() ) { counter++; }
    }
    if( city.prop('required') ) {
      if( city.val() ) {
        counter++;
        if( !isNumeric( city.val() ) ) {
          sendArr['city'] = '';
        }
      }
    }
    if( year.val() && month.val() && day.val() ) {
      counter++;
    }






    console.log(sendArr);
    console.log(counter);

    if( sendArr && counter == 8 ) {
      $.ajax({
        url: '/account/update-profile/' + $('#user_').val() + '/',
        type: 'POST',
        data: sendArr,
        success: function( response ) {
          console.log( response );
          if( response ) {
            showStateMsg( response.state, response.status );
          }
        },
        error: function( error ) {
          console.log( error );
        }
      });
    } else {
      showStateMsg( 'error', 'Required fields are empty!' );
    }
  });


  // get cities list by country code
  $('#slct-country').change(function() {
    var option = $(this).find(':selected'),
      defaultCity = option.text(),
      code = option.data('code');
    console.log(code);
    if( code ) {
      $.get(
        '/api/geodata/cities/',
        { name: code.toUpperCase() },
        function( response ) {
          console.log( response );
          var cities;
          if( response instanceof Array && response.length ) {
            cities = response;
          } else {
            cities = [{
              id: defaultCity,
              name: defaultCity
            }];
          }
          var cityList = $('#slct-city');
          cityList.children().remove();
          cityList.dropdown('clear');
          if( cityList && cityList.length ) {
            for( var i = 0; len = cities.length, i < len; i++ ) {
              cityList.append('<option value="' + cities[i].id + '" data-title="' + cities[i].name + '">' + cities[i].name + '</option>');
            }
          }
        }
      );
    }
  });

  refreshDate($('#slct-date-day'), $('#slct-date-month'), $('#slct-date-year'));


  $('input[name=user-uname]').on('change, focusout', function() {
    checkUName($(this));
  });
  $('input[name=user-uname]').focus(function() {
    errMsg( $(this).parent(), 'off' );
  });

  $('input[name=email]').on('change, focusout', function() {
    checkUName($(this));
  });
  $('input[name=email]').focus(function() {
    errMsg( $(this).parent(), 'off' );
  });

});

function showStateMsg( state, msg ) {
  var stateClass = '',
    title = '';

  if( state == 'success' ) {
    stateClass = 'positive';
    title = 'Success!';
  } else {
    stateClass = 'negative';
    title = 'Error!';
  }

  $('body').find('#pset-msg').remove();
  $('body').append(
    '<div class="ui ' + stateClass + ' message" id="pset-msg">' +
    '<div class="header">' + title + '</div>' +
    '<p>' + msg + '</p>' +
    '</div>'
  );

  $('#pset-msg').slideDown(250);

  setTimeout(function() {
    $('#pset-msg').slideUp(250);
  }, 2000);
}


function getCCList(c) {
  var  cCon = '',
    params = {};
  if( c && c == 'country' ) {
    url = '/api/geodata/countries';
    cCon = $('#slct-country');
  } else if( c && c == 'city' ) {
    url = '/api/geodata/cities/';
    cCon = $('#slct-city');
    params['name'] = $('#slct-country').find(':selected').data('code');
  }

  $.get(
    url,
    params,
    function( data ) {
      if( c == 'country' ) {
        setCountrySelect( data, cCon );
      } else {
        setCitySelect( data, cCon );
      }
    }
  );
}

function setCountrySelect( arr, c ) {
  if( arr instanceof Array && arr.length ) {
    c.children().remove();
    c.append('<option value="">Choose country</option>');
    for( var i = 0; len = arr.length, i < len; i++ ) {
      c.append(
        '<option value="' + arr[i].id + '" data-code="' + arr[i].sortname + '">' + arr[i].name + '</option>'
      );
    }

    var selected = getCookie('country');
    if( selected ) {
      setTimeout(function() {
        c.parent()
          .dropdown('refresh')
          .dropdown('set selected', selected);
        deleteCookie('country');

        // set city list
        getCCList('city');
      }, 0);
    }
  }
}

function setCitySelect( arr, c ) {
  if( arr instanceof Array && arr.length ) {
    c.children().remove();
    c.append('<option value="">Choose city</option>');
    for( var i = 0; len = arr.length, i < len; i++ ) {
      c.append(
        '<option value="' + arr[i].id + '">' + arr[i].name + '</option>'
      );
    }
    setTimeout(function() {
      c.parent()
        .dropdown('refresh')
        .dropdown('set selected', getCookie('city'));
      deleteCookie('city');
    }, 0);
  }
}


function getInfo() {
  $.ajax({
    url: '/api/profile/' + $('#current_user_username').val() + '/detail/',
    type: 'GET',
    success: function( response ) {
      setFields( response );
    },
    error: function( error ) {
      console.log( error );
    }
  });
}

function setFields( data ) {
  console.log(data);
  if( !data && !data.length ) { return }
  $('input[name=user-fname]').val( data.first_name );
  $('input[name=user-lname]').val( data.last_name );
  $('input[name=user-uname]').val( data.username );
  $('input[name=email]').val( data.email );
  $('input[name=phone]').val( data.profile.phone );
  $('input[name=address]').val( data.profile.address );


  if( data.profile.birth_date ) {
    var day = $('#slct-date-day'),
      month = $('#slct-date-month'),
      year = $('#slct-date-year');

    var date = data.profile.birth_date.split('-');
    if( isNumeric(date[0]) ) {
      year.parent().dropdown('set selected', +date[0]);
    }
    if( isNumeric(date[1]) ) {
      month.parent().dropdown('set selected', (+date[1] - 1));
    }
    if( isNumeric(date[2]) ) {
      if( year.val() && month.val() ) {
        var maxDayInM = new Date(parseInt(year.val()), parseInt(month.val()), 1).daysInMonth();

        resetSelect( day, 'Day' );
        refillSelect( day, maxDayInM );
        if( +date[2] <= maxDayInM ) {
          console.log( +date[2] );
          day.parent().dropdown('set selected', +date[2]);
          day.siblings('.text').text(+date[2]);
        } else {
          day.prop('selectedIndex', 0);
        }
      }
    }
  }

  if( data.profile.gender ) {
    $('input[name=gender]').parent().dropdown('set selected', data.profile.gender);
  }

  if( data.profile.country ) {
    var c = data.profile.country;
    setCookie('country', c, { path: '/' });
    getCCList('country');
  } else {
    getCCList('country');
  }

  if( data.profile.city ) {
    var c = data.profile.city;
    setCookie('city', c, { path: '/' });
  }
}


function checkPswd( pswd, pswdConfirm ) {
  return pswd === pswdConfirm;
}

function checkUName(element) {
  console.log('called!');
  if(element.val() != "") {
    $.ajax({
      method: 'POST',
      url: "/api/profile/check/email-username/",
      data: element.data('action') + "=" + element.val(),
      success: function(response) {
        if(response.status === 1) {
          var action = element.data('action').slice(0,1).toUpperCase() + element.data('action').slice(1);
          element.parent().find('.err-msg').remove();
          element.attr('data-errstate', 'on');
          $('<span class="err-msg">' + action + ' is already exists!</span>').insertAfter( element );
        }
        else {
          element.parent().find('.err-msg').remove();
          element.removeAttr('data-errstate');
        }
      }
    });
  } else {
    console.log('show msg!');
    errMsg( element.parent(), 'on', 'This field is required!' );
  }
}