$(function() {
    $('.coming_soon').click(function(e) {
        e.preventDefault();
    }).each(function() {
        $(this).popup({
            position : 'bottom left',
            popup   : '#info-p',
            on: 'click',
            title    : 'Info',
            content  : 'The referenced page at the design state...'
        });
    });


    $('.to-auth').click(function(e) {
        e.preventDefault();
    }).popup({
        on: 'click',
        position: 'bottom center',
        title: 'Info',
        html: 'To view this page you must be authorized. Please <a href="/account/login/" style="text-decoration: underline;">login</a>'
    });

});