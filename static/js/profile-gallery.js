$(function() {

  // init fancy box on click
  $('body').on('click', '[data-fbcustom="gal-item"], [data-fbcustom="album-item"]', function(e) {
    e.preventDefault();
    var $links = [],
      list = ( $(this).attr('data-fbcustom') == 'gal-item' ) ? $('[data-fbcustom="gal-item"]') : $('[data-fbcustom="album-item"]'),
      index = 0,
      $this = this;


    list.each(function() {
      $links.push({
        src: $(this).attr('href'),
        type: $(this).attr('data-type'),
        data_id: $(this).attr('data-id')
      });
      if( this == $this ) { index = list.index(this) }
    });

    $.fancybox.open( $links, {
      idleTime: false,
      loop: true,
      keyboard: false,
      gutter: 0,
      infobar: false,
      trapFocus: false,
      clickSlide: '',
      clickOutside: '',
      buttons: [
        'close'
      ],
      thumbs: {
        hideOnClose: false,
        parentEl: '.fancybox-outer'
      },
      touch: {
        vertical : false
      },
      animationEffect: "fade",
      baseClass: 'fancybox-custom-layout',
      beforeShow: function( instance, current ) {
        console.info( current );
        getMediaDetail( current.data_id, current.type );
      },
      onInit : function( instance ) {
        // Create new wrapping element, it is useful for styling
        // and makes easier to position thumbnails
        instance.$refs.inner.wrap( '<div class="fancybox-outer"></div>' );
      },
      caption : function(instance, item) {
          return '<div class="about-area">' +
                    '<div class="inner-con"></div>' +
                '</div>';
        }
    }, index );
  });

  // like/unlike photo/video on fancy box slide
  $('body').on('click', '.about-area .serv-btn.like', function(e) {
    e.preventDefault();
    var $this = $(this),
      prevAction = ($this.hasClass('active')) ? 'like' : 'unlike',
      prevLikesCount = parseInt( $this.attr('data-count') ) || 0;

    $.post(
      '/_like/add/',
      {
        object_id: $this.attr('data-pid'),
        content_type: $this.attr('data-ptype'),
      },
      function( data ) {
        if( data['status'] == 'ok' ) {
          if( prevAction == 'like' ) {
            $this.removeClass('active');
            $this.attr('data-count', (( prevLikesCount == 0 ) ? 0 : prevLikesCount - 1 ));
          } else {
            $this.addClass('active');
            $this.attr('data-count', ( prevLikesCount + 1 ));
          }
          $this.find('.count').text( $this.attr('data-count') );
        }
    });
  });


  // send comment item: 'comment' || 'reply', Enter event on textarea
  $('body').on('keyup paste cut', '.about-area .tarea', function(e) {
    if( e.keyCode == 13 && !e.shiftKey ) {
      var elType = $(this).attr('data-eltype'),
        o = {};
      if( e.target.value ) {
        if( elType == 'com' ) {
          o['url'] = '/_comment/add/';
          o['method'] = 'POST';
          o['object'] = {
            id: $(this).attr('data-pid'),
            type: $(this).attr('data-ptype')
          };
          o['target'] = {
            type: 'com'
          };
          o['publishcon'] = $(this).attr("data-publishcon");
        } else {
          o['url'] = '/_reply/add/';
          o['method'] = 'POST';
          o['object'] = {
            id: $(this).attr('data-pid'),
          };
          o['target'] = {
            type: 'rpl'
          };
          o['publishcon'] = '#rpl' + o['object']['id'] + ' ' + $(this).attr("data-publishcon");
        }

        // send commentItem: comment or reply
        sendCItem( e.target.value, o);
        clearTextarea( e.target );
      }
    }
    tAutoGrowth(this, 30);
  });
  $('body').on('keydown', '.about-area .tarea', function(e) {
    if( e.keyCode == 13 && !e.shiftKey ) {
      return false;
    }
  });


  // comment/reply get and show list of comment || reply throw link target
  $('body').on('click', '.about-area .serv-btn.comment, .about-area .rpllist-trigger', function(e) {
    e.preventDefault();
    var tCon = $($(this).attr("data-target")),
        elType = $(this).attr('data-eltype');
    if( $(this).attr('data-state') == 'on' ) {
      var o = {};

      $(this).removeAttr('data-state');
      if( elType == 'com' ) {
        o['url'] = '/api/comment/list/';
        o['method'] = 'GET';
        o['object'] = {
          id: $(this).attr('data-pid'),
          type: $(this).attr('data-ptype')
        };
        o['target'] = {
          id: '',
          type: 'com'
        };
      } else {
        o['url'] = '/api/reply/list/';
        o['method'] = 'GET';
        o['object'] = {
          id: $(this).attr('data-pid'),
          type: ''
        };
        o['target'] = {
          id: '',
          type: 'rpl'
        };
      }

      // get list
      getCIList( o );
      showCon( elType );
    } else {
      deleteReplyLoadMoreTrigger( tCon.find('.lmore-pilist') );
      showCon( elType );
    }

    function showCon(elType) {
      var taraeCon = ( elType == 'com' ) ? '.item-add.comment' : '.item-add.reply';
      if( tCon.css('display') == 'none' ) {
        tCon.slideDown(250);
      }
      tCon.find(taraeCon + ' .tarea').focus();
    }
  });
  $('body').on('click', '.about-area .to-reply:not(.rpllist-trigger)', function(e) {
    e.preventDefault();
    clearTextarea( $($(this).attr('data-target')).find('.tarea') );
  });

  $('body').on('click', '.about-area .lmore-trigger', function(e) {
    e.preventDefault();
    var elType = $(this).attr('data-eltype'),
      o = {},
      lastItemId = '';

    if( elType == 'com' ) {
      lastItemId = getLastMsgId( '.published.comment .published-item', 'id' );
      o['url'] = '/api/comment/list/';
      o['method'] = 'GET';
      o['object'] = {
        id: $(this).attr('data-pid'),
        type: $(this).attr('data-ptype')
      };
      o['target'] = {
        id: lastItemId,
        type: 'com'
      };
    } else {
      lastItemId = getLastMsgId( '#rpl' + $(this).attr('data-pid') + ' .published.reply .pi-con', 'id');
      o['url'] = '/api/reply/list/';
      o['method'] = 'GET';
      o['object'] = {
        id: $(this).attr('data-pid'),
        type: ''
      };
      o['target'] = {
        id: lastItemId,
        type: 'rpl'
      };
    }

    // get list
    getCIList( o );
  });


  // pass comment/reply id to popup edit/delete link;
  $('body').on('click', '.about-area .pi-edbtns', function() {
    console.log()
    $('.comedit-item')
      .attr('data-id', $(this).data('id'))
      .attr('data-eltype', $(this).data('eltype'));
  });
  // edit / delete comment AND reply items

  $('body').on('click', '.com-edit .comedit-item', function(e) {
    e.preventDefault();
    var $this = $(this),
      actionType = $this.data('action'),
      itemType = $this.data('eltype'),
      url = '',
      method = '',
      actionTarget = '';

    var confirmModalMsg = getConfirmModalMsg( actionType, itemType );
    if( itemType == 'com' ) {
      if( actionType == 'delete' ) {
        url = '/api/comment/delete/' + $this.data('id');
        method = 'DELETE';
      } else if( actionType == 'edit' ) {
        url = '/api/comment/update/' + $this.data('id') + '/';
        method = 'PUT';
      }
      actionTarget = $('.about-area .published-item[data-id="' + $this.data('id') + '"]');
    } else {
      if( actionType == 'delete' ) {
        url = '/api/reply/delete/' + $this.data('id') + '/';
        method = 'DELETE';
      } else if( actionType == 'edit' ) {
        url = '/api/reply/update/' + $this.data('id') + '/';
        method = 'PUT';
      }
      actionTarget = $('.about-area .published.reply .pi-con[data-id="' + $this.data('id') + '"]');
    }

    console.log( actionTarget );
    var targetItemText = actionTarget.find('.pi-content').text(),
        confirmModal = '<div class="ui modal tiny" id="comrpl-editing">' +
                          '<div class="header">' + confirmModalMsg.header + '</div>' +
                          '<div class="content">' +
                            '<p>' + confirmModalMsg.content + '</p>' +
                          '</div>' +
                          '<div class="actions">' +
                            '<div class="ui negative button">' + confirmModalMsg.btnDeny + '</div>' +
                            '<div class="ui positive button">' + confirmModalMsg.btnApprove + '</div>' +
                          '</div>' +
                        '</div>';

    $('body').append(confirmModal);
    $('#comrpl-editing').modal({
      onHidden: function() {
        $('#comrpl-editing').remove();
      },
      onVisible: function() {
        $('.comrepl-editing').popup('destroy');
        if( actionType == 'edit' ) {
          console.log('edit');
          $('#comrpl-editing .editing-tarea')
            .on('keyup paste cut', function() {
              tAutoGrowth( this, 30 );
            })
            .val( targetItemText ).trigger('paste');
        }
      },
      onDeny: function() {
        console.log('Cancelled by user!');
      },
      onApprove: function() {
        if( actionType == 'edit' ) {
          var modalTarea = $('#comrpl-editing .editing-tarea'),
              t = actionTarget.find('.pi-content');
          $.ajax({
            url: url,
            type: method,
            data: { text: modalTarea.val() },
            success: function( response ) {
              console.log( response );
              t.text( response.text );
            }
          });
        } else {
          $.ajax({
            url: url,
            type: method,
            statusCode: {
              204: function() {
                actionTarget.slideUp(120, function() {
                  $(this).remove();
                  if( itemType == 'com' ) {
                    var countText = $('.about-area .serv-btn.comment .count'),
                      currCount = ( isNumeric(countText.text()) ) ? +(countText.text()) : 0;

                    countText.text( ( currCount < 1 ) ? 0 : (currCount - 1) );
                  }
                });
              }
            }
          });
        }
      }
    })
    .modal('show');

    function getConfirmModalMsg( actionName, itemType ) {
      var msg = {};
      if( actionName == 'edit' ) {
        msg = editMsg( itemType );
      } else {
        msg = deleteMsg( itemType );
      }
      return msg;
    }
    function deleteMsg( itemType ) {
      var msg = {};
      if( itemType == 'com' ) {
        msg['header'] = 'Delete your comment';
        msg['content'] = 'Are you sure you want to delete your comment?'
      } else {
        msg['header'] = 'Delete your reply';
        msg['content'] = 'Are you sure you want to delete your reply?'
      }
      msg['btnApprove'] = 'Yes';
      msg['btnDeny'] = 'Cancel';
      return msg;
    }
    function editMsg( actionItem ) {
      var msg = {};
      if( actionItem == 'com' ) {
        msg['header'] = 'Edit your comment';
        msg['content'] = '<div class="editing-currtext">' +
                          '<textarea class="editing-tarea" rows="1" data-min-rows="1"  placeholder="Write a comment"></textarea>' +
                        '</div>';
      } else {
        msg['header'] = 'Edit your reply';
        msg['content'] = '<div class="editing-currtext">' +
                          '<textarea class="editing-tarea" rows="1" data-min-rows="1"  placeholder="Write a comment"></textarea>' +
                        '</div>';
      }
      msg['btnApprove'] = 'Save';
      msg['btnDeny'] = 'Cancel';
      return msg;
    }
  });

  /*
  * p = {
  *   url: '' - request url
  *   method: '' - GET || POST
  *   object: {
  *     id: - number
  *     type: - 'post' || 'photo' || 'video'
  *   }
  *   target: {
  *     name: 'com' || 'rpl'
  *     id: number
  *   }
  * }
  * */
  function sendCItem( text, p ) {
    if( !text && isEmpty(p) ) { return }
    var o = {};
    if( p['target']['type'] == 'com' ) {
      o['content_type'] = p['object']['type'];
      o['object_id'] = p['object']['id'];
    } else {
      o['comment_id'] = p['object']['id'];
    }
    o['text'] = text;
    $.ajax({
      url: p.url,
      type: p.method,
      data: o,
      success: function( response ) {
        var data = ( response instanceof Array) ? response : [response],
          str = createCITemplate( data, { type: p['target']['type'], pid: p['object']['id'] } );
        addToDOM( str, p['publishcon'], 'append' );

        // change comment count
        if( p['target']['type'] == 'com' ) {
          var countText = $('.about-area .serv-btn.comment .count'),
             currCount = ( isNumeric(countText.text()) ) ? +(countText.text()) : 0;

          countText.text( (currCount + 1) );
        }

        // bind popup to comment editing popup
        bindPopupToEl('.pi-edbtns', {
          on: 'click',
          position: 'bottom center',
          html: '<div class="com-edit">' +
                  '<a href="#" data-id="" data-eltype="' + p['target']['type'] + '" data-action="edit" class="comedit-item"><i class="write icon"></i> Edit</a>' +
                  '<a href="#" data-id="" data-eltype="' + p['target']['type'] + '" data-action="delete" class="comedit-item"><i class="trash outline icon"></i> Delete</a>' +
                '</div>',
          className: {
            popup: 'ui popup comrpl-editing'
          }
        });
      },
      error: function( textStatus ) {
        console.log( textStatus );
      }
    })
  }

  /*
  * p.url - request url
  * p.method - GET || POST
  * p.object: {
  *   id: - number
  *   type: - 'post' || 'photo' || 'video'
  * }
  * p.target: {
  *   name: 'com' || 'rpl'
  *   id: number
  * }
  * */
  function getCIList( p ) {
    if( isEmpty(p) ) { return }
    var ob = {};

    if( p['target']['type'] == 'com' ) {
      ob['app'] = p['object']['type'];
      ob['app_id'] = p['object']['id'];
      ob['comment_id'] = p['target']['id'];
    } else {
      ob['app_id'] = p['object']['id'];
      ob['reply_id'] = p['target']['id'];
    }

    $.ajax({
      url: p['url'],
      method: p['method'],
      data: ob,
      success: function( response ) {
        if( response ) {
          var publishCon = '',
            str = '',
            data = (response instanceof Array) ? response : [response];

          if( p.target.type == 'com' ) {
            publishCon = '.published.comment';
          } else {
            publishCon = '#rpl' + p['object']['id'] + ' .published.reply';
          }

          // delete loadmore trigger for comment || reply container;
          if( data.length < 5 ) {
            deleteReplyLoadMoreTrigger( $(publishCon).siblings('.lmore-pilist') );
          }

          str = createCITemplate( data, { type: p['target']['type'], pid: p['object']['id'] } );
          addToDOM( str, publishCon, 'prepend' );

          // bind popup to comment editing popup
          bindPopupToEl('.pi-edbtns', {
            on: 'click',
            position: 'bottom center',
            html: '<div class="com-edit">' +
                    '<a href="#" data-id="" data-eltype="' + p['target']['type'] + '" data-action="edit" class="comedit-item"><i class="write icon"></i> Edit</a>' +
                    '<a href="#" data-id="" data-eltype="' + p['target']['type'] + '" data-action="delete" class="comedit-item"><i class="trash outline icon"></i> Delete</a>' +
                  '</div>',
            className: {
              popup: 'ui popup comrpl-editing'
            }
          });
        }
      },
      error: function( textStatus ) {
        console.log( textStatus );
      }
    });
  }

  /*
  * cItem: {
  *   pid: number
  *   type: 'com' || 'rpl'
  * }
  * */
  function createCITemplate( data, cItem ) {
    if( isEmpty( data ) ) { return; }
    var str = '',
      editBtns = '';

    if( cItem.type == 'com' ) {
      for( var len = data.length - 1, i = len; i >= 0; i-- ) {

        if( data[i].author.id == $('#user_').val() ) {
          editBtns = '<div class="pi-edbtns" data-id="' + data[i].id + '" data-eltype="com" title="Editing"><i class="ellipsis horizontal icon"></i></div>'
        } else {
          editBtns = '';
        }

        str += '<div class="published-item" data-id="' + data[i].id + '">' +
                '<div class="pi-con">' +
                    editBtns +
                    '<div class="pi-usercard">' +
                        '<div class="piuc-img"><a href="' + data[i].author.url + '"><img src="' + data[i].author.profile.avatar + '" title="' + data[i].author.username + '"></a></div>' +
                        '<div class="piuc-title">' +
                            '<a href="' + data[i].author.url + '" class="piuc-name">' + data[i].author.username + '</a>' +
                            '<span class="piuc-date cp__u-date" data-datetime="' + data[i].created + '"> - recently</span>' +
                        '</div>' +
                    '</div>' +
                    '<div class="pi-content">' +
                        '<p>' + data[i].text + '</p>' +
                    '</div>' +
                    '<div class="pi-reply">' +
                        '<a href="#" class="to-reply rpllist-trigger" data-state="on" data-pid="' + data[i].id + '" data-target="#rpl' + data[i].id + '">Reply</a>' +
                    '</div>' +
                '</div>' +
                '<div id="rpl' + data[i].id + '" class="con-reply">' +
                    '<div class="lmore-pilist"><a href="#" class="lmore-trigger" data-pid="' + data[i].id + '" data-eltype="rpl">Show 5 more replies</a></div>' +
                    '<div class="published reply"></div>' +
                    '<div class="item-add reply">' +
                        '<div class="add-author"><img src="' + data[i].author.profile.avatar + '"></div>' +
                        '<div class="add-tarea">' +
                            '<textarea class="tarea" rows="1" data-min-rows="1" data-pid="' + data[i].id + '" data-publishcon=".published.reply" data-eltype="rpl" placeholder="Write a reply"></textarea>' +
                        '</div>' +
                    '</div>' +
                '</div>' +
            '</div>';
      }
    } else {
      for( var len = data.length - 1, i = len; i >= 0; i-- ) {

        if( data[i].author.id == $('#user_').val() ) {
          editBtns = '<div class="pi-edbtns" data-id="' + data[i].id + '" data-eltype="rpl" title="Editing"><i class="ellipsis horizontal icon"></i></div>'
        } else {
          editBtns = '';
        }

        str += '<div class="pi-con" data-id="' + data[i].id + '">' +
                  editBtns +
                  '<div class="pi-usercard">' +
                      '<div class="piuc-img"><a href="' + data[i].author.url + '"><img src="' + data[i].author.profile.avatar + '" title="' + data[i].author.username + '"></a></div>' +
                      '<div class="piuc-title">' +
                          '<a href="' + data[i].author.url + '" class="piuc-name">' + data[i].author.username + '</a>' +
                          '<span class="piuc-date cp__u-date" data-datetime="' + data[i].created + '"> - recently</span>' +
                      '</div>' +
                  '</div>' +
                  '<div class="pi-content">' +
                      '<p>' + data[i].text + '</p>' +
                  '</div>' +
                  '<div class="pi-reply">' +
                     '<a href="#" class="to-reply" data-target="#rpl' + cItem.pid + '">Reply</a>' +
                  '</div>' +
              '</div>';
      }
    }

    return str;
  }

  // reply loadmore remove
  function deleteReplyLoadMoreTrigger( selector ) {
    $(selector).remove();
  }














  $('.ui.dropdown').dropdown();

  imgNormalizeDisplay( $('.tab-content') );

  // tab changes
  $('.tab-link__item').click(function(e) {
    e.preventDefault();
    var target = $($(this).attr('href')),
        targetSiblings = $('.tab-content').not(target);

    $(this).siblings('.tab-link__item').removeClass('active');
    $(this).addClass('active');
    targetSiblings.removeClass('active');
    target.addClass('active');

    if( target.attr('data-action') == 'detailed' ) {
      var tCon = target.find('.tab-inner');
      showLoader( tCon, 'on' );
      $.post(
        '/_album/list/',
        { user_id: $('#user_').val() },
        function( data ) {
          if( data ) {
            target.removeAttr('data-action', 'detailed').html( data );
            showLoader( tCon, 'off' );
            imgNormalizeDisplay( target );
          }
        }
      );
    }
  });

  // input file changing on album create/edit
  $('body').on('change', '#album-cover', function() {
    var file = this.files[0];
    if( file && checkFileExt( file, 'image' ) ) {
      var con = $(this).parent().find('.preview-img');
      if( con && !con.length ) {
        $(this).parent()
          .addClass('img')
          .prepend('<div class="preview-img"></div>');

        con = $(this).parent().find('.preview-img');
      }

      var reader = new FileReader(),
          img = new Image();

      reader.onload = function() {
        img.src = reader.result;

        img.addEventListener('load', function() {
          this.style = getNormalizeCss(
            { width: con.width(), height: con.height() },
            { width: img.naturalWidth, height: img.naturalHeight }
            );
        });
        con.find('img').remove();
        con.append(img);
      };
      reader.readAsDataURL( file );
    } else {
      alert( 'Supported image formats: .png, jpeg, .jpg, .gif' );
    }
  });

  // input file change on add photo/video
  var arrMedia = [];
  $('body').on('change', '#media', function() {
    var point = arrMedia.length,
      errors = '';

    // collect all files to array;
    for( var i = 0; len = this.files.length, i < len; i++ ) {
      arrMedia.push({
        index: generateID(),
        file: this.files[i]
      });
    }

    // console.log(files);
    if( !arrMedia ) {
      errors = "File upload not supported by your browser.";
      alert(errors);
      return;
    }

    // check files before read and create preview object;
    if ( arrMedia && arrMedia.length ) {
      for( var i = point; len = arrMedia.length, i < len; i++ ) {
        var fileOb = arrMedia[i],
          type = fileOb.file.type.split('/')[0];

        if( checkFileExt( fileOb.file, type ) ) {
          readMediaItem( fileOb.file, fileOb.index );
        } else {
          errors = 'File not supported!';
        }
      }
    }

    if ( errors ) {
      alert(errors);
    }


    function readMediaItem( file, index ) {
      var elAnchor = $('#add-photovideo .preview-media .pm-trigger'),
          previewCon = $('#add-photovideo .preview-media'),
          reader = new FileReader();

      window.URL = window.URL || window.webkitURL;
      if( file.type.match('image') ) {
        reader.onload = function() {
          var img = new Image();

          img.src = reader.result;

          var newPImg = $('<div>', {
            class: 'input-file img',
            'data-id': index
          })
          .append(
            $('<div class="preview-img"></div>')
              .append('<div class="overlay"></div>')
              .append( img )
              .append(
                $('<span>', {
                  class: 'remove',
                  title: 'Remove',
                  click: function() {
                    $(this).parent().parent().fadeOut(500, function() {
                      $(this).remove();
                      if(arrMedia.length != 0) {
                        arrMedia = deleteByIndexFromArray( arrMedia, index );
                      }
                    });
                  }
                })
              )
          );

          var con = newPImg.find('.preview-img');
          img.addEventListener('load', function() {
            this.style = getNormalizeCss(
              { width: con.width(), height: con.height() },
              { width: img.naturalWidth, height: img.naturalHeight }
            );

          });

          // insert to last postion;
          newPImg.insertBefore( elAnchor );
          previewCon.scrollLeft( previewCon[0].scrollWidth );
        };
        reader.readAsDataURL( file );

      } else if( file.type.match('video') ) {

        var video = document.createElement('video'),
            canvas = document.createElement('canvas');

        video.src = window.URL.createObjectURL(file);
        canvas.width  = 120;
        canvas.height = 120;

        video.addEventListener('loadeddata', function() {
          canvas.getContext("2d").drawImage(video, 0, 0, 120, 120);
        }, false);

        // create new DOM element(preview-img container);
        var newPImg = $('<div>', {
          class: 'input-file img',
          'data-id': index,
        })
        .append(
          $('<div class="preview-img"></div>')
            .append('<div class="overlay"></div><span class="play-icon"></span>')
            .append( canvas )
            .append(
              $('<span>', {
                class: 'remove',
                title: 'Remove',
                click: function() {
                  $(this).parent().parent().fadeOut(500, function() {
                    $(this).remove();
                    if(arrMedia.length != 0) {
                      arrMedia = deleteByIndexFromArray( arrMedia, index );
                    }
                  });
                }
              })
            )
        );

        // insert to last postion;
        newPImg.insertBefore( elAnchor );
        previewCon.scrollLeft( previewCon[0].scrollWidth );

        window.URL.revokeObjectURL(file);
      }
    }
  });


  $('.ui.modal').find('.header .close').click(function() {
    var target = $($(this).attr('data-pid'));
    target
      .modal({
        onHidden: function() {
          resetForm( target, target.attr('data-title') );
        }
      })
      .modal('hide');
  });

  // album detail
  $('body').on('click', '.gal-detail', function(e) {
    e.preventDefault();
    var t = $('#tab-albums'),
      tCon = t.find('.tab-inner');

    showLoader( tCon, 'on' );

    $.ajax({
      url: '/_album/detail/',
      type: 'POST',
      data: { album_id: $(this).data('id') },
      dataType: 'html',
      success: function( response ) {
        if( response ) {
          t.attr('data-action', 'detailed').html( response );
          showLoader( tCon, 'off' );
          imgNormalizeDisplay( t );
        }
      }
    });
  });

  // handle approve button on add photo/video || create album
  $('body').on('click', '.serv-modal .approve', function(e) {
    e.preventDefault();
    var type = $(this).attr('data-ptype'),
      id = $(this).attr('data-id'),
      action = $(this).attr('data-action'),
      context = $(this).attr('data-actioncontext');

    switch( type ) {
      case 'album':
          if( action == 'add' ) { approveCreateAlbum() }
          else if( action == 'edit' ) { approveEditAlbum( id ) }
          else if( action == 'del' ) { deleteMediaItem( id, type, context ) }
        break;
      case 'photovid':
          var actionContext = $(this).attr('data-actioncontext');
          if( action == 'add' ) { approveAddPhotoVid( actionContext ) }
        break;
      case 'photo':
          if( action == 'edit' ) { approveEditPhoto( id ) }
          else if( action == 'del' ) { deleteMediaItem( id, type ) }
        break;
      case 'vid':
          if( action == 'edit' ) { approveEditVideo( id ) }
          else if( action == 'del' ) { deleteMediaItem( id, type ) }
    }
  });


  // modal trigger for modal: add photo/video || create album
  $('body').on('click', '.modal-trigger', function(e) {
    e.preventDefault();
    var o = {
          type: $(this).data('ptype'),
          action: 'add',
          params: {
            id: $(this).data('pid'),
            formClearContextKey: getModalFormClearContextKey( $(this).attr('data-ptype'), 'add'),
            sizeType: 'tiny',
            cssClass: ''
          }
        };

    if( $(this).data('ptype') == 'album' ) {
      o.params['cssClass'] = 'album';
    } else {
      o.params['cssClass'] = 'photo-video';
    }

    var m = $('.ui.modal.serv-modal');
    setModalParams( o, m );
    if( $(this).hasClass('mt-detail') ) {
      m.find('form')
        .prepend('<input type="hidden" name="album-id" value="' + $(this).data('id') + '">')
        .find('.approve').attr('data-actioncontext', 'al-detail');
    }
    m.modal('show');
  });

  // modal trigger for modal edit: photo || video || album
  $('body').on('click', '.edit-btn', function(e) {
    e.preventDefault();
    e.stopPropagation();
    var elId = $(this).closest('.media-block').data('id')
            || $(this).closest('.gal-detail').data('id')
            || $(this).data('id'),
        o = {
          type: $(this).data('ptype'),
          action: 'edit',
          params: {
            id: $(this).data('pid'),
            formClearContextKey: '',
            sizeType: 'tiny',
            cssClass: ''
          }
        };

    if( $(this).data('ptype') == 'album' ) {
      o.params['cssClass'] = 'album';
    } else {
      o.params['cssClass'] = 'edit-photo-video';
      o.params['sizeType'] = 'small';
    }

    var m = $('.ui.modal.serv-modal');
    setModalParams( o, m );
    console.log( 'elId: ' + elId );
    if( elId ) {
      $('.serv-modal .approve').attr('data-id', elId);
      setEditModalDefaultData({ id: elId, type: o.type, con: m });

      // set attribute for delete element
      $(this).parent().attr('data-deltarget', elId);
    }
    if( $(this).hasClass('mt-detail') ) {
      m.find('button[data-action="del"]').attr('data-actioncontext', 'mt-detail');
    }
    m.modal('show');
  });


  // form reset functions for modals
  function resetForm( targetCon, context ) {
    var con = $(targetCon);
    if( con && con.length && con.find('form').length ) {
      con.find('form')[0].reset();
    }

    if( context == 'photovid-add' ) {
      resetAddPhotoVid( targetCon );
    } else if( context == 'photovid-edit' ) {
      // call reset function
    } else if( context == 'album' ) {
      resetAddAlbum( targetCon );
    }
  }

  function resetAddPhotoVid( targetCon ) {
    var con = $(targetCon);
    if( con && !con.length ) return;
    con.find('.preview-media .input-file.img').remove();
    arrMedia = [];
  }

  function resetAddAlbum( targetCon ) {
    var con = $(targetCon);
    if( con && !con.length ) return;
    con.find('.input-file')
      .removeClass('img')
      .find('.preview-img').remove();
  }


  // create template: album || photo || video
  function createAlbumTemplate( data ) {
    return '<div class="gal-item col">' +
              '<a href="#" data-id="' + data.id + '" class="gal-detail">' +
                '<div class="media-block">' +
                  '<span class="edit-btn" title="Edit album" data-ptype="album" data-pid="album-edit"></span>' +
                  '<img src="' + data.album_art + '">' +
                  '<span class="media-count">' +
                    '<span class="item-count img"><span class="count">' + data.photo.length + '</span></span>' +
                    '<span class="item-count video"><span class="count">' + data.video.length + '</span></span>' +
                  '</span>' +
                '</div>' +
                '<div class="media-title">' + data.description + '</div>' +
                '</a>' +
            '</div>';
  }

  function createGalleryItemTemplate( data, type ) {
    if( isEmpty( data ) ) { return }
    var type = data.get_model_name.toLowerCase(),
      str = '';

    if( type == 'photo' ) {
      str = '<div class="gal-item col">' +
                '<a href="' + data.image + '" data-fbcustom="gal-item" data-type="image" class="media-block media-img" data-id="' + data.id + '">' +
                    '<span class="hover-icon"></span>' +
                    '<span class="edit-btn" data-pid="edit-photovideo" data-ptype="photo" title="Edit item"></span>' +
                    '<img src="' + data.image + '">' +
                '</a>' +
            '</div>';
    } else {
      str = '<div class="gal-item col">' +
                '<a href="/_video/video/' + data.id + '/" data-fbcustom="gal-item" data-type="iframe" class="media-block media-video" data-id="' + data.id + '">' +
                    '<span class="hover-icon"></span>' +
                    '<span class="edit-btn" data-pid="edit-photovideo" data-ptype="vid" title="Edit item"></span>' +
                    '<img src="' + data.thumbnail + '">' +
                '</a>' +
            '</div>';
    }
    return str;
  }

  // delete media item of gallery: 'album' || 'photo' || 'video'
  function deleteMediaItem( id, type, context ) {
    var url = '',
      msg = '';

    if( type == 'album' ) {
      url = '/api/album/delete/' + id;
      msg = 'Album successfully deleted!';
    } else if( type == 'photo' ) {
      url = '/api/photo/delete/' + id;
      msg = 'Photo successfully deleted!';
    } else if( type == 'vid' ) {
      url = '/api/video/delete/' + id;
      msg = 'Video successfully deleted!';
    }

    $.ajax({
      url: url,
      type: "DELETE",
      statusCode: {
        204: function() {

          console.log('Media item successfully deleted!');
          $('.serv-modal').modal('hide');

          if( context ) {
            var t = $('.tab-content.active'),
              tCon = t.find('.tab-inner');

            showLoader( tCon, 'on' );
            $.post(
              '/_album/list/',
              { user_id: $('#user_').val() },
              function( data ) {
                if( data ) {
                  t.removeAttr('data-action', 'detailed').html( data );
                  tCon = t.find('.tab-inner');
                  showLoader( tCon, 'off' );
                  showMsg( tCon, msg, 2000 );
                  imgNormalizeDisplay( t );
                }
              }
            );

          } else {
            var con = $('.tab-content.active');
            showMsg( con.find('.tab-inner'), msg, 2000 );
            con.find('[data-deltarget=' + id + ']').closest('.gal-item').fadeOut(250, function() {
              $(this).remove();
            });
          }
        }
      }
    });
  }

  // approve actions: 'add' || 'edit'
  // for elements: 'album' || 'photo' || 'video' || 'photovid'

  // handler function after click approve button on: add photo/video || create album
  function approveCreateAlbum() {
    var modal = $('.serv-modal');
    if( modal && !modal.length ) { return }

    var b = true,
      fd = new FormData();

    fd.append('name', modal.find('[name=alubm-name]').val());
    fd.append('description', modal.find('[name=album-desc]').val());
    fd.append('album_art', modal.find('#album-cover')[0].files[0]);

    if( b ) {
      showLoader( modal.find('.content'), 'on' );

      $.ajax({
       url: '/_album/add/',
       type: 'POST',
       data: fd,
       cache: false,
       contentType: false,
       processData: false,
       success: function( response ) {

        resetForm( modal, 'album-add' );
        showLoader( modal.find('.content'), 'off' );

        if( !isEmpty( response ) ) {
          var con = $('#tab-albums .row'),
            strEl = createAlbumTemplate( response );

          // remove message about empty media list;
          $('#tab-albums').find('.mlist-empty').remove();

          con.prepend( strEl );
          imgNormalizeDisplay( con );
          showMsg( con.parent(), 'Album successfully created!', 2000 );
          modal.modal('hide');
        }
       },
       error: function( textStatus ) {
         console.log( textStatus );
       }
      });
    } else {
      // validation error handling
      console.log(b);
    }
  }

  // /_album/add_files/
  function approveAddPhotoVid( actionContext ) {
    var modal = $('.serv-modal');
    if( modal && !modal.length ) { return }

    var b = true,
      fd = new FormData(),
      url = '';

    if( actionContext == 'al-detail' ) {
      var albumIdInput = modal.find('[name="album-id"]');
      if( albumIdInput && albumIdInput.length &&  albumIdInput.val() ) {
        fd.append('album_id', albumIdInput.val());
      }
      url = '/_album/add_files/';
    } else {
      url = '/_photo/photo-video-upload/';
    }

    // collect files;
    for( var i = 0; len = arrMedia.length, i < len; i++ ) {
      if( arrMedia[i] && arrMedia[i].file ) {
        fd.append('file', arrMedia[i].file);
      }
    }

    fd.append('description', modal.find('[name=photovid-desc]').val());

    if( b ) {
      var t = $('.tab-content.active'),
        tCon = t.find('.tab-inner');
      showLoader( modal.find('.content'), 'on' );

      $.ajax({
       url: url,
       type: 'POST',
       data: fd,
       cache: false,
       contentType: false,
       processData: false,
       success: function( response ) {

        resetForm( modal, 'photovid-add' );
        showLoader( modal.find('.content'), 'off' );

        if( response ) {
          // remove message about empty media list;
          tCon.find('.mlist-empty').remove();

          for( var i = 0; len = response.length, i < len; i++ ) {
            tCon.find('.row').prepend( createGalleryItemTemplate( response[i] ) );
          }
          imgNormalizeDisplay( tCon );
          showMsg( tCon, 'Gallery elements successfully added!', 2000 );
          modal.modal('hide');
        }
       },
       error: function( textStatus ) {
         console.log( textStatus );
       }
      });
    } else {
      // validation error handling
      console.log(b);
    }
  }

  // approve edit action
  function approveEditAlbum( id ) {
    var m = $('.serv-modal'),
      fd = new FormData();

    // collect data
    fd.append('name', m.find('[name=alubm-name]').val());
    fd.append('description', m.find('[name="album-desc"]').val());
    if( m.find('#album-cover')[0].files[0] ) {
      fd.append('album_art', m.find('#album-cover')[0].files[0]);
    }
    $.ajax({
      url: '/api/album/update/' + id + '/',
      type: 'PUT',
      data: fd,
      cache: false,
      contentType: false,
      processData: false,
      statusCode: {
        200: function() {
          console.log('Album successfully updated!');

          var con = $('.tab-content.active');
          showMsg( con.find('.tab-inner'), 'Album successfully updated!', 2000 );
          m.modal('hide');
        },
        204: function() {
          console.log('Album successfully updated!');

          var con = $('.tab-content.active');
          showMsg( con.find('.tab-inner'), 'Album successfully updated!', 2000 );
          m.modal('hide');
        }
      }
    });
  }

  function approveEditPhoto( id ) {
    var m = $('.serv-modal');

    $.ajax({
      url: '/api/photo/update/' + id + '/',
      type: 'PUT',
      data: {
        album_id: m.find('.album-list [name=album]').val(),
        description: m.find('[name="photovid-desc"]').val(),
        set_avatar: m.find('#asprofile-img').prop('checked')
      },
      statusCode: {
        200: function() {
          console.log('Photo successfully updated!');

          var con = $('.tab-content.active');
          showMsg( con.find('.tab-inner'), 'Photo successfully updated!', 2000 );
          m.modal('hide');
        },
        204: function() {
          console.log('Photo successfully updated!');

          var con = $('.tab-content.active');
          showMsg( con.find('.tab-inner'), 'Photo successfully updated!', 2000 );
          m.modal('hide');
        }
      }
    });
  }

  function approveEditVideo( id ) {
    var m = $('.serv-modal');

    $.ajax({
      url: '/api/video/update/' + id + '/',
      type: 'PUT',
      data: {
        album_id: m.find('.album-list [name=album]').val(),
        description: m.find('[name="photovid-desc"]').val()
      },
      statusCode: {
        200: function() {
          console.log('Video successfully updated!');

          var con = $('.tab-content.active');
          showMsg( con.find('.tab-inner'), 'Video successfully updated!', 2000 );
          m.modal('hide');
        },
        204: function() {
          console.log('Video successfully updated!');

          var con = $('.tab-content.active');
          showMsg( con.find('.tab-inner'), 'Video successfully updated!', 2000 );
          m.modal('hide');
        }
      }
    });
  }


  function showLoader( targetStage, state, text ) {
    var stage = $(targetStage);

    if( stage && !stage.length ) { return }
    text = text || 'on';
    state = state || 'off';

    var loader = ( text == 'on') ? '<div class="ui text loader">Loading</div>' : '<div class="ui loader"></div>';

    if( state == 'on' ) {
      stage.prepend(
          '<div class="ui dimmer">' +
              loader +
            '</div>'
        ).find('.ui.dimmer')
          .dimmer({ closable: false })
          .dimmer('show');
    } else {
      stage.find('.ui.dimmer').fadeOut(250, function() {
        $(this).remove();
      });
    }
  }

  function showMsg( container, msgText, timeLife ) {
    // timeLife - time life of msg[ time in milliseconds ];
    var con = $(container);
    if( con && !con.length ) { return }

    var msg = con.find('#modal-msg');
    if (!(msg && msg.length)) {
      con.prepend(
        '<div class="ui success message" id="modal-msg">' +
        '<i class="close icon"></i>' +
        '<div class="header">' + msgText + '</div>' +
        '</div>'
      );
    } else {
      msg.replaceWith(
        '<div class="ui success message" id="modal-msg">' +
        '<i class="close icon"></i>' +
        '<div class="header">' + msgText + '</div>' +
        '</div>'
      );
    }
    msg = con.find('#modal-msg');

    if( timeLife ) {
      setTimeout(function() {
        msg.fadeOut(250, function() {
          $(this).remove();
        });
      }, timeLife);
    }
  }

  /*
  * ob = {
  *   type: 'photovid' || 'photo' || 'vid' || 'album'
  *   action: 'add' || 'edit'
  *   params: {
  *     id: '#id',
  *     mformClearContextKey: '',
  *     sizeType: 'tiny' || 'small',
  *     cssClass: 'album' || 'photo-video' || 'edit-photo-video'
  *   }
  * }
  * */
  function setModalParams( ob, modal ) {
    if(isEmpty(ob)) { return };

    var modalSizeTypeClasses = ['tiny', 'small'],
        contentCustomClasses = ['album', 'photo-video', 'edit-photo-video'];

    modal.attr('id', ob.params.id);
    modal.attr('data-title', ob.params.formClearContextKey);
    modal.find('.header .txt').text(getModalTitle( ob.type, ob.action ));
    modal.find('.header .close').attr('data-pid', ('#' + ob.params.id));

    if( modalSizeTypeClasses.indexOf( ob.params.sizeType ) !== -1 ) {
      modal
        .removeClass( modalSizeTypeClasses.toString().replace(/,/g, ' ') )
        .addClass( ob.params.sizeType );
    }

    var modalContent = modal.find('.content');
    if( contentCustomClasses.indexOf( ob.params.cssClass ) !== -1 ) {
      modalContent
        .removeClass( contentCustomClasses.toString().replace(/,/g, ' ') )
        .addClass( ob.params.cssClass );
    }
    modalContent
      .html( createModalContent( ob.type, ob.action ) );
    modalContent.find('.approve')
      .attr('data-ptype', ob.type);
  }

  /*
  * ob = {
  *   type: 'photovid' || 'photo' || 'vid' || 'album'
  *   action: 'add' || 'edit'
  * }
  * */
  function createModalContent( type, action ) {
    var modals = {
      album: {
        add: createModalContentAlbum,
        edit: createModalContentAlbum
      },
      photo: {
        edit: createModalContentEditPhoto
      },
      vid: {
        edit: createModalContentEditVideo
      },
      photovid: {
        add: createModalContentAddPV
      }
    };

    return ( modals[ type ] ) ? modals[type][action](action) : '';
  }

  function createModalContentAlbum(action) {
    var btns = '';
    if( action == 'add' ) {
      btns = '<div class="btn-area">' +
                '<button type="button" data-action="add" class="ui orange button approve txt-uppercase">Create</button>' +
            '</div>';
    } else {
      btns = '<div class="btn-area">' +
                '<button type="button" data-action="edit" class="ui orange button approve txt-uppercase">Save changes</button>' +
                '<button type="button" data-action="del" class="ui grey button approve txt-uppercase">Delete album</button>' +
            '</div>';
    }
    return '<form>' +
                '<div class="row">' +
                    '<div class="col">' +
                        '<div class="field">' +
                            '<label>Name of album</label>' +
                            '<input type="text" name="alubm-name" class="fitem">' +
                        '</div>' +
                        '<div class="field">' +
                            '<label>Description of album</label>' +
                            '<textarea name="album-desc" placeholder="Something about this album" rows="5" class="fitem"></textarea>' +
                        '</div>' +
                    '</div>' +
                    '<div class="col">' +
                        '<div class="field">' +
                            '<label>Select the album art</label>' +
                            '<div class="input-file">' +
                                '<label for="album-cover"></label>' +
                                '<input type="file" id="album-cover" name="album-cover" accept="image/*">' +
                            '</div>' +
                        '</div>' +
                    '</div>' +
                '</div>' +
                btns +
            '</form>';
  }

  function createModalContentAddPV() {
    var tagFriendField = '<div class="field" style="display: none;">' +
        '<label>Tag your friend</label>' +
        '<div class="ui fluid multiple search selection dropdown f-list">' +
            '<input type="hidden" name="friends">' +
            '<i class="dropdown icon"></i>' +
            '<div class="default text">Select friends</div>' +
            '<div class="menu"></div>' +
        '</div>' +
    '</div>'; // do not delete;

    return '<form>' +
              '<div class="field">' +
                  '<label>Select photo/video</label>' +
                  '<div class="preview-media">' +
                      '<div class="input-file pm-trigger">' +
                          '<label for="media"></label>' +
                          '<input type="file" id="media" name="media" accept="image/*, video/*" multiple>' +
                      '</div>' +
                  '</div>' +
              '</div>' +
              '<div class="field">' +
                  '<label>Description</label>' +
                  '<textarea name="photovid-desc" rows="4" class="fitem"></textarea>' +
              '</div>' +
              '<div class="btn-area">' +
                  '<button type="button" data-action="add" class="ui orange button txt-uppercase approve">Add</button>' +
              '</div>' +
          '</form>';
  }

  function createModalContentEditPhoto() {
    return '<form>' +
              '<div class="row">' +
                  '<div class="col">' +
                      '<div class="present-img"></div>' +
                      '<div class="chb-outer" data-action="asprofile-img">' +
                          '<div class="chb-item">' +
                              '<input type="checkbox" id="asprofile-img" name="asprofile-img" data-chb="img-id">' +
                              '<label for="asprofile-img">' +
                                  '<span class="chb-custom"></span>' +
                                  '<span class="title">Make profile picture</span>' +
                              '</label>' +
                          '</div>' +
                      '</div>' +
                  '</div>' +
                  '<div class="col">' +
                      '<div class="field">' +
                          '<label>Description</label>' +
                          '<textarea name="photovid-desc" rows="6" class="fitem"></textarea>' +
                      '</div>' +
                      '<div class="field">' +
                          '<label>Move to the other album</label>' +
                          '<div class="ui fluid search selection dropdown album-list">' +
                              '<input type="hidden" name="album">' +
                              '<i class="dropdown icon"></i>' +
                              '<div class="default text">Choose the album</div>' +
                              '<div class="menu"></div>' +
                          '</div>' +
                          '<div class="note">* Select the album to move this photo</div>' +
                      '</div>' +
                      '<div class="btn-area" style="margin-top: 55px;">' +
                          '<button type="button" data-action="edit" class="ui orange button txt-uppercase approve">Save changes</button>' +
                          '<button type="button" data-action="del" class="ui grey button txt-uppercase approve">Delete photo</button>' +
                      '</div>' +
                  '</div>' +
              '</div>' +
          '</form>';
  }

  function createModalContentEditVideo() {
    return '<form>' +
              '<div class="row">' +
                  '<div class="col">' +
                      '<div class="present-img"></div>' +
                  '</div>' +
                  '<div class="col">' +
                      '<div class="field">' +
                          '<label>Description</label>' +
                          '<textarea name="photovid-desc" rows="6" class="fitem"></textarea>' +
                      '</div>' +
                      '<div class="field">' +
                          '<label>Move to the other album</label>' +
                          '<div class="ui fluid search selection dropdown album-list">' +
                              '<input type="hidden" name="album">' +
                              '<i class="dropdown icon"></i>' +
                              '<div class="default text">Choose the album</div>' +
                              '<div class="menu"></div>' +
                          '</div>' +
                          '<div class="note">* Select the album to move this video</div>' +
                      '</div>' +
                      '<div class="btn-area" style="margin-top: 55px;">' +
                          '<button type="button" data-action="edit" class="ui orange button txt-uppercase approve">Save changes</button>' +
                          '<button type="button" data-action="del" class="ui grey button txt-uppercase approve">Delete video</button>' +
                      '</div>' +
                  '</div>' +
              '</div>' +
          '</form>';
  }


  function getModalTitle( type, action ) {
    var title = {
      album: {
        add: 'Create album',
        edit: 'Edit album'
      },
      photo: {
        edit: 'Edit photo'
      },
      vid: {
        edit: 'Edit video'
      },
      photovid: {
        add: 'Add photo/video'
      }
    };

    return ( title[ type ] ) ? title[type][action] : '';
  }

  function getModalFormClearContextKey( type, action ) {
    var context = {
      album: {
        add: 'album',
        edit: 'album'
      },
      photovid: {
        add: 'photovid-add'
      }
    };
    return ( context[type] ) ? context[type][action] : '';
  }


  function getMediaDetail(id, type) {
    var url = '/_comment/get_for_gallery/',
      type = ( type == 'image' ) ? 'photo' : 'video';
    $.post(url, { object_id: id, content_type: type }, function( data ) {
      if( data ) {
        var con = $('.about-area');
        con.find('.inner-con').html( data );
      }
    }, 'html');
  }


  // set default data on edit modal
  function setEditModalDefaultData( p ) {
    if( isEmpty( p ) ) { return; }
    var type = {
          album: setEditAlbumModal,
          photo: setEditPhotoModal,
          vid: setEditVideoModal
        };

    if( Object.keys( type ).indexOf( p.type ) !== -1 ) {
      type[p.type]( p.con, p.id );
    }
  }

  function setEditAlbumModal( m, id ) {
    if( m && !m.length && id ) { return }

    $.get('/api/album/detail/' + id, function( data ) {
      m.find('.input-file')
        .addClass('img')
        .prepend('<div class="preview-img"><img src="' + data.album_art + '"></div>');
      m.find('[name="alubm-name"]').val( data.name );
      m.find('[name="album-desc"]').val( data.description );

      imgNormalizeDisplay( m );
    });
  }

  function setEditPhotoModal( m, id ) {
    if( m && !m.length && id ) { return }

    $.get('/api/photo/detail/' + id, function( data ) {
      if( !data ) { return };

      m.find('.present-img').html('<img src="' + data.image + '">');
      if( data.is_avatar ) {
        m.find('[data-action="asprofile-img"]').remove();
      }
      m.find('[name="photovid-desc"]').val( data.description );

      // get album list
      var albumId = data.in_albums;
      $.get('/api/album/list/', function( list ) {
        if( list ) {
          var idList = [],
            albumList = m.find('.album-list'),
            str = '';

          for( var i = 0; len = list.length, i < len; i++ ) {
            idList.push(list[i].id);
            str += '<div class="item" data-value="' + list[i].id + '"><span class="option-icon"><img src="' + list[i].album_art + '"></span>' + list[i].name + '</div>';
          }
          albumList.find('.menu').html( str );
          albumList.dropdown();

          var rs = intersect( albumId, idList ); // get album id, which contains this media item: photo
          if( rs.length ) {
            albumList.dropdown('set selected', rs[0]);
          }
        }
      });

      imgNormalizeDisplay( m.find('.present-img') );
    });
  }

  function setEditVideoModal( m, id ) {
    if( m && !m.length && id ) { return }

    $.get('/api/video/detail/' + id, function( data ) {
      m.find('.present-img').html('<img src="' + data.thumbnail + '">');
      m.find('[name="photovid-desc"]').val( data.description );

      // get album list
      var albumId = data.in_albums;
      $.get('/api/album/list/', function( list ) {
        if( list ) {
          var idList = [],
            albumList = m.find('.album-list'),
            str = '';

          for( var i = 0; len = list.length, i < len; i++ ) {
            idList.push(list[i].id);
            str += '<div class="item" data-value="' + list[i].id + '"><span class="option-icon"><img src="' + list[i].album_art + '"></span>' + list[i].name + '</div>';
          }
          albumList.find('.menu').html( str );
          albumList.dropdown();

          var rs = intersect( albumId, idList ); // get album id, which contains this media item: video
          if( rs.length ) {
            albumList.dropdown('set selected', rs[0]);
          }
        }
      });

      imgNormalizeDisplay( m.find('.present-img') );
    });
  }
});