var wsRoom,
	currentRoomId,
	roomList = $('.chat-flist a.friend__usercard');

$(function() {
	// on /chat/ location, check unread message states;
	if( checkWLPathName( /\/chat\/$/ig ) ) {
		var unreadMsg = $('.chat .unread-msgcount');
		for( var i = 0; len = unreadMsg.length, i < len; i++ ) {
			var unreadMsgCount = parseInt( unreadMsg.eq(i).text() );
			if( !isNumeric( unreadMsgCount ) ) {
				unreadMsg.eq(i).text('').hide();
			}
		}
	}

	// handling click event(change room);
	roomList.click(function( e ) {
		e.preventDefault();
		e.stopPropagation();
		$(this).removeClass('inactive');

		roomList.not( $(this) )
			.addClass('inactive')
			.removeClass('on');
		$(this).addClass('on');
		$('.chat-area .chat-preloader').addClass('active');

		// show 'message displayBoard'
		$('.chat-area .chat-pretitle').remove();
		var dboard = $('.chat-area .chat-areamain');
		if( dboard.css('display') == 'none' ) {
			dboard.show();
			console.log('it was display none!');
		}

		// change count of unread messages;
		var um = $(this).find('.unread-msgcount'),
			umCount = parseInt( um.text() );
		if( !isNaN( umCount ) && umCount != null ) {
			var currentCount = getCookie('notifyCount');
			currentCount = ( currentCount != null && !isNaN( currentCount ) ) ? parseInt( currentCount ) : 0;
			umCount = ( currentCount > umCount ) ? ( currentCount - umCount ) : 0;
			setCookie('notifyCount', umCount, {path: '/'});

			if( umCount == 0 ) {
				$('a.notify')
					.removeClass('notify-on')
					.addClass('notify-off')
					.find('.notify-count').text('');
			} else {
				$('a.notify').find('.notify-count').text('+ ' + umCount);
			}
		}
		um.text('').hide();

		// reinitialize chat room(websocket connection);
		var newRoomId = $(this).data('room-id'),
			lastSendedMsg = $(this).find('.state-info'),
			currUserId = $('#user').val();
		if( currentRoomId != newRoomId ) {
			currentRoomId = newRoomId;
			initRoom( newRoomId );
			console.log('Connedted to: ' + currentRoomId);

			// add message to 'message displayBoard'
			wsRoom.onmessage = function( message ) {
				var data = JSON.parse( message.data ),
					currentUserName = $('#current_user_username').val(),
					scrollHeight = 0,
					shortMsg = '',
					insertData;

				console.log( message );

				if( currentUserName == data.user.username ) {
					insertData = {
						user: {
							sender: {
								avatar: data.user.avatar
							},
							receiver: {},
						},
						message: {
							msg_type: 'sent',
							msg_text: data.message,
							msg_created: new Date(),
							msg_sender: data.user.username,
							msg_receiver: ''
						}
					}
				} else {
					insertData = {
						user: {
							sender: {},
							receiver: {
								avatar: data.user.avatar
							}
						},
						message: {
							msg_type: 'receive',
							msg_text: data.message,
							msg_created: new Date(),
							msg_sender: data.user.username,
							msg_receiver: ''
						}
					}
				}

				// insert message element to DOM;
				addToDOM(
					createMsgTemplate( insertData ),
					'.msg-display',
					'append'
				);

				// set last sended message to usercard;
				var prefix = '';
				prefix = ( data.user.id == currUserId) ? 'You: ' : '';
				lastSendedMsg.text(prefix + insertData.message.msg_text.trim());

				// set scroll to last message;
				scrollHeight = $('.msg-display')[0].scrollHeight;
				$('.msg-display').scrollTop(scrollHeight);
			}

			wsRoom.onclose = function(e) {
				console.log(newRoomId + ' room: CLOSED');
				console.log('code: ' + e.code + ' reason: ' + e.reason);
			}

			wsRoom.onerror = function(e) {
				console.log('error: ' + e.message);
			}
		}

		// init message items
		getMsgDisplayItems(
			$('#user_').val(),
			$(this).data('user-id'),
			newRoomId
		)
	});


	$('#msg-send').click(function( e ) {
		sendMessage( $('#msg').val() );
		clearTextarea( '#msg' );
	});


	$('#msg')
		.on('keyup', function( e ) {
			if( e.keyCode == 13 && !e.shiftKey ) {
				sendMessage( e.target.value );
				clearTextarea( e.target );
			}
			tAutoGrowth(this);

		})
		.on('paste', function() {
			tAutoGrowth(this);
		})
		.on('cut', function() {
			tAutoGrowth(this);
		})
		.on('keydown', function(e) {
			if( e.keyCode == 13 && !e.shiftKey ) {
				return false;
			}
		});


	var b = true,
		requestedRoomId = $('#req_room').val(),
		msgDisplayBoard = $('.msg-display');

	if( requestedRoomId ) {
		chatRoomInitByRequset( requestedRoomId );
	}

	msgDisplayBoard.on('scroll', function() {
		if( this.scrollTop == 0 ) {
			if( b ) {
				var scHeight = this.scrollHeight,
					lastMsgId = getLastMsgId('.chat .msg-display .msg-item', 'id'),
					activeRoomId = $('.chat .friend__usercard').not('.inactive').eq(0).data('room-id');
				console.log('room_id: ' + activeRoomId);
				console.log('message_id: ' + lastMsgId);

				var start = new Date();
		    	$('.chat-area .chat-preloader').addClass('active');
				$.ajax({
					url: $('#msg-form').attr('action'),
					type: 'POST',
					data: "room_id=" + activeRoomId + "&message_id=" + lastMsgId,
					success: function( response ) {
				    	b = response.length != 0;
						executeTime( start );
						refreshMsgDisplay(response, '.msg-display', true);

						var tempScHeight = msgDisplayBoard[0].scrollHeight;
						msgDisplayBoard.scrollTop( tempScHeight - scHeight );
					},
					error: function( err ) {
						console.log( err.text );
					}
				});
			} else {
				console.log('content is empty!');
			}
		}
	});

});


function sendMessage( message ) {
	message = message.trim();
	if( message != '' ) {
		wsRoom.send( message );
	}
}


function createMsgTemplate( data ) {
	var str = '',
		cssMsgType = '',
		avatar = '',
		tempDate = new Date( data.message.msg_created ).toLocaleTimeString();

	tempDate = tempDate.substr(0, 5);
	if( data.message.msg_type == 'sent' ) {
		cssMsgType = 'msg-right';
		avatar = ( data.user.sender ) ? data.user.sender.avatar : ''
	} else {
		cssMsgType = 'msg-left';
		avatar = ( data.user.receiver ) ? data.user.receiver.avatar : '';
	}

	
	if( typeof( avatar ) == 'object' ) {
		str = $('<div>', {
			class: 'msg-item ' + cssMsgType,
			'data-id': data.message.msg_id
		})
		.append(
			$('<div>', {
				class: 'msg__author'
			})
			.append(avatar)
		)
		.append(
			$('<div>', {
				class: 'msg__content',
			})
			.append(
				$('<div>', {
					class: 'msg__content-text',
					text: data.message.msg_text
				})
			)
			.append(
				$('<div>', {
					class: 'msg__content-send',
				})
				.append(
					$('<span>', {
						class: 'msg__send-time',
						text: tempDate
					})
				)
				.append('<span class="msg__send-state"><i class="check circle outline icon"></i></span>')
			)
		);
	} else {
		// creating string element;
		str = '<div class="msg-item ' + cssMsgType + '" data-id="' + data.message.msg_id + '">' +
			'<div class="msg__author"><img src="' + avatar + '"></div>' +
			'<div class="msg__content">' +
			'<div class="msg__content-text">' + data.message.msg_text + '</div>' +
			'<div class="msg__content-send">' +
			'<span class="msg__send-time">' + tempDate + '</span>' +
			'<span class="msg__send-state"><i class="check circle outline icon"></i></span>' +
			'</div>';
	}

	return str;
}


function getMsgDisplayItems( currentUserId, targetUserId, roomId ) {
	var data,
		start = new Date();
	$.ajax({
        url: $('#msg-form').attr('action'),
        type: "POST",
        data: "user=" + currentUserId + "&user_=" + targetUserId + "&room_id=" + roomId,
        success: function( response ) {
        	executeTime( start );
        	refreshMsgDisplay(response, '.msg-display');
        },
        error: function( err ) {
            console.log(err);
        }
    });
}


function refreshMsgDisplay( content, target, rewriteOff ) {
	var scrollHeight = 0,
		datePoint = '',
		anchorElement = '',
		anchorDate = '',
		currentUserAvatar = $('#profileImg'),
		chatFriendAvatar = $('.chat .friend__usercard').not('.inactive').eq(0).find('img'),
		addType = 'prepend';

	if( !rewriteOff ) {
		$( target ).children().remove();
		addType = 'append';
		datePoint = new Date().toDateString();
		addToDOM(
			createDateLine( getDateString( datePoint ), 'on' ),
			target,
			addType
		);
	}

	if( !content.length ) {
		$('.chat-area .chat-preloader').removeClass('active');
		return
	}

	currentUserAvatar.attr('title', '');
	chatFriendAvatar.attr('title', '');

	anchorElement = $('.chat__date[data-anchor=on]').eq(0);
	anchorDate = recoverDateFromString( anchorElement.find('span').text() );
	for( var i = 0; len = content.length, i < len; i++ ) {
		var tempDate = new Date( content[i].created ).toDateString(),
			data = {
				user: {
					sender: {
						avatar: currentUserAvatar.clone()
					},
					receiver: {
						avatar: chatFriendAvatar.clone()
					}
				},
				message: {
					msg_id: content[i].message_id,
					msg_type: content[i].message_type,
					msg_text: content[i].text,
					msg_created: content[i].created,
					msg_sender: content[i].sender,
					msg_receiver: content[i].receiver
				}
			}

		if( datePoint != tempDate ) {
			datePoint = tempDate;
			if( anchorDate != datePoint ) {
				anchorDate = datePoint;
			
				// off last anchor;
				anchorElement.attr('data-anchor', 'off');

				// search position of anchor element;
				anchorElement = searchAnchorPosition( anchorDate, $('.chat__date') );

				// create new date line html element;
				insertToDOM(
					createDateLine(	getDateString( datePoint ), 'on' ),
					anchorElement,
					'before'
				);

				// set new element to anchorElement;
				anchorElement = $('.chat__date[data-anchor=on]').eq(0);
				anchorDate = recoverDateFromString( anchorElement.find('span').text() );
			}
		}

		// insert message element to DOM
		insertToDOM( createMsgTemplate( data ), anchorElement, 'after' );
	}

	$('.chat-area .chat-preloader').removeClass('active');
	if( !rewriteOff ) {
		scrollHeight = $(target)[0].scrollHeight;
		$(target).scrollTop(scrollHeight);
	}
}


function initRoom( roomId ) {
	if( typeof( wsRoom ) == 'object' ) {
		if( wsRoom.readyState == 3 ) {
			wsRoom = new WebSocket(protocol + window.location.host + '/chat/' + roomId);
		} else {
			wsRoom.close();
			wsRoom = new WebSocket(protocol + window.location.host + '/chat/' + roomId);
		}
	} else {
		wsRoom = new WebSocket(protocol + window.location.host + '/chat/' + roomId);
	}
}


function executeTime( startTime ) {
	console.log('executeTime(seconds): ' + ( new Date() - startTime ) / 1000 );
}

/*
*	get date string;
*	date format: month_string day_number, year_number
*	(example: October 12, 2017);
*/
function getDateString( dateString ) {
	var day, month, year,
		arrMonth = [	
		'January',
		'February',
		'March',
		'April',
		'May',
		'June',
		'July',
		'August',
		'September',
		'October',
		'November',
		'December'
	];

	if( dateString == '' || dateString == null ) { return }
 	day = new Date( dateString ).getDate();
 	month = new Date( dateString ).getMonth();
 	year = new Date( dateString ).getFullYear();

	year = ( year == new Date().getFullYear() ) ? '' : (', ' + year);
	return arrMonth[month] + ' ' + day + year;
}

// create html template; date line for sorting message by date;
function createDateLine( dateText, anchor ) {
	return ( dateText != '' ) ? 
			( '<div class="chat__date" ' + (( anchor ) ? ('data-anchor=' + anchor) : '') + '>' +
			 	'<div class="chat__date-con">' +
				'<span>' + dateText + '</span>' +
				'</div>' +
				'</div>'
			)
			: '';
}

/*
*	return normal dateString;
*	input string format: month_string day_number, year_number
*	(example: October 12, 2017);
*/
function recoverDateFromString( string ) {
	var tempArr = string.split(' '),
		year = ( tempArr.length < 3 ) ? new Date().getFullYear() : tempArr[2];
	return new Date( tempArr[0] + ' ' + tempArr[1] + ' ' + year ).toDateString();
}

function getBoundaryElement( referenceAnchor, boundaryPosition ) {
	return ( boundaryPosition == 'next' ) ?
				$(referenceAnchor).next() :
				$(referenceAnchor).prev();
}

function searchAnchorPosition( anchorDate, anchorList ) {
	var anchorPos = 0,
		anchorDateList = [];
	if( anchorDate == '' ) { return; }
	if( !anchorList.length ) { return anchorDate; }
	if( anchorList.length == 1 ) { return anchorList.eq(0); }

	for( var i = 0; len = anchorList.length, i < len; i++ ) {
		anchorDateList[i] = recoverDateFromString( anchorList.eq(i).find('span').text() );
	}
	anchorDateList.push( anchorDate );
	anchorDateList.sort(function( a, b ) {
		return new Date( a ) - new Date( b );
	});

	anchorPos = anchorDateList.indexOf( anchorDate );
	return anchorList.eq( anchorPos );
}


function isMobile() {
	var isMobile = false; //initiate as false
	// device detection
	if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) 
	    || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))) isMobile = true;

	return isMobile;
}

function chatRoomInitByRequset( roomId ) {
	var room = $('.chat-flist a.friend__usercard[data-room-id=' + roomId + ']');
	console.log( room );
	room.trigger('click');
}