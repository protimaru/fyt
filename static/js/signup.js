$(document).ready(function() {

  $('.ui.dropdown').dropdown();

  // get country list
  var country = $('#sf-country');
  $.get( '/api/geodata/countries', function( data ) {
    if( data ) {
      for( var i = 0; len = data.length, i < len; i++ ) {
        country.append(
          '<option value="' + data[i].sortname + '_' + data[i].id + '">' + data[i].name + '</option>'
        );
      }
    }
  });

  // get cities list
  var selects = $('.sf-input-gr .cc select');
  if( selects && selects.length ) {
    selects.change(function() {
      if( $(this).data('cat') == 'country' ) {
        var val = $(this).val().split('_'),
          defaultCity = $(this).find('option:selected').text();
        if( val[0] ) {
          $.get(
            '/api/geodata/cities/',
            { name: val[0].toUpperCase() },
            function( response ) {
              console.log( response );
              var cities;
              if( response instanceof Array && response.length ) {
                cities = response;
              } else {
                cities = [{
                  id: defaultCity,
                  name: defaultCity
                }];
              }

              var cityList = $('#sf-city');
              cityList.children().remove();
              cityList.dropdown('clear');
              if( cityList && cityList.length ) {
                for( var i = 0; len = cities.length, i < len; i++ ) {
                  cityList.append('<option value="' + cities[i].id + '" data-title="' + cities[i].name + '">' + cities[i].name + '</option>');
                }
              }
            }
          )
        }
      }

      if( $(this).val() != '' ) {
        errMsg( $(this).parent().parent(), 'off' );
      } else {
        errMsg( $(this).parent().parent(), 'on', 'This fields is required!' );
      }
    });
  }

  // check first_name and last_name
  $('#sf-fname').change(function() {
    console.log( $(this).val() );
    if( $(this).val() != '' ) {
      if( $(this).val().length > 1 ) {
        errMsg( $(this).parent(), 'off' );
      } else {
        errMsg( $(this).parent(), 'on', 'This field required at least 2 characters!' );
      }
    } else {
      errMsg( $(this).parent(), 'on', 'This field is required!' );
    }
  });
  $('#sf-fname').focus(function() {
    errMsg( $(this).parent(), 'off' );
  });
  $('#sf-fname').focusout(function() {
    if( $(this).val() != '' ) {
      if( $(this).val().length > 1 ) {
        errMsg( $(this).parent(), 'off' );
      } else {
        errMsg( $(this).parent(), 'on', 'This field required at least 2 characters!' );
      }
    } else {
      errMsg( $(this).parent(), 'on', 'This field is required!' );
    }
  });

  // check last_name and last_name
  $('#sf-lname').change(function() {
    console.log( $(this).val() );
    if( $(this).val() != '' ) {
      if( $(this).val().length > 1 ) {
        errMsg( $(this).parent(), 'off' );
      } else {
        errMsg( $(this).parent(), 'on', 'This field required at least 2 characters!' );
      }
    } else {
      errMsg( $(this).parent(), 'on', 'This field is required!' );
    }
  });
  $('#sf-lname').focus(function() {
    errMsg( $(this).parent(), 'off' );
  });
  $('#sf-lname').focusout(function() {
    if( $(this).val() != '' ) {
      if( $(this).val().length > 1 ) {
        errMsg( $(this).parent(), 'off' );
      } else {
        errMsg( $(this).parent(), 'on', 'This field required at least 2 characters!' );
      }
    } else {
      errMsg( $(this).parent(), 'on', 'This field is required!' );
    }
  });


  // check username to exists
  $('#sf-username').change(function() {
    checkUName( $(this) );
  });
  $('#sf-username').focus(function() {
    errMsg( $(this).parent(), 'off' );
  });
  $('#sf-username').focusout(function() {
    checkUName( $(this) );
  });

  // check email to exists
  $('#sf-email').change(function() {
    checkUName( $(this) );
  });
  $('#sf-email').focus(function() {
    errMsg( $(this).parent(), 'off' );
  });
  $('#sf-email').focusout(function() {
    checkUName( $(this) );
  });


  // check date
  var dateParentCon = $('.sf-input-gr.date');
  $('.ui.dropdown.date').focus(function() {
    errMsg( dateParentCon, 'off' );
  });
  $('.ui.dropdown.date').focusout(function() {
    if( $('#sf-day').val() && $('#sf-month') && $('#sf-year') ) {
      errMsg( dateParentCon, 'off' );
    } else {
      errMsg( dateParentCon, 'on', 'This fields are required!' );
    }
  });

  // check gender
  $('#sf-gender input[type=hidden]').change(function() {
    if( $(this).val() != '' ) {
      errMsg( $(this).parent().parent(), 'off' );
    } else {
      errMsg( $(this).parent().parent(), 'on', 'This fields is required!' );
    }
  });
  $('#sf-gender').focus(function() {
    errMsg( $(this).parent(), 'off' );
  });
  $('#sf-gender').focusout(function() {
    var input = $(this).find('input[type=hidden]');
    if( input.val() == '' ) {
      errMsg( $(this).parent(), 'on', 'This fields is required!' );
    }
  });

  // check city
  $('.ui.dropdown.cc').focus(function() {
    errMsg( $(this).parent(), 'off' );
  });
  $('.ui.dropdown.cc').focusout(function() {
    var select = $(this).find('select');
    if( select.val() == '' ) {
      errMsg( $(this).parent(), 'on', 'This fields is required!' );
    }
  });


  // check passwords confirmation
  var pswd = $('#sf-pswd'),
    conPswd = $('#sf-pswd-con');
  pswd[0].addEventListener('input', function() {
    if( $(this).val() == '' ) {
      errMsg( $(this).parent(), 'on', 'This field is required!' );
      conPswd.val('');
      errMsg( conPswd.parent(), 'off');
    } else {
      errMsg( $(this).parent(), 'off' );
      if( conPswd != '' ) {
        conPswd.val('');
        errMsg( conPswd.parent(), 'off');
      }
    }
  });
  pswd.focus(function() {
    errMsg( $(this).parent(), 'off' );
  });
  pswd.focusout(function() {
    if( $(this).val() == '' ) {
      errMsg( $(this).parent(), 'on', 'This field is required!' );
    }
  });

  conPswd.change(function() {
    if( checkPswdConfirmartion( $(this), pswd ) ) {
      errMsg( $(this).parent(), 'off' );
    } else {
      errMsg( $(this).parent(), 'on', 'Passwords don\'t match!' );
    }
  });
  conPswd.focus(function() {
    errMsg( $(this).parent(), 'off' );
  });
  conPswd.focusout(function() {
    if( checkPswdConfirmartion( $(this), pswd ) ) {
      errMsg( $(this).parent(), 'off' );
      if( pswd.val() == '' && $(this).val() == '' ) {
        errMsg( $(this).parent(), 'on', 'This field is required!' );
      }
    } else {
      errMsg( $(this).parent(), 'on', 'Passwords don\'t match!' );
    }
  });


  refreshDate($('#sf-day'), $('#sf-month'), $('#sf-year'));


  // send form
  $('#sf-send').click(function(e) {
    e.preventDefault();
    var counter = 0,
      fLength = 10;
    sendArr = {};

    var fName = $('#sf-fname');
    if( fName.val() != '' ) {
      if( fName.val().length > 1 ) {
        errMsg( fName.parent(), 'off' );
        sendArr['first_name'] = fName.val();
        counter++;
      } else {
        errMsg( fName.parent(), 'on', 'This field required at least 2 characters!' );
      }
    } else {
      errMsg( fName.parent(), 'on', 'This field is required!' );
    }

    var lName = $('#sf-lname');
    if( lName.val() != '' ) {
      if( lName.val().length > 1 ) {
        errMsg( lName.parent(), 'off' );
        sendArr['last_name'] = lName.val();
        counter++;
      } else {
        errMsg( lName.parent(), 'on', 'This field required at least 2 characters!' );
      }
    } else {
      errMsg( lName.parent(), 'on', 'This field is required!' );
    }

    var day = $('.date select[name=day]'),
      month = $('.date select[name=month]'),
      year = $('.date select[name=year]');
    if( isNumeric(day.val()) && isNumeric(month.val()) && isNumeric(year.val()) ) {
      sendArr['birth_date'] = year.val() + '-' + (+month.val() + 1) + '-' + day.val();
      counter++;
      errMsg( '.sf-input-gr.date', 'off' );
    } else {
      errMsg( '.sf-input-gr.date', 'on', 'This fields are required!' );
    }

    var gender = $('#sf-gender input[type=hidden]');
    if( gender.val() != '' ) {
      sendArr['gender'] = gender.val();
      counter++;
      errMsg( gender.parent().parent(), 'off' );
    } else {
      errMsg( gender.parent().parent(), 'on', 'This fields is required!' );
    }


    var country = $('#sf-country');
    if( country.val() != '' ) {
      sendArr['country'] = country.val().split('_')[1];
      counter++;
      errMsg( country.parent().parent(), 'off' );
    } else {
      errMsg( country.parent().parent(), 'on', 'This fields is required!' );
    }


    var city = $('#sf-city');
    if( city.val() != '' ) {
      if( isNumeric(city.val()) ) {
        sendArr['city'] = city.val();
      } else {
        sendArr['city'] = '';
      }
      counter++;
      errMsg( city.parent().parent(), 'off' );
    } else {
      errMsg( city.parent().parent(), 'on', 'This fields is required!' );
    }

    var username = $('#sf-username');
    if( username.val() != '' ) {
      if( username.attr('data-errstate') == 'on') {
        errMsg( username.parent(), 'on', 'Username already exist!' );
      } else {
        sendArr['username'] = username.val();
        counter++;
        errMsg( username.parent(), 'off' );
      }
    } else {
      errMsg( username.parent(), 'on', 'This field is required!' );
    }

    var email = $('#sf-email');
    if( email.val() != '' ) {
      if( email.attr('data-errstate') == 'on') {
        errMsg( email.parent(), 'on', 'Email already exist!' );
      } else {
        sendArr['email'] = email.val();
        counter++;
        errMsg( email.parent(), 'off' );
      }
    } else {
      errMsg( email.parent(), 'on', 'This field is required!' );
    }


    var pswd = $('#sf-pswd'),
      conPswd = $('#sf-pswd-con');
    if( pswd.val() != '' ) {
      if( checkPswdConfirmartion( pswd, conPswd ) ) {
        sendArr['password'] = pswd.val();
        sendArr['conf_password'] = conPswd.val();
        counter += 2;
        errMsg( pswd.parent(), 'off' );
        errMsg( conPswd.parent(), 'off' );
      } else {
        errMsg( conPswd.parent(), 'on', 'Passwords don\'t match!' );
      }
    } else {
      errMsg( pswd.parent(), 'on', 'This field is required!' );
      if( conPswd.val() != '' ) {
        conPswd.val('');
      }
      errMsg( conPswd.parent(), 'on', 'This field is required!' );
    }

    var addr = $('#sf-address'),
      phone = $('#sf-lname');

    if( addr.val() ) {
      sendArr['address'] = addr.val();
    }
    if( phone.val() ) {
      sendArr['phone'] = phone.val();
    }


    if( counter == fLength && !isEmpty( sendArr ) ) {
      $.ajax({
        type: 'POST',
        data: sendArr,
        success: function( response ) {
          console.log(response);
          $('#sign .wrapper')
            .prepend(
              '<div class="ui success message" style="display: none;">' +
              '<i class="close icon"></i>' +
              '<div class="header">Your user registration was successful.</div>' +
              '<p>You will be redirected to authorization page automatically in fiew seconds. To following the link manually: <a href="http://127.0.0.1:8000' + response.url + '" style="text-decoration: underline">Login</a></p>' +
              '</div>'
            )
            .find('.ui.message.success').slideDown(250);
          setTimeout(function() {
            window.location.href = window.location.origin + '/account/login/';
          }, 5000);
        },
        error: function( error ) {
          console.log( error );
        }
      });
    }
  });

});

function checkUName(element) {
  console.log('called!');
  if(element.val() != "") {
    $.ajax({
      method: 'POST',
      url: "/api/profile/check/email-username/",
      data: element.data('action') + "=" + element.val(),
      success: function(response) {
        if(response.status === 1) {
          var action = element.data('action').slice(0,1).toUpperCase() + element.data('action').slice(1);
          element.parent().find('.err-msg').remove();
          element.attr('data-errstate', 'on');
          $('<span class="err-msg">' + action + ' is already exists!</span>').insertAfter( element );
        }
        else {
          element.parent().find('.err-msg').remove();
          element.removeAttr('data-errstate');
        }
      }
    });
  } else {
    console.log('show msg!');
    errMsg( element.parent(), 'on', 'This field is required!' );
  }
}

function checkPswdConfirmartion( p1, p2 ) {
  var b = true;
  if( p1.val() != p2.val() ) {
    b = false;
  } else {
    b = true;
  }
  return b;
}

function checkPswds( pswdSelector, conPswdSelector ) {
  var pswd = $(pswdSelector),
    conPswd = $(conPswdSelector),
    b = false;
  if( (pswd && pswd.length) && (conPswd && conPswd.length) ) {
    if( pswd.val() != '' && conPswd != '' && pswd.val() == conPswd.val() ) {
      b = true;
    }
  }
  return b;
}