// check file extension
function checkFileExt( file, fileType ) {
  fileType = ( fileType ) ? fileType : 'image';
  if( fileType == 'image' ) {
    return (/\.(png|jpeg|jpg|gif)$/i).test(file.name)
  } else if( fileType == 'video' ) {
    return (/\.(avi|mp4|mkv)$/i).test(file.name)
  }
  return false;
}


/*
 * delete by index from array of objects
 * for arrays like
 *  [
 *      { index: 1, someprop: 'somval' },
 *      { index: 2, someprop: 'someval' }
 *  ]
 */
function deleteByIndexFromArray( array, index ) {
  if( array == null && array.length == 0 ) {
    return [];
  }
  if( !index ) {
    return array;
  }
  for( var i = 0; len = array.length, i < len; i++ ) {
    if( array[i].index == index ) {
      array.splice(i, 1);
    }
  }
  return array;
}


/*
 * generate str id
 * possible chars [a-zA-Z0-9]
 * len: int, max value <= possible.length, default: 6;
 * */
function generateID( len ) {
  var elID = '',
    length = ( isNumeric(len) ) ? len : 6,
    possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  length = ( length > possible ) ? possible.length : length;
  for(var i = 0; i < length; i++) {
    elID += possible.charAt(Math.floor(Math.random() * possible.length));
  }
  return elID;
}


// check to number type
function isNumeric(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}


// get random numbers between two values
function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min)) + min; //The maximum is exclusive and the minimum is inclusive
}


// check object to empty
function isEmpty( obj ) {
  for( var key in obj ) {
    if( obj.hasOwnProperty( key ) )
      return false;
  }
  return true;
}


/**
 * Shuffles array in place.
 * @param {Array} a items An array containing the items.
 */
function shuffle(a) {
  var j, x, i;
  for (i = a.length - 1; i > 0; i--) {
    j = Math.floor(Math.random() * (i + 1));
    x = a[i];
    a[i] = a[j];
    a[j] = x;
  }
}


// returns array with unique items;
Array.prototype.unique = function() {
  return this.filter(function (value, index, self) {
    return self.indexOf(value) === index;
  });
}


// intersection between to arrays;
function intersect(a, b) {
  var d = {};
  var results = [];
  for (var i = 0; i < b.length; i++) {
    d[b[i]] = true;
  }
  for (var j = 0; j < a.length; j++) {
    if (d[a[j]])
      results.push(a[j]);
  }
  return results;
}



function customSubStr( str, characterCount, startIndex ) {
  startIndex = startIndex || 0;
  characterCount = characterCount || 120;
  if( str.length > 150 ) {
    str = str.slice( startIndex, characterCount ) + '...';
  }
  return str;
}


$.fn.clearForm = function() {
  return this.each(function() {
    var type = this.type, tag = this.tagName.toLowerCase();
    if( tag == 'form' )
      return $(':input', this).clearForm();
    if( type == 'checkbox' || type == 'radio' )
      this.checked = false;
    else if( type != 'submit' && type != 'button' )
      this.value = '';
    else if ( tag == 'select' ) {
      var parent = $(this).parent();
      if( parent.hasClass('dropdown') ) {
        parent.dropdown('clear');
      } else {
        this.selectedIndex = -1;
      }
    }
  });
};


function checkWLPathName( protype ) {
  return protype.test( window.location.pathname.toString() );
}


// get earlier dated message ID;
function getLastMsgId( selector, attrName ) {
	var list = $(selector),
		minId = list.eq(0).data(attrName);
	for( var i = 0; len = list.length, i < len; i++ ) {
		if( minId > list.eq(i).data(attrName) ) {
			minId = list.eq(i).data(attrName);
		}
	}
	return minId;
}


function addToDOM( content, target, addType ) {
	( addType == 'append' ) ?
		$(target).append( content ) :
		$(target).prepend( content );
}

function insertToDOM( content, target, insertMode ) {
	if( content == '' &&  target == '' && insertMode == '' ) {
		return;
	}
	content = ( typeof( content ) == 'object' ) ? $('<div>').append(content).html() : content;
	if( insertMode == 'before' ) {
		$(content).insertBefore(target);
	} else {
		$(content).insertAfter(target);
	}
}

function tAutoGrowth(ob) {
	var minRows = ob.getAttribute('data-min-rows') | 0,
		maxRows = 10,
		rows = 0,
		fontSize = parseFloat( $(ob).css('font-size') ),
		lineHeight = parseFloat( $(ob).css('line-height') ) || fontSize * 1.2,
		padding = parseFloat( $(ob).css('padding-top') ) + parseFloat( $(ob).css('padding-bottom') ) || 0;

	ob.rows = minRows;
	rows = Math.ceil( ( ob.scrollHeight - padding ) / lineHeight );

	if( rows > maxRows ) {
		ob.rows = maxRows;
		$(ob).addClass('oflow');
	} else {
		ob.rows = rows;
		$(ob).removeClass('oflow');
	}
}

function clearTextarea( target ) {
	$(target).val('').focus();
}


// select date handling;
Date.prototype.daysInMonth = function() {
  return 33 - new Date(this.getFullYear(), this.getMonth(), 33).getDate();
};

function refreshDate(dTarget, mTarget, yTarget) {
  yTarget.change(function() {
    var val = $(this).val(),
      dVal = +(dTarget.val()),
      month = ( mTarget.val() ) ? mTarget.val() : new Date().getMonth(),
      maxDayInM = new Date(parseInt( val ), parseInt( month ), 1).daysInMonth();

    resetSelect( dTarget, 'Day' );
    refillSelect( dTarget, maxDayInM );
    if( ( dVal != "" || dVal != null ) && dVal <= maxDayInM ) {
      dTarget.parent().dropdown('set selected', dVal);
    } else {
      dTarget.prop('selectedIndex', 0);
    }

    // show message
    if( checkAll() ) {
      errMsg( '.sf-input-gr.date', 'off' );
    } else {
      errMsg( '.sf-input-gr.date', 'on', 'This fields are required!' );
    }
  });

  mTarget.change(function() {
    var val = $(this).val(),
      dVal = +(dTarget.val()),
      year = ( yTarget.val() ) ? yTarget.val() : new Date().getFullYear(),
      maxDayInM = new Date(parseInt( year ), parseInt( val ), 1).daysInMonth();

    resetSelect( dTarget, 'Day' );
    refillSelect( dTarget, maxDayInM );
    if( ( dVal != "" || dVal != null ) && dVal <= maxDayInM ) {
      dTarget.parent().dropdown('set selected', dVal);
    } else {
      dTarget.prop('selectedIndex', 0);
    }

    // show message
    if( checkAll() ) {
      errMsg( '.sf-input-gr.date', 'off' );
    } else {
      errMsg( '.sf-input-gr.date', 'on', 'This fields are required!' );
    }
  });

  dTarget.change(function() {
    // show message
    if( checkAll() ) {
      errMsg( '.sf-input-gr.date', 'off' );
    } else {
      errMsg( '.sf-input-gr.date', 'on', 'This fields are required!' );
    }
  });

  function checkAll() {
    return yTarget.val() != '' && mTarget.val() != '' && dTarget.val() != '';
  }
}

function resetSelect( select, text ) {
  select.children().remove();
  select.parent().dropdown('clear');
}

function refillSelect( selector, optionsCount ) {
  var select = $(selector);
  if( select && !select.length && optionsCount ) { return }

  select.append('<option value="">Day</option>');
  for( var i = 1; i <= optionsCount; i++ ) {
    select.append('<option value="' + i + '">' + i + '</option>');
  }
  select.parent().dropdown('refresh');
}

function errMsg( selector, state, msg, addType ) {
  var con = $(selector),
    addType = addType || 'append';
  if( con && con.length ) {
    if( state == 'on' ) {
      con.find('.err-msg').remove();
      if( addType == 'prepend' ) {
        con.prepend( '<span class="err-msg">' + msg + '</span>' );
      } else {
        con.append( '<span class="err-msg">' + msg + '</span>' );
      }
    } else {
      con.find('.err-msg').remove();
    }
  }
}


function bindPopupToEl( selector, options ) {
  var target = $(selector);
  options = ( !isEmpty( options ) ? options : { on: 'click', position: 'bottom center' } );
  if( target && target.length ) {
    $(target).each(function() {
      $(this).popup( options );
    });
  }
}


/*
 * 'element' jqueryObject or selector;
 * color: string
 */
function bgColorSwitcher( element, color ) {
  var el = $( element );
  if( el && el.length ) {
    el.css('background-color', color);
  }
}


/*
 * jqObList - jquery object list
 * cssOb - object, 2 property:
 *   stateClassNames - using for check displayState,
 *                     and removed from element (input type: 'string' or 'array')
 *   animation - define animate params, has 2 properties:
 *       animateClass - define animate type(input type: 'string' or 'array')
 *       duration - define animation duration
 */
function toDisplayNone( jqObList, cssOb ) {
  if( jqObList.length == 0 || jqObList == null ) { return }

  // check stateClassNames
  if( cssOb.stateClassNames ) {
    if( typeof( cssOb.stateClassNames ) == 'object' ) {
      cssOb.stateClassNames = cssOb.stateClassNames.join( ' ' );
    }
  } else {
    cssOb.stateClassNames = '';
  }

  // check animationClass
  if( cssOb.animation.animateClass ) {
    if( typeof( cssOb.animation.animateClass ) == 'object' ) {
      cssOb.animation.animateClass = cssOb.animation.animateClass.join( ' ' );
    }
  } else {
    cssOb.animation.animateClass = 'scale';
  }

  // check animation duration, if not correct set default value
  cssOb.animation.duration = (/^\d*.?\d+s{1}$/g.test( cssOb.animation.duration )) ?
    cssOb.animation.duration : '0.3s';

  // start checking;
  for( var i = 0; len = jqObList.length, i < len; i++ ) {
    if( jqObList.eq(i).css('display') != 'none' ) {
      jqObList.eq(i)
        .removeClass( cssOb.stateClassNames )
        .transition({
          animation: cssOb.animation.animateClass,
          duration: cssOb.animation.duration
        })
    }
  }
}


// возвращает cookie с именем name, если есть, если нет, то undefined
function getCookie(name) {
  var matches = document.cookie.match(new RegExp(
    "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
  ));
  return matches ? decodeURIComponent(matches[1]) : undefined;
}

// устанавливает cookie с именем name и значением value
// options - объект с свойствами cookie (expires, path, domain, secure)
function setCookie( name, value, options ) {
  options = options || {};
  var expires = options.expires;

  if( typeof expires == "number" && expires ) {
    var d = new Date();
    d.setTime(d.getTime() + expires * 1000);
    expires = options.expires = d;
  } else if( typeof expires == "string" && expires ) {
    var d = new Date();
    if( isNumeric( Date.parse( expires ) ) ) {
      d.setTime( Date.parse( expires ) );
      expires = options.expires = d;
    }
  }
  if( expires && expires.toUTCString() ) {
    options.expires = expires.toUTCString();
  }

  value = encodeURIComponent( value );
  var updatedCookie = name + "=" + value;

  for( var propName in options ) {
    updatedCookie += "; " + propName;
    var propValue = options[propName];
    if( propValue !== true ) {
      updatedCookie += "=" + propValue;
    }
  }
  document.cookie = updatedCookie;
}

// удаляет cookie с именем name
function deleteCookie( name ) {
  setCookie(name, "", {
    expires: 'Thu, 01 Jan 1970 00:00:00 UTC',
    path: '/'
  })
}




function checkFieldGroup( gr, msgOb ) {
  if( gr && gr.length ) {
    if( gr.filter(':checked').length ) {
      ( msgOb.state === true ) ? errMsg( msgOb.con, 'off' ) : '';
      return true;
    }
    ( msgOb.state === true ) ? errMsg( msgOb.con, 'on', 'This fields are required!', msgOb.addType ) : '';
  }
  return false;
}

function checkField( el, msgState ) {
  console.log( el );
  var rule = getRuleByEl( el );
  return checkByRule( el, rule, msgState );
}

function getRuleByEl( el ) {
  var tagName = el[0].tagName.toLowerCase(),
      type = el.attr('type'),
      rule = {};
  if( tagName == 'input' ) {
    switch( type ) {
      case 'text':
        rule['notEmpty'] = createRule( el, 'notEmpty' );
        rule['minLength'] = createRule( el, 'minLength' );
        rule['maxLength'] = createRule( el, 'maxLength' );
        rule['pattern'] = createRule( el, 'pattern' );
        break;
      case 'email':
        rule['notEmpty'] = createRule( el, 'notEmpty' );
        rule['pattern'] = createRule( el, 'pattern', { p: /\w+@[a-zA-Z]+?\.[a-zA-Z]{2,6}$/ });
        break;
      case 'tel':
        rule['notEmpty'] = createRule( el, 'notEmpty' );
        rule['pattern'] = createRule( el, 'pattern', { p: /^\+{1}[0-9]{1,15}$/ } );
        break;
      case 'hidden':
        rule['notEmpty'] = createRule( el, 'notEmpty' );
        break;
      case 'file':
        rule['notEmpty'] = createRule( el, 'notEmpty' );
        break;
    }
  } else if( tagName == 'select' ) {
      rule['notEmpty'] = createRule( el, 'notEmpty' );
  } else if( tagName == 'textarea' ) {
      rule['notEmpty'] = createRule( el, 'notEmpty' );
  }
  console.log(rule);
  return rule;
}

function checkByRule( e, obRule, msgState ) {
  if( isEmpty( obRule ) ) {
    return false;
  }
  var con;
  if( msgState === true ) {
    con = e.data('msg') ? $(e.data('msg')) : e.parent();
    console.log(con);
  }
  for( var k in obRule ) {
    if( !obRule[k].rule( e ) ) {
      if( msgState === true ) {
        errMsg( con, 'on', obRule[k].errMsg );
      }
      return false;
    }
  }
  errMsg( con, 'off');
  return true;
}

function createRule( el, name, op ) {
  var o;
  switch( name ) {
    case 'notEmpty':
      o = {
        rule: function ( el ) {
          return el.val() != '';
        },
        errMsg: getMsgByErrName({ errName: name })
      };
      break;
    case 'minLength':
      var min = 2;
      o = {
        rule: function( el ) {
          return el.val().length >= min;
        },
        errMsg: getMsgByErrName({ errName: name, minCharLength: min })
      };
      break;
    case 'maxLength':
      var max = 120;
      o = {
        rule: function( el ) {
          return el.val().length <= max;
        },
        errMsg: getMsgByErrName({ errName: name, maxCharLength: max })
      };
      break;
    case 'pattern':
      o = {
        rule: function( el ) {
          var pattern = ( op && op.p ) ? op.p : /^[A-Z0-9]/i;
          return pattern.test( el.val() );
        },
        errMsg: getMsgByErrName({ errName: name })
      };
      break;
  }
  return o;
}

function getMsgByErrName( o ) {
  var msg = '';
  switch( o.errName ) {
    case 'notEmpty': msg = 'This field is required!';
      break;
    case 'minLength': msg = 'This field required at least ' + o.minCharLength + ' characters!';
      break;
    case 'maxLength': msg = 'This field required maximum ' + o.maxCharLength + ' characters!';
      break;
    case 'pattern': msg = 'Please, enter valid characters!';
  }
  return msg;
}




function imgNormalizeDisplay( context ) {
    context = ( $(context) && $(context).length ) ? $(context) : '';
    if( context ) {
      context.each(function() {
        $(this).find('img').each(function() {
          if( this.naturalWidth || this.naturalHeight ) { // if images are already downloaded and cached in browser
            var css = getNormalizeCss(
                    { width: $(this).parent().width(), height: $(this).parent().height() },
                    { width: this.naturalWidth, height: this.naturalHeight }
                  );
              this.setAttribute( 'style', css );
          } else {  // set listener to 'load' event to get natural width and height of image
            this.addEventListener('load', function() {
              var css = getNormalizeCss(
                    { width: $(this).parent().width(), height: $(this).parent().height() },
                    { width: this.naturalWidth, height: this.naturalHeight }
                  );
              this.setAttribute( 'style', css );
            });
          }
        });
      })
    }
  }

function getNormalizeCss( pOb, cOb ) {
  if( isEmpty(pOb) && isEmpty(cOb) ) {
    return
  }
  var css = '';
  if( cOb.height / cOb.width > 1 ) {
    var coeff = cOb.height / cOb.width,
      computedHeight = pOb.width * coeff,
      mTop = ( computedHeight - pOb.height ) / 2;

    css = 'min-height: 100%; width: 100%; margin-top: -' + mTop + 'px;';
  } else {
    var coeff = cOb.width / cOb.height,
      computedWidth = pOb.height * coeff,
      mLeft = ( computedWidth - pOb.width ) / 2;

    css = 'min-width: 100%; height: 100%; margin-left: -' + mLeft + 'px;';
  }

  return css;
}