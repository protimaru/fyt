var map = {},
    infoWindowPublic = {},
    markers = [],
    geocoder = {};

function initMap() {
    map = new google.maps.Map(document.getElementById('map-container'), {
        center: { lat: -34.397, lng: 150.644 },
        zoom: 14,
        disableDefaultUI: true
    });

    geocoder = new google.maps.Geocoder;
    infoWindowPublic = new google.maps.InfoWindow();
    setCenterByParam();
}

function setCenterByParam() {
    var pos = {};
    // Try HTML5 geolocation.
    if( navigator.geolocation ) {
        navigator.geolocation.getCurrentPosition(function( position ) {
            pos = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };
            map.setCenter( pos );

            var userGeolocationMarker = new google.maps.Marker({
              position: pos,
              title: 'Your current location',
              map: map,
              animation: google.maps.Animation.DROP
            });

            userGeolocationMarker.addListener('click', function() {
              var iw = new google.maps.InfoWindow({
                content: 'Your current location'
              });
              iw.open(map, this);
            });

            geocodeLatLng(geocoder, pos);
            setCookie( 'location', JSON.stringify( pos ), { path: '/' } );
        }, function( error ) {
            // on location error
            console.log('Error: The Geolocation service failed. error.code:' + error.code);

            // call function to get country and city form registrated data of user
            getCountryInfo();
        });
    } else {
        // on location error;
        console.log('Error: Your browser doesn\'t support geolocation.');

        // call function to get country and city form registrated data of user
        getCountryInfo();
    }
}

// set center map and drop markers my current geolocation
function geocodeLatLng(geocoder, location) {
    geocoder.geocode({ 'location': location }, function(results, status) {
        if (status === 'OK') {
            if (results) {
                var code = recursivelyIterateProperties( results[0].address_components );
                if( code ) {
                    $.ajax({
                        url: '/api/arena/get/coordinates/',
                        type: 'POST',
                        data: 'country_code=' + code,
                        success: function( response ) {
                            console.log(response);
                            if( !isEmpty( response ) ) {
                                drop( response );
                            }
                        },
                        error: function( err ) {
                            console.log( err )
                        }
                    });
                }
            } else {
                console.log('No results found');
            }
    } else {
      console.log('Geocoder failed due to: ' + status);
    }
  });
}

// get geodata(lat, lng) by address and drop markers of arenas
function geocodeAddress(geocoder, address, resultsMap) {
    geocoder.geocode({'address': address}, function(results, status) {
        if (status === 'OK') {
            resultsMap.setCenter(results[0].geometry.location);
            var marker = new google.maps.Marker({
                map: resultsMap,
                position: results[0].geometry.location
            });
            map.setCenter( results[0].geometry.location );

            $.ajax({
                url: '/api/arena/get/coordinates/',
                type: 'POST',
                data: 'country_code=' + 'UZ',
                success: function( response ) {
                    console.log(response);
                    if( !isEmpty( response ) ) {
                        drop( response );
                    }
                },
                error: function( err ) {
                    console.log( err )
                }
            });

        } else {
            console.log('Geocode was not successful for the following reason: ' + status);
        }
    });
}

function getCountryInfo() {
    $.ajax({
        url: '/arena/get/location/',
        type: 'POST',
        data: 'gotLocation=fail',
        success: function( response ) {
            console.log( response );
            if( !isEmpty( response ) ) {
                var address = 'Tashkent, UZ';
                geocodeAddress( geocoder, address, map );
            }
        },
        error: function( error ) {
            console.log( error );
        }
    });
}

function hideMarker( marker ) {
    if( marker ) {
        marker.setMap( null );
    }
}

function removeMarker( marker ) {
    marker = null;
}

function drop( markerArray ) {
    for (var i = 0; len = markerArray.length, i < len; i++) {
        if( markerArray[i].latitude && markerArray[i].longitude ) {
            addMarkerWithTimeout( markerArray[i], i * 200 );
        }
    }
}

function addMarkerWithTimeout( markerParams, timeout ) {
    if( markerParams.latitude && markerParams.longitude ) {
        setTimeout(function() {
            var marker = new google.maps.Marker({
                position: { lat: +markerParams.latitude, lng: +markerParams.longitude },
                title: markerParams.name,
                icon: markerParams.types[0].icon,
                map: map,
                animation: google.maps.Animation.DROP
            });

            var iwTemplate = createIWTempalate( markerParams );

            marker.addListener('click', function() {
                map.setCenter( this.getPosition() );
                infoWindowPublic.setContent( iwTemplate );
                infoWindowPublic.open(map, marker);

                if( infoWindowPublic.getContent().indexOf('arena-attachedimg') !== -1 ) {
                    setSliderOnIW(
                        '#arena-attachedimg',
                        { width: '100%', height: 240 },
                        { width: 100, height: 80 }
                    );
                } else {
                    removeIWLoader( '#iw-con .iwcon-wrapper' );
                }

            });
            markers.push(marker);
        }, timeout);
    }
}

function createIWTempalate( markerData ) {
    var str = '',
        imgBlock = '',
        ratingTemplate = '',
        strDescCount = 150;
    if( markerData && markerData.images instanceof Array && markerData.images.length ) {
        if( markerData.images.length > 1 ) {
            imgBlock = '';
            var slide = '',
                thumbnail = '';
            for( var i = 0; len = markerData.images.length, i < len; i++ ) {
                slide += '<div class="sp-slide"><img class="sp-image lid" src="' + markerData.images[i].image + '"></div>';
                thumbnail += '<div class="sp-thumbnail"><img class="sp-thumbnail-image lid" src="' + markerData.images[i].image + '"></div>';
            }
            imgBlock = '<div class="iw-imgblock">' +
                        '<div class="slider-pro" id="arena-attachedimg">' +
                        '<div class="sp-slides">' +
                        slide+
                        '</div>'+
                        '<div class="sp-thumbnails">' +
                        thumbnail +
                        '</div>' +
                        '</div>' +
                        '</div>';
        } else {
            imgBlock = '<div class="iw-imgblock img"><img src="' + markerData.images[0].image + '"></div>';
        }
    }

    if( markerData.rating && isNumeric(markerData.rating) ) {
        var ratingCount = Math.round( +markerData.rating );

        ratingTemplate = '<div class="raiting">';
        for( var i = 0; i < 5; i++ ) {
            if( i < ratingCount ) {
                ratingTemplate += '<span class="ritem voted"><i class="star icon"></i></span>';
            } else {
                ratingTemplate += '<span class="ritem"><i class="star icon"></i></span>';
            }
        }
        ratingTemplate += '</div>';
    }

    // training count
    var tcount = '';
    if( isNumeric( markerData.trainings_count ) && +markerData.trainings_count > 0 ) {
        tcount = '<a href="#" id="tlistmodal-trigger" data-pid="' + markerData.id + '" class="tcount txt-orange">' + (+markerData.trainings_count) + '</a>';
    } else {
        tcount = '<span class="tcount txt-orange">0</span>';
    }


    str = '<div id="iw-con" data-id="' + markerData.id + '">' +
            '<div class="iwcon-wrapper static">' +
            '<div class="loader-overlay"><div class="ui active loader"></div></div>' +
            '<div class="iw-title"><span class="iwtitle-icon"><img src="' + markerData.types[0].icon + '" alt="location icon" title="' + markerData.name + '"></span>' + markerData.name + '</div>' +
            '<div class="iw-content">' +
            imgBlock +
            '<div class="iw-descblock">' +
            '<div class="iwdesc-item">' +
            ratingTemplate +
            '</div>' +
            '<div class="iwdesc-item">' +
            '<div class="iwdesc__item-title">Description</div>' +
            '<div class="iwdesc__item-txt">' + customSubStr( markerData.description, 150 ) + '</div>' +
            '</div>' +
            '<div class="iwdesc-item">' +
            '<ul class="listed-info">' +
            '<li>' +
            '<span class="linfo-icon"><img src="/static/images/icon/mobile-phone.png" alt="mobile phone icon"></span>' +
            '<a href="tel:+' + markerData.phone + '">' + markerData.phone + '</a>' +
            '</li>' +
            '<li>' +
            '<span class="linfo-icon"><img src="/static/images/icon/mail.png" alt="mail icon"></span>' +
            '<a href="mailto:' + markerData.email + '">' + markerData.email + '</a>' +
            '</li>' +
            '</ul>' +
            '</div>' +
            '<div class="iwdesc-item">' +
            '<div class="iwdesc__item-txt">' +
            '<div class="training-note">Have a ' + tcount + '<img src="/static/images/icon/gym.png" alt="training icon">training</div>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>';

    return str;
}

function removeIWLoader( selector ) {
    var target = $(selector);
    if( target && target.length ) {
        target
            .removeClass('static')
            .find('.loader-overlay').remove();
    }
}

function setSliderOnIW( selector, previewParams, thumbnailParams, tempState ) {
    var jOb = $(selector);
    tempState = tempState ? true : false;
    previewParams = !isEmpty( previewParams ) ? previewParams : { width: '100%', height: 240 };
    thumbnailParams = !isEmpty( thumbnailParams ) ? thumbnailParams : { width: 100, height: 80 };

    if( thumbnailParams.position
        && [ 'top', 'let', 'bottom', 'right' ].indexOf( thumbnailParams.position ) === -1 ) {
        thumbnailParams.position = 'bottom'; // set default value;
    }

    if( jOb && jOb.length ) {
        var imgList = jOb.find('img');

        if( imgList && imgList.length ) {
            var loaded = 0;
            if( !tempState ) {
                imgList.on('load', function() {
                    if( ++loaded == imgList.length ) {
                        jOb.sliderPro({
                            width: previewParams.width,
                            height: previewParams.height,
                            arrows: false,
                            buttons: false,
                            autoplay: false,
                            fullScreen: true,
                            thumbnailWidth: thumbnailParams.width,
                            thumbnailHeight: thumbnailParams.height,
                            thumbnailsPosition: thumbnailParams.position,
                            thumbnailPointer: true
                        });
                    }
                    removeIWLoader( '#iw-con .iwcon-wrapper' );
                });
            } else {
                jOb.sliderPro({
                    width: previewParams.width,
                    height: previewParams.height,
                    arrows: false,
                    buttons: false,
                    autoplay: false,
                    fullScreen: true,
                    thumbnailWidth: thumbnailParams.width,
                    thumbnailHeight: thumbnailParams.height,
                    thumbnailsPosition: thumbnailParams.position,
                    thumbnailPointer: true
                });
                removeIWLoader( '#iw-con .iwcon-wrapper' );
            }
        }
    }
    return;
}

// recursively itearate google maps geocoder results address_components to find country code;
var code;
function recursivelyIterateProperties( ob ) {
    if( ob instanceof Array ) {
        for( var i = 0; len = ob.length, i < len; i++ ) {
            recursivelyIterateProperties( ob[i] );
            if( code ) {
                return code;
            }
        }
    } else if( typeof ob === 'object' ) {
        for( var prop in ob ) {
            if( prop == 'types' && ( ob[prop] instanceof Array ) ) {
                if( ob[prop].indexOf('country') !== -1 ) {
                    console.log( '---' + ob['short_name'] + '---' );
                    return code = ob['short_name'];
                }
            }
            recursivelyIterateProperties( ob[prop] );
            if( code ) {
                return code;
            }
        }
    } else {
        console.log( ob );
    }
    return;
}


$(function() {

  $('.ui.dropdown').dropdown();

  // trigger for call add location panel
  $('#addloc-trigger').click(function(e) {
    e.preventDefault();
    e.stopPropagation();

    var c = $($(this).data('target'));
    c.fadeIn(300);

    // set add location panel some user fields
    $.get('/api/profile/' + $('#current_user_username').val() + '/detail/', function( data ) {
      if( data ) {
        $('#loc-phone').val( '+' + data.profile.phone );
        $('#loc-email').val( data.email );
      }
    });
  });

  // get country list
  var country = $('#loc-country');
  $.get('/api/geodata/countries', function( data ) {
    if( data ) {
      for( var i = 0; len = data.length, i < len; i++ ) {
        country.append(
          '<option value="' + data[i].sortname + '_' + data[i].id + '">' + data[i].name + '</option>'
        );
      }
    }
  });

  // get training type list;
  var chbCon = $('.stage .training-catlist');
  $.get('/api/sporttype/list/types/', function( data ) {
    if( data instanceof Array && data.length ) {
      chbCon.children().remove();
      for( var i = 0; len = data.length, i < len; i++ ) {
        chbCon.append(
          '<div class="form-gr">' +
          '<input type="checkbox" id="attype-chb' + i + '" name="' + data[i].name + '" value="' + data[i].id + '">' +
          '<label for="attype-chb' + i + '">' +
          '<span class="chb">' +
          '<span class="span-inner">' +
          '<span class="square"></span>' +
          '<i class="check square icon"></i>' +
          '</span>' +
          '</span>' +
          '<span class="item-icon"><img src="' + data[i].icon + '" alt="' + data[i].name + '" title="' + data[i].name + '"></span> ' + data[i].name +
          '</label>' +
          '</div>'
        );
      }
    }
  });


  // close button, for closing add location panel
  $('.wclose span').click(function() {
    var c = $($(this).data('target'));
    c.fadeOut(300);
    hideMarker( userMarker );
  });



  // get city list by country code
  var select = $('.stage .address .cc select');
  select.change(function() {
    var address = {
        'components': {},
        'type': ''
      },
      color = ( $(this).val() ) ? '#fff' : '#f9f9f9';

    // select bg-color change on selected
    bgColorSwitcher( $(this).parent(), color );

    if( $(this).val() ) {
      if ($(this).data('cat') == 'country') {
        var defaultCity = $(this).find('option:selected').text(),
          val = $(this).val().split('_');
        if( val[0] ) {
          $.get(
            '/api/geodata/cities/',
            { name: val[0].toUpperCase() },
            function( response ) {
              console.log( response );
              var cities;
              if( response instanceof Array && response.length ) {
                cities = response;
              } else {
                cities = [{
                  id: defaultCity,
                  name: defaultCity
                }];
              }
              var cityList = $('#loc-city');
              cityList.children().remove();
              cityList.dropdown('clear');
              if( cityList && cityList.length ) {
                cityList.append('<option value="">Choose city</option>');
                for( var i = 0; len = cities.length, i < len; i++ ) {
                  cityList.append('<option value="' + cities[i].id + '" data-title="' + cities[i].name + '">' + cities[i].name + '</option>');
                }
              }
            }
          )
        }
        address['components']['country'] = defaultCity;
        address['type'] = 'country';
        setMapByAddress( address, map );
      } else if( $(this).data('cat') == 'city' ) {
        var countryVal = $('#loc-country option:selected').text(),
          cityName = $(this).find('option:selected').data('title');

        if( cityName ) {
          address['components']['city'] = cityName;
          address['type'] = 'city';
        }
        if( !address['components']['country'] ) {
          address['components']['country'] = countryVal;
          if( !cityName ) {
            address['type'] = 'country';
          }
        }
        setMapByAddress( address, map );
      }
    }
  });

  $('#addloccon form :input').keydown(function(e) {
    if( e.keyCode == 13 ) {
      return false;
    }
  });


  // addlocation form listeners for inputs;
  $('#loc-name')
    .focus(function() { errMsg( $(this).parent(), 'off' ); })
    .focusout(function() { checkField( $(this), true ) });

  $('#loc-phone')
    .focus(function() { errMsg( $(this).parent(), 'off' ); })
    .focusout(function() { checkField( $(this), true ); });

  $('#loc-email')
    .focus(function() { errMsg( $(this).parent(), 'off' ) })
    .focusout(function() { checkField( $(this), true ) });

  $('#loc-desc')
    .focus(function() { errMsg( $(this).parent(), 'off' ) })
    .focusout(function() { checkField( $(this), true ) });

  $('#loc-country').parent()
    .focus(function() { errMsg( $(this).find('select').data('msg'), 'off' ) })
    .focusout(function() { console.log( checkField( $(this).find('select'), true ) ) });

  $('#loc-city').parent()
    .focus(function() { errMsg( $(this).find('select').data('msg'), 'off' ) })
    .focusout(function() { console.log( checkField( $(this).find('select'), true ) ) });

  // add location panel, change stages
  $('.btn-area .btn-circle').click(function() {
    var parentCon = $('#addloccon form'),
      btnPrev = parentCon.find('.btn-area .btn-prev'),
      btnNext = parentCon.find('.btn-area .btn-next'),
      btnSubmit = parentCon.find('#adloc-btn'),
      currentLayer = getLayer( $(this), 'data-current' ),
      nextLayer,
      prevLayer;

    if( currentLayer ) {

      nextLayer = getLayer( currentLayer, 'data-snext' );
      prevLayer = getLayer( currentLayer, 'data-sprev' );

      if( $(this)[0] == btnNext[0] ) {
        // validation code here...
        switch( currentLayer.data('number') ) {
          case 1:
              var inputs = currentLayer.find(':input').not('button, input[type=submit]'),
                counter = 0;
              inputs = inputs.map(function() {
                if( $(this).prop('required') ) {
                  return this;
                }
              });
              for( var i = 0; len = inputs.length, i < len; i++ ) {
                if( checkField( inputs.eq(i), true ) ) {
                  counter++;
                }
              }
              if( counter != inputs.length ) {
                return false;
              }
            break;
          case 2:
              var inputs = currentLayer.find(':input').not('button, input[type=submit]'),
                counter = 0;
              inputs = inputs.map(function() {
                if( $(this).prop('required') ) {
                  return this;
                }
              });
              if( checkField( inputs.eq(0), true ) ) {
                counter++;
              }
              if( checkField( inputs.eq(1), true ) ) {
                counter++;
              }
              if( inputs.eq(2).val() != '' ) {
                counter++;
                errMsg( inputs.eq(2).parent(), 'off' )
              } else {
                errMsg(
                  inputs.eq(2).parent(),
                  'on',
                  'Please, hit the button and select geolocation!',
                  'prepend'
                );
              }
              if( counter != inputs.length ) {
                return false;
              }
            break;
          case 3:
            var c = currentLayer.find('input[type=checkbox]');
            if( !checkFieldGroup( c, { state: true, con: $('.tlist-con').parent(), addType: 'prepend' } ) ) {
              return false;
            } else {
              errMsg( $('.tlist-con').parent(), 'off' );
            }
            break;
        }

        if( nextLayer ) {
          currentLayer.removeClass('active');
          nextLayer.addClass('mv-in active').removeClass('mv-out');

          btnNext.attr('data-current', ('#' + nextLayer.attr('id')));
          btnPrev.attr('data-current', ('#' + nextLayer.attr('id')));

          // reinit currentLayer;
          currentLayer = nextLayer;
        }
      } else {
        if( prevLayer ) {
          currentLayer.removeClass('mv-in active').addClass('mv-out');
          prevLayer.addClass('mv-in active');

          btnNext.attr('data-current', ('#' + prevLayer.attr('id')));
          btnPrev.attr('data-current', ('#' + prevLayer.attr('id')));

          // reinit currentLayer;
          currentLayer = prevLayer;
        }
      }

      if( getLayer( currentLayer, 'data-snext' ) ) {
        btnNext.parent().addClass('active');
        btnSubmit.parent().removeClass('active').attr('style', '');
      } else {
        btnNext.parent().removeClass('active');
        btnSubmit.parent().fadeIn(300, function() {
          $(this).addClass('active');
        });
      }
      getLayer( currentLayer, 'data-sprev' ) ? btnPrev.parent().addClass('active') : btnPrev.parent().removeClass('active');
    }
  });

  $('#adloc-btn').click(function(e) {
    e.preventDefault();

    if( !addLocFiles.length ) {
      errMsg( $('.dphoto-preview').parent(), 'on', 'Please, select at least 1 photo!', 'prepend' );
      return false;
    } else {
      errMsg( $('.dphoto-preview').parent(), 'off' );
    }

    var geodata = $('#umarkerGdata').val().split('|'),
      fd = new FormData();

    for( var i = 0; i < addLocFiles.length; i++ ) {
      if(addLocFiles[i] && addLocFiles[i].file) {
        fd.append('images', addLocFiles[i].file);
      }
    }

    // training types
    var typeList = $('.stage .training-catlist input[type=checkbox]:checked');
    for( var i = 0; len = typeList.length, i < len; i++ ) {
      fd.append('types', typeList.eq(i).val());
    }

    fd.append( 'name', $('#loc-name').val() );
    fd.append( 'city', $('#loc-country').val().split('_')[1] );
    fd.append( 'city', $('#loc-city').val() );
    if( $('#loc-address').val() == '' ) {
      fd.append('address', $('#loc-country').find(':selected').text() + ',' + $('#loc-city').find(':selected').text());
    } else {
      fd.append('address', $('#loc-address').val());
    }
    fd.append( 'phone', $('#loc-phone').val() );
    fd.append( 'email', $('#loc-email').val() );
    fd.append( 'description', $('#loc-desc').val() );
    fd.append( 'latitude', geodata[0] );
    fd.append( 'longitude', geodata[1] );
    fd.append( 'csrfmiddlewaretoken', getCookie('csrftoken') );

    $.ajax({
      url: '/arena/create/arena/',
      type: 'POST',
      cache: false,
      contentType: false,
      processData: false,
      data: fd,
      success: function( response ) {
        console.log( response );
        if( response instanceof Object || response instanceof Array ) {

          resetFrom('.newlocation form');

          if( userMarker ) {
            userMarker.setMap(null);
            userMarker = null;
          }

          var markerParams = response,
            marker = new google.maps.Marker({
              position: { lat: +markerParams.latitude, lng: +markerParams.longitude },
              title: markerParams.name,
              icon: markerParams.types[0].icon,
              map: map,
              animation: google.maps.Animation.DROP
            });

          var iwTemplate = createIWTempalate( markerParams );

          marker.addListener('click', function() {
            map.setCenter( this.getPosition() );
            infoWindowPublic.setContent( iwTemplate );
            infoWindowPublic.open(map, marker);

            if( infoWindowPublic.getContent().indexOf('arena-attachedimg') !== -1 ) {
              setSliderOnIW(
                '#arena-attachedimg',
                { width: '100%', height: 240 },
                { width: 100, height: 80 }
              );
            } else {
              removeIWLoader( '#iw-con .iwcon-wrapper' );
            }

          });
          markers.push(marker);
        }

      },
      error: function( err ) {
        console.log( err );
      }
    });

    console.log( $('#addloccon').find('form').serialize() );

  });

  var addLocFiles = [];
  $('#inputphoto').change(function(e) {
    var point = addLocFiles.length,
      errors = '';

    // collect all files to array;
    for( var i = 0; len = this.files.length, i < len; i++ ) {
      addLocFiles.push({
        index: generateID(),
        file: this.files[i]
      });
    }

    if( !addLocFiles ) {
      errors = "File upload not supported by your browser.";
      alert(errors);
      return;
    }

    if ( addLocFiles && addLocFiles[0] ) {
      for( var i = point; len = addLocFiles.length, i < len; i++ ) {
        var fileOb = addLocFiles[i],
          type = fileOb.file.type.split('/')[0];

        if( checkFileExt( fileOb.file, type ) ) {
          setPreviewPhoto( fileOb.file, addLocFiles, fileOb.index );
        } else {
          errors = 'File not supported!';
        }
      }
    }

    if ( errors ) {
      alert(errors);
    }
  });


  var userMarker;
  // get marker
  $('#getmarker-btn').click(function() {
    setMapByAddress( getAddressFromFields(), map, true );
  });

  // call modal for display trainings list
  $('body').on('click', '#tlistmodal-trigger', function(e) {
    e.preventDefault();

    var pid = $(this).data('pid'),
      modalContainer = $('#training-listmodal');
    console.log( pid );
    pid = isNumeric( pid ) ? +pid : 0;
    $.ajax({
      url: '/api/training/list/trainings/',
      type: 'POST',
      data: 'arena_id=' + pid,
      success: function( response ) {
        console.log( response );
        if( response instanceof Array && response.length ) {
          var targetCon = $('#training-listmodal .training-list');
          if( targetCon && targetCon.length ) {
            targetCon.children().remove();
            for( var i = 0; len = response.length, i < len; i++ ) {
              var item = createTrainingListItemTemplate( response[i] );
              targetCon.append( item );
              console.log(item);
            }
            modalContainer.modal('show');
          }
        }
      },
      error: function( error ) {
        console.log( error.text );
      }
    });

    modalContainer.modal({
      onDeny: function() {
        resetModalStage('.trainingdetail-con');
      },
      onHidden: function() {
        resetModalStage('.trainingdetail-con');
        if( $('#trainings-attachedimg').data('sliderPro') ) {
          $('#trainings-attachedimg').data('sliderPro').destroy();
        }
      }
    });
  });

  // change stage to detail
  $('body').on('click', '.tlist-item', function(e) {
    e.preventDefault();
    var detailStage = $('.trainingdetail-con'),
      preloader = detailStage.find('.trainingdetail-preloader');

    $.get(
      '/api/training/detail/trainings/' + $(this).data('id'),
      function( response ) {
        if( response ) {
          detailStage.find('.trainingdetail-wrapper').children().remove();
          detailStage.find('.trainingdetail-wrapper').append( createTrainingDetailTemplate( response ) );
          detailStage.addClass('active');

          setSliderOnIW(
            '#trainings-attachedimg',
            { width: '100%', height: 400 },
            { width: 120, height: 100, position: 'right' },
            true
          );

          if( preloader && preloader.length ) {
            preloader.addClass('active');
            setTimeout(function() {
              preloader.removeClass('active');
            }, 1000);
          }

          detailStage.find('#back').click(function() {
            detailStage.removeClass('active');
            detailStage.find('.trainingdetail-wrapper').children().remove();
          });
        }
      });
  });


  function makeStrAddress( arrData ) {
    var str = '',
      k;
    if( !isEmpty( arrData ) ) {
      k = arrData['type'];
      switch(k) {
        case 'country':
          str += arrData['components'][k];
          break;
        case 'city':
          str += arrData['components']['country'] + ', ' + arrData['components'][k];
          break;
        case 'addr':
          str += arrData['components']['country'] + ', ' + arrData['components']['city'] + ', ' + arrData['components']['addr'];
          break;
        default:
          str += arrData['components']['country'];
      }
    }
    return str;
  }

  function setMapByAddress( address, map, markerState ) {
    console.log(address);
    var strAddress = makeStrAddress( address );
    $('#locfind-errmsg').fadeOut(0).text('');

    console.log(strAddress);
    if( strAddress && map ) {
      var geocoder = new google.maps.Geocoder;
      geocoder.geocode({'address': strAddress}, function(results, status) {
        if (status === 'OK') {
          var pos = results[0].geometry.location;
          map.setCenter( pos );
          map.setZoom(14);

          if( markerState ) {
            if( userMarker ) {
              userMarker.setPosition( pos );
            } else {
              userMarker = new google.maps.Marker({
                map: map,
                position: pos,
                icon: "/static/images/map/userMarker.png",
                draggable: true,
                animation: google.maps.Animation.BOUNCE,
                title: 'Drag for select location'
              });

              userMarker.addListener('dragend', function() {
                $('#umarkerGdata').val( this.getPosition().lat() + '|' + this.getPosition().lng() );
                $('#umarkerGdata').parent().find('.err-msg').remove();
              });
            }

            $('#umarkerGdata').val( pos.lat() + '|' + pos.lng()  );
            $('#umarkerGdata').parent().find('.err-msg').remove();
          }
        } else {
          address['type'] = 'city';
          setMapByAddress( address, map, markerState );
          $('#locfind-errmsg')
            .text('Nothing found at the requested address!')
            .fadeIn(100);
          console.log('Geocode was not successful for the following reason: ' + status);
        }
      });
    }
  }

  function getAddressFromFields() {
    var country = $('#loc-country'),
      city = $('#loc-city'),
      addr = $('#loc-address'),
      arrAddress = {
        'components': [],
        'type': ''
      };

    if( country.val() ) {
      var option = country.find('option:selected').text();
      if( option ) {
        arrAddress['components']['country'] = option;
        arrAddress['type'] = 'country';
      }
    }

    if( city.val() ) {
      var option = city.find('option:selected').data('title');
      if( option ) {
        arrAddress['components']['city'] = option;
        arrAddress['type'] = 'city';
      }
    }

    if( addr.val() ) {
      arrAddress['components']['addr'] = addr.val();
      arrAddress['type'] = 'addr';
    }
    return arrAddress;
  }

  function resetFrom( formSelector ) {
    var formCon = $(formSelector);
    if( formCon && formCon.length ) {
      // reset layers
      formCon.find('.stage.mv-in')
        .removeClass('mv-in active')
        .addClass('mv-out');
      formCon.find('.stage').eq(0)
        .removeClass('mv-out')
        .addClass('mv-in active');

      // reset buttons
      formCon.find('.btn-prevcon')
        .removeClass('active')
        .find('button').attr('data-current', '#step1');

      formCon.find('.btn-nextcon')
        .addClass('active')
        .find('button').attr('data-current', '#step1');

      formCon.find('.btn-submit').fadeOut(0);


      var fieldList = formCon.find(':input').not('button, input[type=submit], input[type=reset]');
      for( var i = 0; len = fieldList.length, i < len; i++ ) {
        var tagName = fieldList[i].tagName.toLowerCase(),
          type = ( tagName == 'input' ) ? fieldList[i].getAttribute('type') : '';

        if( tagName == 'select' ) {
          fieldList.eq(i).val('');
        }

        if( type == 'checkbox' || type == 'radio' ) {
          fieldList.eq(i).prop('checked', false);
        } else {
          fieldList.eq(i).val('');
        }
      }

      // reset dropdowns
      formCon.find('.ui.dropdown').dropdown('clear');

      // reset training preview img
      formCon.find('.dphoto-preview .dphoto__preview-item.img').remove();

      // close add location tab
      $('.wclose span').trigger('click');

      // remove error notify
      formCon.find('#locfind-errmsg').remove();

      addLocFiles = [];
    }
  }
});


// search training filters
$(function() {

  // search container trigger
  $('#searchtrain-con .st-trigger').click(function() {
    var con = $($(this).data('target'));
    con.toggleClass('active');
    $(this).find('img').toggleClass('goback');
  });

  // choose location control btns
  $('.chcontrol-btns button').click(function() {
    $('.chcontrol-btns button').not($(this)).removeClass('active');
    $(this).addClass('active');
    var tab = $($(this).data('target')),
      tabList = $('.chlvalue-tab').not($(this));
    for( var i = 0; len = tabList.length, i < len; i++ ) {
      if( tabList.eq(i).hasClass('active') ) {
        tabList.eq(i).removeClass('active');
      }
    }
    tab.addClass('active');

    var action = $(this).data('action');
    if( action && action == 'getloc' ) {
      var pos = getCookie('location');
      if( pos ) {
        pos = JSON.parse(pos);
      }
      console.log(pos);
      if( pos instanceof Object && !isEmpty(pos)) {
        map.setCenter( pos );
        geocoder.geocode({ 'location': pos }, function(results, status) {
          if (status === 'OK') {
            if (results) {
              console.log(results[0].address_components);
              var ob = {};
              if( results[0].address_components.length ) {
                var rs = results[0].address_components;
                if( rs.length < 2 ) {
                  ob['country'] = rs[rs.length - 1].long_name;
                  ob['city'] = rs[rs.length - 2].long_name;
                } else if( rs.length > 2 ) {
                  ob['country'] = rs[rs.length - 1].long_name;
                  ob['city'] = rs[rs.length - 2].long_name;
                  var addr = '';
                  for( var i = rs.length - 3; i >= 0; i-- ) {
                    console.log( rs[i].long_name );
                    if( i == 0 ) {
                      addr += rs[i].long_name;
                    } else {
                      addr += rs[i].long_name + ', ';
                    }
                  }
                  ob['address'] = addr;
                }
              }
              if( !isEmpty( ob ) ) {
                setLocationFields('#tab-getloc', ob);
              } else {
                $('#floc-errmsg').text('The Geolocation service failed, choose location manually!').fadeIn(100);
              }
            } else {
              console.log('No results found');
              $('#floc-errmsg').text('The Geolocation service failed, choose location manually!').fadeIn(100);
            }
          } else {
            console.log('Geocoder failed due to: ' + status);
            $('#floc-errmsg').text('The Geolocation service failed, choose location manually!').fadeIn(100);
          }
        });
      } else {
        // Try HTML5 geolocation.
        if( navigator.geolocation ) {
          navigator.geolocation.getCurrentPosition(function( position ) {
            pos = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };
            map.setCenter( pos );
            setCookie( 'location', JSON.stringify( pos ), { path: '/' } );

            geocoder.geocode({ 'location': location }, function(results, status) {
              if (status === 'OK') {
                if (results) {
                  console.log(results[0].address_components);
                } else {
                  console.log('No results found');
                }
              } else {
                console.log('Geocoder failed due to: ' + status);
              }
            });
          }, function( error ) {
            // on location error
            console.log('Error: The Geolocation service failed. error.code:' + error.code);
            $('#floc-errmsg').text('The Geolocation service failed, choose location manually!').fadeIn(100);
          });
        } else {
          // on location error;
          console.log('Error: Your browser doesn\'t support geolocation.');
          $('#floc-errmsg').text('The Geolocation service failed, choose location manually!').fadeIn(100);
        }
      }
    }
  });

  // get country list
  var country = $('#sloc-country');
  $.get('/api/geodata/countries', function( data ) {
    if( data ) {
      for( var i = 0; len = data.length, i < len; i++ ) {
        country.append(
          '<option value="' + data[i].sortname + '_' + data[i].id + '">' + data[i].name + '</option>'
        );
      }
    }
  });

  var select = $('#searchtrain-con #tab-chooseloc select');
  select.change(function() {
    if( $(this).val() ) {
      if ($(this).data('cat') == 'country') {
        var defaultCity = $(this).find('option:selected').text(),
          val = $(this).val().split('_'),
          str = '';

        if( val[0] ) {
          $.get(
            '/api/geodata/cities/',
            { name: val[0].toUpperCase() },
            function( response ) {
              console.log( response );
              var cities;
              if( response instanceof Array && response.length ) {
                cities = response;
              } else {
                cities = [{
                  id: defaultCity,
                  name: defaultCity
                }];
              }
              var cityList = $('#sloc-city');
              cityList.children().remove();
              cityList.dropdown('clear');
              if( cityList && cityList.length ) {
                for( var i = 0; len = cities.length, i < len; i++ ) {
                  cityList.append('<option value="' + cities[i].id + '" data-title="' + cities[i].name + '">' + cities[i].name + '</option>');
                }
              }
            }
          );
          str = defaultCity;
        }

        if( str ) {
          setMapByAddress( str );
          console.log('change map center to: ' + str);
        } else {
          console.log('address string is empty!');
        }
      } else if( $(this).data('cat') == 'city' ) {
        var countryVal = $('#sloc-country option:selected').text(),
          cityName = $(this).find('option:selected').data('title'),
          str = countryVal + ', ' + cityName;

        if( str ) {
          setMapByAddress( str );
          console.log('change map center to: ' + str);
        } else {
          console.log('address string is empty!');
        }
      }
    }
  });

  var addressInput = $('#searchtrain-con .chlocation-value input[type=text].address');
  addressInput.change(function() {
    var val = this.value;
    if( val ) {
      var str = $('#sloc-country').find(':selected').text() + ', ' + $('#sloc-city').find(':selected').data('title');
      str += ', ' + val;
      setMapByAddress( str );
    }
  });


  // get training type list;
  var chbCon = $('#searchtrain-con .training-catlist');
  $.get('/api/sporttype/list/types/', function( data ) {
    if( data instanceof Array && data.length ) {
      chbCon.children().remove();
      for( var i = 0; len = data.length, i < len; i++ ) {
        chbCon.append(
          '<div class="form-gr">' +
          '<input type="checkbox" id="ttype-chb' + i + '" name="' + data[i].name + '" value="' + data[i].id + '">' +
          '<label for="ttype-chb' + i + '">' +
          '<span class="chb">' +
          '<span class="span-inner">' +
          '<span class="square"></span>' +
          '<i class="check square icon"></i>' +
          '</span>' +
          '</span>' +
          '<span class="item-icon"><img src="' + data[i].icon + '" alt="' + data[i].name + '" title="' + data[i].name + '"></span> ' + data[i].name +
          '</label>' +
          '</div>'
        );
      }
    }
  });

  function setMapByAddress( address ) {
    if( address ) {
      var geocoder = new google.maps.Geocoder;
      geocoder.geocode({'address': address}, function(results, status) {
        if (status === 'OK') {
          var pos = results[0].geometry.location;
          map.setCenter( pos );
          map.setZoom(14);

          $('#floc-errmsg').fadeOut(100);
        } else {
          $('#floc-errmsg')
            .text('Nothing found at the requested address!')
            .fadeIn(100);
          console.log('Geocode was not successful for the following reason: ' + status);
        }
      });
    }
  }

  function setLocationFields( selector, data ) {
    var parentCon = $(selector);
    if( parentCon && parentCon.length ) {
      var country = parentCon.find('input[name=country]'),
        city = parentCon.find('input[name=city]'),
        address = parentCon.find('input[name=address]');
      if( !isEmpty(data) ) {
        country.val( data.country );
        city.val( data.city );
        address.val( data.address );
      }
    }
    console.log( data );
  }

  // get training name list
  $('#trainername')[0].addEventListener('input', function() {
    var input = $(this),
      val = this.value,
      menu = $(this).siblings('.rs-block');
    if( val ) {
      $.get(
        '/api/profile/list/',
        { name: val },
        function( data ) {
          console.log( data );
          showMenu( data, menu, input );
        }
      );
    } else {
      menu.fadeOut(100);
    }
  });
  $('#trainername').focus(function() {
    var input = $(this),
      val = this.value,
      menu = $(this).siblings('.rs-block');
    if( val ) {
      $.get(
        '/api/profile/list/',
        { name: val },
        function( data ) {
          console.log( data );
          showMenu( data, menu, input );
        }
      );
    } else {
      menu.fadeOut(100);
    }
    errMsg( $(this).parent(), 'off' );
  });
  $('#trainername').focusout(function() {
    $(this).siblings('.rs-block').fadeOut(100);
    checkField( $(this), true );
  });

  // get sport arena list;
  $('#sporthallname')[0].addEventListener('input', function() {
    var input = $(this),
      val = this.value,
      menu = $(this).siblings('.rs-block');
    if( val ) {
      $.get(
        '/api/arena/list/arenas/',
        { name: val },
        function( data ) {
          console.log( data );
          var arr = [];
          if( data instanceof Array && data.length ) {
            for( var i = 0; len = data.length, i < len; i++ ) {
              arr.push({
                id: data[i].id,
                name: data[i].name
              });
            }
          }
          showMenu( arr, menu, input );
        }
      );
    } else {
      menu.fadeOut(100);
    }
  });
  $('#sporthallname').focus(function() {
    var input = $(this),
      val = this.value,
      menu = $(this).siblings('.rs-block');
    if( val ) {
      $.get(
        '/api/arena/list/arenas/',
        { name: val },
        function( data ) {
          console.log( data );
          var arr = [];
          if( data instanceof Array && data.length ) {
            for( var i = 0; len = data.length, i < len; i++ ) {
              arr.push({
                id: data[i].id,
                name: data[i].name
              });
            }
          }
          showMenu( arr, menu, input );
        }
      );
    } else {
      menu.fadeOut(100);
    }
    errMsg( $(this).parent(), 'off' );
  });
  $('#sporthallname').focusout(function() {
    $(this).siblings('.rs-block').fadeOut(100);
    checkField( $(this), true );
  });




  $('body').on('click', '#searchtrain-con form .training-catlist input[type=checkbox]', function() {
    var c = $('#searchtrain-con form .training-catlist input[type=checkbox]'),
      con = $('#searchtrain-con form .tot-inner').parent();
    if( c.filter(':checked').length ) {
      errMsg( con, 'off' );
    }
  });

  $('#search-btn').click(function(e) {
    e.preventDefault();
    var form = $('#searchtrain-con form'),
      c = form.find('.training-catlist input[type=checkbox]'),
      counter = 0;


    if( checkFieldGroup( c, { state: true, con: $('.tot-inner').parent(), addType: 'prepend' } ) ) {
      errMsg( $('.tot-inner').parent(), 'off' );
    } else {
      counter++;
    }

    if( checkField( $('#trainername'), true ) ) {
      errMsg( $('#trainername').parent(), 'off' );
      counter++;
    }

    if( checkField( $('#sporthallname'), true ) ) {
      errMsg( $('#sporthallname').parent(), 'off' );
      counter++;
    }

    if( counter != 3 ) {
      return false;
    }

  });

  $('#searchtrain-con form :input').keydown(function(e) {
    if( e.keyCode == 13 ) {
      return false;
    }
  });

  function showMenu( data, menu, input ) {
    if( data instanceof Array && data.length ) {
      menu.children().remove();
      for( var i = 0; len = data.length, i < len; i++ ) {
        if( data[i].first_name && data[i].last_name ) {
          name = data[i].first_name + ' ' + data[i].last_name;
        } else if( data[i].username ) {
          name = data[i].username;
        } else {
          name = data[i].name;
        }
        menu.append(
          $('<div>', {
            class: 'item',
            'data-value': data[i].id,
            text: name,
            click: function(e) {
              e.preventDefault();
              var txt = $(this).text();
              input.val(txt);
              menu.fadeOut(100);
            }
          })
        );
      }
      menu.fadeIn(100);
    } else {
      menu.children().remove();
      menu.append('<div class="item">No results found!</div>');
      menu.fadeIn(100);
      // menu.fadeOut(100);
    }
  }
});


// returns jquery ob which selector was named on attribute 'attrName' for 'jOb';
function getLayer( jOb, attrName ) {
  var temp = jOb ? $(jOb.attr(attrName)) : null;
  return temp ? (( temp && temp.length ) ? temp : null) : null;
}

function createTrainingListItemTemplate( data ) {
  var item = $('<div>', {
    class: "tlist-item",
    'data-id': data.id
  })
    .append(
      '<div class="tlitemtitle-con">' +
      '<div class="tlitem-title">' +
      '<span class="tlitem__title-icon">' +
      '<img src="' + data.types.icon + '" alt="trainings type icon" title="' + data.types.name + '">' +
      '</span>' + data.types.name.toUpperCase() +
      '</div>'
    )
    .append(
      '<div class="tlitem-content">' +
      '<div class="tlitem-txt">' +
      '<div class="tlitem-name">' +
      '<div class="tlitem__name-title txt-orange">Name of trainings:</div>' +
      '<div class="tlitem__name-value txt-uppercase">' + data.name + '</div>' +
      '</div>' +
      '<div class="tlitem-desc">' +
      '<div class="tlitem__desc-title">Description of trainings:</div>' +
      '<div class="tlitem__desc-txt">' + customSubStr( data.description, 150 ) + '</div>' +
      '</div>' +
      '</div>' +
      '<div class="tlitem__preview-img"><img src="' + data.images[0].image + '"></div>' +
      '</div>'
    );
  return item;
}

function createTrainingDetailTemplate( data ) {
  var str = '',
    imgBlock = '',
    trainerBlock = '',
    tplan = '',
    tdays = '';

  if( data.images.length > 1 ) {
    var slides = '<div class="sp-slides">',
      thumbnails = '<div class="sp-thumbnails">';
    imgBlock = '<div class="tdetail-imgblock"><div class="slider-pro" id="trainings-attachedimg">';
    for( var i = 0; len = data.images.length, i < len; i++ ) {
      slides += '<div class="sp-slide"><img class="sp-image lid" src="' + data.images[i].image + '"></div>';
      thumbnails += '<div class="sp-thumbnail"><img class="sp-thumbnail-image lid" src="' + data.images[i].image + '"></div>';
    }
    slides += '</div>';
    thumbnails += '</div>';
    imgBlock += slides + thumbnails + '</div></div>';
  } else {
    imgBlock = '<div class="tdetail-imgblock img"><img src="' + data.images[0].image + '"></div>'
  }

  // trainer block
  if( data.user ) {
    var trainerName = ( data.user.first_name && data.user.last_name ) ? ( data.user.first_name + ' ' + data.user.last_name ) : data.user.username;
    trainerBlock = '<div class="tdetail-trainer">' +
      '<div class="title">Trainer:</div>' +
      '<div class="tdtrainer-content">' +
      '<div class="tdtrainer-imgcon">' +
      '<a href="' + data.user.url + '" class="tdtrainer-img"><img src="' + data.user.profile.avatar + '" title="' + trainerName + '"></a>' +
      '<a href="' + data.user.url + '" class="tdtrainer-name">' + trainerName + '</a>' +
      '</div>' +
      '<div class="tdtrainer-info">' +
      '<p class="tdtinfo-item"><span class="tdtinfo__item-title">Email:</span><a href="mailto:' + data.user.email + '">' + data.user.email + '</a></p>' +
      '<p class="tdtinfo-item"><span class="tdtinfo__item-title">Phone:</span><a href="tel:' + data.user.profile.phone + '">' + data.user.profile.phone + '</a></p>' +
      '<p class="tdtinfo-item">' +
      '<span class="tdtinfo__item-title">About:</span>' +
      customSubStr( data.trainer_description, 150 ) +
      '</p>' +
      '</div>' +
      '</div>' +
      '</div>';
  } else {
    trainerBlock = '<div class="tdetail-trainer">' +
      '<div class="title">Trainer:</div>' +
      '<div class="tdtrainer-content">' +
      '<div class="tdtrainer-imgcon">' +
      '<div class="tdtrainer-img"><img src="' + data.trainer_image + '" title="' + data.trainer_name + '"></div>' +
      '<div class="tdtrainer-name">' + data.trainer_name + '</div>' +
      '</div>' +
      '<div class="tdtrainer-info">' +
      '<p class="tdtinfo-item"><span class="tdtinfo__item-title">Email:</span><a href="mailto:' + data.email + '">' + data.email + '</a></p>' +
      '<p class="tdtinfo-item"><span class="tdtinfo__item-title">Phone:</span><a href="tel:123-45-56">123-45-56</a></p>' +
      '<p class="tdtinfo-item">' +
      '<span class="tdtinfo__item-title">About:</span>' +
      customSubStr( data.trainer_description, 150 ) +
      '</p>' +
      '</div>' +
      '</div>' +
      '</div>';
  }

  if( data.days ) {
    tdays += '<div class="tdetail-time tdetail-block">' +
      '<div class="title">Time:</div>' +
      '<div class="tdetail__time-con">' +
      '<table class="ui fixed celled table">' +
      '<thead>' +
      '<tr>' +
      '<th>Day</th>' +
      '<th>Time</th>' +
      '</tr>' +
      '</thead>' +
      '<tbody>';
    for( var i = 0; len = data.days.length, i < len; i++ ) {
      tdays += '<tr>' +
        '<td>' + data.days[i].name + '</td>';
      if( data.days[i].training_day ) {
        tdays += '<td>';
        for( var j = 0; jlen = data.days[i].training_day.length, j < jlen; j++ ) {
          if( j == jlen - 1 ) {
            tdays += data.days[i].training_day[j].time;
          } else {
            tdays += data.days[i].training_day[j].time + '<br/>';
          }
        }
        tdays += '</td></tr>';
      }
    }
    tdays += '</tbody></table></div></div>';
  }

  if( data.price ) {
    tplan += '<div class="tdetail-price tdetail-block">' +
      '<div class="title">Price:</div>' +
      '<div class="tdetail__price-con">' +
      '<table class="ui fixed single line celled table">' +
      '<thead>' +
      '<tr>' +
      '<th>Time</th>' +
      '<th>Cost</th>' +
      '</tr>' +
      '</thead>' +
      '<tbody>';
    for( var i = 0; len = data.price.length, i < len; i++ ) {
      tplan += '<tr>' +
        '<td>' + data.price[i].duration + '</td>' +
        '<td>' + data.price[i].price + ' <span class="currency">' + data.price[i].currency + '</span></td>' +
        '</tr>';
    }
    tplan += '</tbody></table></div></div>';
  }

  str += '<div class="tdetail__title-con">' +
    '<div class="tdetail-title">' +
    '<span class="tdetail__title-icon"><img src="' + data.types.icon + '" title="'+ data.types.name.toUpperCase() +'"></span>' + data.types.name.toUpperCase() +
    '</div>' +
    '</div>' +
    '<div class="tdetail-content">' +
    '<div class="tdetail-name">' +
    '<div class="title">Name of trainings:</div>' +
    '<div class="tdetail__name-value txt-uppercase">' + data.name.toUpperCase() + '</div>' +
    '</div>' +
    imgBlock +
    '<div class="tdetail-desc">' +
    '<div class="title">Description of trainings:</div>' +
    '<div class="tdetail__desc-txt">' +  customSubStr( data.description, 150 ) + '</div>' +
    '</div>' +
    trainerBlock +
    '<div class="tdetail-address tdetail-block">' +
    '<div class="title">Address:</div>' +
    '<div class="tdetail-address-txt">The Palm Jumeirah, Blabla st. 145.</div>' +
    '</div>' +
    tdays +
    tplan +
    '</div>' +
    '<div class="tdaction-btns">' +
    '<button type="button" id="back" data-target="" class="ui grey button txt-uppercase">Back</button>' +
    '<button type="button" id="have-workout" class="ui orange button txt-uppercase">Have a workout</button>' +
    '</div>';

  return str;
}

function resetModalStage(selector) {
  var stage = $(selector);
  if( !stage && !stage.length ) {
    return false;
  }
  stage.removeClass('active');
}

function setPreviewPhoto( file, fileArray, index ) {
  var reader = new FileReader(),
    previewContainer = $('.stage .dphoto__preview-item').not('.img').eq(0);

  previewContainer.find('label').remove();
  previewContainer
    .append('<div class="dppitem-overlay"></div>')
    .append(
      $('<span>', {
        class: 'rm-photo',
        title: 'remove photo',
        click: function() {
          $(this).parent().remove();
          if(fileArray.length != 0) {
            fileArray = deleteByIndexFromArray( fileArray, index );
          }
        }
      }).append('<i class="remove icon"></i>')
    )
    .addClass('img');

  previewContainer.parent().append(
    '<div class="dphoto__preview-item">' +
    '<label for="inputphoto"><i class="plus icon"></i></label>' +
    '</div>'
  );

  reader.onload = function( e ) {
    var image = new Image();
    image.src = e.target.result;

    image.addEventListener('load', function() {
      var imgCSS = '',
        imgWidth = this.naturalWidth,
        imgHeight = this.naturalHeight;

      if( imgHeight / imgWidth > 1 ) {
        imgCSS = 'min-height: 100%; width: 100%; ';
        var computedHeight = ( previewContainer.width() * imgHeight / imgWidth ),
          mTop = ( previewContainer.height() - computedHeight ) / 2;
        imgCSS += ( 'margin-top: ' + mTop + 'px' );
      } else {
        imgCSS = 'height: 100%; min-width: 100%; ';
        var computedWidth = ( previewContainer.height() * imgWidth / imgHeight ),
          mLeft = ( previewContainer.width() - computedWidth ) / 2;
        imgCSS += ( 'margin-left: ' + mLeft + 'px' );
      }
      previewContainer.append( image );
      previewContainer.find('img').attr( 'style', imgCSS );
    });
  };
  reader.readAsDataURL( file );
}