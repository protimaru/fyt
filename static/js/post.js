var files = [],
  arrayOfAttrID = [];

$(function() {
  // feed edit/delete button
  $('body').on('click', '.feed__editing-con a', function(e) {
    e.preventDefault();
    e.stopPropagation();

    $($(this).data('target'))
      .transition('scale')
      .toggleClass('fem-active');

    // hide all displaying(active) menus, when displayed some of them
    toDisplayNone(
      $('.feed__editing-menu').not( $(this).data('target') ),
      {
        stateClassNames: 'fem-active',
        animation: {
          animateClass: 'scale',
          duration: '0.3s',
        }
      }
    );
  });

  // feed edit menu call modal
  $('body').on('click', '.feed__editing-menu a', function(e) {
    e.preventDefault();
    var id = $(this).data('pid');
    $($(this).data('target'))
      .modal({
        onApprove : function() {
          console.log('pid: ' + id);
          $.ajax({
            url: '/api/post/delete/' + id + '/',
            type: 'DELETE',
            statusCode: {
              204: function() {
                $('.feed[data-id="' + id + '"]').fadeOut(250, function() {
                  $(this).remove();
                });
              }
            }
          });
        }
      })
      .modal('show');
  });

  // share
  $('body').on('click', 'a.f-share', function(e) {
    e.preventDefault();
    e.stopPropagation();

    $($(this).data('target'))
      .transition({
        animation: 'vertical flip',
        duration: '0.3s'
      })
      .toggleClass('fem-active');

    toDisplayNone(
      $('.share__btns').not( $(this).data('target') ),
      {
        stateClassNames: 'fem-active',
        animation: {
          animateClass: ['vertical', 'flip'],
          duration: '0.3s',
        }
      }
    );
  });

  // like
  $('body').on('click', 'a.like', function(e) {
    e.preventDefault();
    e.stopPropagation();

    var a_like = $(this),
      countCon = $(this).find('.likes-count');
    $.post(
      '/_like/add/',
      {
        object_id: $(this).data('id'),
        content_type: 'post'
      },
      function( data ) {
        if ( data['status'] == 'ok' ) {
          var prevAction = a_like.data('action'),
            prevLikes = parseInt( a_like.attr('data-count') ) || 0;

          // toggle link text
          a_like.find('i').css('background-position', prevAction == 'like' ? '-24px -42px' : '-1px -42px');

          // update total likes
          prevLikes = ( prevAction == 'like' ) ? prevLikes + 1 : ( ( prevLikes == 0 ) ? 0 : prevLikes - 1 );
          a_like.attr('data-count', prevLikes);
          countCon.text(prevLikes);

          // toggle data-action
          prevAction = ( prevAction == 'like' ) ? 'unlike' : 'like';
          a_like.data('action', prevAction);
        }
    });
  });

  $(document).click(function() {
    // share btns container;
    toDisplayNone(
      $('.share__btns'),
      {
        stateClassNames: 'fem-active',
        animation: {
          animateClass: 'vertical flip',
          duration: '0.3s',
        }
      }
    );

    // feed edit menu hide when clicking outside
    toDisplayNone(
      $('.feed__editing-menu').not( $(this).data('target') ),
      {
        stateClassNames: 'fem-active',
        animation: {
          animateClass: 'scale',
          duration: '0.3s',
        }
      }
    );
  });



  // handling submit event;
  $('#post-btn').click(function(e) {
    e.preventDefault();
    var fd = new FormData();

    for( var i = 0; i < files.length; i++ ) {
      if(files[i] && files[i].file) {
        fd.append('file', files[i].file);
      }
    }
    if($('#post-text').val()) {
      fd.append('context', $('#post-text').val());
    } else {
      fd.append('context', '');
    }
    fd.append('csrfmiddlewaretoken', getCookie('csrftoken'));

    $.ajax({
      url: $('#post-form').attr('action'),
      type: "POST",
      cache: false,
      contentType: false,
      processData: false,
      data: fd,
      beforeSend: function() {
        $('.u-post .pr-overlay').addClass('active');
      },
      success: function(response) {
        console.log(response);
        var feed = createFeedTemplate( response );
        if( !isEmpty( feed ) ) {
          $('.feed-list').prepend( feed );

          // check if response contains media params array;
          var media = getMedia( response );
          if( !isEmpty( media ) ) {
            normalizeMediaDisplay( $('.feed-list').find('.feed').eq(0), getMediaParams( media ) );
            arrayOfAttrID = [];
          }
        }
      },
      complete: function() {
        // reset form and global variables;
        $('.u-post .pr-overlay').removeClass('active');
        $('#post-form')[0].reset();
        files = [];
        $('.pmpreview-img').not('.pm-add').remove();
        pmAddShowControll();
      }
    });
  });

  // input file change handler;
  $("#inputfile").on("change", function() {
    var point = files.length,
      errors = '';

    // collect all files to array;
    for( var i = 0; len = this.files.length, i < len; i++ ) {
      files.push({
        index: generateID(),
        file: this.files[i]
      });
    }

    // console.log(files);
    if( !files ) {
      errors = "File upload not supported by your browser.";
      alert(errors);
      return;
    }

    // check files before read and create preview object;
    if ( files && files[0] ) {
      for( var i = point; len = files.length, i < len; i++ ) {
        var fileOb = files[i],
          type = fileOb.file.type.split('/')[0];

        if( checkFileExt( fileOb.file, type ) ) {
          readMedia( fileOb.file, fileOb.index );
        } else {
          errors = 'File not supported!';
        }
      }
    }

    if ( errors ) {
      alert(errors);
    }
  }).focus();



    /* Comments and Replies */
  // textarea auto-size handler init;
  $('body').on('keyup paste cut', '.fc-tarea', function(e) {
    if( e.keyCode == 13 && !e.shiftKey ) {
      var url = '';
      if( $(this).data( 'eltype' ) == 'com' ) {
        url = '/_comment/add/';
      } else if( $(this).data('eltype') == 'rpl' ) {
        url = '/_reply/add/';
      }

      // send commentItem;
      sendCommentItem( e.target.value, {
        url: url,
        pid: $(this).data('pid'),
        pidType: 'post',
        eltype: $(this).data('eltype'),
        publishcon: $(this).data('publishcon')
      });

      clearTextarea( e.target );
      console.log('comment send to server!');
    }
    tAutoGrowth(this, 30);
  });
  $('body').on('keydown', '.fc-tarea', function(e) {
    if( e.keyCode == 13 && !e.shiftKey ) {
      return false;
    }
  });

  // load comments
  $('body')
    .one('click', '.feed .f-comment', function(e) {
      e.preventDefault();
      var target = $($(this).attr('data-target'));
      target.find('.fc-tarea').val('').focus();
    })
    .on('click', '.feed .f-comment[data-state=on]', function(e) {
      console.log($(this).data('pid'));
      $(this).attr('data-state', 'off');
      // get comment items
      getCommentItems({
        url: '/api/comment/list/',
        requestType: 'GET',
        requestedElement: 'comment',
        referencedItem: 'post',
        post: {
          id: $(this).data('pid'),
        },
        comment: {
          id: ''
        },
        requestTrigger: {
          element: $($(this).data('target')).find('.lmore-trigger'),
          afterLoadAll: 'delete'
        }
      });
    })
    .on('click', '.feed .f-comment', function(e) {
      e.preventDefault();
      var target = $($(this).attr('data-target'));

      target.slideDown(200, function() {
        if(target.css('display') == 'none') {
          target.find('.fc__add .fc-tarea').eq(0).val('');
          target.find('.fc-reply').slideUp(0).find('.fc-tarea').eq(0).val('');
        }
      });
      target.find('.fc-tarea').focus();
    });

  // open/close reply area
  $('body')
    .one('click', '.feed .cp__reply .exc-reply', function(e) {
      e.preventDefault();
      var target = $($(this).attr('data-target'));
      target.find('.fc-tarea').val('').focus();
    })
    .on('click', '.exc-reply.rpl-lmore[data-state=on]', function(e) {
      console.log($(this).data('pid'));
      $(this).attr('data-state', 'off');
      // get comment items
      getCommentItems({
        url: '/api/reply/list/',
        requestType: 'GET',
        requestedElement: 'reply',
        comment: {
          id: $(this).data('pid'),
        },
        reply: {
          id: ''
        },
        requestTrigger: {
          element: $($(this).data('target')).find('.lmore-trigger'),
          afterLoadAll: 'delete'
        }
      });
    })
    .on('click', '.feed .cp__reply .exc-reply', function(e) {
      e.preventDefault();
      var target = $($(this).attr('data-target'));

      target.slideDown(200, function() {
        if(target.css('display') == 'none') {
          target.find('.fc-tarea').eq(0).val('');
        }
      });
      target.find('.fc-tarea').focus();
    });

  // load more comment or reply items;
  $('body')
    .on('click', '.lmore-trigger', function(e) {
      e.preventDefault();
      var lastItemId = 0;

      if( $(this).data('eltype') == 'com' ) {
        lastItemId = getLastMsgId('#com' + $(this).data('pid') + ' .fc__published-item', 'id');
        // get comment items
        getCommentItems({
          url: '/api/comment/list/',
          requestType: 'GET',
          requestedElement: 'comment',
          referencedItem: 'post',
          post: {
            id: $(this).data('pid'),
          },
          comment: {
            id: lastItemId,
          },
          requestTrigger: {
            element: $(this),
            afterLoadAll: 'delete'
          }
        });
      } else if( $(this).data('eltype') == 'rpl' ) {
        lastItemId = getLastMsgId('#rpl' + $(this).data('pid') + ' .cp__con', 'id');
        // get reply items
        getCommentItems({
          url: '/api/reply/list/',
          requestType: 'GET',
          requestedElement: 'reply',
          comment: {
            id: $(this).data('pid'),
          },
          reply: {
            id: lastItemId,
          },
          requestTrigger: {
            element: $(this),
            afterLoadAll: 'delete'
          }
        });
      }
      console.log('lastItemId: ' + lastItemId);
    });

  // pass comment/reply id to popup edit/delete link;
  $('body').on('click', '.com-edbtns', function() {
    $('.comedit-item')
      .attr('data-id', $(this).data('id'))
      .attr('data-eltype', $(this).data('eltype'));
  });
  // edit / delete comment AND reply items
  $('body').on('click', '.com-edit .comedit-item', function(e) {
    e.preventDefault();
    var $this = $(this),
      actionType = $this.data('action'),
      itemType = $this.data('eltype'),
      url = '',
      method = '',
      actionTarget = '';

    var confirmModalMsg = getConfirmModalMsg( actionType, itemType );
    if( itemType == 'com' ) {
      if( actionType == 'delete' ) {
        url = '/api/comment/delete/' + $this.data('id') + '/';
        method = 'DELETE';
      } else if( actionType == 'edit' ) {
        url = '/api/comment/update/' + $this.data('id') + '/';
        method = 'PUT';
      }
      actionTarget = $('.fc__published-item[data-id="' + $this.data('id') + '"]');
    } else {
      if( actionType == 'delete' ) {
        url = '/api/reply/delete/' + $this.data('id') + '/';
        method = 'DELETE';
      } else if( actionType == 'edit' ) {
        url = '/api/reply/update/' + $this.data('id') + '/';
        method = 'PUT';
      }
      actionTarget = $('.fcreply__published .cp__con[data-id="' + $this.data('id') + '"]');
    }

    var targetItemText = actionTarget.find('.cp__content').text(),
        confirmModal = '<div class="ui modal tiny" id="comrpl-editing">' +
                          '<div class="header">' + confirmModalMsg.header + '</div>' +
                          '<div class="content">' +
                            '<p>' + confirmModalMsg.content + '</p>' +
                          '</div>' +
                          '<div class="actions">' +
                            '<div class="ui negative button">' + confirmModalMsg.btnDeny + '</div>' +
                            '<div class="ui positive button">' + confirmModalMsg.btnApprove + '</div>' +
                          '</div>' +
                        '</div>';

    $('body').append(confirmModal);
    $('#comrpl-editing').modal({
      onHidden: function() {
        $('#comrpl-editing').remove();
      },
      onVisible: function() {
        $('.comrepl-editing').popup('destroy');
        if( actionType == 'edit' ) {
          $('.editing-tarea')
            .on('keyup paste cut', function() {
              tAutoGrowth( this, 30 );
            })
            .val( targetItemText ).trigger('paste');
        }
      },
      onDeny: function() {
        console.log('Cancelled by user!');
      },
      onApprove: function() {
        if( actionType == 'edit' ) {
          var modalTarea = $('#comrpl-editing .editing-tarea'),
              t = actionTarget.find('.cp__content');
          $.ajax({
            url: url,
            type: method,
            data: { text: modalTarea.val() },
            success: function( response ) {
              console.log( response );
              t.text( response.text );
            }
          });
        } else {
          $.ajax({
            url: url,
            type: method,
            statusCode: {
              204: function() {
                actionTarget.slideUp(120, function() {
                  $(this).remove();
                });
              }
            }
          });
        }
      }
    })
    .modal('show');

    function getConfirmModalMsg( actionName, itemType ) {
      var msg = {};
      if( actionName == 'edit' ) {
        msg = editMsg( itemType );
      } else {
        msg = deleteMsg( itemType );
      }
      return msg;
    }
    function deleteMsg( itemType ) {
      var msg = {};
      if( itemType == 'com' ) {
        msg['header'] = 'Delete your comment';
        msg['content'] = 'Are you sure you want to delete your comment?'
      } else {
        msg['header'] = 'Delete your reply';
        msg['content'] = 'Are you sure you want to delete your reply?'
      }
      msg['btnApprove'] = 'Yes';
      msg['btnDeny'] = 'Cancel';
      return msg;
    }
    function editMsg( actionItem ) {
      var msg = {};
      if( actionItem == 'com' ) {
        msg['header'] = 'Edit your comment';
        msg['content'] = '<div class="editing-currtext">' +
                          '<textarea class="editing-tarea" rows="1" data-min-rows="1"  placeholder="Write a comment"></textarea>' +
                        '</div>';
      } else {
        msg['header'] = 'Edit your reply';
        msg['content'] = '<div class="editing-currtext">' +
                          '<textarea class="editing-tarea" rows="1" data-min-rows="1"  placeholder="Write a comment"></textarea>' +
                        '</div>';
      }
      msg['btnApprove'] = 'Save';
      msg['btnDeny'] = 'Cancel';
      return msg;
    }
  });

});


// readMedia file(image, video) for get thumnail(preview img);
function readMedia( file, index ) {
  console.log(file);
  var elBefore = $('.pmpreview-img.pm-add'),
    elPreviewContainer = $('.pmedia-preview'),
    fileReader = new FileReader(),
    useBlob = false && window.URL; // Set to `true` to use Blob instead of Data-URL
  window.URL = window.URL || window.webkitURL;

  if( file.type.match('image') ) {

    // works on file selected via input;
    fileReader.onload = function() {
      var image = new Image(),
        newPImg = $('<div></div>');

      image.addEventListener('load', function() {
        // create new DOM element(preview-img container);
        newPImg = $('<div>', {
          class: 'pmpreview-img',
          'data-id': index,
        })
          .append(
            $('<span>', {
              class: 'remover-btn',
              title: 'remove',
              click: function(e) {
                $(this).parent().fadeOut(500, function() {
                  console.log($(this));
                  $(this).remove();
                  if(files.length != 0) {
                    files = deleteByIndexFromArray( files, index );
                  }
                  pmAddShowControll();
                });
              }
            })
              .append('<i class="remove icon"></i>')
          )
          .append('<div class="remover-overlay"></div>')
          .append(this);

        // insert to last postion;
        newPImg.insertBefore(elBefore);
        elPreviewContainer.scrollLeft(elPreviewContainer[0].scrollWidth);
        pmAddShowControll();

        // clean objectURL, created useBlob object(if client browser is supports)
        if ( useBlob ) {
          window.URL.revokeObjectURL(image.src);
        }
      });

      // set objectURL or data/base64 resource;
      image.src = useBlob ? window.URL.createObjectURL(file) : fileReader.result;
    };

    fileReader.readAsDataURL(file);

  } else if( file.type.match('video') ) {
    var video = document.createElement('video'),
      canvas = document.createElement('canvas');

    video.src = window.URL.createObjectURL(file);
    canvas.width  = 120;
    canvas.height = 120;

    video.addEventListener('loadeddata', function() {
      canvas.getContext("2d").drawImage(video, 0, 0, 120, 120);
    }, false);

    // create new DOM element(preview-img container);
    var newPImg = $('<div>', {
      class: 'pmpreview-img',
      'data-id': index,
    })
      .append(
        $('<span>', {
          class: 'remover-btn',
          title: 'remove',
          click: function(e) {
            $(this).parent().fadeOut(500, function() {
              $(this).remove();
              if(files.length != 0) {
                files = deleteByIndexFromArray( files, index );
              }
              pmAddShowControll();
            });
          }
        })
          .append('<i class="remove icon"></i>')
      )
      .append('<div class="remover-overlay"></div>')
      .append(canvas)
      .append('<span class="play-icon"><i class="video play outline icon"></i></span>');

    // insert to last postion;
    newPImg.insertBefore(elBefore);
    elPreviewContainer.scrollLeft(elPreviewContainer[0].scrollWidth);
    pmAddShowControll();

    window.URL.revokeObjectURL(file);
  }
}


// handling add more square on preview files line
function pmAddShowControll() {
  var sOb = $('.pmpreview-img.pm-add');
  if( sOb.parent().children().length < 2 ) {
    sOb.addClass('display-off');
  } else {
    sOb.removeClass('display-off');
  }
}

// add posts to DOM in loop;
function addPostArrayToDOM( target, array, addType ) {
  if( !$(target) ) { return }
  if( !array && array.length ) { return }

  addType = ( addType ) ? addType : 'append';
  for( var i = 0; len = array.length, i < len; i++ ) {
    var feed = createFeedTemplate( array[i] );
    if( !isEmpty( feed ) ) {
      ( addType == 'append' ) ? $(target).append( feed ) : $(target).prepend( feed );

      var selectedFeed = $(target).find('.feed[data-last="true"]').eq(0);
      if( !isEmpty( array[i] ) ) {
        normalizeMediaDisplay( selectedFeed, getMediaParams( getMedia( array[i] ) ) );
        arrayOfAttrID = [];
      }
      selectedFeed.removeAttr('data-last');
    }
  }
}

function createFeedTemplate( data ) {
  console.log( data );
  if( isEmpty( data ) ) { return }
  var post = data,
    media, //= ( post.media && post.media.length ) ? createFeedMediaLayout( post.media ) : '',
    stateLine = '',
    feedContext = '',
    commentsCount = 0,
    loadMoreComment = '',
    likedState = 'like',
    likeCss = 'background-position: -1px -42px',
    likesCount;

  media = createFeedMediaLayout( getMedia( post ) );

  likesCount = isNumeric( post.likes_count ) ? +post.likes_count : 0;
  if( post.liked_users instanceof Array && post.liked_users.indexOf( +$('#user').val() ) !== -1 ) {
    likeCss = 'background-position: -24px -42px';
    likedState = 'unlike';
  } else {
    likeCss = 'background-position: -1px -42px';
    likedState = 'like';
  }

  var currentUId = +$('#user').val() || 0,
    viewedUId = +$('#user_').val() || 0;

  if( !isEmpty( post.author ) ) {
    if( post.author.id == currentUId && currentUId == viewedUId ) {
      stateLine = '<div class="feed__editing">' +
        '<div class="feed__editing-con">' +
        '<a href="#" data-target="#fedit_menu' + post.id + '"><i class="ellipsis horizontal icon"></i></a>' +
        '</div>' +
        '<div id="fedit_menu' + post.id + '" class="feed__editing-menu">' +
        '<ul>' +
        // '<li><a href="#" data-target="#post-edit" data-pid="' + post.id + '">Edit</a></li>' +
        '<li><a href="#" data-target="#post-delete" data-pid="' + post.id + '">Delete</a></li>' +
        '</ul>' +
        '</div>' +
        '</div>';
    }
  } else {
    stateLine = '';
  }


  if( post.text ) {
    feedContext = '<div class="fpost-content">' + post.text + '</div>';
  }

  if( !isNumeric( post.comments_count ) ) {
    commentsCount = 0;
  } else {
    commentsCount = +post.comments_count;
    if( commentsCount > 5 ) {
      loadMoreComment = '<div class="lmore-comment"><a href="#" class="lmore-trigger" data-pid="' + post.id + '" data-eltype="com">Show 5 more comments</a></div>';
    }
  }

  var feedTemplate = '<div class="feed" data-id="' + post.id + '" data-last="true" >' +
    '<div class="feed__stateline">' +
    stateLine +
    '</div>' +
    '<div class="feed__usercard">' +
    '<div class="feed__usercard-img"><a href="' + post.author.url + '"><img src="' + getPropertyByName( post.author.profile, 'avatar' ) + '" title="' + getPropertyByName( post.author, 'username' ) + '"></a></div>' +
    '<div class="feed__usercard-info">' +
    '<div class="fui__con">' +
    '<a href="' + post.author.url + '" class="fui__name">' + getPropertyByName( post.author, 'username' ) + '</a>' +
    '<span class="state-info"></span>' +
    '</div>' +
    '<div class="fui__date"><span class="cp__u-date" data-datetime="' + post.created +'">less than a minute</span></div>' +
    '</div>' +
    '</div>' +
    '<div class="feed__content">' +
    feedContext +
    media +
    '</div>' +
    '<div class="feed__serv">' +
    '<form action="">' +
    '<div class="feed__serv-share">' +
    '<div class="f-prom">' +
    '<a href="#" class="f-comment" data-target="#com' + post.id + '" data-pid="' + post.id + '" data-state="on"><i class="comment-ico"></i> <span><span class="com-count">' + post.comments_count + '</span> Comments</span></a>' +
    '</div>' +
    // '<div class="f-prom">' +
    //     '<a href="#" class="f-share" data-target="#share_b' + post.id + '" data-id="' + post.id + '">' +
    //         '<i class="share-ico"></i> <span>Share</span>' +
    //     '</a>' +
    //     '<div id="share_b' + post.id + '" class="share__btns">' +
    //         '<div class="share__btns-con">' +
    //             '<div class="ya-share2" data-services="vkontakte,facebook,gplus,twitter" data-limit="4"></div>' +
    //         '</div>' +
    //     '</div>' +
    // '</div>' +
    '<div class="f-prom">' +
    '<a href="#" class="f-like like" data-id="' + post.id + '" data-action="' + likedState + '" data-count="' + likesCount + '">' +
    '<i class="like-ico" style="' + likeCss + '"></i>' +
    '<span><span class="likes-count">' + likesCount + '</span> Like</span>' +
    '</a>' +
    '</div>' +
    '</div>' +
    '<div class="feed__serv-share" style="display: none;">' +
    '<div class="f-likecount" data-id="' + post.id + '"><i class="like-ico"></i> <span>' + likesCount + '</span></div>' +
    '<div class="f-commcount"><span>' + post.comments_count + '</span> Comments</div>' +
    '</div>' +
    '<div class="feed__serv-comment" id="com' + post.id + '">' +
    loadMoreComment +
    '<div class="fc__published"></div>' +
    '<div class="fc__add">' +
    '<div class="fc__con-img"><img src="' + getPropertyByName( post.author.profile, 'avatar' ) + '" title="' + getPropertyByName( post.author, 'username' ) + '"></div>' +
    '<div class="fc__con-tarea">' +
    '<textarea class="fc-tarea" rows="1" data-min-rows="1" data-pid="' + post.id + '" data-publishcon="fc__published" data-eltype="com" placeholder="Write a comment"></textarea>' +
    '</div>' +
    '</div>' +
    '</div>' +
    '</form>' +
    '</div>' +
    '</div>';
  return feedTemplate;
}


// create media item template;
function createMediaItemTemplate( itemOb, groupId, moreMediaOb ) {
  var str = '';
  if( isEmpty( itemOb ) ) { return str; }

  if( isEmpty( moreMediaOb ) ) {
    if( itemOb.type == 'video' ) {
      str = '<a href="javascript:;" data-src="/_video/video/' + itemOb.id + '/" data-type="iframe" data-fancybox="' + groupId + '">' +
              '<span class="playbtn-area"></span>' +
              '<img src="' + itemOb.url + '">' +
            '</a>';
    } else if( itemOb.type == 'image' ) {
      str = '<a href="' + itemOb.url + '" data-fancybox="' + groupId + '"><img src="' + itemOb.url + '"></a>';
    }
  } else {
    if( itemOb.type == 'video' ) {
      str = '<a href="javascript:;" data-src="/_video/video/' + itemOb.id + '/" data-type="iframe" data-fancybox="' + groupId + '" class="' + moreMediaOb.parentClass + '">' +
              '<span class="playbtn-area"></span>' +
              '<img src="' + itemOb.url + '">' +
              '<span class="' + moreMediaOb.childClass + '">+' + moreMediaOb.countVal + '</span>' +
            '</a>';
    } else if( itemOb.type == 'image' ) {
      str = '<a href="' + itemOb.url + '" data-fancybox="' + groupId + '" class="' + moreMediaOb.parentClass + '">' +
              '<img src="' + itemOb.url + '">' +
              '<span class="' + moreMediaOb.childClass + '">+' + moreMediaOb.countVal + '</span>' +
            '</a>';
    }
  }

  return str;
}

function getPropertyByName( obj, pName ) {
  return ( !isEmpty( obj ) ) ? obj[pName] : '';
}

// create template and set layout params
function createFeedMediaLayout( data ) {
  var str = '',
    dataLength = data.length;

  console.log(data);
  if( !data && !data.length ) { return '' }

  console.log( 'mix of videos and images' );
  var attrID = generateID();
  arrayOfAttrID.push(attrID);

  if( dataLength == 1 ) {
    str = '<div class="post-media" id="' + attrID + '"><div class="pm-row"><div class="col col1">' +
      createMediaItemTemplate( { id: data[0].id, url: data[0].url, type: data[0].type }, attrID ) +
      '</div></div></div>';

  } else if( dataLength > 1 ) {
    var mediaItemTemplate = '';

    // create string template for layout type == 'horiz'
    if( checkLayoutType( data ) == 'horiz' ) {
      console.log( 'horiz' );
      var colType = '';
      str = '<div class="post-media" id="' + attrID + '"><div class="pm-row">';
      for( var i = 0; i < dataLength; i++ ) {
        if( dataLength == 2 ) {
          colType = 'col2';
        } else if( dataLength == 3 ) {
          if( i == 1 ) {
            str += '</div><div class="pm-row">';
          }
          colType = ( i < 1 ) ? 'col1' : 'col2';
        } else if( dataLength == 4 ) {
          if( i == 1 ) {
            str += '</div><div class="pm-row">';
          }
          colType = ( i < 1 ) ? 'col1' : 'col3';
        } else if( dataLength > 4 ) {
          if( i == 2 ) {
            str += '</div><div class="pm-row">';
          }
          colType = ( i < 2 ) ? 'col2' : 'col3';
        }

        mediaItemTemplate = createMediaItemTemplate( { id: data[i].id, url: data[i].url, type: data[i].type }, attrID );
        if( dataLength > 5 && i >= 4 ) {
          if( i == 4 ) {
            var moreMediaBlock = {
              parentClass: 'colmore',
              childClass: 'count-text',
              countVal: ( dataLength - 5 )
            };

            str += '<div class="col ' + colType + '">' +
              createMediaItemTemplate( { id: data[i].id, url: data[i].url, type: data[i].type }, attrID, moreMediaBlock ) +
              '</div>';
          } else {
            str += '<div class="col ' + colType + '" style="display: none;">' + mediaItemTemplate + '</div>';
          }
        } else {
          str += '<div class="col ' + colType + '">' + mediaItemTemplate + '</div>';
        }
      }
      str += '</div></div>';
    } else { // create string template for layout type == 'vert';
      console.log('vert');
      str = '<div class="post-media" id="' + attrID + '"><div class="pm-row col2">';
      for( var i = 0; i < dataLength; i++ ) {
        if( dataLength < 5 ) {
          if( i == 1 ) {
            str += '</div><div class="pm-row col2">';
          }
        } else {
          if( i == 2 ) {
            str += '</div><div class="pm-row col2">';
          }
        }

        mediaItemTemplate = createMediaItemTemplate( { id: data[i].id, url: data[i].url, type: data[i].type }, attrID );
        if( dataLength > 5 && i >= 4 ) {
          if( i == 4 ) {
            var moreMediaBlock = {
              parentClass: 'colmore',
              childClass: 'count-text',
              countVal: ( dataLength - 5 )
            };

            str += '<div class="col col1">' +
              createMediaItemTemplate( { id: data[i].id, url: data[i].url, type: data[i].type }, attrID, moreMediaBlock ) +
              '</div>';
          } else {
            str += '<div class="col col1" style="display: none;">' + mediaItemTemplate + '</div>';
          }
        } else {
          str += '<div class="col col1">' + mediaItemTemplate + '</div>';
        }
      }
      str += '</div></div>';
    }
  }

  return str;
}


/*
 * check width height relation, result: vertical or horizontal layout of items
 * returns string: vert | horiz
 */
function checkLayoutType( array ) {
  var layoutType = 'horiz',
    len = array.length;

  if( len == 2 ) {
    for( var i = 0; i < len; i++ ) {
      if( array[i].type == 'image' ) {
        if( isVertLayout( array[i], 1.2 ) ) {
          layoutType = 'vert';
        } else {
          layoutType = 'horiz';
        }
      }
    }
  } else if( len > 2 && len < 5 ) {
    if( array[0].type == 'video' ) {
      layoutType = 'horiz';
    } else {
      if( isVertLayout( array[0], 1.2 ) ) {
        layoutType = 'vert';
      } else {
        layoutType = 'horiz';
      }
    }
  } else if( len > 4 ) {
    if( array[0].type == 'video' ) {
      if( isVertLayout( array[1], 1.2 ) ) {
        layoutType = 'vert';
      } else {
        layoutType = 'horiz';
      }
    } else if( array[1].type == 'video' ) {
      if( isVertLayout( array[0], 1.2 ) ) {
        layoutType = 'vert';
      } else {
        layoutType = 'horiz';
      }
    } else {
      if( isVertLayout( array[0], 1.2 ) || isVertLayout( array[1], 1.2 ) ) {
        layoutType = 'vert';
      } else {
        layoutType = 'horiz';
      }
    }
  }
  return layoutType;
}

/*
 * returns true/false, checks height/width relation,
 * input params:
 * item - object, with properties[height, width]
 * coeff - number, coefficient to check height/width relation
 */
function isVertLayout( item, coeff ) {
  return item.height / item.width >= coeff;
}

function getMedia( ob ) {
  var tempArr = [];
  if( isEmpty(ob) ) { return tempArr; }
  if( ob.image instanceof Array && ob.image.length ) {
    var images = ob.image;
    for( var i = 0; len = images.length, i < len; i++ ) {
      tempArr.push({
        id: images[i].id,
        url: images[i].image,
        type: 'image',
        width: +images[i].width,
        height: +images[i].height,
        created: images[i].created
      });
    }
  }
  if( ob.video instanceof Array && ob.video.length ) {
    var videos = ob.video;
    for( var i = 0; len = videos.length, i < len; i++ ) {
      tempArr.push({
        id: videos[i].id,
        type: 'video',
        url: videos[i].thumbnail,
        width: videos[i].width,
        height: videos[i].height,
        created: videos[i].created
      });
    }
  }
  return tempArr;
}

function getMediaParams( array ) {
  var result = [];
  if( isEmpty( array ) ) { return result; }
  for( var i = 0; len = array.length, i < len; i++ ) {
    result.push({
      width: array[i].width,
      height: array[i].height
    });
  }
  return result;
}

/*
 * normalize displaying on 'col' container( for media type == image )
 * works after all images loaded and added to DOM
 * checks relation of width / height, and set optimal positions(centering vertical and horizontal);
 */
function normalizeMediaDisplay( targetContainer, imgParams ) {
  if( !$(targetContainer) || isEmpty( imgParams ) ) { return; }

  // if( imgParams && isEmpty( imgParams ) ) { return }
  var cols = $(targetContainer).find('.col') || [];
  if( cols.length < 3 ) {
    console.log(' less than 3! ');
    var childParams = {},
      parentParams = {},
      cssParent = '',
      cssChild = '';

    for( var i = 0; len = cols.length, i < len; i++ ) {
      var parent = cols.eq(i),
        child = parent.find('img').eq(0);

      if( cols.length == 1 ) {
        parentParams.width = parent.width();
        if( child.length ) {
          parentParams.height = parentParams.width * imgParams[i].height / imgParams[i].width;
          parentParams.maxHeight = 450;
          parentParams.minHeight = 230;
          if( parentParams.height > parentParams.maxHeight ) {
            parentParams.height = parentParams.maxHeight;
          } else if( parentParams.height < parentParams.minHeight ) {
            parentParams.height = parentParams.minHeight;
          }
        }
        cssParent = 'max-height = ' + parentParams.maxHeight + '; height: ' + parentParams.height + 'px;';
      } else if( cols.length == 2 ) {
        parentParams.width = parent.width();
        parentParams.height = parent.height();
      }


      if( child.length ) {
        childParams.width = imgParams[i].width;
        childParams.height = imgParams[i].height;

        var temp = calcMargins( parentParams, childParams );
        cssChild = ( ( temp.marginTop ) ? 'margin-top: -' + temp.marginTop + 'px; ' : '' ) +
          ( ( temp.marginLeft ) ? 'margin-left: -' + temp.marginLeft + 'px; ' : '' );
        cssChild += getHWCss( parentParams, childParams );
        child.attr( 'style', cssChild );
      }
      parent.attr( 'style', cssParent );
    }
  } else {
    var isVertLayout = ( $(targetContainer).find('.pm-row').hasClass('col2') ) ? true : false,
      childParams = {},
      parentParams = {},
      cssParent = '',
      cssChild = '',
      len = cols.length;
    console.log( 'more than 2' );
    if( isVertLayout ) {
      console.log(' isVertical: true ');
      for( var i = 0; i < len; i++ ) {
        var parent = cols.eq(i),
          child = parent.find('img').eq(0),
          heightRightSideItem = 0,
          heightLeftSideItem = 0;

        if( len == 3 ) {
          heightRightSideItem = parent.height();
          heightLeftSideItem = heightRightSideItem * 2;
          console.log('colsCount: 3');
        }
        if( len == 4 ) {
          heightRightSideItem = 180,
          heightLeftSideItem = heightRightSideItem * 3;
          console.log('colsCount: 4');
        }

        if( len == 5 ) {
          heightRightSideItem = 180,
          heightLeftSideItem = heightRightSideItem * 3 / 2;

          if( i < 2) {
            parentParams.width = parent.width();
            parentParams.height = heightLeftSideItem;
            cssParent = 'max-height: ' + heightLeftSideItem + 'px; height: ' + heightLeftSideItem + 'px;';
          } else {
            parentParams.width = parent.width();
            parentParams.height = heightRightSideItem;
            cssParent = 'height: ' + heightRightSideItem + 'px; min-height: ' + heightRightSideItem + 'px;';
          }
          console.log('colsCount: 5');
        } else {
          if( i < 1) {
            parentParams.width = parent.width();
            parentParams.height = heightLeftSideItem;
            cssParent = 'max-height: ' + heightLeftSideItem + 'px; height: ' + heightLeftSideItem + 'px;';
            console.log('cssParent: ' + cssParent);
          } else {
            parentParams.width = parent.width();
            parentParams.height = heightRightSideItem;
            cssParent = 'height: ' + heightRightSideItem + 'px; min-height: ' + heightRightSideItem + 'px;';
          }
        }

        if( child.length ) {
          childParams.width = imgParams[i].width;
          childParams.height = imgParams[i].height;

          var temp = calcMargins( parentParams, childParams );
          cssChild = ( ( temp.marginTop ) ? 'margin-top: -' + temp.marginTop + 'px; ' : '' ) +
            ( ( temp.marginLeft ) ? 'margin-left: -' + temp.marginLeft + 'px; ' : '' );
          cssChild += getHWCss( parentParams, childParams );
          child.attr( 'style', cssChild );
        }
        parent.attr( 'style', cssParent );
      }
    } else {
      console.log(' isVertical: false ');
      var tempHeight = 0;
      for( var i = 0; i < len; i++ ) {
        var parent = cols.eq(i),
          child = parent.find('img').eq(0);

        parentParams.width = parent.width();
        parentParams.height = parent.height();
        parentParams.maxHeight = 450;
        parentParams.minHeight = 230;

        if( len > 2 && len < 5 ) {
          if( i == 0 ) {
            if( child.length ) {
              parentParams.height = parentParams.width * imgParams[i].height / imgParams[i].width;
              if( parentParams.height > parentParams.maxHeight ) {
                parentParams.height = parentParams.maxHeight;
              } else if( parentParams.height < parentParams.minHeight ) {
                parentParams.height = parentParams.minHeight;
              }
            }
            cssParent = 'max-height = ' + parentParams.maxHeight + '; height: ' + parentParams.height + 'px;';
            parent.attr( 'style', cssParent );
          }
        } else if( len > 4 ) {
          if( i < 2 ) {
            if( tempHeight && i == 1 ) {
              parentParams.height = tempHeight;
            } else {
              if( child.length ) {
                parentParams.height = parentParams.width * imgParams[i].height / imgParams[i].width;
                if( parentParams.height > parentParams.maxHeight ) {
                  parentParams.height = parentParams.maxHeight;
                } else if( parentParams.height < parentParams.minHeight ) {
                  parentParams.height = parentParams.minHeight;
                }
              }
            }
            cssParent = 'max-height = ' + parentParams.maxHeight + '; height: ' + parentParams.height + 'px;';
            parent.attr( 'style', cssParent );
          }
          if( i == 0 ) {
            tempHeight = parentParams.height;
          }
        }

        if( child.length ) {
          childParams.width = imgParams[i].width;
          childParams.height = imgParams[i].height;

          var temp = calcMargins( parentParams, childParams );
          cssChild = ( ( temp.marginTop ) ? 'margin-top: -' + temp.marginTop + 'px; ' : '' ) +
            ( ( temp.marginLeft ) ? 'margin-left: -' + temp.marginLeft + 'px; ' : '' );
          cssChild += getHWCss( parentParams, childParams );
          child.attr( 'style', cssChild );
        }
      }
    }
  }
}

/* calculating marginTop and marginLeft to position center(by horiz and vert) */
function calcMargins( parentParams, childParams ) {
  var mLeft = '',
    mTop = '',
    childComputedHeight = 0,
    childComputedWidth = 0;

  if( compareWHRealtion( parentParams, childParams ) == 'width' ) {
    childComputedWidth = parentParams.width;
    childComputedHeight = childComputedWidth * childParams.height / childParams.width;
  } else {
    childComputedHeight = parentParams.height;
    childComputedWidth =  childComputedHeight * childParams.width / childParams.height;
  }

  if( childComputedHeight > parentParams.height ) {
    mTop = ( childComputedHeight - parentParams.height ) / 2;
  }
  if( childComputedWidth > parentParams.width ) {
    mLeft = ( childComputedWidth - parentParams.width ) / 2;
  }

  return {
    marginTop: mTop,
    marginLeft: mLeft
  }
}

// input to object( with properties: width, height )
function compareWHRealtion( parentParams, childParams ) {
  return ( parentParams.width / parentParams.height >= childParams.width / childParams.height ) ? 'width' : 'height';
}

// returns css for height or width of img element, by checking H/W realations of parent and child params;
function getHWCss( parentParams, childParams ) {
  var css = '';
  if( compareWHRealtion( parentParams, childParams ) == 'width' ) {
    css = 'width: 100%;';
  } else {
    css = 'height: 100%;';
  }
  return css;
}



/* Comments and Replies */

/*
 * commentText - text of sending comment
 * sendParamsOb - object with properties:
 */
function sendCommentItem( commentText, sendParamsOb ) {
  commentText = commentText.trim();
  if( commentText != '' && sendParamsOb != null ) {
    var ob = {};
    if( sendParamsOb.eltype == 'com' ) {
      ob['content_type'] = sendParamsOb.pidType;
      ob['object_id'] = sendParamsOb.pid;
    } else if( sendParamsOb.eltype == 'rpl' ) {
      ob['comment_id'] = sendParamsOb.pid;
    }
    ob['text'] = commentText;
    sendParamsOb.publishcon = $('#' + sendParamsOb.eltype + sendParamsOb.pid + ' .' + sendParamsOb.publishcon);
    $.ajax({
      url: sendParamsOb.url,
      type: 'POST',
      data: ob,
      success: function( response ) {
        // call to function to add new comment...
        addToDOM(
          createCommentTemplate({
            user: {
              id: response.author.id,
              name: response.author.username,
              avatar: response.author.profile.avatar,
              page_url: response.author.url
            },
            item: {
              id: response.id,
              pid: sendParamsOb.pid,
              text: response.text,
              created: response.created,
              eltype: sendParamsOb.eltype
            }
          }),
          sendParamsOb.publishcon,
          'append'
        );

        // bind popup to comment editing popup
        bindPopupToEl('.com-edbtns', {
          on: 'click',
          position: 'bottom right',
          html: '<div class="com-edit">' +
                  '<a href="#" data-id="" data-eltype="' + sendParamsOb.eltype + '" data-action="edit" class="comedit-item"><i class="write icon"></i> Edit</a>' +
                  '<a href="#" data-id="" data-eltype="' + sendParamsOb.eltype + '" data-action="delete" class="comedit-item"><i class="trash outline icon"></i> Delete</a>' +
                '</div>',
          className: {
            popup: 'ui popup comrpl-editing'
          }
        });
        if( sendParamsOb.eltype == 'com' ) {
          // comment count text update
          $('.feed[data-id=' + sendParamsOb.pid + '] .com-count').text( response.comments_count );
        }
      },
      error: function( err ) {
        console.log( err.text );
      }
    });
  }
  return;
}


// create comment or reply element html teplate
function createCommentTemplate( data ) {
  console.log(data);
  return data.item.eltype == 'com' ?
    '<div class="fc__published-item" data-id="' + data.item.id + '">' +
      '<div class="cp__con">' +
        '<div class="com-edbtns" data-id="' + data.item.id + '" data-eltype="com" title="Editing"><i class="ellipsis horizontal icon"></i></div>' +
        '<div class="cp__usercard">' +
          '<div class="cp__img">' +
            '<a href="' + data.user.page_url + '"><img src="' + data.user.avatar + '" title="' + data.user.name + '"></a>' +
          '</div>'+
          '<div class="cp__title">' +
            '<a href="' + data.user.page_url + '" class="cp__u-name">' + data.user.name + '</a>'	+
            '<span class="cp__u-date" data-datetime="' + data.item.created +'">• less than a minute</span>' +
          '</div>' +
        '</div>' +
        '<div class="cp__content">' +
          '<p>' + data.item.text + '</p>' +
        '</div>' +
        '<div class="cp__reply">' +
          '<a href="#" class="exc-reply rpl-lmore" data-state="on" data-pid="' + data.item.id + '" data-target="#rpl' + data.item.id + '">Reply</a>' +
        '</div>' +
      '</div>' +
      '<div class="fc__con-reply" id="rpl' + data.item.id + '" data-user-id="' + data.user.id + '">' +
        '<div class="lmore-comment"><a href="" class="lmore-trigger" data-pid="' + data.item.id + '" data-eltype="rpl">Show 5 more replies</a></div>' +
        '<div class="fcreply__published"></div>' +
        '<div class="fc-reply">' +
          '<div class="fc__con-img"><img src="' + data.user.avatar + '" title="' + data.user.name + '"></div>' +
          '<div class="fc__con-tarea"><textarea class="fc-tarea" rows="1" data-min-rows="1" data-pid="' + data.item.id + '" data-publishcon="fcreply__published" data-eltype="rpl" placeholder="Write a comment"></textarea></div>' +
        '</div>' +
      '</div>' +
    '</div>'
    :
    '<div class="cp__con" data-id="' + data.item.id + '">' +
      '<div class="com-edbtns" data-id="' + data.item.id + '" data-eltype="rpl" title="Editing"><i class="ellipsis horizontal icon"></i></div>' +
      '<div class="cp__usercard">' +
        '<div class="cp__img"><a href="' + data.user.page_url + '"><img src="' + data.user.avatar + '" title="' + data.user.name + '"></a></div>' +
        '<div class="cp__title">' +
          '<a href="' + data.user.page_url + '" class="cp__u-name">' + data.user.name + '</a>' +
          '<span class="cp__u-date" data-datetime="' + data.item.created +'">• less than a minute</span>' +
        '</div>' +
        '</div>' +
      '<div class="cp__content">' +
        '<p>' + data.item.text + '</p>' +
      '</div>' +
      '<div class="cp__reply"><a href="#" class="exc-reply" data-target="#rpl' + data.item.pid + '">Reply</a></div>' +
    '</div>'
}


// get Comment or Reply items
function getCommentItems( dataParams ) {
  var ob = {},
    publishconSuffix = '',
    eltype = '',
    pid = '',
    requestSendStatus = true;
  if( dataParams.requestedElement == 'comment' ) {
    ob['app'] = dataParams.referencedItem;
    ob['app_id'] = dataParams.post.id;
    ob['comment_id'] = ( ( dataParams.comment.id ) ? dataParams.comment.id : '' );

    publishconSuffix = '.fc__published';
    eltype = 'com';
    pid = dataParams.post.id;
  } else {
    ob['app_id'] = dataParams.comment.id;
    ob['reply_id'] = ( ( dataParams.reply.id ) ? dataParams.reply.id : '' );

    publishconSuffix = '.fcreply__published';
    eltype = 'rpl';
    pid = dataParams.comment.id;
  }
  if( requestSendStatus ) {
    $.ajax({
      url: dataParams.url,
      type: dataParams.requestType,
      data: ob,
      success: function( response ) {
        if( response.length ) {
          for( var i = 0; len = response.length, i < len; i++ ) {
            var publishcon = '#' + eltype + pid + ' ' + publishconSuffix;
            addToDOM(
              createCommentTemplate({
                user: {
                  id: response[i].author.id,
                  name: response[i].author.username,
                  avatar: response[i].author.profile.avatar,
                  page_url: response[i].author.url
                },
                item: {
                  id: response[i].id,
                  pid: pid,
                  text: response[i].text,
                  created: response[i].created,
                  eltype: eltype
                }
              }),
              publishcon,
              'prepend'
            );

            // bind popup to comment editing popup
            bindPopupToEl('.com-edbtns', {
              on: 'click',
              position: 'bottom right',
              html: '<div class="com-edit">' +
                      '<a href="#" data-id="" data-eltype="' + eltype + '" data-action="edit" class="comedit-item"><i class="write icon"></i> Edit</a>' +
                      '<a href="#" data-id="" data-eltype="' + eltype + '" data-action="delete" class="comedit-item"><i class="trash outline icon"></i> Delete</a>' +
                    '</div>',
              className: {
                popup: 'ui popup comrepl-editing'
              }
            });
          }
          if( response.length < 5 ) {
            changeDOMElement(
              dataParams.requestTrigger.element,
              dataParams.requestTrigger.afterLoadAll
            )
          }
        } else {
          console.log('data empty!');
          requestSendStatus = false;
          // remove load more trigger;
          changeDOMElement(
            dataParams.requestTrigger.element,
            dataParams.requestTrigger.afterLoadAll
          )
        }
      },
      error: function( err ) {
        console.log( err );
      }
    });
  }
}


// change DOM element( delete, or replace with another element );
function changeDOMElement( target, actionType, replaceElement ) {
  if( $(target) && ( actionType != null || actionType != '' ) ) {
    if( actionType == 'delete' ) {
      $(target).remove();
    } else if( actionType == 'change' ) {
      replaceElement = ( replaceElement ) ? replaceElement : '';
      $(target).replaceWith(replaceElement);
    }
  }
}