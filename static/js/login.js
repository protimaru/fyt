$(function() {
  var form = $('#login-form');
  form.submit(function( e ) {
    e.preventDefault();
    var formData = form.serialize(),
      formUrl = form.attr('action') || '',
      btn = form.find('.sign-in-button');

    $.ajax({
      url: formUrl,
      type: "POST",
      data: formData,
      success: function( response ) {
        btn.removeClass('loading');
        if( response && response.status == 'ok' ) {
            window.location.href = '/u/' + response.username + '/'
        } else {
          form.find('input[type=password]').val('');
          setIncorrectState( '#' + form.attr('id') );
        }
      }
    });
  });

  function setIncorrectState( selector ) {
    $(selector).find('input.sf-field').eq(0)
      .focus()
      .one('keyup', function() {
        $(this)
          .removeClass('field-incorrect')
          .next().transition('fade left');
      })
      .addClass('field-incorrect')
      .next().transition('fade left');
  }
});