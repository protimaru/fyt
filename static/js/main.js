$(function() {

    // add to friend
    $('#addfriend').click(function(e) {
        e.preventDefault();
        var button = $(this);
        $.post($(this).data('url'), { user: $(this).data('id') }, function(data) {
            if( data && data.status ) {
                if( data.status == 'ok' ) {
                    var p = button.parent();
                        p.children().remove();
                        p.append('<div class="ui orange message">Friend request sent</div>');
                }
            }
        });
    });

    $('#chatwf').click(function(e) {
        e.preventDefault();
        var button = $(this),
            fId = $(this).data('id'),
            url = window.location.origin + '/u/' + $('#current_user_username').val() + '/friends/';
        $.post(
            url,
            {
                user1: $('#user').val(),
                user2: fId
            },
            function( data) {
                if( data.status ) {
                    window.location.href = data.status;
                }
            }
        );
    });


    // profile photo edit
    $("#ep-trigger, #ep-t2").click(function() {
        $('#ep')
            .modal({
                onShow: function() {
                    if( !$('#uphoto').val() ) {
                        var imgSrc = $('#profileImg').attr('src'),
                            reader = new FileReader();
                        reader.onload = function(e) {
                            $uploadCrop.croppie('bind', {
                                url: e.target.result
                            }).then(function() {
                                console.log('jQuery bind complete');
                            });


                            setPreviewPhotos( $('#crop-containter').find('img').clone() );

                        };
                        loadXHR( imgSrc ).then(function( blob ) {
                            reader.readAsDataURL( blob );
                        });
                    }
                }
            })
            .modal("show");
        });
    $("#ep .header i.close.icon, #cancel").click(function() {
        $("#ep").modal("hide");
    });


    var $uploadCrop = $('#crop-containter').croppie({
        viewport: {
            width: 240,
            height: 240,
            type: 'circle'
        },
        boundary: {
            width: 340,
            height: 340
        }
    });

    $('#uphoto').on('change', function() {
        if( /^image/i.test(this.files[0].type) ) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $uploadCrop.croppie('bind', {
                    url: e.target.result
                }).then(function() {
                    console.log('jQuery bind complete');
                });
                console.log( e.target );
            };
            reader.readAsDataURL(this.files[0]);
        } else {
            alert('Not supported format, please select from image file formats!');
        }
    });

    $('#sc').on('click', function (ev) {
        ev.preventDefault();
        var fd = new FormData();
        fd.append('avatar', $('#uphoto')[0].files[0] );

        $.ajax({
          url: '/account/upload-avatar/',
          type: 'POST',
          cache: false,
          contentType: false,
          processData: false,
          data: fd,
          success: function( response ) {
              console.log( response );
              if( response ) {
                $('#profileImg, span.profile-img > img').attr('src', response.url);
                $("#ep").modal("hide");
              }
          },
          error: function( textStatus ) {
              console.log( textStatus );
          }
        });

        /*
        $uploadCrop.croppie('result', {
            type: 'canvas',
            size: 'viewport'
        }).then(function (resp) {
            $.ajax({
                url: '/account/upload_photo/',
                type: "POST",
                data: {"image":resp},
                success: function (data) {
                    html = '<img src="' + resp + '" />';
                    $(".photo-preview").html(html);
                    if(data.url != '') {
                        $('#profileImg, span.profile-img > img').attr('src', data.url);
                        $("#ep").modal("hide");
                        // $('form.upload_photo_form').reset();
                    }
                }
            });
        });
        */
    });

    function setPreviewPhotos( img ) {
        if( img && !img.length ) { return false; }
        var containers = $('.pedpreview-item');

        img.removeAttr('class');
        img.removeAttr('style');
        for( var i = 0; i < containers.length; i++ ) {
            containers.eq(i).html('<img>');
        }
    }

    function loadXHR( url ) {
        return new Promise(function( resolve, reject ) {
            try {
                var xhr = new XMLHttpRequest();
                xhr.open("GET", url);
                xhr.responseType = "blob";
                xhr.onerror = function() {reject("Network error.")};
                xhr.onload = function() {
                    if (xhr.status === 200) {resolve(xhr.response)}
                    else {reject("Loading error:" + xhr.statusText)}
                };
                xhr.send();
            }
            catch(err) {reject(err.message)}
        });
    }

});