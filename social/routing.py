from channels.routing import route

channel_routing = [
    route('websocket.connect', 'apps.chat.consumers.ws_connect', path=r'^/chat/(?P<room>\w+)$'),
    route('websocket.receive', 'apps.chat.consumers.ws_message'),
    route("websocket.keepalive", 'apps.chat.consumers.ws_message'),
    # route("websocket.disconnect", 'apps.chat.consumers.ws_disconnect'),
]
