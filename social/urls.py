from django.conf.urls import url, include
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static


urlpatterns = [
    # url(r'^jet/', include('jet.urls', 'jet')),
    url(r'^admin/', admin.site.urls),
    url(r'^activity/', include('apps.activity.urls', namespace='activity')),
    url(r'^account/', include('apps.accounts.urls', namespace='account')),
    url(r'^follow/', include('apps.followers.urls', namespace='follow')),
    url(r'^accounts/', include('allauth.urls')),
    url(r'^post/', include('apps.posts.urls', namespace='post')),
    url(r'^_post/', include('apps.post.urls', namespace='_post')),
    url(r'^_album/', include('apps.album.urls', namespace='_album')),
    url(r'^_photo/', include('apps.photo.urls', namespace='_photo')),
    url(r'^_video/', include('apps.video.urls', namespace='_video')),
    url(r'^_like/', include('apps.like.urls', namespace='_like')),
    url(r'^_comment/', include('apps.comment.urls', namespace='_comment')),
    url(r'^_reply/', include('apps.reply.urls', namespace='_reply')),
    url(r'^api/', include('apps.api.urls', namespace='api')),
    url(r'^arena/', include('apps.arenas.urls', namespace='arena')),
    url(r'^training/', include('apps.trainings.urls', namespace='training')),
    url(r'^offer/', include('apps.offer.urls', namespace='offer')),
    # url(r'^map/', include('apps.map.urls', namespace='map')),
    url(r'^notifications/', include('apps.notifications.urls', namespace='notifications')),
    url(r'^', include('apps.main.urls', namespace='main')),
]


if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL,
                          document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL,
                          document_root=settings.MEDIA_ROOT)

# if settings.DEBUG:
#     import debug_toolbar
#     urlpatterns = [
#         url(r'^__debug__/', include(debug_toolbar.urls)),
#     ] + urlpatterns
